KS PALs
=======

KS PALs is a web application to choose your PALs before registering for the Kinky Salon.

It has been developed for the Kinky Salon in Paris, but can be used in any
other event with a similar PAL policy.

There are two rules:

- One must have selected one or two PALs before being able to buy a ticket
- Each PAL must have selected the other same PALs. No auto-invite, no
  auto-complete, no replacing interpersonal communication.

The app is based on Python 3 with `Django`_ and `Django REST Framework`_ for
the backend, and the UI is in `React.js`_.

Get the source and report bugs on GitLab: https://gitlab.com/abompard/ks-pals

.. _Django: https://djangoproject.com
.. _Django REST Framework: http://www.django-rest-framework.org/
.. _React.js: https://reactjs.org/


Installation
------------

TODO.


Copyright & Licensing
---------------------

This code is copyright Aurélien Bompard <aurelien@bompard.org>.

It is licensed under the the `Affero GPL v3`_ or any later version.

.. _`Affero GPL v3`: http://www.gnu.org/licenses/agpl-3.0.html

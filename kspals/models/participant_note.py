from django.db import models
from django.utils.translation import ugettext_lazy as _

# from .participant import Participant


class ParticipantNote(models.Model):

    participant = models.ForeignKey(
        "Participant", on_delete=models.CASCADE, related_name="notes"
    )
    title = models.CharField(_("title"), max_length=100)
    content = models.TextField()

    class Meta:
        ordering = ["title"]

    def __str__(self):
        return self.title

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from kspals.signals import participant_changed
from .constants import NotificationName, NotificationStatus, TicketStatus
from .participant import Participant


class Notification(models.Model):

    participant = models.ForeignKey("Participant", on_delete=models.CASCADE)
    name = models.CharField(
        choices=NotificationName.as_choices(), max_length=60, db_index=True
    )
    status = models.CharField(
        choices=NotificationStatus.as_choices(),
        default=NotificationStatus.TO_SEND.name,
        max_length=50,
        db_index=True,
    )
    date_created = models.DateTimeField(
        "created at", default=timezone.now, db_index=True
    )
    date_sent = models.DateTimeField(_("sent at"), null=True, blank=True)
    recipient = models.CharField(_("recipient"), max_length=255)
    subject = models.CharField(_("subject"), max_length=255)
    body_text = models.TextField()
    body_html = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{} for {}".format(self.name, self.participant)

    def name_human(self):
        return NotificationName[self.name].value

    @staticmethod
    def get_defaults(name, participant, request, **kwargs):
        defaults = dict(name=name, participant=participant)
        if request is None:
            return defaults
        organizers_emails = ",".join(
            [org.email for org in participant.event.get_users_with_perm("change").all()]
        )
        if name == NotificationName.NEW_PARTICIPANT.name:
            # Notify the admins
            participant_url = "{root}admin/{code}/queues/validation/{pid}/".format(
                root=request.build_absolute_uri(reverse("root")),
                code=participant.event.code,
                pid=participant.pk,
            )
            defaults.update(
                dict(
                    status=NotificationStatus.TO_SEND.name,
                    subject=_("Kinky Salon %s : nouvelle pré-inscription")
                    % participant.event.name,
                    body_text=render_to_string(
                        "kspals/email/{}.txt".format(name.lower()),
                        {
                            "participant": participant,
                            "participant_url": participant_url,
                        },
                    ),
                    body_html=render_to_string(
                        "kspals/email/{}.html".format(name.lower()),
                        {
                            "participant": participant,
                            "participant_url": participant_url,
                            "request": request,
                            "csv_url": request.build_absolute_uri(
                                reverse("export_csv", args=(participant.event.code,))
                            ),
                        },
                    ),
                    recipient=organizers_emails,
                )
            )
        elif name == NotificationName.TICKETING_READY.name:
            if participant.event.is_full():
                status = NotificationStatus.DRAFT.name
            else:
                status = NotificationStatus.TO_SEND.name
            defaults.update(
                dict(
                    subject=_("Kinky Salon : vous pouvez acheter vos billets"),
                    body_text=render_to_string(
                        "kspals/email/{}.txt".format(name.lower()),
                        {"participant": participant},
                    ),
                    recipient=participant.user.email,
                    status=status,
                )
            )
        elif name == NotificationName.BROKEN_PAL.name:
            actor = kwargs["actor"]
            defaults.update(
                dict(
                    subject=_("Kinky Salon : votre PAL ne vous prend plus pour PAL"),
                    body_text=render_to_string(
                        "kspals/email/{}.txt".format(name.lower()),
                        {"participant": participant, "actor": actor},
                    ),
                    recipient=participant.user.email,
                    status=NotificationStatus.TO_SEND.name,
                )
            )
        elif name == NotificationName.ADMIN_BROKEN_PAL_GROUP.name:
            pal_group = kwargs["pal_group"]
            defaults.update(
                dict(
                    subject=_("Kinky Salon %s : un groupe de PALs a été cassé")
                    % participant.event.name,
                    body_text=render_to_string(
                        "kspals/email/{}.txt".format(name.lower()),
                        {
                            "participant": participant,
                            "pal_group": pal_group,
                            "root_url": request.build_absolute_uri(reverse("root")),
                        },
                    ),
                    recipient=organizers_emails,
                    status=NotificationStatus.TO_SEND.name,
                )
            )
        else:
            raise ValueError("Unknown notification name: {}".format(name))
        return defaults

    def send_email(self, dry_run=False):
        recipient_list = self.recipient.split(",")
        msg = EmailMultiAlternatives(
            self.subject, self.body_text, settings.DEFAULT_FROM_EMAIL, recipient_list
        )
        if self.body_html is not None:
            msg.attach_alternative(self.body_html, "text/html")
        if not dry_run:
            msg.send()
            self.status = NotificationStatus.SENT.name
            self.date_sent = timezone.now()
            self.save()
            if self.name == NotificationName.TICKETING_READY.name:
                previous = Participant.objects.get(pk=self.participant.pk)
                self.participant.ticket_status = TicketStatus.URL_SENT.name
                self.participant.save()
                participant_changed.send(
                    Participant, instance=self.participant, previous=previous
                )
        return msg

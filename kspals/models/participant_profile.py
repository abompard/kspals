import logging
import os
import random
import shutil
import string

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from PIL import Image

from .participant import Participant

log = logging.getLogger(__name__)


def picture_path(instance, filename):
    participant = Participant.objects.get(pk=instance.participant_id)
    ext = os.path.splitext(filename)[1].lower()
    new_filename = "".join(
        list(random.choice(string.ascii_lowercase + string.digits) for i_ in range(10))
        + [ext]
    )
    return "kspals/part-pics/{}/{}/{}".format(
        participant.event.code, participant.id, new_filename
    )


class ParticipantProfile(models.Model):

    participant = models.OneToOneField(
        "Participant", on_delete=models.CASCADE, related_name="profile"
    )
    first_name = models.CharField(_("first name"), max_length=60)
    last_name = models.CharField(_("last name"), max_length=60)
    introduction = models.TextField()
    learned_about = models.TextField()
    picture = models.ImageField(
        upload_to=picture_path,
        max_length=254,
        height_field="picture_height",
        width_field="picture_width",
    )
    picture_height = models.IntegerField()
    picture_width = models.IntegerField()
    date_registered = models.DateTimeField("registered at", default=timezone.now)
    low_income = models.BooleanField(_("Low income"), default=False)

    class Meta:
        ordering = ["participant__user__email"]

    def __str__(self):
        return self.full_name

    def on_post_save(self, **kwargs):
        self.participant.date_modified = timezone.now()
        self.participant.save()

    @property
    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    @property
    def picture_thumb(self):
        return "{}.thumb.jpg".format(os.path.splitext(self.picture.name)[0])

    def make_thumb(self):
        picture = self.picture
        thumb_name = self.picture_thumb
        thumb_path = os.path.join(settings.MEDIA_ROOT, thumb_name)
        if os.path.exists(thumb_path):
            return
        log.debug("Generating thumbnail for %s", picture.name)
        try:
            picture.open()
            img = Image.open(picture)
            try:
                exif = img._getexif()
            except AttributeError:
                # Probably a PNG file, don't try to rotate.
                exif = None
            orientation_key = 274  # cf ExifTags
            if exif is not None and orientation_key in exif:
                orientation = exif[orientation_key]
                rotate_values = {
                    3: Image.ROTATE_180,
                    6: Image.ROTATE_270,
                    8: Image.ROTATE_90,
                }
                if orientation in rotate_values:
                    img = img.transpose(rotate_values[orientation])
            img.thumbnail((200, 200))
            if img.mode == "RGBA":
                img = img.convert("RGB")
            img.save(thumb_path, "JPEG")
        except IOError as e:
            log.warning(
                "Cannot create thumbnail for participant %s: %s", self.participant_id, e
            )

    def on_post_delete(self, **kwargs):
        # Called after instance deletion, see the kspals.signals module.
        pictures_dir = os.path.dirname(
            os.path.join(settings.MEDIA_ROOT, self.picture.name)
        )
        try:
            shutil.rmtree(pictures_dir)
        except OSError as e:
            log.warning(
                "Could not delete picture directory %s: %s", pictures_dir, str(e)
            )

from kspals.config import MIN_PAL_GROUP_SIZE, MAX_PAL_GROUP_SIZE


def pal_group_is_mutual(pal_group):
    """The PALs must have selected each other and only each other."""
    if len(pal_group) < MIN_PAL_GROUP_SIZE or len(pal_group) > MAX_PAL_GROUP_SIZE:
        return False
    for pal in pal_group[1:]:
        if set(pal.get_pal_group()) != set(pal_group):
            return False
    return True


def pal_group_is_approved(pal_group):
    """The PALs must all be approved."""
    if len(pal_group) < MIN_PAL_GROUP_SIZE or len(pal_group) > MAX_PAL_GROUP_SIZE:
        return False
    return all([pal.approved for pal in pal_group])


def pal_group_is_valid(pal_group):
    return pal_group_is_approved(pal_group) and pal_group_is_mutual(pal_group)

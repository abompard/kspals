from django.db import models


class Role(models.Model):

    event = models.ForeignKey("Event", on_delete=models.CASCADE)
    code = models.CharField(max_length=60)
    name = models.CharField(max_length=60)
    default = models.BooleanField(default=False)

    class Meta:
        ordering = ["code"]
        unique_together = ("event", "code")

    def __str__(self):
        return "{} ({})".format(self.name, self.code)

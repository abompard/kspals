import logging

from django.core.cache import cache
from django.db import models
from django.utils import timezone

log = logging.getLogger(__name__)


class PAL(models.Model):

    participant = models.ForeignKey(
        "Participant", on_delete=models.CASCADE, related_name="pal_set"
    )
    pal = models.ForeignKey(
        "Participant", on_delete=models.CASCADE, related_name="pal_of_set"
    )
    date_requested = models.DateTimeField("requested at", default=timezone.now)

    class Meta:
        unique_together = ("participant", "pal")
        ordering = ["date_requested"]
        verbose_name = "PAL"
        verbose_name_plural = "PALs"

    def __str__(self):
        return "{} -> {}".format(self.participant.user.email, self.pal.user.email)

    def _clear_cache(self):
        # Clear participant PAL cache
        keys = [
            "participant:{}:get_pal_group".format(self.participant_id),
            "participant:{}:is_in_mutual_pal_group".format(self.participant_id),
            "participant:{}:is_in_mutual_pal_group".format(self.pal_id),
            "pal:{}:{}:is_mutual".format(self.participant_id, self.pal_id),
            "pal:{}:{}:is_mutual".format(self.pal_id, self.participant_id),
            "event:{}:mutual_participants_count".format(self.participant.event_id),
        ]
        # For now all versions are identical so we can use:
        cache.delete_many(keys, version=1)
        # When versions differ we'll have to use:
        # for key in keys:
        #     cache.delete(key, version=1)

        # Mark both PALs as modified
        self.participant.date_modified = timezone.now()
        self.participant.save()
        self.pal.date_modified = timezone.now()
        self.pal.save()

    def on_post_save(self, **kwargs):
        self._clear_cache()

    def on_post_delete(self, **kwargs):
        self._clear_cache()

    def is_mutual(self):
        key = "pal:{}:{}:is_mutual".format(self.participant_id, self.pal_id)
        version = 1
        return cache.get_or_set(
            key, lambda: self.participant in self.pal.pals.all(), version=version
        )

    is_mutual.boolean = True

    def name(self):
        if not self.is_mutual():
            return None
        if self.pal.has_profile():
            return self.pal.profile.full_name
        else:
            return None

    def has_ticket(self):
        # Only show the other participant's has_ticket status if the selection
        # is mutual, to avoid information leak.
        if not self.is_mutual():
            return None
        return self.pal.has_ticket

    has_ticket.boolean = True

    def approved(self):
        # Only show the other participant's approved status if the selection
        # is mutual, to avoid information leak.
        if not self.is_mutual():
            return None
        return self.pal.approved

from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from .constants import TicketStatus
from .user import User
from .utils import pal_group_is_valid


class Event(models.Model):

    code = models.SlugField(max_length=50, db_index=True, unique=True)
    name = models.CharField(max_length=50, db_index=True)
    location = models.CharField(max_length=50, blank=True)
    date_created = models.DateTimeField(_("date created"), default=timezone.now)
    date_open = models.DateTimeField(_("date d'ouverture"))
    date_reg_open = models.DateTimeField(_("date de début des inscriptions"))
    date_reg_close = models.DateTimeField(_("date de fin des inscriptions"))
    waiting_list_size = models.IntegerField(_("taille de la file d'attente"), default=0)
    full_limit = models.IntegerField(
        _("nombre maximum de participants"), null=True, blank=True
    )
    groups = models.ManyToManyField(Group, through="EventUserGroup")

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    def is_active(self):
        if self.date_open is None:
            return False
        return timezone.now() < self.date_open

    is_active.boolean = True

    def is_on_waiting_list(self):
        sent_count = self.mutual_participants_count()
        return (
            self.full_limit is not None
            and sent_count >= self.full_limit
            and sent_count < (self.full_limit + self.waiting_list_size)
        )

    is_on_waiting_list.boolean = True

    def is_full(self):
        return self.full_limit is not None and self.mutual_participants_count() >= (
            self.full_limit + self.waiting_list_size
        )

    is_full.boolean = True

    def is_registration_open(self):
        # Time period when it's allowed to change one's PALs
        if self.date_open is None:
            return False
        now = timezone.now()
        if now < self.date_reg_open or now >= self.date_reg_close:
            return False
        return True

    is_registration_open.boolean = True

    def get_pal_groups(self):
        pal_groups = {}
        participants = self.participant_set.filter(
            self_registered=True, approved=True
        ).all()
        for p in participants:
            pal_group = p.get_pal_group()
            if pal_group_is_valid(pal_group):
                pal_groups[p] = pal_group
        return pal_groups

    def mutual_participants_count(self):
        def _compute():
            participants = self.participant_set.filter(self_registered=True).all()
            mutual = [p for p in participants if p.is_in_mutual_pal_group()]
            return len(mutual)

        key = "event:{}:mutual_participants_count".format(self.pk)
        version = 1
        return cache.get_or_set(key, _compute, version=version)

    mutual_participants_count.short_description = _(
        "Nombre de participants dans un groupe de PAL cohérent"
    )

    def ticketing_sent_count(self):
        return self.participant_set.filter(
            ticket_status__in=[TicketStatus.URL_SENT.name, TicketStatus.BOUGHT.name]
        ).count()

    ticketing_sent_count.short_description = _(
        "Nombre de participants avec le lien billetterie"
    )

    def tickets_bought_count(self):
        return self.participant_set.filter(
            ticket_status=TicketStatus.BOUGHT.name
        ).count()

    tickets_bought_count.short_description = _("Nombre de participants avec ticket")

    def get_users_with_perm(self, perm):
        perm_codename = "{}_event".format(perm)
        content_type = ContentType.objects.get(app_label="kspals", model="event")
        return User.objects.filter(
            event_groups__event=self,
            event_groups__group__permissions__content_type=content_type,
            event_groups__group__permissions__codename=perm_codename,
        )

    def user_has_perm(self, user, perm):
        return self.get_users_with_perm(perm).filter(pk=user.pk).exists()

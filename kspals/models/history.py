from django.db import models
from django.utils import timezone


class History(models.Model):

    event = models.ForeignKey(
        "Event", on_delete=models.CASCADE, related_name="history_set"
    )
    participant = models.ForeignKey(
        "Participant",
        on_delete=models.CASCADE,
        related_name="history_set",
        null=True,
        blank=True,
    )
    action = models.CharField(max_length=63, db_index=True)
    data = models.CharField(max_length=255, null=True, blank=True)
    date = models.DateTimeField("happened at", default=timezone.now, db_index=True)

    class Meta:
        ordering = ["date"]
        verbose_name_plural = "History"

    def __str__(self):
        result = "{} {}".format(self.participant.user.email, self.action)
        if self.data:
            result = "{} {}".format(result, self.data)
        return result

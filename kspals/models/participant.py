import logging

from django.core.cache import cache
from django.db import models
from django.utils.translation import ugettext_lazy as _

from kspals.signals import participant_changed

from .constants import TicketStatus
from .utils import pal_group_is_approved, pal_group_is_mutual

log = logging.getLogger(__name__)


class Participant(models.Model):

    user = models.ForeignKey("User", on_delete=models.CASCADE)
    event = models.ForeignKey("Event", on_delete=models.CASCADE)
    self_registered = models.BooleanField(default=False, db_index=True)
    approved = models.BooleanField(null=True, blank=True)
    ticket_rate = models.ForeignKey(
        "TicketRate", on_delete=models.SET_NULL, null=True, blank=True
    )
    role = models.ForeignKey("Role", on_delete=models.SET_NULL, null=True, blank=True)
    ticket_status = models.CharField(
        choices=TicketStatus.as_choices(),
        default=TicketStatus.NOT_READY.name,
        max_length=50,
    )
    ticket_name = models.CharField(
        _("ticket name"), max_length=120, null=True, blank=True
    )
    personal_ticket_url = models.CharField(
        max_length=511,
        null=True,
        blank=True,
        help_text=_(
            "Laisser vide pour utiliser l'URL de l'évènement correspondant au tarif"
        ),
    )
    selling_ticket_to = models.ForeignKey(
        "Participant",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="ticket_offer_by",
    )
    date_created = models.DateTimeField("created at", auto_now_add=True)
    date_modified = models.DateTimeField("modified at", auto_now=True)
    drinks = models.IntegerField(_("boissons"), default=0)
    pals = models.ManyToManyField(
        "self", symmetrical=False, through="PAL", through_fields=("participant", "pal")
    )

    class Meta:
        unique_together = ("user", "event")

    def __str__(self):
        return self.user.email

    def on_post_save(self, **kwargs):
        # Clear the get_pal_group cache because it stores the participant instances and the approved status may have changed.
        pal_group = self.get_pal_group()
        keys = ["participant:{}:get_pal_group".format(p.id) for p in pal_group]
        # For now all versions are identical so we can use:
        cache.delete_many(keys, version=1)
        # When versions differ we'll have to use:
        # for key in keys:
        #     cache.delete(key, version=1)

    def has_profile(self):
        from .participant_profile import ParticipantProfile

        try:
            self.profile
        except ParticipantProfile.DoesNotExist:
            return False
        else:
            return True

    @property
    def has_ticket(self):
        return self.ticket_status in (
            TicketStatus.BOUGHT.name,
            TicketStatus.SELLING.name,
        )

    def get_pal_group(self):
        key = "participant:{}:get_pal_group".format(self.pk)
        version = 1
        return cache.get_or_set(
            key,
            lambda: [self] + list(self.pals.select_related("user", "profile").all()),
            version=version,
        )

    def is_in_mutual_pal_group(self):
        key = "participant:{}:is_in_mutual_pal_group".format(self.pk)
        version = 1
        return cache.get_or_set(
            key, lambda: pal_group_is_mutual(self.get_pal_group()), version=version
        )

    is_in_mutual_pal_group.boolean = True

    def is_in_approved_pal_group(self):
        return pal_group_is_approved(self.get_pal_group())

    is_in_approved_pal_group.boolean = True

    def is_in_valid_pal_group(self):
        return self.is_in_approved_pal_group() and self.is_in_mutual_pal_group()

    is_in_valid_pal_group.boolean = True

    def _get_ticket_rate(self):
        # The PAL group must be approved.
        if not self.is_in_approved_pal_group():
            return None
        # The PAL group must be coherent.
        if not self.is_in_mutual_pal_group():
            return None
        # OK! Use the specific rate if any, otherwise use the generic one.
        if self.ticket_rate is None:
            ticket_rate = self.event.ticketrate_set.get(default=True)
            if ticket_rate is None:
                return None
        else:
            ticket_rate = self.ticket_rate
        return ticket_rate

    def ticket_url(self):
        ticket_rate = self._get_ticket_rate()
        if ticket_rate is None:
            return None
        # Use the specific URL if any, otherwise use the ticket rate URL
        if self.personal_ticket_url:
            return self.personal_ticket_url
        if ticket_rate.url is None:
            # Our ticket rate has no URL (and only a coupon code), use the default URL
            default_ticket_rate = self.event.ticketrate_set.get(default=True)
            return default_ticket_rate.url
        else:
            return ticket_rate.url

    def ticket_coupon(self):
        ticket_rate = self._get_ticket_rate()
        if ticket_rate is None:
            return None
        return ticket_rate.coupon

    def ticket_rate_name(self):
        ticket_rate = self._get_ticket_rate()
        if ticket_rate is None:
            return None
        return ticket_rate.name

    def pal_group_registration_date(self):
        result = None
        for p in self.get_pal_group():
            try:
                date_reg = p.profile.date_registered
            except Participant.profile.RelatedObjectDoesNotExist:
                continue
            if result is None or date_reg > result:
                result = date_reg
        return result

    def sell_to(self, participant):
        if participant.approved is False:
            raise ValueError(
                "Impossible de vendre à {}.".format(participant.user.email)
            )
        previous = Participant.objects.get(pk=participant.pk)
        participant.ticket_status = TicketStatus.BOUGHT.name
        participant.ticket_name = self.ticket_name or self.user.email
        participant.approved = True  # XXX: Hmm this sounds dangerous
        participant.save()
        participant_changed.send(Participant, instance=participant, previous=previous)

        previous = Participant.objects.get(pk=self.pk)
        self.approved = False
        self.selling_ticket_to = None
        self.ticket_status = TicketStatus.RESOLD.name
        self.pals.clear()
        self.save()
        participant_changed.send(
            Participant,
            instance=self,
            previous=previous,
        )

from django.db import models
from django.utils.translation import ugettext_lazy as _

from .constants import TicketStatus


class TicketRate(models.Model):

    event = models.ForeignKey("Event", on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_("Tarif"), max_length=60)
    url = models.CharField(max_length=511, null=True, blank=True)
    coupon = models.CharField(max_length=50, null=True, blank=True)
    amount = models.FloatField(null=True, blank=True)
    default = models.BooleanField(default=False)
    low_income = models.BooleanField(default=False)

    class Meta:
        ordering = ["name"]
        unique_together = ("event", "name")

    def __str__(self):
        return self.name

    def bought_count(self):
        return self.event.participant_set.filter(
            ticket_rate=self, ticket_status=TicketStatus.BOUGHT.name
        ).count()

    bought_count.short_description = _("Tickets achetés")

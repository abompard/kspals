from enum import Enum

from django.utils.translation import ugettext_lazy as _


class ModelEnum(Enum):
    @classmethod
    def as_choices(cls):
        return [(m.name, m.value) for m in cls]


class TicketStatus(ModelEnum):
    NOT_READY = _("Not ready")
    URL_SENT = _("URL sent")
    BOUGHT = _("Bought")
    CANCELLED = _("Cancelled")
    SELLING = _("Selling")
    RESOLD = _("Resold")


class NotificationName(ModelEnum):
    NEW_PARTICIPANT = _("New participant")
    TICKETING_READY = _("Ticketing ready")
    BROKEN_PAL = _("Broken PAL")
    ADMIN_BROKEN_PAL_GROUP = _("Admins: broken PAL group")


class NotificationStatus(ModelEnum):
    TO_SEND = _("To send")
    DRAFT = _("Draft")
    SENT = _("Sent")
    TRASH = _("Trash")

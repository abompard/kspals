from django.db import models
from django.contrib.auth.models import Group


class EventUserGroup(models.Model):

    event = models.ForeignKey(
        "Event", on_delete=models.CASCADE, related_name="user_groups"
    )
    user = models.ForeignKey(
        "User", on_delete=models.CASCADE, related_name="event_groups"
    )
    group = models.ForeignKey(
        Group, on_delete=models.CASCADE, related_name="event_users"
    )

    class Meta:
        unique_together = ("event", "user", "group")
        verbose_name = "Event group membership"

    def __str__(self):
        return "User {} in {} for {}".format(
            self.user.email, self.group.name, self.event.name
        )


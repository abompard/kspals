Bonjour {{ participant.profile.full_name }},

Vous étiez en groupe de PAL avec {{actor.profile.full_name}}, mais cette personne ne vous prend plus pour PAL.
Merci de vérifier la situation avec elle, et le cas échéant de trouver un⋅e autre PAL.

Nous vous rappelons qu'il est impossible d'entrer au Kinky Salon sans un groupe de PAL cohérent,
c'est à dire dont chacun des membres désigne en PAL tous les autres.

Amicalement,

L'équipe d'organisation du Kinky Salon

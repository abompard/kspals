import logging

from asgiref.sync import sync_to_async
from django.contrib.auth.models import AnonymousUser
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed

from kspals.permissions import CanViewEvent, ParticipantIsCurrentUser
from .utils import refresh_db_connection

log = logging.getLogger(__name__)


@sync_to_async
@refresh_db_connection
def authenticate_user(data=None):
    data = data or {}
    try:
        token = data["token"]
    except KeyError:
        return AnonymousUser()
    try:
        return TokenAuthentication().authenticate_credentials(token)[0]
    except AuthenticationFailed:
        return AnonymousUser()


@sync_to_async
@refresh_db_connection
def can_watch_participant(user, participant):
    return any(
        [
            ParticipantIsCurrentUser.do_check(user, participant),
            CanViewEvent.do_check(participant.event, user),
        ]
    )


@sync_to_async
@refresh_db_connection
def can_watch_event(user, event):
    return CanViewEvent.do_check(event, user)

from contextlib import contextmanager
from functools import wraps
from django.db import close_old_connections


@contextmanager
def fresh_db_connection():
    # In async mode, old DB connections must be cleaned up.
    # https://forum.djangoproject.com/t/asgi-mysql-server-has-gone-away-and-conn-max-age/5240/5
    close_old_connections()
    try:
        yield
    finally:
        close_old_connections()


def refresh_db_connection(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        with fresh_db_connection():
            return f(*args, **kwargs)

    return wrapper

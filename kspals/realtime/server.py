import logging

import socketio
from asgiref.sync import sync_to_async

from kspals.models import Event, Participant

from .exceptions import MessageError, NotFound, Unauthorized
from .permissions import (authenticate_user, can_watch_event,
                          can_watch_participant)
from .utils import refresh_db_connection

log = logging.getLogger(__name__)


@sync_to_async
@refresh_db_connection
def _get_db_object(Model, key, value):
    return Model.objects.get(**{key: value})


class KSPalsNamespace(socketio.AsyncNamespace):
    async def trigger_event(self, event, *args):
        sid = args[0]
        async with self.session(sid) as session:
            if "user" not in session:
                session["user"] = await authenticate_user()
            args = list(args)
            args.append(session)
            try:
                return await super().trigger_event(event, *args)
            except MessageError as e:
                return e.serialize()

    # def on_connect(self, sid, environ, session):
    #     pass

    # def on_disconnect(self, sid, session):
    #     pass

    async def on_login(self, sid, data, session):
        user = await authenticate_user(data)
        session["user"] = user
        if user.is_authenticated:
            log.info(f"{sid} logged in as {user.get_username()}")
            self.enter_room(sid, f"user:{user.id}")
            await self.emit("ready", data={"user": user.get_username()}, room=sid)

    async def on_logout(self, sid, session):
        for room in self.rooms(sid):
            self.leave_room(sid, room)
        session["user"] = await authenticate_user()
        log.info(f"{sid} logged out")

    async def on_watch_participant(self, sid, data, session):
        user = session["user"]
        try:
            participant = await _get_db_object(Participant, "pk", data["id"])
        except Participant.DoesNotExist:
            raise NotFound()
        if not await can_watch_participant(user, participant):
            raise Unauthorized()
        self.enter_room(sid, f"participant:{participant.id}")

    def on_unwatch_participant(self, sid, data, session):
        pid = data["id"]
        self.leave_room(sid, f"participant:{pid}")

    async def on_watch_event_participants(self, sid, data, session):
        try:
            event = await _get_db_object(Event, "code", data["id"])
        except Event.DoesNotExist:
            raise NotFound()
        if not await can_watch_event(session["user"], event):
            raise Unauthorized()
        self.enter_room(sid, f"event:{event.id}:participants")

    def on_unwatch_event_participants(self, sid, data, session):
        event_id = data["id"]
        self.leave_room(sid, f"event:{event_id}:participants")

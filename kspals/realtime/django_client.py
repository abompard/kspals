from urllib.parse import urlparse

import socketio

from kspals.config import SOCKETIO_ENDPOINT, SOCKETIO_QUEUE

SOCKETIO_CONNECTION = None


def get_socketio():
    global SOCKETIO_CONNECTION
    if SOCKETIO_CONNECTION is None:
        SOCKETIO_CONNECTION = socketio.RedisManager(SOCKETIO_QUEUE, write_only=True)
    return SOCKETIO_CONNECTION


def socketio_emit(event, data, room):
    if SOCKETIO_ENDPOINT is None:
        return
    sio = get_socketio()
    namespace = urlparse(SOCKETIO_ENDPOINT).path
    sio.emit(event, data, room=room, namespace=namespace)


def socketio_close_connection(sender, **kwargs):
    global SOCKETIO_CONNECTION
    if SOCKETIO_CONNECTION is not None:
        SOCKETIO_CONNECTION.redis.close()


def emit_participant_updated(participant, related):
    # Emit a signal to the web clients subscribed to any related participant
    rooms = [f"event:{participant.event.pk}:participants"]
    rooms.extend(["participant:{}".format(p.pk) for p in related])
    for room in rooms:
        socketio_emit("participant_updated", data={"id": participant.id}, room=room)

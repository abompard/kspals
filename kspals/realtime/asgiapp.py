from urllib.parse import urlparse

import django
import socketio
from django.conf import settings

from kspals.config import SOCKETIO_QUEUE, SOCKETIO_ENDPOINT

django.setup()

from .server import KSPalsNamespace  # noqa

# Connect the the message queue
mgr = socketio.AsyncRedisManager(SOCKETIO_QUEUE)
# Create a Socket.IO server
sio = socketio.AsyncServer(
    client_manager=mgr,
    cors_allowed_origins="*" if settings.DEBUG else None,
    async_mode="asgi",
)
# Register events
sio.register_namespace(KSPalsNamespace(urlparse(SOCKETIO_ENDPOINT).path))

# Run it in an ASGI app server like Uvicorn or Daphne
# Wrap with ASGI application
app = socketio.ASGIApp(sio)

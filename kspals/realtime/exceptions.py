class MessageError(Exception):
    CODE = "ERROR"

    def __init__(self, code=None, msg=None):
        self.code = code or self.CODE
        self.msg = msg or ""

    def serialize(self):
        return {"error": {"code": self.code, "msg": self.msg}}


class Unauthorized(MessageError):
    CODE = "UNAUTHORIZED"


class NotFound(MessageError):
    CODE = "NOT_FOUND"

from django.core.signals import request_finished
from django.dispatch import receiver

from kspals.models import PAL, Participant
from kspals.signals import pal_added, pal_removed, pal_updated, participant_changed

from .django_client import emit_participant_updated, socketio_close_connection

request_finished.connect(socketio_close_connection)


@receiver(participant_changed, sender=Participant)
def realtime_notify_participant(sender, **kwargs):
    """Emit a signal to the web clients subscribed to any related participant"""
    participant = kwargs["instance"]
    emit_participant_updated(participant, participant.get_pal_group())


@receiver([pal_added, pal_updated, pal_removed], sender=PAL)
def realtime_notify_pal(sender, **kwargs):
    participant = kwargs["instance"].participant
    pal_group = set(participant.get_pal_group())
    if "previous" in kwargs:
        pal_group.add(kwargs["previous"])
    emit_participant_updated(participant, pal_group)

from django import template
from django.conf import settings
from django.urls import reverse


register = template.Library()


@register.inclusion_tag('kspals/includes/participant.html', takes_context=True)
def participant(context, participant):
    request = context["request"]
    context = {
        "obj": participant,
        "admin_url": request.build_absolute_uri(
            reverse('admin:kspals_participant_change', args=(participant.pk,))
        ),
        "picture_url": request.build_absolute_uri(
            "{}{}".format(settings.MEDIA_URL, participant.profile.picture)
        ),
        "picture_thumb_url": request.build_absolute_uri(
            "{}{}".format(settings.MEDIA_URL, participant.profile.picture_thumb)
        ),
    }
    return context

import csv

from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render

from kspals.models import Event
from kspals.utils import get_csv_rows


@login_required
def participants_csv(request, event_code):
    event = get_object_or_404(Event, code=event_code)
    if not event.user_has_perm(request.user, "view"):
        raise PermissionDenied
    waiting_list = False
    try:
        waiting_list = bool(int(request.GET.get("waitinglist", "0")))
    except TypeError:
        pass
    # Create the HttpResponse object with the appropriate CSV header.
    oldmac_format = request.GET.get("oldmac", False)
    if oldmac_format:
        response = HttpResponse(content_type="text/csv; charset=iso8859-15")
        delimiter = ";"
    else:
        response = HttpResponse(content_type="text/csv")
        delimiter = ","
    response["Content-Disposition"] = 'attachment; filename="{}-{}.csv"'.format(
        event.code, "waitinglist" if waiting_list else "participants"
    )
    writer = csv.writer(response, delimiter=delimiter)
    writer.writerows(get_csv_rows(event, unapproved=waiting_list))
    return response


@login_required
def participants_html(request, event_code):
    event = get_object_or_404(Event, code=event_code)
    if not event.user_has_perm(request.user, "view"):
        raise PermissionDenied
    query = (
        event.participant_set.filter(self_registered=True, approved=True)
        .order_by("profile__first_name", "profile__last_name")
        .select_related("profile")
    )
    # Filter out participants created in the admin interface (no profile)
    participants = [p for p in query if p.has_profile()]
    context = {"event": event, "participants": participants}
    return render(request, "kspals/export_html.html", context)

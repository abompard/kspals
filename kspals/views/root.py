import logging
import os

from django.conf import settings
from django.contrib.auth import get_user_model, login, logout
from django.http import HttpResponse
from drfpasswordless.views import ObtainAuthTokenFromCallbackToken
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from kspals.config import (
    LOGO_URL,
    MATOMO_ID,
    MATOMO_URL,
    SOCKETIO_ENDPOINT,
    REACT_APP_DIR,
)
from kspals.serializers.user import UserSerializer

log = logging.getLogger(__name__)


def index_view(request):
    try:
        with open(os.path.join(REACT_APP_DIR, "build", "index.html")) as f:
            return HttpResponse(f.read())
    except FileNotFoundError:
        logging.exception("Production build of app not found")
        return HttpResponse(
            """
            This URL is only used when you have built the production
            version of the app. Visit http://localhost:3000/ instead, or
            run `npm run build` to test the production version.
            """,
            status=501,
        )


@api_view(["GET"])
@permission_classes([])
def initial_state(request):
    initial_state = {
        "config": {
            "logoUrl": LOGO_URL,
            "matomoUrl": MATOMO_URL,
            "matomoId": MATOMO_ID,
            "socketIOEndpoint": SOCKETIO_ENDPOINT,
        },
        "current": {"user": None},
    }
    if settings.DEBUG:
        try:
            autologin_email = getattr(settings, "AUTOLOGIN")
        except AttributeError:
            pass
        else:
            User = get_user_model()
            login(request, User.objects.get(email=autologin_email))
            log.info("Autologged-in as %s." % autologin_email)
    if request.user.is_authenticated:
        serializer = UserSerializer(request.user, context={"request": request})
        initial_state["current"]["user"] = request.user.id
        initial_state["entities"] = {"users": {request.user.id: serializer.data}}
    return Response(initial_state, status=status.HTTP_200_OK)


@api_view(["POST"])
def logout_view(request):
    if not request.user.is_authenticated:
        return Response(
            {"detail": "Couldn't log you out: you must be authenticated."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    token = request.META.get("HTTP_AUTHORIZATION")
    if not token:
        return Response(
            {"detail": "Couldn't log you out: no token found."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    token = token.split(" ", 1)[1]
    if token != request.user.auth_token.key:
        return Response(
            {"detail": "Couldn't log you out: token does not match."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    logout(request)
    return Response({"detail": "You have been logged out."}, status=status.HTTP_200_OK)


class ObtainAuthTokenFromCallbackTokenAndLogin(ObtainAuthTokenFromCallbackToken):
    """Subclassed to login the user with Django."""

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.validated_data["user"]
            token, created = Token.objects.get_or_create(user=user)

            if created:
                # Initially set an unusable password if a user is created through this.
                user.set_unusable_password()
                user.save()

            if token:
                # Log the user in with Django
                login(request, user)
                log.info("User %s logged in." % user.email)
                # Add the current serialized user
                serializer = UserSerializer(user, context={"request": request})
                # Return our key for consumption.
                return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            log.error(
                "Couldn't log in unknown user. Errors on serializer: %s"
                % (serializer.error_messages,)
            )
        return Response(
            {"detail": "Couldn't log you in. Try again later."},
            status=status.HTTP_400_BAD_REQUEST,
        )

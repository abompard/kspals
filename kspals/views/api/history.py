from rest_framework import viewsets
from django_filters import rest_framework as filters
from django.utils.translation import gettext_lazy as _

from kspals.models import History
from kspals.serializers.history import (
    EventHistorySerializer,
    ParticipantHistorySerializer,
)
from .utils import AdminObjectViewSet, EventChildMixin, ParticipantChildMixin


class HistoryFilter(filters.FilterSet):
    participant = filters.CharFilter(field_name="participant_id")
    email = filters.CharFilter(
        field_name="participant__user__email", lookup_expr="icontains"
    )
    order = filters.OrderingFilter(
        fields={
            "participant_id": "participant",
            "participant__user__email": "email",
            "action": "action",
            "date": "date",
        },
        field_labels={
            "participant_id": _("Participant ID"),
            "participant__user__email": _("Participant Email"),
            "action": _("Action"),
            "date": _("Date"),
        },
    )

    class Meta:
        model = History
        fields = {"date": ["exact", "gt", "lt"], "action": ["exact"]}


class HistoryViewSet(EventChildMixin, AdminObjectViewSet):

    queryset = History.objects.order_by("-date")
    serializer_class = EventHistorySerializer
    filterset_class = HistoryFilter


class ParticipantHistoryViewSet(ParticipantChildMixin, viewsets.ModelViewSet):

    queryset = History.objects.order_by("-date")
    serializer_class = ParticipantHistorySerializer

from rest_framework import permissions, viewsets

from kspals.models import Event
from kspals.permissions import CanChangeEvent
from kspals.serializers.event import EventAdminSerializer, EventSerializer


class EventViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows events to be viewed or edited.
    """

    queryset = Event.objects.all().order_by("-date_open")
    lookup_field = "code"
    lookup_url_kwarg = "code"

    def get_permissions(self):
        if self.action == "list":
            permission_classes = []
        elif self.action in ("create", "destroy"):
            permission_classes = [permissions.IsAdminUser]
        else:
            # retrieve, update, partial_update
            permission_classes = [permissions.IsAuthenticated]
        if self.action in ("update", "partial_update"):
            permission_classes.append(CanChangeEvent)
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        if self.action in ("list", "create"):
            # We can't check if we have perms
            return EventSerializer
        event = self.get_object()
        if event.user_has_perm(self.request.user, "view"):
            return EventAdminSerializer
        return EventSerializer

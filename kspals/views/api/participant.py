import logging

from django.shortcuts import get_object_or_404
from django_filters import rest_framework as filters
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from kspals.models import Event, History, Participant, User
from kspals.models.constants import TicketStatus
from kspals.permissions import (CanChangeEvent, CanViewEvent,
                                ParticipantIsCurrentUser)
from kspals.serializers.participant import (
    EventParticipantsAdminFullSerializer, EventParticipantsAdminSerializer,
    EventParticipantsSerializer)
from kspals.signals import new_participant, participant_changed
from kspals.utils import get_or_create_participant

from .utils import EventChildMixin

log = logging.getLogger(__name__)


class ParticipantFilter(filters.FilterSet):
    date_modified = filters.DateTimeFromToRangeFilter()

    class Meta:
        model = Participant
        fields = ["date_modified"]


class ParticipantViewSet(EventChildMixin, viewsets.ModelViewSet):

    queryset = Participant.objects.order_by("user__email").select_related(
        "event", "user", "profile", "role", "ticket_rate"
    )
    filterset_class = ParticipantFilter

    def _make_nested_request_data(self):
        profile_props = (
            "first_name",
            "last_name",
            "introduction",
            "learned_about",
            "picture",
            "low_income",
        )
        data = {"profile": {}}
        for key, value in self.request.data.items():
            if key in profile_props:
                data["profile"][key] = value
            else:
                data[key] = value
        # Remove the picture key if empty: necessary for updates.
        if "picture" in data["profile"] and not data["profile"]["picture"]:
            del data["profile"]["picture"]
        return data

    def get_permissions(self):
        permission_classes = [permissions.IsAuthenticated]
        if self.action == "list":
            permission_classes.append(CanViewEvent)
        if self.action in ("retrieve", "update", "partial_update", "destroy"):
            permission_classes.append(ParticipantIsCurrentUser | CanChangeEvent)
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        event = self.get_event()
        if event and event.user_has_perm(self.request.user, "view"):
            if self.action == "list":
                return EventParticipantsAdminSerializer
            else:
                return EventParticipantsAdminFullSerializer
        return EventParticipantsSerializer

    def get_object(self):
        if self.kwargs.get("pk") == "me":
            obj = get_object_or_404(
                self.filter_queryset(self.get_queryset()), user=self.request.user
            )
            self.check_object_permissions(self.request, obj)
            return obj
        else:
            return super().get_object()

    def retrieve(self, request, pk=None, event_code=None):
        obj = self.get_object()
        serializer = self.get_serializer(obj)
        return Response(serializer.data)

    def create(self, request, event_code=None):
        event = get_object_or_404(Event, code=event_code)
        is_admin = event.user_has_perm(self.request.user, "change")
        if not is_admin and not event.is_registration_open():
            return Response(
                {"detail": "Pre-registration is closed."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = self._make_nested_request_data()
        serializer = self.get_serializer(data=data)
        if not serializer.is_valid():
            return Response(
                {"fields": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )
        if is_admin and "email" in data:
            user, created = User.objects.get_or_create(email=data["email"])
            if created:
                user.set_unusable_password()
                user.save()
        else:
            user = request.user
        obj = serializer.save(user=user)
        new_participant.send(Participant, instance=obj, request=request)
        participant_changed.send(Participant, instance=obj, request=request)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, pk=None, event_code=None, partial=False):
        obj = self.get_object()
        previous = Participant.objects.get(pk=obj.pk)
        data = self._make_nested_request_data()
        serializer = self.get_serializer(obj, data=data, partial=partial)
        if not serializer.is_valid():
            return Response(
                {"fields": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )
        obj = serializer.save()
        participant_changed.send(
            Participant, instance=obj, previous=previous, request=request
        )
        return Response(serializer.data)

    def perform_destroy(self, instance):
        super().perform_destroy(instance)
        log.info(
            "Participant {} was deleted by {}".format(
                instance.user.email, self.request.user.email
            )
        )

    @action(detail=True, methods=["POST", "DELETE"])
    def sell(self, request, pk=None, event_code=None):
        if request.method == "POST":
            return self._sell_ticket(request)
        elif request.method == "DELETE":
            return self._cancel_sale(request)

    def _sell_ticket(self, request):
        participant = self.get_object()
        # Validate participant
        if participant.ticket_status != TicketStatus.BOUGHT.name:
            return Response(
                {
                    "non_field_errors": [
                        "Il faut avoir acheté son billet avant de pouvoir le vendre"
                    ]
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        selling_to = get_or_create_participant(
            request.data["sell_to"], participant.event
        )
        if selling_to.approved is False:
            return Response(
                {
                    "fields": {
                        "sell_to": "Vous ne pouvez pas vendre votre billet à ce⋅tte participant⋅e."
                    }
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        previous_seller = Participant.objects.get(pk=participant.pk)
        previous_sellee = Participant.objects.get(pk=selling_to.pk)
        # Sell the ticket
        if request.data.get("autoapprove", False) and participant.event.user_has_perm(
            self.request.user, "change"
        ):
            # Admin: don't go through the acceptance workflow.
            participant.sell_to(selling_to)
            log.info(
                "Participant {} sold their ticket to {} (admin action)".format(
                    participant.user.email, selling_to.user.email
                )
            )
        else:
            participant.selling_ticket_to = selling_to
            participant.ticket_status = TicketStatus.SELLING.name
            participant.save()
            # Call save() to update the date_modified field (the ticket_offered_by list has now changed).
            selling_to.save()
            log.info(
                "Participant {} wants to sell their ticket to {}".format(
                    participant.user.email, selling_to.user.email
                )
            )
        History.objects.create(
            event=participant.event,
            participant=participant,
            action="sell_to",
            data=selling_to.user.email,
        )
        participant_changed.send(
            Participant, instance=participant, previous=previous_seller, request=request
        )
        participant_changed.send(
            Participant, instance=selling_to, previous=previous_sellee, request=request
        )
        new_participant_data = self.get_serializer(participant).data
        headers = self.get_success_headers(new_participant_data)
        return Response(
            new_participant_data, status=status.HTTP_200_OK, headers=headers
        )

    def _cancel_sale(self, request):
        participant = self.get_object()
        # Validate participant
        if participant.ticket_status == TicketStatus.RESOLD.name:
            return Response(
                {"non_fields_errors": ["Trop tard, la vente a été acceptée."]},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if participant.ticket_status != TicketStatus.SELLING.name:
            return Response(
                {
                    "non_fields_errors": [
                        "Vous n'êtes pas en train de vendre votre ticket."
                    ]
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        # Cancel selling the ticket
        previous_seller = Participant.objects.get(pk=participant.pk)
        formerly_selling_to = participant.selling_ticket_to
        participant.selling_ticket_to = None
        participant.ticket_status = TicketStatus.BOUGHT.name
        participant.save()
        log.info(
            "Participant {} cancelled the reselling of their ticket to {}".format(
                participant.user.email, formerly_selling_to
            )
        )
        History.objects.create(
            event=participant.event,
            participant=participant,
            action="cancel_selling",
            data=formerly_selling_to.user.email,
        )
        participant_changed.send(
            Participant, instance=participant, previous=previous_seller, request=request
        )
        new_participant_data = self.get_serializer(participant).data
        headers = self.get_success_headers(new_participant_data)
        return Response(
            new_participant_data, status=status.HTTP_200_OK, headers=headers
        )

    @action(detail=True, methods=["POST"])
    def buy(self, request, pk=None, event_code=None):
        participant = self.get_object()
        # Validate participant
        if participant.ticket_status == TicketStatus.BOUGHT.name:
            return Response(
                {"non_field_errors": ["Vous avez déjà acheté votre billet."]},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if not request.data.get("sold_by"):
            return Response(
                {"fields": {"sold_by": ["Valeur invalide."]}},
                status=status.HTTP_400_BAD_REQUEST,
            )
        sold_by = get_object_or_404(
            Participant, pk=request.data["sold_by"], selling_ticket_to=participant
        )
        # Buy the ticket
        sold_by.sell_to(participant)
        log.info(
            "Participant {} bought the ticket from {}".format(
                participant.user.email, sold_by.user.email
            )
        )
        History.objects.create(
            event=participant.event,
            participant=participant,
            action="bought_from",
            data=sold_by.user.email,
        )
        new_participant_data = self.get_serializer(participant).data
        headers = self.get_success_headers(new_participant_data)
        return Response(
            new_participant_data, status=status.HTTP_200_OK, headers=headers
        )

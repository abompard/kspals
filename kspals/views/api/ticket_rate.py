from kspals.models import TicketRate
from kspals.serializers.ticket_rate import (
    EventTicketRateAdminSerializer,
    EventTicketRateSerializer,
)

from .utils import AdminObjectViewSet, EventChildMixin


class TicketRateViewSet(EventChildMixin, AdminObjectViewSet):

    queryset = TicketRate.objects.order_by("name")
    serializer_class = EventTicketRateSerializer

    def get_serializer_class(self):
        event = self.get_event()
        if event and event.user_has_perm(self.request.user, "view"):
            return EventTicketRateAdminSerializer
        return EventTicketRateSerializer

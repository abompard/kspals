from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.response import Response
from django_filters import rest_framework as filters

from kspals.serializers.notification import EventNotificationsSerializer

from kspals.models import Notification, Participant
from kspals.models.constants import NotificationName, TicketStatus
from kspals.signals import participant_changed
from .utils import AdminObjectViewSet, EventChildMixin


class NotificationFilter(filters.FilterSet):
    status__in = filters.BaseInFilter(field_name="status")

    class Meta:
        model = Notification
        fields = ["name", "status", "status__in"]


class NotificationViewSet(EventChildMixin, AdminObjectViewSet):

    queryset = Notification.objects.order_by("date_created")
    serializer_class = EventNotificationsSerializer
    filterset_class = NotificationFilter
    event_lookup_kwarg = "participant__event__code"

    def create(self, request, event_code=None):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                {"fields": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )
        # Special case: for ticketing_ready notifications, we send it to the PALs too.
        if serializer.validated_data["name"] == NotificationName.TICKETING_READY.name:
            serializers = []
            participant = Participant.objects.get(
                pk=serializer.validated_data["participant_id"]
            )
            if participant.ticket_status == TicketStatus.NOT_READY.name:
                serializers.append(serializer)
            for pal in list(
                participant.pals.filter(ticket_status=TicketStatus.NOT_READY.name)
            ):
                pal_data = {"name": request.data["name"], "participant_id": pal.pk}
                pal_serializer = self.get_serializer(data=pal_data)
                if not pal_serializer.is_valid():
                    return Response(
                        {"fields": pal_serializer.errors},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                serializers.append(pal_serializer)
            if not serializers:
                return Response(
                    {
                        "detail": _(
                            "Tous les membres du groupe de PAL connaissent déjà l'adresse de la billetterie."
                        )
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
            for s in serializers:
                notif = s.save()
                previous = Participant.objects.get(pk=notif.participant.pk)
                notif.participant.ticket_status = TicketStatus.URL_SENT.name
                notif.participant.save()
                participant_changed.send(
                    Participant, instance=notif.participant.pk, previous=previous
                )
            return Response(
                [s.data for s in serializers], status=status.HTTP_201_CREATED
            )
        else:
            serializer.save()
            headers = self.get_success_headers(serializer.data)
            return Response(
                serializer.data, status=status.HTTP_201_CREATED, headers=headers
            )

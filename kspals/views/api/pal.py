from rest_framework import viewsets

from kspals.models import PAL
from kspals.serializers.pal import ParticipantPalsSerializer
from kspals.signals import pal_removed
from .utils import ParticipantChildMixin


class PalViewSet(ParticipantChildMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows pals to be viewed or edited.
    """

    queryset = PAL.objects.all()
    serializer_class = ParticipantPalsSerializer

    def perform_destroy(self, instance):
        participant = instance.participant
        pal = instance.pal
        instance.delete()
        pal_removed.send(
            PAL,
            request=self.request,
            instance=instance,
            participant=participant,
            previous=pal,
        )

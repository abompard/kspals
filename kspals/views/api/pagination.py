from rest_framework import pagination, response


class DetailedPageNumberPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        return response.Response(
            {
                "results": data,
                "page": {
                    "next": self.get_next_link(),
                    "previous": self.get_previous_link(),
                    "count": self.page.paginator.count,
                    "page_size": self.page.paginator.per_page,
                    "number": self.page.number,
                },
            }
        )

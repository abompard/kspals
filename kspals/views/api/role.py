from kspals.models import Role
from kspals.serializers.role import EventRoleSerializer
from .utils import AdminObjectViewSet, EventChildMixin


class RoleViewSet(EventChildMixin, AdminObjectViewSet):

    queryset = Role.objects.order_by("code")
    serializer_class = EventRoleSerializer

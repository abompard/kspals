from django.shortcuts import get_object_or_404
from rest_framework import viewsets, permissions

from kspals.permissions import (
    CanViewEvent,
    CanChangeEvent,
    ParentParticipantIsCurrentUser,
)
from kspals.models.event import Event
from kspals.models.participant import Participant


class AdminObjectViewSet(viewsets.ModelViewSet):
    """Those objects are read-only for users who can only view, read-write for user who can change."""

    def get_permissions(self):
        if self.action in ("create", "update", "partial_update", "destroy"):
            permission_classes = [CanChangeEvent]
        else:
            permission_classes = [CanViewEvent]
        return [permission() for permission in permission_classes]


class EventChildMixin:
    event_lookup_kwarg = "event__code"

    def filter_queryset(self, queryset):
        event_code = self.kwargs.get("event_code")
        if event_code:
            queryset = queryset.filter(**{self.event_lookup_kwarg: event_code})
        return super().filter_queryset(queryset)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        event_code = self.kwargs["event_code"]
        event = get_object_or_404(Event, code=event_code)
        context["event"] = event
        return context

    def get_event(self):
        event_code = self.kwargs.get("event_code")
        if not event_code:
            return None
        try:
            return Event.objects.get(code=self.kwargs["event_code"])
        except Event.DoesNotExist:
            return None


class ParticipantChildMixin(EventChildMixin):
    event_lookup_kwarg = "participant__event__code"

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        participant_pk = self.kwargs.get("participant_pk")
        return queryset.filter(participant_id=participant_pk)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        participant_pk = self.kwargs["participant_pk"]
        context["participant"] = get_object_or_404(Participant, pk=participant_pk)
        return context

    def get_permissions(self):
        permission_classes = [permissions.IsAuthenticated]
        if self.action in ("create", "update", "partial_update", "destroy"):
            admin_perm = CanChangeEvent
        else:
            admin_perm = CanViewEvent
        permission_classes.append(ParentParticipantIsCurrentUser | admin_perm)
        return [permission() for permission in permission_classes]

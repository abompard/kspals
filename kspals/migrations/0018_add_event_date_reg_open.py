from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [("kspals", "0017_notification_constants")]

    operations = [
        migrations.AddField(
            model_name="event",
            name="date_reg_open",
            field=models.DateTimeField(
                default=django.utils.timezone.now,
                verbose_name="date de début des inscriptions",
            ),
            preserve_default=False,
        )
    ]

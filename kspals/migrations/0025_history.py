from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


def move_history(apps, schema_editor):
    History = apps.get_model("kspals", "History")
    PALHistory = apps.get_model("kspals", "PALHistory")
    for h in PALHistory.objects.all():
        if h.action == "update" and h.previous is not None:
            data = "from {} to {}".format(h.previous.user.email, h.pal.user.email)
        else:
            data = h.pal.user.email
        History.objects.create(
            participant=h.participant, action="pal_{}".format(h.action), data=data
        )


class Migration(migrations.Migration):

    dependencies = [("kspals", "0024_drinks")]

    operations = [
        migrations.CreateModel(
            name="History",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("action", models.CharField(max_length=63)),
                ("data", models.CharField(blank=True, max_length=255, null=True)),
                (
                    "date",
                    models.DateTimeField(
                        default=django.utils.timezone.now, verbose_name="happened at"
                    ),
                ),
                (
                    "participant",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="history_set",
                        to="kspals.Participant",
                    ),
                ),
            ],
            options={"verbose_name_plural": "History", "ordering": ["date"]},
        ),
        migrations.RunPython(move_history),
        migrations.DeleteModel(name="PALHistory"),
    ]

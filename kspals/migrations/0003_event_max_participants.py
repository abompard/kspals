# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-14 09:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kspals', '0002_profile_notifications_ticket_urls'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='max_participants',
            field=models.IntegerField(default=100, verbose_name='nombre maximum de participants'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='event',
            name='date_open',
            field=models.DateTimeField(verbose_name="date d'ouverture"),
        ),
        migrations.AlterField(
            model_name='event',
            name='date_reg_close',
            field=models.DateTimeField(verbose_name='date de fin des inscriptions'),
        ),
    ]

# Generated by Django 2.1.7 on 2019-04-16 14:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("kspals", "0022_add_indices")]

    operations = [
        migrations.RemoveField(model_name="event", name="waiting_list_limit"),
        migrations.AddField(
            model_name="event",
            name="waiting_list_size",
            field=models.IntegerField(
                default=0, verbose_name="taille de la file d'attente"
            ),
        ),
    ]

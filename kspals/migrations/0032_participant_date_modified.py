# Generated by Django 2.1.7 on 2019-07-31 09:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kspals', '0031_history_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='participant',
            name='date_modified',
            field=models.DateTimeField(auto_now=True, verbose_name='modified at'),
        ),
        migrations.AlterField(
            model_name='participant',
            name='date_created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='created at'),
        ),
    ]

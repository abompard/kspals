from django.db import migrations, models


def move_drinks(apps, schema_editor):
    Participant = apps.get_model("kspals", "Participant")
    for participant in Participant.objects.all():
        for note in participant.notes.all():
            if note.title == "Boissons":
                participant.drinks = int(note.content)
                participant.save()
                note.delete()


class Migration(migrations.Migration):

    dependencies = [("kspals", "0023_rename_event_waiting_list")]

    operations = [
        migrations.AddField(
            model_name="participant",
            name="drinks",
            field=models.IntegerField(default=0, verbose_name="boissons"),
        ),
        migrations.RunPython(move_drinks),
    ]

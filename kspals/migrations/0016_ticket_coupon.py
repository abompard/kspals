# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-03-15 12:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kspals', '0015_notification'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticketrate',
            name='coupon',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='ticketrate',
            name='url',
            field=models.CharField(blank=True, max_length=511, null=True),
        ),
    ]

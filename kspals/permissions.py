from rest_framework import permissions

from kspals.models import PAL, Event, Participant, ParticipantProfile


class ParticipantIsCurrentUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return self.do_check(request.user, obj)

    @staticmethod
    def do_check(user, obj):
        if user.is_superuser or user.is_staff:
            return True
        if isinstance(obj, Participant):
            return obj.user == user
        elif isinstance(obj, PAL) or isinstance(obj, ParticipantProfile):
            return obj.participant.user == user
        else:
            raise RuntimeError("Unsupported object: {}".format(repr(obj)))


class ParentParticipantIsCurrentUser(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser or request.user.is_staff:
            return True
        participant_pk = request.resolver_match.kwargs.get("participant_pk")
        if participant_pk is None:
            raise RuntimeError("Unsupported view: {}".format(repr(view)))
        participant = Participant.objects.get(pk=participant_pk)
        if participant is None:
            return True  # This will end up in 404
        return participant.user == request.user


class UserHasEventPerm(permissions.BasePermission):

    perm = None

    def has_permission(self, request, view):
        url_kwargs = request.resolver_match.kwargs
        if "code" in url_kwargs:
            event_code = url_kwargs.get("code")
        else:
            event_code = url_kwargs.get("event_code")
        if event_code is None:
            raise RuntimeError("Unsupported view: {}".format(repr(view)))
        event = Event.objects.get(code=event_code)
        if event is None:
            return True  # This will end up in 404
        return self.do_check(event, request.user)

    def has_object_permission(self, request, view, obj):
        if isinstance(obj, Event):
            event = obj
        elif hasattr(obj, "event"):
            event = obj.event
        elif hasattr(obj, "participant"):
            event = obj.participant.event
        else:
            raise RuntimeError("Unsupported object: {}".format(repr(obj)))
        return self.do_check(event, request.user)

    @classmethod
    def do_check(cls, event, user):
        if user.is_superuser or user.is_staff:
            return True
        return event.user_has_perm(user, cls.perm)


class CanViewEvent(UserHasEventPerm):
    perm = "view"


class CanChangeEvent(UserHasEventPerm):
    perm = "change"

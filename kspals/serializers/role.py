from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer

from kspals.models import Role


class EventRoleSerializer(NestedHyperlinkedModelSerializer):

    parent_lookup_kwargs = {"event_code": "event__code"}

    class Meta:
        model = Role
        fields = ("url", "id", "code", "name", "default")
        read_only_fields = ("id",)

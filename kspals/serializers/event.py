from django.db.models import Q
from rest_framework import serializers

from kspals.models import Event
from kspals.models.constants import TicketStatus

from .role import EventRoleSerializer
from .ticket_rate import EventTicketRateAdminSerializer, EventTicketRateSerializer
from .user import OtherUserSerializer


class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = (
            "url",
            "code",
            "name",
            "location",
            "date_open",
            "date_reg_open",
            "date_reg_close",
            "is_active",
            "is_registration_open",
            "is_on_waiting_list",
            "is_full",
            "ticket_rates",
            "participants_url",
        )
        read_only_fields = (
            "is_active",
            "is_registration_open",
            "is_on_waiting_list",
            "is_full",
            "participants_url",
        )
        extra_kwargs = {"url": {"lookup_field": "code"}}

    participants_url = serializers.HyperlinkedIdentityField(
        view_name="participant-list", lookup_field="code", lookup_url_kwarg="event_code"
    )
    ticket_rates = serializers.SerializerMethodField()

    def get_ticket_rates(self, obj):
        rates = obj.ticketrate_set.filter(Q(default=True) | Q(low_income=True))
        return EventTicketRateSerializer(
            instance=rates, many=True, context=self.context
        ).data


class EventAdminSerializer(EventSerializer):
    class Meta(EventSerializer.Meta):
        fields = EventSerializer.Meta.fields + (
            "full_limit",
            "waiting_list_size",
            "ticketing_sent_count",
            "tickets_bought_count",
            "roles",
            "ticket_rates",
            "ticket_statuses",
            "users_can_view",
            "users_can_change",
            "notifications_url",
        )
        read_only_fields = EventSerializer.Meta.read_only_fields + (
            "ticketing_sent_count",
            "tickets_bought_count",
            "ticket_statuses",
            "users_can_view",
            "users_can_change",
            "notifications_url",
        )

    notifications_url = serializers.HyperlinkedIdentityField(
        view_name="notification-list",
        lookup_field="code",
        lookup_url_kwarg="event_code",
    )
    roles = EventRoleSerializer(source="role_set", many=True, read_only=True)
    ticket_rates = EventTicketRateAdminSerializer(
        source="ticketrate_set", many=True, read_only=True
    )
    ticket_statuses = serializers.SerializerMethodField()
    users_can_view = serializers.SerializerMethodField()
    users_can_change = serializers.SerializerMethodField()

    def get_ticket_statuses(self, obj):
        return [{"name": s.value, "code": s.name} for s in TicketStatus]

    def get_users_can_view(self, obj):
        return [
            OtherUserSerializer(u).data for u in obj.get_users_with_perm("view").all()
        ]

    def get_users_can_change(self, obj):
        return [
            OtherUserSerializer(u).data for u in obj.get_users_with_perm("change").all()
        ]

from rest_framework import serializers
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer

from kspals.models.history import History


class EventHistorySerializer(NestedHyperlinkedModelSerializer):

    parent_lookup_kwargs = {"event_code": "event__code"}

    class Meta:
        model = History
        fields = ("url", "id", "participant", "action", "data", "date")
        read_only_fields = fields

    participant = serializers.SlugRelatedField(read_only=True, slug_field="id")


class ParticipantHistorySerializer(EventHistorySerializer):

    parent_lookup_kwargs = {
        "event_code": "participant__event__code",
        "participant_pk": "participant__pk",
    }

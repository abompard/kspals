from rest_framework import serializers
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer

from kspals.models import Notification, Participant
from kspals.models.constants import NotificationStatus, NotificationName
from .participant import EventParticipantsAdminSerializer


class EventNotificationsSerializer(NestedHyperlinkedModelSerializer):

    parent_lookup_kwargs = {"event_code": "participant__event__code"}

    class Meta:
        model = Notification
        fields = (
            "url",
            "id",
            "name",
            "name_human",
            "status",
            "participant",
            "participant_id",
            "date_created",
            "date_sent",
            "recipient",
            "subject",
            "body_text",
            "body_html",
        )
        read_only_fields = (
            "url",
            "id",
            "name_human",
            "participant",
            "date_created",
            "date_sent",
            "recipient",
            "subject",
            "body_text",
            "body_html",
        )

    participant = EventParticipantsAdminSerializer(required=False)
    participant_id = serializers.IntegerField(write_only=True, required=False)

    def validate_name(self, value):
        if value not in [m.name for m in NotificationName]:
            raise serializers.ValidationError("Invalid name value.")
        return NotificationName[value].name

    def validate_status(self, value):
        if value not in [m.name for m in NotificationStatus]:
            raise serializers.ValidationError("Invalid status value.")
        return NotificationStatus[value].name

    def validate_participant_id(self, value):
        try:
            participant = Participant.objects.get(pk=value)
        except Participant.DoesNotExist:
            raise serializers.ValidationError("No such participant.")
        if participant.event != self.context["event"]:
            raise serializers.ValidationError(
                "Participant {} is not from event {}.".format(
                    participant.pk, self.context["event"].code
                )
            )
        # Check they self_registered
        if not participant.self_registered:
            raise serializers.ValidationError(
                "Participant {} did not self-register.".format(participant.pk)
            )
        if not participant.has_profile():
            # Created in the admin interface
            raise serializers.ValidationError(
                "Participant {} has no profile.".format(participant.pk)
            )
        return participant.id

    def to_representation(self, instance):
        """Convert the status enum to member names"""
        result = super().to_representation(instance)
        result["name"] = NotificationName[result["name"]].name
        result["status"] = NotificationStatus[result["status"]].name
        return result

    def create(self, validated_data):
        """
        Populate the values that are computed instead of trusting the user input.
        """
        name = validated_data["name"]
        # Add the participant
        participant = validated_data["participant"] = Participant.objects.get(
            pk=validated_data.pop("participant_id")
        )
        # Check validity for ticketing_ready notifications
        if (
            name == NotificationName.TICKETING_READY.name
            and not participant.is_in_valid_pal_group()
        ):
            raise serializers.ValidationError(
                {
                    participant.pk: [
                        "Participant {} is not in a valid PAL group.".format(
                            participant.pk
                        )
                    ]
                }
            )
        # Check that we haven't sent that notification already
        if (
            participant.notification_set.filter(name=name)
            .exclude(status=NotificationStatus.TRASH.name)
            .exists()
        ):
            raise serializers.ValidationError(
                {
                    participant.pk: [
                        "The notification {!r} for participant {} has already been sent.".format(
                            NotificationName[name].value, participant.pk
                        )
                    ]
                }
            )
        # Use sensible values instead of trusting user input.
        other_defaults = Notification.get_defaults(
            name, participant, self.context["request"]
        )
        validated_data.update(other_defaults)
        return super().create(validated_data)

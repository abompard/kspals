from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer

from kspals.models import PAL
from kspals.config import MAX_PAL_GROUP_SIZE
from kspals.signals import pal_added, pal_updated
from kspals.utils import EMAIL_RE, get_or_create_participant


class PalIdFromEmail(serializers.SlugRelatedField):
    """
    A read-write field that represents the target of the relationship
    by a unique 'slug' attribute.
    """

    default_error_messages = {
        "invalid": _("Invalid value."),
        "invalid_email": _("Adresse email invalide"),
    }

    def to_internal_value(self, data):
        try:
            if not EMAIL_RE.match(data):
                self.fail("invalid_email")
            return get_or_create_participant(data, self.parent.context["event"])
        except (TypeError, ValueError):
            self.fail("invalid")

    def get_queryset(self):
        pass  # Allow the field to be read-write


class ParticipantPalsSerializer(NestedHyperlinkedModelSerializer):

    parent_lookup_kwargs = {
        "event_code": "participant__event__code",
        "participant_pk": "participant__pk",
    }

    class Meta:
        model = PAL
        fields = (
            "url",
            "id",
            "email",
            "name",
            "is_mutual",
            "has_ticket",
            "approved",
            "pal",
        )

    email = serializers.SlugRelatedField(
        source="pal.user", read_only=True, slug_field="email"
    )
    has_ticket = serializers.NullBooleanField(read_only=True)
    approved = serializers.NullBooleanField(read_only=True)
    pal = PalIdFromEmail(read_only=False, slug_field="id")

    def validate_pal(self, value):
        if not value:
            raise serializers.ValidationError(_("Invalid data"))
        if value.user.pk == self.context["request"].user.pk:
            raise serializers.ValidationError(
                _("Vous ne pouvez pas être votre propre PAL")
            )
        if PAL.objects.filter(
            participant=self.context["participant"], pal=value
        ).exists():
            raise serializers.ValidationError(_("Vous avez déjà désigné ce PAL."))
        return value

    def create(self, validated_data):
        if self.context["participant"].pals.count() >= MAX_PAL_GROUP_SIZE - 1:
            raise serializers.ValidationError(
                _("Vous avez déjà atteint le nombre maximum de PALs")
            )
        instance, created = PAL.objects.get_or_create(
            participant=self.context["participant"], pal=validated_data["pal"]
        )
        pal_added.send(PAL, instance=instance, request=self.context["request"])
        return instance

    def update(self, instance, validated_data):
        old_pal = instance.pal
        instance.pal = validated_data["pal"]
        instance.save()
        pal_updated.send(
            PAL, instance=instance, previous=old_pal, request=self.context["request"]
        )
        return instance

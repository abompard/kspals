from rest_framework import serializers

from kspals.models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for the connected user"""

    class Meta:
        model = User
        fields = ('id', 'email', 'full_name', 'date_joined', 'is_superuser', 'is_staff', 'token')

    token = serializers.CharField(
        source="auth_token"
    )


class OtherUserSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a user that is not the connected user (excludes private fields)"""

    class Meta:
        model = User
        fields = ('id', 'email', 'full_name')

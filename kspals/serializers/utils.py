from rest_framework.relations import SlugRelatedField


class NestedSlugRelatedField(SlugRelatedField):
    instance_lookup = {"parent": "pk"}

    def __init__(self, *args, **kwargs):
        self.instance_lookup = kwargs.pop("instance_lookup", self.instance_lookup)
        super(NestedSlugRelatedField, self).__init__(*args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        lookup = {}
        for key, value in self.instance_lookup.items():
            lookup[key] = getattr(self.parent.instance, value)
        qs = qs.filter(**lookup)
        return qs

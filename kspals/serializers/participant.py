import os

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.utils import model_meta
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer

from kspals.models import (
    Participant,
    ParticipantNote,
    ParticipantProfile,
    Role,
    TicketRate,
)
from kspals.models.constants import TicketStatus

from .pal import ParticipantPalsSerializer
from .utils import NestedSlugRelatedField


class ParticipantProfileSerializer(serializers.ModelSerializer):

    parent_lookup_kwargs = {
        "event_code": "event__code",
        "participant_pk": "participant__pk",
    }

    class Meta:
        model = ParticipantProfile
        fields = (
            "first_name",
            "last_name",
            "full_name",
            "introduction",
            "learned_about",
            "picture",
            "picture_height",
            "picture_width",
            "picture_thumb",
            "date_registered",
            "low_income",
        )
        read_only_fields = (
            "full_name",
            "picture_height",
            "picture_width",
            "date_registered",
        )

    low_income = serializers.BooleanField()

    def validate_picture(self, value):
        if value.size > 1024 * 1024 * 1024:  # 10Mo
            raise serializers.ValidationError(_("Le fichier ne doit pas dépasser 10Mo"))
        basename, ext = os.path.splitext(value.name)
        if ext.lower() not in (".png", ".jpg", ".jpeg"):
            raise serializers.ValidationError(
                _("Les formats autorisés sont PNG et JPEG")
            )
        return value

    def _get_picture_thumb_url(self, instance):
        url = "{}{}".format(settings.MEDIA_URL, instance.picture_thumb)
        request = self.context.get("request", None)
        if request is not None:
            return request.build_absolute_uri(url)
        return url

    def to_representation(self, instance):
        """Convert picture_thumb to a full URL"""
        result = super().to_representation(instance)
        result["picture_thumb"] = self._get_picture_thumb_url(instance)
        return result


class OptionalParticipantProfileSerializer(ParticipantProfileSerializer):
    class Meta(ParticipantProfileSerializer.Meta):
        extra_kwargs = dict(
            (name, {"required": False})
            for name in ParticipantProfileSerializer.Meta.fields
        )

    low_income = serializers.BooleanField(required=False)


class MinimalParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ("id", "email")
        read_only_fields = ("id",)

    email = serializers.SlugRelatedField(
        source="user", read_only=True, slug_field="email"
    )


class EventParticipantsSerializer(NestedHyperlinkedModelSerializer):

    parent_lookup_kwargs = {"event_code": "event__code"}

    class Meta:
        model = Participant
        fields = (
            "url",
            "id",
            "email",
            "profile",
            "self_registered",
            "approved",
            "role",
            "ticket_rate",
            "ticket_coupon",
            "has_ticket",
            "ticket_name",
            "ticket_url",
            "approved_pal_group",
            "mutual_pal_group",
            "valid_pal_group",
            "pals",
            "selling_ticket_to",
            "ticket_offer_by",
        )
        read_only_fields = (
            "approved",
            "approved_pal_group",
            "mutual_pal_group",
            "valid_pal_group",
            "ticket_rate",
            "has_ticket",
            "ticket_name",
            "ticket_url",
            "ticket_coupon",
            "ticket_offer_by",
        )

    email = serializers.SlugRelatedField(
        source="user", read_only=True, slug_field="email"
    )
    approved_pal_group = serializers.BooleanField(
        source="is_in_approved_pal_group", read_only=True
    )
    mutual_pal_group = serializers.BooleanField(
        source="is_in_mutual_pal_group", read_only=True
    )
    valid_pal_group = serializers.BooleanField(
        source="is_in_valid_pal_group", read_only=True
    )
    profile = ParticipantProfileSerializer()
    pals = ParticipantPalsSerializer(source="pal_set", many=True, read_only=True)
    role = serializers.SlugRelatedField(read_only=True, slug_field="name")
    ticket_rate = serializers.SlugRelatedField(read_only=True, slug_field="name")
    selling_ticket_to = MinimalParticipantSerializer(allow_null=True, required=False)
    ticket_offer_by = MinimalParticipantSerializer(many=True, required=False)

    def create(self, validated_data):
        event = self.context["event"]
        user = validated_data["user"]
        # Participant may already exist if previously added as a PAL.
        instance, created = Participant.objects.get_or_create(event=event, user=user)
        instance.self_registered = True
        instance.save()
        if not instance.has_profile() and len(validated_data["profile"]) > 0:
            ParticipantProfile.objects.create(
                participant=instance, **validated_data["profile"]
            )
        return instance

    def update(self, instance, validated_data):
        try:
            profile = instance.profile
        except ParticipantProfile.DoesNotExist:
            profile = ParticipantProfile.objects.create(
                participant=instance, **validated_data["profile"]
            )
        else:
            for prop, value in validated_data["profile"].items():
                setattr(profile, prop, value)
            profile.save()
        return instance


class ParticipantNoteSerializer(serializers.ModelSerializer):
    """Serializer for the participant notes"""

    parent_lookup_kwargs = {
        "event_code": "event__code",
        "participant_pk": "participant__pk",
    }

    class Meta:
        model = ParticipantNote
        fields = ("id", "title", "content")


class EventParticipantsAdminSerializer(EventParticipantsSerializer):
    class Meta(EventParticipantsSerializer.Meta):
        model = Participant
        fields = EventParticipantsSerializer.Meta.fields + (
            "ticket_status",
            "has_ticket",
            "notes",
            "drinks",
            "pal_group_registration_date",
            "date_modified",
        )
        read_only_fields = (
            "approved_pal_group",
            "mutual_pal_group",
            "valid_pal_group",
            "ticket_url",
            "ticket_coupon",
            "pal_group_registration_date",
            "date_modified",
        )

    role = NestedSlugRelatedField(
        read_only=False,
        slug_field="code",
        required=False,
        queryset=Role.objects.all(),
        instance_lookup={"event": "event"},
    )
    ticket_rate = NestedSlugRelatedField(
        read_only=False,
        slug_field="name",
        required=False,
        queryset=TicketRate.objects.all(),
        instance_lookup={"event": "event"},
    )
    notes = ParticipantNoteSerializer(many=True, read_only=True)
    selling_ticket_to = serializers.SlugRelatedField(
        read_only=True, slug_field="pk", allow_null=True
    )
    ticket_offer_by = serializers.SlugRelatedField(
        read_only=True, slug_field="pk", many=True
    )

    def validate_ticket_status(self, value):
        try:
            return TicketStatus[value].name
        except KeyError:
            statuses = [s.name for s in TicketStatus]
            raise serializers.ValidationError(
                _("Les valeurs autorisées pour ticket_status sont %s")
                % ", ".join(statuses)
            )

    def update(self, instance, validated_data):
        super().update(instance, validated_data)
        # Update the profile
        validated_data.pop("profile")
        # From ModelSerializer.update()
        info = model_meta.get_field_info(instance)
        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                field = getattr(instance, attr)
                field.set(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance

    def to_representation(self, instance):
        """Convert the status enum to member names"""
        result = super().to_representation(instance)
        result["ticket_status"] = TicketStatus[result["ticket_status"]].name
        return result


class ParticipantPalsFullSerializer(ParticipantPalsSerializer):

    pal = EventParticipantsAdminSerializer(read_only=True)


class EventParticipantsAdminFullSerializer(EventParticipantsAdminSerializer):
    pals = ParticipantPalsFullSerializer(source="pal_set", many=True, read_only=True)
    profile = OptionalParticipantProfileSerializer()

from rest_framework import serializers
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer

from kspals.models import TicketRate


class EventTicketRateSerializer(NestedHyperlinkedModelSerializer):

    parent_lookup_kwargs = {"event_code": "event__code"}

    class Meta:
        model = TicketRate
        fields = ("id", "name", "amount", "default", "low_income")
        read_only_fields = fields


class EventTicketRateAdminSerializer(EventTicketRateSerializer):
    class Meta(EventTicketRateSerializer.Meta):
        fields = EventTicketRateSerializer.Meta.fields + ("url", "ticket_url", "coupon")
        read_only_fields = ("id",)
        extra_kwargs = {"coupon": {"allow_null": True}}

    ticket_url = serializers.URLField(source="url", max_length=511, allow_blank=True)

    def build_field(self, field_name, info, model_class, nested_depth):
        """Make the relational url field have precedence over the model's url property."""
        if field_name == "url":
            return self.build_url_field(field_name, model_class)
        return super().build_field(field_name, info, model_class, nested_depth)

from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm as BaseUserCreationForm
from django.utils.translation import gettext, gettext_lazy as _


class UserCreationForm(BaseUserCreationForm):

    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        required=False,
        help_text="Leave empty to set an unusable password.\n%s" % password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        required=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    def save(self, commit=True):
        user = super().save(commit=False)
        if not self.cleaned_data["password1"]:
            user.set_unusable_password()
        if commit:
            user.save()
        return user

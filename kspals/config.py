from django.conf import settings

MIN_PAL_GROUP_SIZE = getattr(settings, "MIN_PAL_GROUP_SIZE", 2)
MAX_PAL_GROUP_SIZE = getattr(settings, "MAX_PAL_GROUP_SIZE", 3)
LOGO_URL = getattr(settings, "LOGO_URL", "http://www.kinkysalon.fr")
MATOMO_URL = getattr(settings, "MATOMO_URL", None)
MATOMO_ID = getattr(settings, "MATOMO_ID", None)
SOCKETIO_QUEUE = getattr(settings, "SOCKETIO_QUEUE", "redis://")
SOCKETIO_ENDPOINT = getattr(settings, "SOCKETIO_ENDPOINT", None)
REACT_APP_DIR = getattr(settings, "REACT_APP_DIR", "js")
DEFAULT_ROLES = getattr(settings, "DEFAULT_ROLES", [])
DEFAULT_TICKET_RATES = getattr(settings, "DEFAULT_TICKET_RATES", [])
TICKETING = getattr(settings, "TICKETING", {})

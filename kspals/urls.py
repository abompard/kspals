from django.conf.urls import url, include

import kspals.views.root
import kspals.views.api
from kspals.views.api import (
    event,
    participant,
    pal,
    notification,
    role,
    ticket_rate,
    history,
)
import kspals.views.export
from rest_framework_nested import routers
from kspals import views

router = routers.DefaultRouter()
router.register("events", views.api.event.EventViewSet)

events_router = routers.NestedSimpleRouter(router, "events", lookup="event")
events_router.register("participants", views.api.participant.ParticipantViewSet)

participants_router = routers.NestedSimpleRouter(
    events_router, "participants", lookup="participant"
)
participants_router.register("pals", views.api.pal.PalViewSet)
participants_router.register("history", views.api.history.ParticipantHistoryViewSet)

events_router.register("notifications", views.api.notification.NotificationViewSet)
events_router.register("roles", views.api.role.RoleViewSet)
events_router.register("ticket-rates", views.api.ticket_rate.TicketRateViewSet)
events_router.register("history", views.api.history.HistoryViewSet)

urlpatterns = [
    url(r"^api/", include(router.urls)),
    url(r"^api/", include(events_router.urls)),
    url(r"^api/", include(participants_router.urls)),
    url(r"^api/", include("drfpasswordless.urls")),
    url(
        r"^api/callback/auth-and-login/$",
        views.root.ObtainAuthTokenFromCallbackTokenAndLogin.as_view(),
        name="auth_callback_login",
    ),
    url(r"^logout$", views.root.logout_view, name="logout"),
    url(
        r"^(?P<event_code>[a-zA-Z0-9_-]+)/participants.csv",
        views.export.participants_csv,
        name="export_csv",
    ),
    url(
        r"^(?P<event_code>[a-zA-Z0-9_-]+)/participants.html",
        views.export.participants_html,
        name="export_html",
    ),
    url(r"^initial-state$", views.root.initial_state, name="initial_state"),
    url(r"^", views.root.index_view, name="root"),
]

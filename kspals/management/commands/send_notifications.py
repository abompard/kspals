from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone

from kspals.models import Notification
from kspals.models.constants import NotificationStatus


class Command(BaseCommand):

    help = "Send pending notifications for Kinky Salon Pals"

    def add_arguments(self, parser):
        parser.add_argument(
            "-n", "--dry-run", action="store_true", help="Only show what would be done"
        )
        parser.add_argument(
            "-d",
            "--delay",
            type=int,
            default=0,
            help="Only send notifications older than this value in minutes",
        )

    def handle(self, *args, **options):
        query = Notification.objects.filter(
            status=NotificationStatus.TO_SEND.name
        ).order_by("date_created")
        if options["delay"]:
            threshold = timezone.now() - timedelta(minutes=options["delay"])
            query = query.filter(date_created__lt=threshold)
        total = query.count()
        for notif in query:
            notif.send_email(dry_run=options["dry_run"])
            if options["verbosity"] > 1:
                self.stdout.write(self.style.SUCCESS("Sent {}".format(notif)))
        if options["verbosity"] > 1:
            self.stdout.write("Sent {} emails.".format(total))
            pending_count = Notification.objects.filter(
                status=NotificationStatus.DRAFT.name
            ).count()
            if pending_count:
                self.stdout.write(
                    "There is {} notification(s) waiting for approval.".format(
                        pending_count
                    )
                )

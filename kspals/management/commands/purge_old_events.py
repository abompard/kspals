import datetime

from django.core.management.base import BaseCommand
from django.utils import timezone

from kspals.models import Event


class Command(BaseCommand):

    help = "Delete participants of old events."

    def add_arguments(self, parser):
        parser.add_argument(
            "-m",
            "--max-age",
            type=int,
            default=1,
            help="Delete participants of events older than MAX_AGE months",
        )
        parser.add_argument(
            "-n", "--dry-run", action="store_true", help="Only show what would be done"
        )

    def handle(self, *args, **options):
        threshold = timezone.now() - datetime.timedelta(days=options["max_age"] * 31)
        for event in Event.objects.filter(date_open__lte=threshold):
            if options["verbosity"] >= 1:
                self.stdout.write(
                    f"Removing {event.participant_set.count()} participants from {event.name}"
                )
            if options["dry_run"]:
                continue
            for participant in event.participant_set.all():
                participant.delete()

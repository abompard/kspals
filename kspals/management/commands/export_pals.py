import csv

from django.core.management.base import BaseCommand, CommandError

from kspals.models import Event
from kspals.utils import get_csv_rows


class Command(BaseCommand):

    help = 'Exports the Kinky Salon Pals to CSV'

    def add_arguments(self, parser):
        parser.add_argument("code", help="Kinky Salon edition code")
        parser.add_argument("-o", "--output", help="Write to this file (CSV)")

    def handle(self, *args, **options):
        try:
            event = Event.objects.get(code=options["code"])
        except Event.DoesNotExist:
            raise CommandError("KS event %s does not exist" % options["code"])
        if options["output"]:
            output = open(options["output"], "w")
        else:
            output = self.stdout
        writer = csv.writer(output)
        rows = get_csv_rows(event)
        writer.writerows(rows)
        self.stdout.write(self.style.SUCCESS("Exported {} participants.".format(len(rows) - 1)))

import datetime

from dateutil.parser import parse
from django.contrib.auth.models import Group, Permission
from django.core.management.base import BaseCommand

from kspals.config import DEFAULT_ROLES, DEFAULT_TICKET_RATES
from kspals.models import Event, Role, TicketRate, User
from kspals.models.event_user_group import EventUserGroup


class Command(BaseCommand):

    help = "Create a Kinky Salon event"

    def add_arguments(self, parser):
        parser.add_argument("-c", "--code", type=str, help="Event code (ex: ksp-6)")
        parser.add_argument(
            "-d", "--date", type=parse, help="Event date", required=True
        )
        parser.add_argument("-o", "--owner", type=str, help="Event owner email address")
        parser.add_argument("-g", "--group", type=str, help="Event admin group name")
        parser.add_argument("name", type=str, help="Event name")

    def handle(self, *args, **options):
        if not options.get("code"):
            options["code"] = options["name"].lower().replace(" ", "-")
        if not options["date"]:
            options["date"] = datetime.datetime.now()

        date_reg_open = options["date"] - datetime.timedelta(days=31)
        date_reg_close = options["date"] - datetime.timedelta(days=1)

        event = Event.objects.create(
            code=options["code"],
            name=options["name"],
            date_open=options["date"],
            date_reg_open=date_reg_open,
            date_reg_close=date_reg_close,
        )

        owner, _created = User.objects.get_or_create(email=options["owner"])
        group, _created = Group.objects.get_or_create(name=options["group"])
        for perm_name in ("view", "change", "delete"):
            permission = Permission.objects.get(codename=f"{perm_name}_event")
            if permission not in group.permissions.all():
                group.permissions.add(permission)
        EventUserGroup.objects.create(event=event, user=owner, group=group)
        for user in User.objects.filter(is_superuser=True):
            EventUserGroup.objects.get_or_create(event=event, user=user, group=group)
        if options["verbosity"] > 0:
            self.stdout.write("Created event {}".format(event.name))
        # Roles
        for role in DEFAULT_ROLES:
            Role.objects.create(event=event, **role)
            if options["verbosity"] > 0:
                self.stdout.write(
                    "Created role {} in event {}".format(role["name"], event.name)
                )
        # Ticket Rates
        for rate in DEFAULT_TICKET_RATES:
            TicketRate.objects.create(event=event, **rate)
            if options["verbosity"] > 0:
                self.stdout.write(
                    "Created ticket rate {} in event {}".format(
                        rate["name"], event.name
                    )
                )

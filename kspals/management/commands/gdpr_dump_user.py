from django.contrib.auth import get_user_model
from django.contrib.admin.utils import NestedObjects
from django.core import serializers
from django.core.management.base import BaseCommand, CommandError
from django.db import router


class Command(BaseCommand):

    help = "Exports a user's data"

    OBFUSCATE = {"kspals.Notification": ["recipient"]}

    def add_arguments(self, parser):
        parser.add_argument("email", help="The user's email address")
        parser.add_argument("-o", "--output", help="Write to this file (JSON)")
        parser.add_argument(
            "--indent",
            default=None,
            dest="indent",
            type=int,
            help="Specifies the indent level to use when pretty-printing output.",
        )

    def handle(self, *args, **options):
        email = options["email"]
        indent = options["indent"]
        output = options["output"]
        # Collect data
        User = get_user_model()
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            raise CommandError("No user with this email address.")
        collector = NestedObjects(using=router.db_for_read(user._meta.model))
        collector.collect([user])
        objects = set()
        for obj_class, instances in collector.data.items():
            if obj_class._meta.label in self.OBFUSCATE:
                for instance in instances:
                    for private_attr_name in self.OBFUSCATE[obj_class._meta.label]:
                        setattr(instance, private_attr_name, None)
            objects.update(instances)
        # Export data
        self.stdout.ending = None
        stream = open(output, "w") if output else None
        progress_output = None
        if output and self.stdout.isatty() and options["verbosity"] > 0:
            progress_output = self.stdout
        serializers.serialize(
            "json",
            objects,
            indent=indent,
            use_natural_foreign_keys=False,
            use_natural_primary_keys=False,
            stream=stream or self.stdout,
            progress_output=progress_output,
            object_count=len(objects),
        )
        if stream:
            stream.close()

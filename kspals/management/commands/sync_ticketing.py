import datetime
import re
from collections import defaultdict
from enum import Enum

from authlib.integrations.requests_client import OAuth2Session
from django.core.management.base import BaseCommand
from django.utils import timezone

from kspals.config import TICKETING
from kspals.models import Event, Participant, TicketRate
from kspals.models.constants import TicketStatus
from kspals.models.history import History
from kspals.signals import participant_changed


class Command(BaseCommand):

    help = "Sync the ticketing data of the Kinky Salon Pals"

    def add_arguments(self, parser):
        parser.add_argument(
            "-e", "--event", type=str, help="Only sync the event with this code"
        )
        parser.add_argument(
            "-n", "--dry-run", action="store_true", help="Only show what would be done"
        )

    def handle(self, *args, **options):
        backend = "helloasso"
        sync_manager = HelloAssoSyncManager(
            backend, options["verbosity"], options["dry_run"]
        )
        if options["event"]:
            events = [Event.objects.get(code=options["event"])]
        else:
            events = list(Event.objects.filter(date_open__gte=timezone.now()))
        for event in events:
            try:
                sync_manager.sync_event(event)
            except HelloAssoClientError as e:
                self.stderr.write(self.style.ERROR(str(e)))
                return


class HelloAssoSyncManager:
    SERVER = "https://api.helloasso.com"
    API = f"{SERVER}/v5"

    def __init__(self, backend_name="helloasso", verbosity=1, dry_run=False):
        self.backend_name = backend_name
        self.verbosity = verbosity
        self.dry_run = dry_run

    def login(self, config):
        token_endpoint = f"{self.SERVER}/oauth2/token"
        client = OAuth2Session(config["client_id"], config["client_secret"])
        client.fetch_token(token_endpoint, grant_type="client_credentials")

    def sync_event(self, event):
        client = HelloAssoClient(TICKETING[self.backend_name])
        # campaign = None
        # for campaign in client.get_list("/campaigns.json", params={"type": "EVENT"}):
        #     if campaign["slug"] == event.code:
        #         break
        # else:
        #     raise HelloAssoClientError(
        #         f"Could not find the corresponding campaign for {event.code}"
        #     )
        if self.verbosity >= 1:
            print(f"Getting actions from HelloAsso for {event.name}...")
        client.login()
        all_items = []
        for item in client.get_items_for_event(event.code):
            item = Item(event, item)
            if not item.processed:
                print("Not processed??", item.data)
                continue
            if item.participant is None:
                print(
                    f"Can't find this participant for {event.name}: "
                    f"{item.email} ({item.payer_email}) {item.type} {item.data!r}"
                )
                continue
            all_items.append(item)
        self.handle_inscriptions(all_items)
        self.handle_drinks(all_items)
        self.handle_donations(all_items)

    def handle_inscriptions(self, items):
        def handle_inscription(item):
            if item.type != ItemType.REGISTRATION:
                return
            if item.participant.ticket_status in (
                TicketStatus.BOUGHT.name,
                TicketStatus.CANCELLED.name,
            ):
                return
            if not item.participant.self_registered:
                print(
                    f"WARNING: {item.participant.user.email} did not register, "
                    f"but they bought a ticket for {item.event.name!r} nonetheless!"
                )
                print(repr(item.data))
                return
            if not item.participant.approved:
                print(
                    f"WARNING: {item.participant.profile.full_name} is not approved, "
                    f"but they bought a ticket for {item.event.name!r} nonetheless!"
                )
                print(repr(item.data))
                return
            if item.participant.ticket_status == TicketStatus.NOT_READY:
                print(
                    f"WARNING: {item.participant.profile.full_name} wasn't sent the "
                    f"ticketing URL, but they bought a ticket for {item.event.name!r} nonetheless!"
                )
                print(repr(item.data))
                return
            print(
                f"Registration of {item.participant.profile.full_name} with rate "
                f"{item.ticket_rate.name}"
            )
            if self.verbosity >= 2:
                print(repr(item.data))
            previous = Participant.objects.get(pk=item.participant.pk)
            item.participant.ticket_rate = item.ticket_rate
            item.participant.ticket_status = TicketStatus.BOUGHT.name
            if not self.dry_run:
                item.participant.save()
                History.objects.create(
                    event=item.participant.event,
                    participant=item.participant,
                    action="ticket_bought",
                    date=item.date,
                )
                participant_changed.send(
                    Participant,
                    instance=item.participant,
                    previous=previous,
                )

        for item in items:
            handle_inscription(item)

    def handle_drinks(self, items):
        drinks = defaultdict(list)
        bought_at = {}
        for item in items:
            if not item.drinks:
                continue
            drinks[item.participant].append(item.drinks)
            bought_at[item.participant] = item.date
        for participant, drinks_sets in drinks.items():
            if not participant.self_registered:
                print(
                    f"WARNING: {participant.user.email} did not register, "
                    "but they bought drinks nonetheless!"
                )
                continue
            if not participant.approved:
                print(
                    f"WARNING: {participant.profile.full_name} is not approved, "
                    "but they bought drinks nonetheless!"
                )
                continue
            if participant.drinks != 0:
                continue  # Don't touch existing values
            total = sum(drinks_sets)
            if self.verbosity >= 1:
                print(f"Drinks for {participant.profile.full_name}: {total}")
            if not self.dry_run:
                participant.drinks = total
                participant.save()
                History.objects.create(
                    event=participant.event,
                    participant=participant,
                    action="drinks_bought",
                    data=total,
                    date=bought_at[participant],
                )

    def handle_donations(self, items):
        for item in items:
            if item.type != ItemType.DONATION:
                continue
            if self.verbosity >= 1:
                print(f"{item.participant.profile.full_name} : don de {item.amount}")


class HelloAssoClientError(Exception):
    def __init__(self, text, code=None):
        self.code = code
        self.text = text

    def __str__(self):
        if self.code is None:
            return self.text
        else:
            return "{} ({})".format(self.text, self.code)


class HelloAssoClient:
    SERVER = "https://api.helloasso.com"
    API = f"{SERVER}/v5"

    def __init__(self, config):
        self.config = config
        self.client = None

    def login(self):
        self.client = OAuth2Session(
            self.config["client_id"], self.config["client_secret"]
        )
        token_endpoint = f"{self.SERVER}/oauth2/token"
        self.client.fetch_token(token_endpoint, grant_type="client_credentials")

    def get(self, url, params=None):
        url = f"{self.API}/{url}"
        response = self.client.get(url, params=params)
        if response.ok:
            return response.json()
        else:
            raise HelloAssoClientError(response.text, response.status_code)

    def get_list(self, url, params=None):
        page = 1
        max_page = 1
        while page <= max_page:
            all_params = params.copy() if params is not None else {}
            all_params["pageIndex"] = page
            response = self.get(url, params=all_params)
            if "pagination" in response:
                page = response["pagination"]["pageIndex"]
                max_page = response["pagination"]["totalPages"]
            yield from response["data"]
            page += 1

    def get_items_for_event(self, event_code):
        form_url = None
        for org in self.get("/users/me/organizations"):
            org_slug = org["organizationSlug"]
            forms = [
                f["formSlug"]
                for f in self.get_list(f"organizations/{org_slug}/forms")
                if f["formType"] == "Event"
            ]
            if event_code in forms:
                form_url = f"organizations/{org_slug}/forms/Event/{event_code}"
        if form_url is None:
            raise HelloAssoClientError(
                "Could not find an event for with code {event_code} in HelloAsso"
            )
        return self.get_list(f"{form_url}/items?withDetails=true")


class ItemType(Enum):
    DONATION = "Donation"
    REGISTRATION = "Registration"


class Item:
    DRINKS_RE = re.compile(r"(\d+) boissons?")
    DATE_RE = re.compile(r"\.\d+([-+]\d\d:\d\d)$")
    EMAIL_NO_PLUS_RE = re.compile(r"^(.+)\+[^@]+(@.+\..+)$")

    def __init__(self, event, data):
        self.event = event
        self.data = data
        self._participant = None
        self._ticket_rate = None

    @property
    def participant(self):
        if self._participant is None:
            self._participant = self._find_participant()
        return self._participant

    def _find_participant(self):
        possible_addresses = [
            self.email,
            self.payer_email,
            self.EMAIL_NO_PLUS_RE.sub(r"\1\2", self.email),
            self.EMAIL_NO_PLUS_RE.sub(r"\1\2", self.payer_email),
        ]
        for addr in possible_addresses:
            try:
                return Participant.objects.get(
                    event=self.event, ticket_name__iexact=addr
                )
            except Participant.DoesNotExist:
                try:
                    return Participant.objects.get(
                        event=self.event, user__email__iexact=addr
                    )
                except Participant.DoesNotExist:
                    pass

    @property
    def ticket_rate(self):
        if self._ticket_rate is None:
            self._ticket_rate = self._find_ticket_rate()
        return self._ticket_rate

    def _find_ticket_rate(self):
        coupon = None
        if "discount" in self.data:
            coupon = self.data["discount"]["code"].split(" : ")[0]
        ticket_rate = (
            TicketRate.objects.filter(
                event=self.event, amount=self.amount, coupon=coupon
            )
            .order_by("-default", "-low_income")
            .first()
        )
        if ticket_rate is None:
            print(
                "Can't find the ticket rate for",
                self.data["name"],
                self.amount,
                self.data,
            )
            ticket_rate = TicketRate.objects.get(event=self.event, default=True)
        return ticket_rate

    @property
    def email(self):
        for field in self.data.get("customFields", []):
            if field["name"] == "Email":
                return field["answer"]
        return self.payer_email

    @property
    def payer_email(self):
        return self.data["payer"]["email"]

    @property
    def type(self):
        if self.data["type"] == "Donation":
            return ItemType.DONATION
        if self.data["type"] == "Registration":
            return ItemType.REGISTRATION
        raise ValueError(f"Unknown item type: {self.data['type']!r}")

    @property
    def date(self):
        return datetime.datetime.fromisoformat(
            self.DATE_RE.sub(r"\1", self.data["order"]["date"])
        )

    @property
    def processed(self):
        return self.data["state"] == "Processed"

    @property
    def amount(self):
        return self.data["amount"] / 100

    @property
    def drinks(self):
        total = 0
        for order in self.data.get("options", []):
            value = self.DRINKS_RE.sub(r"\1", order["name"])
            if value:
                total += int(value)
        return total

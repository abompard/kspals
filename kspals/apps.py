from django.apps import AppConfig


class KsPalsConfig(AppConfig):
    name = "kspals"
    verbose_name = "KS Pals"
    default_auto_field = "django.db.models.AutoField"

    def ready(self):
        import kspals.realtime.signals  # noqa:F401
        import kspals.signals  # noqa:F401
        import kspals.signals.listeners  # noqa:F401

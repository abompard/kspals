from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from kspals.models import Event, TicketRate, Role
from kspals.models.event_user_group import EventUserGroup


class RoleInline(admin.TabularInline):
    model = Role
    min_num = 1
    extra = 0


class TicketRateInline(admin.TabularInline):
    model = TicketRate
    min_num = 1
    extra = 0
    readonly_fields = ("bought_count",)


class EventUserGroupInline(admin.TabularInline):
    model = EventUserGroup
    extra = 0


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):

    list_display = ("name", "location", "date_open")
    prepopulated_fields = {"code": ("name",)}
    date_hierarchy = "date_open"
    list_filter = ("location",)
    ordering = ("-date_open",)
    readonly_fields = (
        "date_created",
        "is_active",
        "is_registration_open",
        "is_on_waiting_list",
        "is_full",
        "mutual_participants_count",
        "ticketing_sent_count",
        "tickets_bought_count",
        "drinks_count",
    )
    search_fields = ("name", "location")
    inlines = [TicketRateInline, RoleInline, EventUserGroupInline]

    def drinks_count(self, instance):
        return sum([p.drinks for p in instance.participant_set.exclude(drinks=0)])

    drinks_count.short_description = _("Nombre de boissons")

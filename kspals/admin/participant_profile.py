from django.contrib import admin

from kspals.models import ParticipantProfile


@admin.register(ParticipantProfile)
class ParticipantProfileAdmin(admin.ModelAdmin):

    list_display = ("participant", "first_name", "last_name", "date_registered")
    list_filter = ("participant__ticket_status", "participant__approved")
    ordering = ("-date_registered", "first_name", "last_name")
    readonly_fields = ("date_registered", "picture_height", "picture_width")
    search_fields = (
        "first_name",
        "last_name",
        "participant__user__email",
        "participant__event__name",
    )
    date_hierarchy = "date_registered"

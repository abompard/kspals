from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

from kspals.models import User
from kspals.forms import UserCreationForm


@admin.register(User)
class UserAdmin(BaseUserAdmin):

    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (_("Personal info"), {"fields": ("full_name",)}),
        (_("Permissions"), {"fields": ("is_active", "is_superuser")}),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (None, {"classes": ("wide",), "fields": ("email", "password1", "password2")}),
    )
    list_display = ("email", "full_name", "is_staff", "date_joined")
    list_filter = ("is_superuser", "is_active")
    search_fields = ("email", "full_name")
    ordering = ("email",)
    filter_horizontal = ()
    readonly_fields = ("date_joined",)
    add_form = UserCreationForm

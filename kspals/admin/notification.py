from django.contrib import admin

from kspals.models import Notification


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):

    list_display = ("participant", "__str__", "date_sent")
    ordering = ("-date_sent",)
    search_fields = (
        "participant__user__email",
        "participant__profile__first_name",
        "participant__profile__last_name",
    )
    list_filter = ("participant__event__name", "name")

from django.contrib import admin

from kspals.models import PAL


@admin.register(PAL)
class PalAdmin(admin.ModelAdmin):

    list_display = ("participant", "pal", "is_mutual", "date_requested")
    list_filter = ("participant__event__name",)
    ordering = ("participant__event__name", "participant__user__email")
    readonly_fields = ("is_mutual", "date_requested")
    search_fields = (
        "participant__user__email",
        "pal__user__email",
        "participant__event__name",
    )

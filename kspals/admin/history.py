from django.contrib import admin

from kspals.models import History


@admin.register(History)
class HistoryAdmin(admin.ModelAdmin):

    list_display = ("participant", "__str__", "date")
    ordering = ("-date",)
    search_fields = (
        "participant__user__email",
        "participant__profile__first_name",
        "participant__profile__last_name",
    )
    list_filter = ("participant__event__name",)

from django.contrib import admin

from kspals.models import TicketRate


@admin.register(TicketRate)
class TicketRateAdmin(admin.ModelAdmin):

    list_display = ("name", "event", "amount", "default")
    list_filter = ("event__name",)
    ordering = ("-default", "name")
    search_fields = ("name",)
    readonly_fields = ("bought_count",)

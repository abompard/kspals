from django.contrib import admin

from kspals.models import Participant, PAL, ParticipantProfile, History, ParticipantNote
from kspals.config import MAX_PAL_GROUP_SIZE


class ParticipantProfileInline(admin.StackedInline):
    model = ParticipantProfile
    readonly_fields = ("date_registered", "picture_height", "picture_width")


class ParticipantNoteInline(admin.TabularInline):
    model = ParticipantNote
    extra = 0
    fields = ("title", "content")
    verbose_name = "Note"


class HistoryInline(admin.TabularInline):
    model = History
    fk_name = "participant"
    extra = 0
    fields = ("__str__", "date")
    readonly_fields = fields
    ordering = ("-date",)


class AbstractParticipantPalInline(admin.TabularInline):
    model = PAL
    fk_name = "participant"
    fields = ("pal", "is_mutual", "approved", "has_ticket")
    extra = 0
    readonly_fields = ("is_mutual", "approved", "has_ticket")
    max_num = MAX_PAL_GROUP_SIZE - 1

    def approved(self, instance):
        return instance.pal.approved

    def has_ticket(self, instance):
        return instance.pal.has_ticket

    has_ticket.boolean = True


class ParticipantPalInline(AbstractParticipantPalInline):
    fk_name = "participant"
    fields = ("pal", "is_mutual", "approved", "has_ticket")


class ParticipantPalOfInline(AbstractParticipantPalInline):
    fk_name = "pal"
    fields = ("participant", "is_mutual", "approved", "has_ticket")
    verbose_name = "PAL of"
    verbose_name_plural = "PAL of"
    extra = 0


@admin.register(Participant)
class ParticipantAdmin(admin.ModelAdmin):

    list_display = (
        "user",
        "event",
        "approved",
        "ticket_status",
        "date_created",
        "is_in_mutual_pal_group",
        "is_in_approved_pal_group",
    )
    list_editable = ("approved", "ticket_status")
    filter_horizontal = ("pals",)
    list_filter = (
        "event__name",
        "ticket_status",
        "approved",
        "self_registered",
        "ticket_rate__name",
    )
    ordering = ("-date_created", "user__email")
    readonly_fields = (
        "date_created",
        "date_modified",
        "is_in_mutual_pal_group",
        "is_in_approved_pal_group",
        "is_on_waiting_list",
        "ticket_url",
        "pal_group",
    )
    search_fields = (
        "user__email",
        "event__name",
        "profile__first_name",
        "profile__last_name",
    )
    date_hierarchy = "date_created"
    inlines = [
        ParticipantProfileInline,
        ParticipantNoteInline,
        ParticipantPalInline,
        ParticipantPalOfInline,
        HistoryInline,
    ]

    def pal_group(self, instance):
        return " , ".join(sorted(p.user.email for p in instance.get_pal_group()))

    def is_on_waiting_list(self, instance):
        return (
            instance.event.is_on_waiting_list() or instance.event.is_full()
        ) and not instance.approved

    is_on_waiting_list.boolean = True

    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #    if db_field.name == "price_rate":
    #        kwargs["queryset"] = TicketRates.objects.filter(owner=request.user)
    #    return super(ParticipantAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

from django.contrib import admin

from kspals.models.event_user_group import EventUserGroup


@admin.register(EventUserGroup)
class GroupAdmin(admin.ModelAdmin):
    ordering = ("event__code",)
    list_filter = ("event__name",)

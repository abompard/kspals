from rest_framework.views import exception_handler
from rest_framework.exceptions import ValidationError


def type_friendly_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if response is not None:
        # Move the field errors to their own subkey.
        if isinstance(exc, ValidationError) and "detail" not in response.data:
            response.data = {"fields": response.data}

        # Now add the HTTP status code to the response.
        response.data["status_code"] = response.status_code

    return response

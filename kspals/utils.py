import logging
import re

from django.utils.translation import ugettext_lazy as _

from kspals.config import MAX_PAL_GROUP_SIZE
from kspals.models import (
    ParticipantProfile,
    ParticipantNote,
    Notification,
    History,
    User,
    Participant,
)
from kspals.models.constants import NotificationName, NotificationStatus, TicketStatus
from kspals.signals import participant_changed


log = logging.getLogger(__name__)


EMAIL_RE = re.compile(".+@.+\..+")


def ouinon(value):
    return _("oui") if value else _("non")


def get_csv_rows(event, unapproved=False):
    note_titles = list(
        ParticipantNote.objects.filter(participant__event=event)
        .values_list("title", flat=True)
        .distinct()
    )
    rows = [
        [
            _("Prénom"),
            _("Nom"),
            _("Email"),
            _("Date d'inscription"),
            _("Date d'approbation"),
            _("Prénom PAL1"),
            _("Nom PAL1"),
            _("Email PAL1"),
            _("Prénom PAL2"),
            _("Nom PAL2"),
            _("Email PAL2"),
            _("PALs approuvés"),
            _("PALs cohérents"),
            _("Ticket acheté"),
            _("Nom sur le ticket"),
            _("Role"),
            _("Tarif"),
            _("Boissons"),
        ]
        + note_titles
    ]
    date_format = "%Y-%m-%d %H:%M:%S"

    def _name_and_email(participant):
        try:
            result = [participant.profile.first_name, participant.profile.last_name]
        except ParticipantProfile.DoesNotExist:
            result = ["", ""]
        result.append(participant.user.email)
        return result

    approved = None if unapproved else True
    query = event.participant_set.filter(
        self_registered=True, approved=approved
    ).order_by("profile__date_registered")
    for participant in query:
        row = _name_and_email(participant)
        try:
            row.append(participant.profile.date_registered.strftime(date_format))
        except ParticipantProfile.DoesNotExist:
            row.append(participant.date_created)
        approval = (
            participant.history_set.filter(action="approved").order_by("-date").first()
        )
        if approval is None:
            row.append("")
        else:
            row.append(approval.date.strftime(date_format))
        pals = participant.pals.all()
        for pal in pals:
            row.extend(_name_and_email(pal))
        for _fill in range(MAX_PAL_GROUP_SIZE - len(pals) - 1):
            row.extend(["", "", ""])
        try:
            ticket_name = participant.ticket_name or participant.profile.full_name
        except ParticipantProfile.DoesNotExist:
            ticket_name = ""
        row.extend(
            [
                ouinon(participant.is_in_approved_pal_group()),
                ouinon(participant.is_in_mutual_pal_group()),
                ouinon(participant.has_ticket),
                ticket_name,
                participant.role.name,
                participant.ticket_rate,
                participant.drinks,
            ]
        )
        for title in note_titles:
            try:
                note = participant.notes.get(title=title).content
            except ParticipantNote.DoesNotExist:
                note = ""
            row.append(note)
        rows.append(row)
    return rows


def limit_pic_size(width, height, max_size):
    if width > max_size:
        height = height * max_size / width
        width = max_size
    if height > max_size:
        width = width * max_size / height
        height = max_size
    return int(width), int(height)


def notif_exists(participant, name):
    return (
        participant.notification_set.filter(name=name)
        .exclude(status=NotificationStatus.TRASH.name)
        .exists()
    )


def notify_ticketing_ready(participant, request):
    # Check validity
    if not participant.is_in_valid_pal_group():
        return
    notif_name = NotificationName.TICKETING_READY.name
    for p in participant.get_pal_group():
        if p.ticket_status != TicketStatus.NOT_READY.name:
            continue
        # Check that we haven't sent that notification already
        if notif_exists(p, notif_name):
            continue
        # Historize
        History.objects.create(event=p.event, participant=p, action="ticketing_ready")
        # Notify the participant
        notif_kwargs = Notification.get_defaults(notif_name, p, request)
        Notification.objects.create(**notif_kwargs)
        previous = Participant.objects.get(pk=p.pk)
        # Update the participant's ticket_status
        p.ticket_status = TicketStatus.URL_SENT.name
        p.save()
        participant_changed.send(
            Participant, instance=p, previous=previous, request=request
        )
        log.info(
            "Sent the ticketing link to participant {} ({})".format(
                p.profile.full_name, p.pk
            )
        )


def get_or_create_participant(email, event):
    user, created = User.objects.get_or_create(email=email.lower())
    if created:
        user.set_unusable_password()
        user.save()
    participant, created = Participant.objects.get_or_create(event=event, user=user)
    return participant

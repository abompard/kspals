import logging

from django.db.models.signals import post_delete, post_save, pre_save
from django.dispatch import receiver
from django.utils import timezone

from kspals.models import PAL, History, Notification, Participant, ParticipantProfile
from kspals.models.constants import NotificationName, NotificationStatus, TicketStatus
from kspals.models.utils import pal_group_is_valid
from kspals.utils import notif_exists, notify_ticketing_ready

from . import new_participant, pal_added, pal_removed, pal_updated, participant_changed

log = logging.getLogger(__name__)


@receiver(post_save, sender=PAL)
def pal_post_save_callback(sender, **kwargs):
    instance = kwargs.pop("instance")
    instance.on_post_save(**kwargs)


@receiver(post_delete, sender=PAL)
def pal_post_delete_callback(sender, **kwargs):
    instance = kwargs.pop("instance")
    instance.on_post_delete(**kwargs)


@receiver(post_save, sender=Participant)
def participant_post_save_callback(sender, **kwargs):
    instance = kwargs.pop("instance")
    instance.on_post_save(**kwargs)


@receiver(post_save, sender=ParticipantProfile)
def participant_profile_post_save_callback(sender, **kwargs):
    instance = kwargs.pop("instance")
    instance.on_post_save(**kwargs)


@receiver(post_delete, sender=ParticipantProfile)
def participant_profile_post_delete_callback(sender, **kwargs):
    instance = kwargs.pop("instance")
    instance.on_post_delete(**kwargs)


@receiver(new_participant, sender=Participant)
def notify_new_participant(sender, **kwargs):
    participant = kwargs["instance"]
    request = kwargs["request"]
    # Check they self_registered
    if not participant.self_registered:
        return
    if not participant.has_profile():
        return  # Created in the admin interface
    log.info(
        "A new participant registered for {}: {} <{}>".format(
            participant.event.name,
            participant.profile.full_name,
            participant.user.email,
        )
    )
    # Historize
    History.objects.create(
        event=participant.event, participant=participant, action="registered"
    )
    # Check that we haven't sent that notification already
    notif_name = NotificationName.NEW_PARTICIPANT.name
    if notif_exists(participant, notif_name):
        return
    # Notify the admins
    notif_kwargs = dict(participant=participant, name=notif_name)
    notif_kwargs.update(Notification.get_defaults(notif_name, participant, request))
    Notification.objects.create(**notif_kwargs)


@receiver(participant_changed, sender=Participant)
def notify_ticketing_ready_participant_changed(sender, **kwargs):
    participant = kwargs["instance"]
    request = kwargs.get("request")
    if not request:
        return
    notify_ticketing_ready(participant, request)


@receiver([pal_added, pal_updated], sender=PAL)
def notify_ticketing_ready_pal_changed(sender, **kwargs):
    instance = kwargs["instance"]
    request = kwargs["request"]
    # print("on pal_added or pal_updated", instance.participant)
    notify_ticketing_ready(instance.participant, request)


@receiver(participant_changed, sender=Participant)
def historize_participant_change(sender, **kwargs):
    participant = kwargs["instance"]
    previous = kwargs.get("previous")
    data = None
    if previous is not None and previous.approved is None and participant.approved:
        action = "approved"
    elif (
        previous is not None
        and previous.approved is None
        and participant.approved is False
    ):
        action = "rejected"
    elif previous is not None and previous.ticket_status != participant.ticket_status:
        action = "ticket_status_change"
        data = participant.ticket_status
    elif previous is not None and previous.drinks != participant.drinks:
        action = "drinks_bought"
        data = participant.drinks - previous.drinks
    else:
        action = "changed"
    History.objects.create(
        event=participant.event, participant=participant, action=action, data=data
    )


@receiver(participant_changed, sender=Participant)
def update_date_modified_participant_change(sender, **kwargs):
    participant = kwargs["instance"]
    previous = kwargs.get("previous")
    if previous is None:
        return
    if (
        previous.approved != participant.approved
        or previous.ticket_status != participant.ticket_status
    ):
        for pal in participant.get_pal_group():
            pal.date_modified = timezone.now()
            pal.save()


@receiver(pal_added, sender=PAL)
def historize_pal_added(sender, **kwargs):
    instance = kwargs["instance"]
    log.info(
        "Participant {} added PAL {}".format(
            instance.participant.user.email, instance.pal.user.email
        )
    )
    History.objects.create(
        event=instance.participant.event,
        participant=instance.participant,
        action="pal_add",
        data=instance.pal.user.email,
    )


@receiver(pal_updated, sender=PAL)
def historize_pal_updated(sender, **kwargs):
    instance = kwargs["instance"]
    previous = kwargs["previous"]
    log.info(
        "Participant {} changed PAL from {} to {}".format(
            instance.participant.user.email,
            previous.user.email,
            instance.pal.user.email,
        )
    )
    History.objects.create(
        event=instance.participant.event,
        participant=instance.participant,
        action="pal_update",
        data="from {} to {}".format(previous.user.email, instance.pal.user.email),
    )


@receiver(pal_removed, sender=PAL)
def historize_pal_removed(sender, **kwargs):
    participant = kwargs["participant"]
    previous = kwargs["previous"]
    log.info(
        "Participant {} removed PAL {}".format(
            participant.user.email, previous.user.email
        )
    )
    History.objects.create(
        event=participant.event,
        participant=participant,
        action="pal_remove",
        data=previous.user.email,
    )


@receiver(post_save, sender=ParticipantProfile)
def make_thumbnail(sender, **kwargs):
    kwargs["instance"].make_thumb()


@receiver(pre_save, sender=Participant)
def set_default_ticket_rate_and_role(sender, **kwargs):
    instance = kwargs["instance"]
    if instance.ticket_rate is None:
        instance.ticket_rate = instance.event.ticketrate_set.get(default=True)
    if instance.role is None:
        instance.role = instance.event.role_set.get(default=True)


@receiver([pal_updated, pal_removed], sender=PAL)
def notify_broken_pal(sender, **kwargs):
    """
    If A stops choosing B as a PAL and B still chooses A, notify B.
    """
    instance = kwargs["instance"]
    request = kwargs["request"]
    previous = kwargs["previous"]
    if not PAL.objects.filter(participant=previous, pal=instance.participant).exists():
        # Not mutual, don't notify
        return
    # Only notify if the ticket has already been bought or the ticketing link sent
    if previous.ticket_status not in (
        TicketStatus.URL_SENT.name,
        TicketStatus.BOUGHT.name,
    ):
        return
    # Inform the other that their PAL group has been broken.
    notif_kwargs = Notification.get_defaults(
        NotificationName.BROKEN_PAL.name, previous, request, actor=instance.participant
    )
    Notification.objects.create(**notif_kwargs)


@receiver([pal_added, pal_updated], sender=PAL)
def unnotify_repaired_pal(sender, **kwargs):
    """
    If A broke their link to B and repairs it afterwards, remove the notification to B.
    """
    instance = kwargs["instance"]
    if not instance.is_mutual():
        # Not mutual, don't try to un-notify
        return
    # Inform the other that their PAL group has been broken.
    try:
        notif = Notification.objects.get(
            name=NotificationName.BROKEN_PAL.name,
            participant=instance.pal,
            status=NotificationStatus.TO_SEND.name,
        )
    except Notification.DoesNotExist:
        pass
    else:
        notif.delete()


@receiver([pal_removed, pal_updated], sender=PAL)
def notify_admin_broken_pal_previous(sender, **kwargs):
    """Removing a PAL may break:
    - the user's PAL group
    - the removed user's PAL group
    Changing a PAL may break:
    - the user's PAL group
    - the updated user's PAL group
    - the former PAL's PAL group
    """
    request = kwargs["request"]
    instance = kwargs["instance"]
    previous = kwargs["previous"]
    # Previous PAL group
    previous_pal_group = set(instance.participant.get_pal_group()) | set([previous])
    if instance.pal != previous:
        previous_pal_group.remove(instance.pal)
    # Check if the PAL group was valid. We can't use pal_group_is_valid because the data has changed now, and get_pal_group() won't return the previous value on the participant.
    pals = {}
    for p in previous_pal_group:
        pals[p.pk] = set(p.get_pal_group())
    pals[instance.participant_id] = previous_pal_group
    pal_groups = list(pals.values())
    group_is_valid = all((pg == pal_groups[0]) for pg in pal_groups)
    if not group_is_valid:
        # Only notify when the PAL group becomes invalid.
        return
    _check_broken_pal_group(instance.participant, previous_pal_group, request)


@receiver([pal_added, pal_updated], sender=PAL)
def notify_admin_broken_pal_new(sender, **kwargs):
    """Adding a PAL may break:
    - the user's PAL group
    - the added user's PAL group.
    Changing a PAL may break:
    - the user's PAL group
    - the updated user's PAL group
    - the former PAL's PAL group
    """
    request = kwargs["request"]
    instance = kwargs["instance"]
    # New PAL group
    new_pal_group = set(instance.participant.get_pal_group()) | set(
        instance.pal.get_pal_group()
    )
    if pal_group_is_valid(list(new_pal_group)):
        # Only notify when the PAL group becomes invalid.
        return
    _check_broken_pal_group(instance.participant, new_pal_group, request)


def _check_broken_pal_group(participant, pal_group, request):
    """
    Notify users and admins if a PAL group becomes invalid after the ticket
    link has been sent or the ticket has been bought.
    """
    # Check ticket state
    required_ticket_states = [TicketStatus.URL_SENT.name, TicketStatus.BOUGHT.name]
    has_ticket = any(
        [(member.ticket_status in required_ticket_states) for member in pal_group]
    )
    if not has_ticket:
        return  # Nothing to worry about here.
    # Notify the admins
    notif_kwargs = Notification.get_defaults(
        NotificationName.ADMIN_BROKEN_PAL_GROUP.name,
        participant,
        request,
        pal_group=pal_group,
    )
    Notification.objects.create(**notif_kwargs)

from django.dispatch import Signal

# Define this signal to make sure the ParticipantProfile has been created.
new_participant = Signal(providing_args=["instance", "request"])
# Signals related to PAL history
pal_added = Signal(providing_args=["instance", "request"])
pal_removed = Signal(providing_args=["instance", "participant", "previous", "request"])
pal_updated = Signal(providing_args=["instance", "previous", "request"])
# Participant changed
participant_changed = Signal(providing_args=["instance", "request"])

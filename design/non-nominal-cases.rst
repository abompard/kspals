Non-nominal cases
=================


Event threshold & waiting list
------------------------------

- Ticket URL sent, the ticket aren't bought, and the event is full before the ticket can be bought
  → Compute the total participants by counting the people we sent the ticket URL to

- If sending the ticket URL is a manual operation, there can be more valid PAL groups than the event threshold because of the delay between a valid group and the sending of the ticket URL.
  → Only make it a manual operation when the event is full

- People should still be able to register when the event is full. In that case, when the PAL group is complete, the link to the tickets should not be sent but be in a waiting list in the admin UI


PAL cases
---------

- Ticket bought but the PAL group is invalidated later.
  → manual solution, the ticket is cancelled and refunded

- Person A removes Person B from their PALs but does not communicate with them, Person B arrives at the door thinking that their PAL group is valid.
  → notify Person B when their PALs group becomes invalid because of somebody else's action.

import merge from "lodash/merge";
import { normalize, Schema } from "normalizr";
import { APIError } from "./types";
import { ObjectMap } from "./utils";

interface FetchConfig extends RequestInit {
  headers: {
    [key: string]: string;
  }; // FIXME: without this I get error 7017
  // method?: RequestInit["method"];
  // body?: RequestInit["body"];
}

// export interface ApiCallProps {
//   url: string;
//   token?: string;
//   fetchConfig?: FetchConfig;
// }

function getBaseFetchConfig(
  token?: string | null,
  useJson: boolean = false
): RequestInit {
  let config: FetchConfig = {
    headers: {}
  };
  if (token) {
    config.credentials = "same-origin";
    config.headers["Authorization"] = `Token ${token}`;
  }
  if (useJson) {
    config.headers!["Content-Type"] = "application/json";
  }
  return config;
}

const makeErrorPromise = (
  response: Response,
  error: string | Error
): Promise<APIError> =>
  Promise.reject({
    detail: error.toString(),
    httpStatus: response.status
  });

export function apiCall(
  url: string,
  schema: Schema | null,
  token?: string | null,
  fetchConfig?: RequestInit
): Promise<any> {
  console.log("Calling API:", url, fetchConfig);
  const baseFetchConfig = getBaseFetchConfig(token, true);
  let options: RequestInit = merge({}, baseFetchConfig, fetchConfig);
  return fetch(url, options).then(
    response => {
      const contentType = response.headers.get("content-type");
      const isJson = contentType && contentType.includes("application/json");
      // console.log("isJson", isJson, contentType);
      if (isJson) {
        return response.json().then(
          json => {
            if (!response.ok) {
              json.httpStatus = json.status_code || response.status;
              return Promise.reject(json);
            }
            let result: ObjectMap = {};
            if (schema !== null) {
              result = Object.assign(
                result,
                normalize(json.results ? json.results : json, schema)
              );
            } else {
              result.results = json.results;
            }
            result.page = json.page;
            return Promise.resolve(result);
          },
          error => makeErrorPromise(response, error)
        );
      } else {
        return response.text().then(
          (content: string): Promise<string | APIError> => {
            if (!response.ok) {
              return makeErrorPromise(response, content);
            }
            return Promise.resolve(content);
          },
          error => makeErrorPromise(response, error)
        );
      }
    },
    error =>
      Promise.reject({
        detail: error.toString(),
        httpStatus: 42
      })
  );
}

export function backendCall(
  url: string,
  token?: string | null,
  fetchConfig?: RequestInit
): Promise<Response> {
  console.log("Calling backend:", url, fetchConfig);
  const baseFetchConfig = getBaseFetchConfig(token, false);
  let options: RequestInit = merge({}, baseFetchConfig, fetchConfig);
  return fetch(url, options).then(response => {
    if (!response.ok) {
      return Promise.reject(response);
    }
    return Promise.resolve(response);
  });
}

export const loadAllPages = (
  initialUrl: string,
  callback: (url: string) => Promise<any>
): Promise<any> => {
  return callback(initialUrl).then(
    result => {
      if (result && result.page && result.page.next) {
        return loadAllPages(result.page.next, callback);
      } else {
        return result;
      }
    },
    error => {
      throw error;
    }
  );
};

/*
 * Types of objects returned by the API
 */

export interface PaginatedResult<T> {
  results: Array<T>;
  page: {
    count: number;
    previous: string | null;
    next: string | null;
    number: number;
    page_size: number;
  };
}

export interface BackendValidationError {
  readonly [key: string]: Array<string>;
}

export interface APIError {
  readonly detail?: string;
  readonly fields?: BackendValidationError;
  readonly httpStatus: number;
}

export interface IParticipantProfile {
  first_name: string;
  last_name: string;
  full_name: string;
  introduction: string;
  learned_about: string;
  picture: string;
  picture_height: number;
  picture_width: number;
  picture_thumb: string;
  date_registered: string;
  low_income: boolean;
}

export interface IParticipantNote {
  id: number;
  title: string;
  content: string;
}

export interface IPal {
  url: string;
  id: number;
  email: string;
  name: string;
  is_mutual: boolean;
  has_ticket: boolean;
  approved: boolean;
  pal: IParticipant;
}

export type TicketStatus =
  | "NOT_READY"
  | "URL_SENT"
  | "BOUGHT"
  | "SELLING"
  | "CANCELLED";

export interface IParticipant {
  readonly url: string;
  readonly id: number;
  readonly email: string;
  profile: IParticipantProfile;
  self_registered: boolean;
  approved: boolean | null;
  role: string | null;
  ticket_rate: string;
  has_ticket: boolean;
  ticket_name: string | null;
  ticket_url: string | null;
  ticket_coupon: string | null;
  ticket_status?: TicketStatus;
  drinks: number;
  approved_pal_group: boolean;
  mutual_pal_group: boolean;
  valid_pal_group: boolean;
  pal_group_registration_date: string;
  selling_ticket_to: IParticipant | null;
  ticket_offer_by: Array<IParticipant>;
  pals: Array<IPal>;
  notes: Array<IParticipantNote>;
  history: Array<IHistory>;
}

export interface IRole {
  id: number;
  url: string;
  code: string;
  default: boolean;
  name: string;
}
export type RoleAttributes = Partial<Pick<IRole, "code" | "default" | "name">>;

export interface ITicketRate {
  id: number;
  url: string;
  name: string;
  default: boolean;
  low_income: boolean;
  coupon?: string;
  amount: number;
  ticket_url?: string;
}
export type TicketRateAttributes = Partial<
  Pick<
    ITicketRate,
    "name" | "default" | "low_income" | "coupon" | "amount" | "ticket_url"
  >
>;

export interface ITicketStatus {
  name: string;
  code: string;
}

export interface IEvent {
  url: string;
  code: string;
  name: string;
  location: string;
  date_open: string;
  date_reg_open: string;
  date_reg_close: string;
  is_active: boolean;
  is_registration_open: boolean;
  is_on_waiting_list: boolean;
  is_full: boolean;
  full_limit: number | null;
  waiting_list_size: number;
  ticketing_sent_count: number;
  tickets_bought_count: number;
  participants_url: string;
  participants: Array<IParticipant>;
  users_can_view?: Array<IUser>;
  users_can_change?: Array<IUser>;
  roles?: Array<IRole>;
  ticket_rates: Array<ITicketRate>;
  ticket_statuses?: Array<ITicketStatus>;
  notifications_url: string;
  notifications: Array<INotification>;
  history: Array<IHistory>;
}

export interface IUser {
  id: number;
  email: string;
  full_name: string;
  date_joined: Date;
  is_superuser: boolean;
  is_staff: boolean;
  token: string;
  participant: number | null;
}

export interface IOtherUser {
  id: number;
  email: string;
  full_name: string;
}

export interface INotification {
  id: number;
  date_created: Date;
  date_sent: Date | null;
  name: "TICKETING_READY" | "NEW_PARTICIPANT";
  name_human: string;
  participant: IParticipant;
  recipient: string;
  status: "TO_SEND" | "DRAFT" | "SENT" | "TRASH";
  subject: string;
  url: string;
  body_text: string;
  body_html: string | null;
}

export interface IHistory {
  id: number;
  participant: IParticipant | null;
  action: string;
  data: string;
  date: string;
}

export type PaginatedEntity = IEvent | IParticipant | INotification | IHistory;

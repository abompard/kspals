import { IParticipant } from "./types";

export interface ObjectMap {
  [key: string]: any;
}
export const sortByAttr = (attrName: string) => (
  o1: ObjectMap | string,
  o2: ObjectMap | string
): number => {
  let target1 = o1,
    target2 = o2;
  attrName.split(".").forEach((attrElem: string): void => {
    target1 = (target1 as ObjectMap)[attrElem];
    target2 = (target2 as ObjectMap)[attrElem];
  });
  if (typeof target1 === "string") {
    target1 = target1.toLowerCase();
  }
  if (typeof target2 === "string") {
    target2 = target2.toLowerCase();
  }
  if (target1 < target2) {
    return -1;
  }
  if (target1 > target2) {
    return 1;
  }
  return 0;
};

export const sortByAttrs = (attrNames: Array<string>) => (
  o1: ObjectMap | string,
  o2: ObjectMap | string
): number => {
  for (let attr of attrNames) {
    const result = sortByAttr(attr)(o1, o2);
    if (result !== 0) {
      return result;
    }
  }
  return 0;
};

export const filterParticipants = (
  participants: Array<IParticipant>,
  filter: string
): Array<IParticipant> => {
  if (!filter) {
    return participants;
  }
  return participants.filter(
    participant =>
      participant.profile.full_name.toLowerCase().search(filter) !== -1 ||
      participant.email.toLowerCase().search(filter) !== -1 ||
      (participant.ticket_name &&
        participant.ticket_name.toLowerCase().search(filter) !== -1)
  );
};

export type FormValues<T> = { [k in keyof T]: string };

export const nullBoolToString = (value: boolean | null | undefined): string =>
  value === true ? "true" : value === false ? "false" : "";

export const stringToNullBool = (value: string): boolean | null =>
  value === "true" ? true : value === "false" ? false : null;

export const nullNumberToString = (value: number | null | undefined): string =>
  value === null || value === undefined ? "" : value.toString();

export const stringToNullNumber = (value: string): number | null =>
  value === "" ? null : Number(value);

export const unique = (value: Array<any>): Array<any> => {
  return value.filter((v, i, self) => self.indexOf(v) === i);
};

import { schema } from "normalizr";
import { EntitiesState } from "../reducers/entities";

export const userSchema = new schema.Entity("users");

export const palSchema = new schema.Entity("pals");

export const participantSchema = new schema.Entity("participants", {
  pals: [palSchema]
});
participantSchema.define({
  selling_ticket_to: participantSchema,
  ticket_offer_by: [participantSchema]
});

export const palFullSchema = new schema.Entity("pals", {
  pal: participantSchema
});

export const participantFullSchema = new schema.Entity("participants", {
  pals: [palFullSchema],
  selling_ticket_to: participantSchema,
  ticket_offer_by: [participantSchema]
});

export const participantsSchema = new schema.Array(participantSchema);

palSchema.define({ pal: participantSchema });

export const roleSchema = new schema.Entity("roles");
export const ticketRateSchema = new schema.Entity("ticket_rates");
export const ticketStatusSchema = new schema.Entity(
  "ticket_status",
  {},
  { idAttribute: "code" }
);

export const notificationSchema = new schema.Entity("notifications", {
  participant: participantSchema
});
export const notificationsSchema = new schema.Array(notificationSchema);

export const historySchema = new schema.Entity("history", {
  participant: participantSchema
});
export const historyListSchema = new schema.Array(historySchema);

participantFullSchema.define({ history: historyListSchema });

export const eventSchema = new schema.Entity(
  "events",
  {
    participants: [participantSchema],
    users_can_view: [userSchema],
    users_can_change: [userSchema],
    roles: [roleSchema],
    ticket_rates: [ticketRateSchema],
    ticket_statuses: [ticketStatusSchema],
    notifications: [notificationSchema],
    history: [historySchema]
  },
  {
    idAttribute: "code"
  }
);

export const eventsSchema = new schema.Array(eventSchema);

type SchemaRegistry = {
  [entity in keyof EntitiesState]?: {
    object: schema.Entity;
    list: schema.Array;
  };
};
export const schemaRegistry: SchemaRegistry = {
  events: {
    object: eventSchema,
    list: eventsSchema
  },
  participants: {
    object: participantSchema,
    list: participantsSchema
  },
  notifications: {
    object: notificationSchema,
    list: notificationsSchema
  },
  history: {
    object: historySchema,
    list: historyListSchema
  }
};

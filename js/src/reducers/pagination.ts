import { LogoutAction, PagedListActions } from "../actions/types";
import { EntitiesState } from "./entities";
import { FilterDict } from "../types";
import _ from "lodash";

interface EntityPagesState {
  pages: {
    [pageNr: number]: Array<number | string>;
  };
  count: number | null;
  pageSize: number | null;
  filters: FilterDict;
}

export const initialPageState: EntityPagesState = {
  pages: {},
  count: null,
  pageSize: null,
  filters: {}
};

// Updates the pages cache in response to any action with response.page.

export type PaginationState = {
  [key in keyof EntitiesState]?: EntityPagesState
};

const entitiesActions: { [key in keyof EntitiesState]?: string } = {
  events: "LOAD_EVENTS_SUCCESS",
  participants: "LOAD_PARTICIPANTS_SUCCESS",
  notifications: "LOAD_NOTIFICATIONS_SUCCESS",

  history: "LOAD_HISTORY_SUCCESS"
};

export const createPaginationReducer = (entity: keyof EntitiesState) => {
  return (
    state: EntityPagesState = initialPageState,
    action: PagedListActions | LogoutAction
  ) => {
    if (action.type === "LOGOUT_SUCCESS") {
      return initialPageState;
    }
    action = action as PagedListActions;
    const allowedAction = entitiesActions[entity];
    if (action.type !== allowedAction) {
      return state;
    }
    const page = action.response.page;
    if (!page) {
      return state;
    }
    const newState: EntityPagesState = {
      ...state,
      count: page.count,
      pageSize: page.page_size,
      filters: page.filters,
      // If the filter did not change, keep the pages, otherwise reset them
      pages: _.isEqual(state.filters, action.response.page.filters)
        ? { ...state.pages }
        : {}
    };
    newState.pages[page.number] = action.response.result;
    return newState;
  };
};

export const paginationTopReducer = (
  state: PaginationState = {},
  action: PagedListActions | LogoutAction
) => {
  return {
    events: createPaginationReducer("events")(state.events, action),
    participants: createPaginationReducer("participants")(
      state.participants,
      action
    ),
    notifications: createPaginationReducer("notifications")(
      state.notifications,
      action
    ),
    history: createPaginationReducer("history")(state.history, action)
  };
};

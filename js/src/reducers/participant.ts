import { AllActions } from "../actions/types";
import { BackendValidationError } from "../api/types";
import { EntityUiState, initialEntityUiState } from "./uiTypes";

export interface ParticipantValidationUiState {
  fieldErrors: BackendValidationError;
  error: string | null;
}

export interface ParticipantUiState extends EntityUiState {
  isLoading: boolean;
  mustRegister: boolean;
  lastLoad: string | null;
}

export type CurrentParticipantState = number | null;

export const initialUiState = {
  ...initialEntityUiState,
  mustRegister: false,
  lastLoad: null
};

export const currentParticipantReducer = (
  state: CurrentParticipantState = null,
  action: AllActions
): CurrentParticipantState => {
  switch (action.type) {
    case "LOAD_PARTICIPANT_SUCCESS":
    case "REGISTER_PARTICIPANT_SUCCESS":
      return action.response.result;
    case "CANCEL_PARTICIPANT_SUCCESS":
    case "LOGOUT_SUCCESS":
      return null;
    default:
      return state;
  }
};

export const participantUiReducer = (
  state: ParticipantUiState = initialUiState,
  action: AllActions
): ParticipantUiState => {
  switch (action.type) {
    case "LOAD_PARTICIPANT_REQUEST":
    case "LOAD_PARTICIPANTS_REQUEST":
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case "LOAD_PARTICIPANT_FAILURE":
      return {
        ...state,
        isLoading: false,
        mustRegister: action.error.httpStatus === 404
      };
    case "LOAD_PARTICIPANT_SUCCESS":
      const participantId = action.response.result;
      const mustRegister =
        action.response.entities.participants[participantId].profile === null;
      return {
        ...state,
        mustRegister,
        isLoading: false,
        hasInitiallyLoaded: true
      };
    case "REGISTER_PARTICIPANT_SUCCESS":
      return {
        ...state,
        mustRegister: false
      };
    case "LOAD_PARTICIPANTS_SUCCESS":
      return {
        ...state,
        lastLoad: action.date_request
      };
    case "LOAD_PARTICIPANTS_FAILURE":
      return {
        ...state,
        isLoading: false,
        error: action.error.detail || "Unknown error"
      };
    case "LOAD_PARTICIPANTS_DONE":
      return {
        ...state,
        isLoading: false,
        hasInitiallyLoaded: true
      };
    case "LOGOUT_SUCCESS":
      return initialUiState;
    default:
      return state;
  }
};

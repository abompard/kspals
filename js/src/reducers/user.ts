import { AllActions } from "../actions/types";
import { EntityUiState, initialEntityUiState } from "./uiTypes";

export type CurrentUserState = number | null;

export const currentUserReducer = (
  state: CurrentUserState = null,
  action: AllActions
): CurrentUserState => {
  switch (action.type) {
    case "TOKEN_SUCCESS":
      return action.response.result;
    case "LOGOUT_SUCCESS":
      return null;
    case "INITIAL_STATE_SUCCESS":
      return action.response.current.user;
    default:
      return state;
  }
};

export const userUiReducer = (
  state: EntityUiState = initialEntityUiState,
  action: AllActions
): EntityUiState => {
  switch (action.type) {
    case "TOKEN_SUCCESS":
    case "LOGOUT_SUCCESS":
    case "INITIAL_STATE_SUCCESS":
      return {
        error: null,
        isLoading: false,
        hasInitiallyLoaded: true
      };
    default:
      return state;
  }
};

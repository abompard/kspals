import { combineReducers } from "redux";
import { AllActions, IFlashMessage } from "../actions/types";
import { eventUiReducer } from "./event";
import { historyUiReducer } from "./history";
import { notificationUiReducer } from "./notification";
import { palsUiReducer } from "./pals";
import { participantUiReducer, ParticipantUiState } from "./participant";
import {
  EntityUiState,
  InitialUiState,
  ValidatingEntityUiState
} from "./uiTypes";
import { userUiReducer } from "./user";

export interface UiState {
  initialState: InitialUiState;
  user: EntityUiState;
  event: EntityUiState;
  participant: ParticipantUiState;
  pals: Array<EntityUiState>;
  notification: ValidatingEntityUiState;
  history: EntityUiState;
  flashMessages: Array<IFlashMessage>;
  socketIOActive: boolean;
}

const initialRequestState: InitialUiState = {
  isLoading: false,
  error: null,
  hasInitiallyLoaded: false
};

export const initialStateReducer = (
  state: InitialUiState = initialRequestState,
  action: AllActions
): InitialUiState => {
  switch (action.type) {
    case "INITIAL_STATE_REQUEST":
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case "INITIAL_STATE_FAILURE":
      return {
        ...state,
        isLoading: false,
        error: action.error
      };
    case "INITIAL_STATE_SUCCESS":
      return {
        ...state,
        isLoading: false,
        error: null,
        hasInitiallyLoaded: true
      };
    default:
      return state;
  }
};

export const flashMessagesReducer = (
  state: Array<IFlashMessage> = [],
  action: AllActions
): Array<IFlashMessage> => {
  switch (action.type) {
    case "ADD_FLASH_MESSAGE":
      return [...state, action.message];
    case "DELETE_FLASH_MESSAGE":
      return state.filter(msg => msg.id !== action.id);
    case "PRUNE_FLASH_MESSAGES":
      return state.filter(msg => !msg.shown);
    case "FLAG_FLASH_MESSAGE_AS_SHOWN":
      return state.map(message => {
        if (message.id === action.id && !message.shown) {
          message.shown = true;
        }
        return message;
      });
    default:
      return state;
  }
};

export const socketIOReducer = (
  state: boolean = false,
  action: AllActions
): boolean => {
  switch (action.type) {
    case "SET_SOCKETIO_ACTIVE":
      return action.status;
    default:
      return state;
  }
};

export const uiReducer = combineReducers({
  initialState: initialStateReducer,
  user: userUiReducer,
  event: eventUiReducer,
  participant: participantUiReducer,
  pals: palsUiReducer,
  notification: notificationUiReducer,
  history: historyUiReducer,
  flashMessages: flashMessagesReducer,
  socketIOActive: socketIOReducer
});

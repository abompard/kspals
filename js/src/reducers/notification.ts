import { AllActions } from "../actions/types";
import { initialEntityUiState, ValidatingEntityUiState } from "./uiTypes";

export const notificationUiReducer = (
  state: ValidatingEntityUiState = initialEntityUiState,
  action: AllActions
): ValidatingEntityUiState => {
  switch (action.type) {
    case "LOAD_NOTIFICATIONS_REQUEST":
    case "VALIDATE_NOTIFICATION_REQUEST":
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case "LOAD_NOTIFICATIONS_FAILURE":
    case "VALIDATE_NOTIFICATION_FAILURE":
      return {
        ...state,
        error: action.error.detail || null,
        fieldErrors: action.error.fields,
        isLoading: false
      };
    case "LOAD_NOTIFICATIONS_DONE":
    case "VALIDATE_NOTIFICATION_SUCCESS":
      return {
        isLoading: false,
        error: null,
        hasInitiallyLoaded: true
      };
    default:
      return state;
  }
};

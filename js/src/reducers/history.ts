import { AllActions } from "../actions/types";
import { initialEntityUiState, EntityUiState } from "./uiTypes";

export const historyUiReducer = (
  state: EntityUiState = initialEntityUiState,
  action: AllActions
): EntityUiState => {
  switch (action.type) {
    case "LOAD_HISTORY_REQUEST":
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case "LOAD_HISTORY_FAILURE":
      return {
        ...state,
        error: action.error.detail || null,
        isLoading: false
      };
    case "LOAD_HISTORY_SUCCESS":
      return {
        isLoading: false,
        error: null,
        hasInitiallyLoaded: true
      };
    default:
      return state;
  }
};

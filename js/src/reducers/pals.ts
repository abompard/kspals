import { AllActions } from "../actions/types";
import { EntityUiState, initialEntityUiState } from "./uiTypes";

export const palsUiReducer = (
  state: Array<EntityUiState> = [],
  action: AllActions
): Array<EntityUiState> => {
  switch (action.type) {
    case "LOAD_PARTICIPANT_SUCCESS":
    case "REGISTER_PARTICIPANT_SUCCESS":
      const participantId = action.response.result;
      const participant = action.response.entities.participants[participantId];
      return participant.pals.map(() => initialEntityUiState);
    case "LOGOUT_SUCCESS":
      return [];
    default:
      return state;
  }
};

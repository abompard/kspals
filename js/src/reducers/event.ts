import { AllActions } from "../actions/types";
import { EntityUiState, initialEntityUiState } from "./uiTypes";

export const initialUiState = {
  ...initialEntityUiState
};

export const eventUiReducer = (
  state: EntityUiState = initialUiState,
  action: AllActions
): EntityUiState => {
  switch (action.type) {
    case "LOAD_EVENT_REQUEST":
    case "LOAD_EVENTS_REQUEST":
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case "LOAD_EVENT_FAILURE":
    case "LOAD_EVENTS_FAILURE":
      return {
        ...state,
        error: action.error.detail || null,
        isLoading: false
      };
    case "LOAD_EVENT_SUCCESS":
      return {
        ...state,
        isLoading: false,
        error: null,
        hasInitiallyLoaded: true
      };
    case "LOAD_EVENTS_SUCCESS":
      return {
        ...state,
        isLoading: false,
        error: null
      };
    default:
      return state;
  }
};

export type CurrentEventCodeState = string | null;

export const currentEventCodeReducer = (
  state: CurrentEventCodeState = null,
  action: AllActions
): CurrentEventCodeState => {
  switch (action.type) {
    case "LOAD_EVENT_FAILURE":
      return null;
    case "LOAD_EVENT_SUCCESS":
      return action.response.result;
    default:
      return state;
  }
};

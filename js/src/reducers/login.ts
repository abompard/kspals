import { AllActions } from "../actions/types";

export interface LoginState {
  loginCodeSending: boolean;
  loginCodeSent: boolean;
  loginCodeSentFor: string | null;
  loginCodeEntering: boolean;
  loginCodeEntered: boolean;
  requestLoginCodeError: string | null;
  requestTokenError: string | null;
  loggingOut: boolean;
}

const initialState = {
  loginCodeSending: false,
  loginCodeSent: false,
  loginCodeSentFor: null,
  loginCodeEntering: false,
  loginCodeEntered: false,
  requestLoginCodeError: null,
  requestTokenError: null,
  loggingOut: false
};

export const loginReducer = (
  state: LoginState = initialState,
  action: AllActions
): LoginState => {
  switch (action.type) {
    case "LOGIN_CODE_REQUEST":
      return {
        ...state,
        loginCodeSending: true,
        loginCodeSent: false,
        requestLoginCodeError: null
      };
    case "LOGIN_CODE_SUCCESS":
      return {
        ...state,
        loginCodeSending: false,
        loginCodeSent: true,
        loginCodeSentFor: action.email,
        requestLoginCodeError: null,
        requestTokenError: null
      };
    case "LOGIN_CODE_FAILURE":
      return {
        ...state,
        loginCodeSending: false,
        loginCodeSent: false,
        requestLoginCodeError: action.error.detail || null
      };
    case "TOKEN_REQUEST":
      return {
        ...state,
        loginCodeEntering: true,
        loginCodeEntered: false,
        requestLoginCodeError: null,
        requestTokenError: null
      };
    case "TOKEN_SUCCESS":
      return {
        ...state,
        loginCodeEntering: false,
        loginCodeEntered: true,
        requestTokenError: null
      };
    case "TOKEN_FAILURE":
      let requestTokenError = null;
      let requestLoginCodeError = state.requestLoginCodeError;
      if (action.error.fields && action.error.fields.email) {
        requestLoginCodeError = action.error.fields.email[0];
      } else if (action.error.fields && action.error.fields.token) {
        requestTokenError = action.error.fields.token[0];
      } else if (action.error.detail) {
        requestTokenError = action.error.detail;
      } else if (action.error.fields && action.error.fields.non_field_errors) {
        requestTokenError = action.error.fields.non_field_errors[0];
      }
      return {
        ...state,
        loginCodeEntering: false,
        loginCodeEntered: false,
        loginCodeSent: false, // Make the first input available again
        requestTokenError,
        requestLoginCodeError
      };
    case "LOGOUT_REQUEST":
      return {
        ...state,
        loggingOut: true
      };
    case "LOGOUT_SUCCESS":
    case "LOGOUT_FAILURE":
      return {
        ...state,
        loginCodeSending: false,
        loginCodeSent: false,
        loginCodeEntering: false,
        loginCodeEntered: false,
        loggingOut: false
      };
    default:
      return state;
  }
};

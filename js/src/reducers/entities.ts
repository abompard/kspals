import { LogoutAction, EntitiesActionSuccesses } from "../actions/types";
import mergeWith from "lodash/mergeWith";
import {
  IParticipant,
  IPal,
  IEvent,
  IUser,
  INotification,
  ITicketRate,
  IRole,
  IHistory
} from "../api/types";

export interface EntitiesState {
  users: {
    [key: number]: IUser;
  };
  events: {
    [key: string]: IEvent;
  };
  participants: {
    [key: number]: IParticipant;
  };
  pals: {
    [key: number]: IPal;
  };
  notifications: {
    [key: number]: INotification;
  };
  roles: {
    [key: number]: IRole;
  };
  ticket_rates: {
    [key: number]: ITicketRate;
  };
  history: {
    [key: number]: IHistory;
  };
}

export const initialState = {
  users: {},
  events: {},
  participants: {},
  pals: {},
  notifications: {},
  roles: {},
  ticket_rates: {},
  history: {}
};

/*
const overwriteOneLevelDeep = (
  // This won't work because users are overwritten when loading an event
  objValue: any,
  srcValue: any,
  key: string,
  object: any,
  source: any,
  stack: { size: number }
): any => {
  if (stack.size >= 1) {
    // Overwrite entities entirely
    return srcValue;
  }
  return undefined;
};
*/

const overwritePals = (
  // Overwrite a participant's PAL list instead of appending
  objValue: any,
  srcValue: any,
  key: string,
  object: any,
  source: any,
  stack: { size: number }
): any => {
  //console.log(stack, key, source, object, srcValue, objValue);
  if (stack.size >= 2 && Array.isArray(objValue)) {
    return srcValue;
  }
  return undefined;
};

// Updates an entity cache in response to any action with response.entities.
export const entitiesReducer = (
  state: EntitiesState = initialState,
  action: EntitiesActionSuccesses | LogoutAction
) => {
  if (action.type === "LOGOUT_SUCCESS") {
    return initialState;
  }
  action = action as EntitiesActionSuccesses;
  if (action.response && action.response.entities) {
    return mergeWith({}, state, action.response.entities, overwritePals);
  }
  return state;
};

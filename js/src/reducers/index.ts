import { combineReducers } from "redux";
//import { eventReducer, EventState } from "./event";
//import { palsReducer, PalState } from "./pals";
import { currentUserReducer, CurrentUserState } from "./user";

import { uiReducer, UiState } from "./ui";
import { configReducer, ConfigState } from "./config";
import { loginReducer, LoginState } from "./login";
import { entitiesReducer, EntitiesState } from "./entities";
import { currentEventCodeReducer, CurrentEventCodeState } from "./event";
import {
  currentParticipantReducer,
  CurrentParticipantState
} from "./participant";
import { PaginationState, paginationTopReducer } from "./pagination";

/*
const noop = (state = null, action) => {
  return state
}
*/

export interface IState {
  ui: UiState;
  config: ConfigState;
  current: {
    user: CurrentUserState;
    eventCode: CurrentEventCodeState;
    participant: CurrentParticipantState;
  };
  entities: EntitiesState;
  login: LoginState;
  pages: PaginationState;
}

const rootReducer = combineReducers({
  entities: entitiesReducer,
  current: combineReducers({
    user: currentUserReducer,
    eventCode: currentEventCodeReducer,
    participant: currentParticipantReducer
  }),
  ui: uiReducer,
  config: configReducer,
  login: loginReducer,
  pages: paginationTopReducer
});

export default rootReducer;

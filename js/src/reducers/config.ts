import { AllActions } from "../actions/types";

export interface ConfigState {
  logoUrl: string;
  matomoUrl?: string | null;
  matomoId?: number | null;
  socketIOEndpoint: string | null;
}

const initialState = {
  logoUrl: "/",
  matomoUrl: null,
  matomoId: null,
  socketIOEndpoint: null
};

export const configReducer = (
  state: ConfigState = initialState,
  action: AllActions
) => {
  switch (action.type) {
    case "INITIAL_STATE_SUCCESS":
      return {
        ...state,
        ...action.response.config
      };
    default:
      return state;
  }
};

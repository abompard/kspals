import { APIError, BackendValidationError } from "../api/types";

export interface RequestUiState {
  isLoading: boolean;
  error: APIError | null;
}

export interface EntityUiState {
  isLoading: boolean;
  error: string | null;
  hasInitiallyLoaded: boolean;
}

export interface InitialUiState extends RequestUiState {
  hasInitiallyLoaded: boolean;
}

export interface ValidatingEntityUiState extends EntityUiState {
  isLoading: boolean;
  fieldErrors?: BackendValidationError;
}

export const initialEntityUiState = {
  isLoading: false,
  error: null,
  hasInitiallyLoaded: false
};

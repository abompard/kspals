import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { addLocaleData } from "react-intl";
import fr from "react-intl/locale-data/fr";
import date_fr from "date-fns/locale/fr";
import { registerLocale } from "react-datepicker";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import Root from "./containers/Root";
import configureStore from "./store/configureStore";
import * as serviceWorker from "./serviceWorker";
import { Store } from "redux";

//ReactDOM.render(<App />, document.getElementById('root'));

const store = configureStore();
addLocaleData([...fr]);
registerLocale("fr", date_fr);
registerLocale("fr-FR", date_fr);

// FIXME: something better than any here.
function setup(RootComponent: React.FunctionComponent<{ store: Store }>) {
  ReactDOM.render(
    <Router>
      <RootComponent store={store} />
    </Router>,
    document.getElementById("root") as HTMLElement
  );
}

setup(Root);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

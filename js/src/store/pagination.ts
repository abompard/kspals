import { PaginatedEntity } from "../api/types";
import { IState } from "../reducers";
import { EntitiesState } from "../reducers/entities";
import { schemaRegistry } from "../api/schemas";
import { denormalize } from "normalizr";
import { FilterDict } from "../types";

export type Page<T> = {
  number: number;
  count: number | null;
  pageSize: number | null;
  filters: FilterDict;
  ids: Array<number | string>;
  objects: Array<T>;
};

// Pagination

export const getPage = (
  state: IState,
  entity: keyof EntitiesState,
  page: number
): Page<PaginatedEntity> => {
  const pageState = state.pages[entity];
  if (!pageState || !schemaRegistry[entity]) {
    return {
      number: page,
      count: null,
      pageSize: null,
      filters: {},
      ids: [],
      objects: []
    };
  }
  const { pages, ...otherPageState } = pageState;
  return {
    ...otherPageState,
    number: page,
    ids: pages[page] || [],
    objects:
      denormalize(pages[page], schemaRegistry[entity]!.list, state.entities) ||
      []
  };
};

import { denormalize } from "normalizr";
import {
  eventSchema,
  notificationSchema,
  palSchema,
  participantFullSchema,
  participantSchema,
  userSchema
} from "../api/schemas";
import { IEvent, INotification, IPal, IParticipant, IUser } from "../api/types";
import { IState } from "../reducers";

/*
 * Shortcuts to access the store
 */

/* User */

export const getUser = (state: IState): IUser | null => {
  if (state.current.user === null) {
    return null;
  }
  return denormalize(state.current.user, userSchema, state.entities) || null;
};

export const getUserToken = (state: IState): string | null => {
  const user = getUser(state);
  return user === null ? null : user.token;
};

/* Event */

export const getEvent = (state: IState): IEvent | null => {
  if (state.current.eventCode === null) {
    return null;
  }
  return (
    denormalize(state.current.eventCode, eventSchema, state.entities) || null
  );
};

export const getEventUrl = (state: IState): string | null => {
  const event = getEvent(state);
  return event === null ? null : event.url;
};

/* Participants */

export const getCurrentParticipant = (state: IState): IParticipant | null => {
  if (state.current.participant === null) {
    return null;
  }
  return (
    denormalize(state.current.participant, participantSchema, state.entities) ||
    null
  );
};

export const getParticipant = (
  state: IState,
  participantId: number,
  full: boolean = false
): IParticipant | null =>
  denormalize(
    participantId,
    full ? participantFullSchema : participantSchema,
    state.entities
  ) || null;

export const getParticipantsList = (
  event: IEvent | null
): Array<IParticipant> => {
  return (event ? event.participants || [] : []).filter(
    participant => participant.profile
  );
};

export const getPal = (state: IState, palId: number): IPal | null =>
  denormalize(palId, palSchema, state.entities) || null;

/* Notifications */

export const getNotification = (
  state: IState,
  notificationId: number
): INotification | null =>
  denormalize(notificationId, notificationSchema, state.entities) || null;

/*
 * Get refined data
 */

export const getParticipantsPendingApproval = (
  participants: Array<IParticipant>
): Array<IParticipant> =>
  participants.filter(participant => participant.approved === null);

export const getParticipantsWithoutPal = (
  participants: Array<IParticipant>
): Array<IParticipant> =>
  participants.filter(
    participant => participant.approved && participant.pals.length === 0
  );

export const getParticipantsNonMutualPals = (
  participants: Array<IParticipant>
): Array<IParticipant> =>
  participants.filter(
    participant =>
      participant.approved &&
      !participant.mutual_pal_group &&
      participant.pals.length !== 0
  );

export const getParticipantsNoTicket = (
  participants: Array<IParticipant>
): Array<IParticipant> =>
  participants.filter(participant => participant.ticket_status === "URL_SENT");

export const getParticipantsTicketingReady = (
  state: IState
): Array<IParticipant> => {
  const event = getEvent(state);
  return getParticipantsList(event).filter(
    participant =>
      participant.valid_pal_group && participant.ticket_status === "NOT_READY"
  );
};

/*
 * Authorization
 */

export const isAdminReadOnly = (state: IState): boolean => {
  const event = getEvent(state);
  const user = getUser(state);
  if (!user || !event || !event.users_can_change) {
    return true;
  }
  return !event.users_can_change.map(u => u.id).includes(user.id);
};

/* Generic utils */

/* Generate a random string for a cheap UUID */
export const getRandomString = () =>
  Math.random()
    .toString(36)
    .substring(2);

/* Types & Interfaces */

export type AddNull<T, K extends keyof T> = {
  [P in keyof T]: P extends K ? T[P] | null : T[P];
};

/* https://stackoverflow.com/a/59939017 */
export const setElementValue = (element: HTMLInputElement, value: string) => {
  const valueSetter = Object.getOwnPropertyDescriptor(element, "value")!.set;
  const prototype = Object.getPrototypeOf(element);
  const prototypeValueSetter = Object.getOwnPropertyDescriptor(
    prototype,
    "value"
  )!.set;

  if (valueSetter && valueSetter !== prototypeValueSetter) {
    prototypeValueSetter!.call(element, value);
  } else {
    valueSetter!.call(element, value);
  }

  element.dispatchEvent(new Event("input", { bubbles: true }));
};

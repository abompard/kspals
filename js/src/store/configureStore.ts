import { createStore, applyMiddleware, compose, StoreEnhancer } from "redux";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import rootReducer from "../reducers";

interface WindowWithReduxDevTools extends Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any;
}

interface ModuleWithHotReload extends NodeModule {
  hot?: any;
}

let enhancers: StoreEnhancer;

if (process.env.NODE_ENV !== "production") {
  const composeEnhancers: any =
    (window as WindowWithReduxDevTools).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ||
    compose;
  enhancers = composeEnhancers(applyMiddleware(thunk, createLogger()));
} else {
  enhancers = applyMiddleware(thunk);
}

const configureStore = (preloadedState?: any) => {
  const store = createStore(rootReducer, preloadedState, enhancers);

  if (
    process.env.NODE_ENV !== "production" &&
    (module as ModuleWithHotReload).hot
  ) {
    // Enable Webpack hot module replacement for reducers
    (module as ModuleWithHotReload).hot.accept("../reducers", () => {
      store.replaceReducer(rootReducer);
    });
  }

  return store;
};

export default configureStore;

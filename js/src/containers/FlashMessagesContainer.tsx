import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  deleteFlashMessage,
  flagFlashMessageAsShown,
  pruneFlashMessages
} from "../actions/flashMessages";
import FlashMessages, { Props } from "../components/FlashMessages";
import { IState } from "../reducers";

class FlashMessagesContainer extends Component<Props> {
  render() {
    return <FlashMessages {...this.props} />;
  }
}

const mapStateToProps = (state: IState) => ({
  messages: state.ui.flashMessages
});
export default withRouter(
  connect(mapStateToProps, {
    deleteFlashMessage,
    pruneFlashMessages,
    flagFlashMessageAsShown
  })(FlashMessagesContainer)
);

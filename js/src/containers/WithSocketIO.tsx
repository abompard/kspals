import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { IState } from "../reducers";
import { SocketIOContext } from "./SocketIOProvider";

function getDisplayName(WrappedComponent: React.ComponentType<any>) {
  return WrappedComponent.displayName || WrappedComponent.name || "Component";
}

type PropsWithActive = {
  socketIOActive: boolean;
  [propName: string]: any;
};

/*
import { Socket } from "socket.io-client";
type WrappedProps = PropsWithActive & {
  socketIO: Socket | null;
}
*/

function withSocketIO(
  WrappedComponent: React.ComponentType<any>
): React.ComponentType<any> {
  class WithSocketIO extends Component<any> {
    render() {
      return (
        <SocketIOContext.Consumer>
          {value => <WrappedComponent socketIO={value} {...this.props} />}
        </SocketIOContext.Consumer>
      );
    }
  }
  (WithSocketIO as React.ComponentType<
    PropsWithActive
  >).displayName = `WithSocketIO(${getDisplayName(WrappedComponent)})`;
  return WithSocketIO;
}

const mapStateToProps = (state: IState) => {
  return {
    socketIOActive: state.ui.socketIOActive
  };
};

export default compose(connect(mapStateToProps), withSocketIO);

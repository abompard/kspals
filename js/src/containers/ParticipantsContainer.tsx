import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { loadOneParticipant, loadParticipants } from "../actions/admin";
import { IEvent, IParticipant } from "../api/types";
import FAIcon from "../components/FAIcon";
import { IState } from "../reducers";
import { getEvent, getParticipantsList } from "../store/utils";
import SocketIOWatcher from "./SocketIOWatcher";

interface Props extends RouteComponentProps<{}> {
  event: IEvent;
  isLoading: boolean;
  error: string | null;
  participants: Array<IParticipant>;
  socketIOActive: boolean;
  loadParticipants(): void;
  loadOneParticipant(participant: IParticipant): void;
  children?: React.ReactNode;
}

class ParticipantsContainer extends React.Component<Props> {
  unlisten: (() => void) | null = null;

  componentDidMount() {
    this.props.loadParticipants();
    if (!this.props.socketIOActive) {
      this.unlisten = this.props.history.listen((location, action) => {
        this.props.loadParticipants();
      });
    }
  }

  componentWillUnmount() {
    this.unlisten && this.unlisten();
  }

  onParticipantUpdated = (data: any) => {
    if (data.id) {
      const participant = this.props.participants.filter(id => data.id);
      if (participant.length > 0) {
        this.props.loadOneParticipant(participant[0]);
      }
    }
  };

  render() {
    if (this.props.error) {
      return (
        <div className="alert alert-danger">
          Erreur:
          <code className="ms-2">{this.props.error}</code>
        </div>
      );
    }
    return (
      <div className="position-relative">
        {this.props.isLoading && (
          <div className="position-absolute text-muted" style={{ right: 0 }}>
            <FAIcon name="spinner" className="fa-spin me-2" />
            Loading...
          </div>
        )}
        <SocketIOWatcher
          model="event_participants"
          instanceIds={[this.props.event.code]}
          onUpdate={this.onParticipantUpdated}
        />
        {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  return {
    isLoading: state.ui.participant.isLoading,
    error: state.ui.participant.error,
    event,
    participants: getParticipantsList(event),
    socketIOActive: state.ui.socketIOActive
  };
};

export default withRouter(
  connect(mapStateToProps, {
    loadParticipants,
    loadOneParticipant
  })(ParticipantsContainer)
);

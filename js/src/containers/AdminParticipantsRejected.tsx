import React, { Component } from "react";
import { connect } from "react-redux";
import { sortByAttrs } from "../api/utils";
import {
  AdminParticipantsRejected,
  Props as ComponentProps
} from "../components/AdminParticipantsRejected";
import { IState } from "../reducers";
import { getEvent, getParticipantsList } from "../store/utils";

interface Props extends ComponentProps {
  isLoading: boolean;
}

class AdminParticipantsRejectedContainer extends Component<Props> {
  render() {
    const { isLoading, ...props } = this.props;
    if (isLoading) {
      return null;
    }
    return <AdminParticipantsRejected {...props} />;
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  return {
    event,
    participants: getParticipantsList(event)
      .filter(participant => participant.approved === false)
      .sort(sortByAttrs(["profile.date_registered"])),
    isLoading: state.ui.participant.isLoading
  };
};

export default connect(mapStateToProps)(AdminParticipantsRejectedContainer);

import React, { Component } from "react";
import { connect } from "react-redux";
import { sortByAttrs } from "../api/utils";
import {
  AdminParticipantsDrinks,
  Props as ComponentProps
} from "../components/AdminParticipantsDrinks";
import { IState } from "../reducers";
import { getEvent, getParticipantsList } from "../store/utils";

interface Props extends ComponentProps {
  isLoading: boolean;
}

class AdminParticipantsDrinksContainer extends Component<Props> {
  render() {
    const { isLoading, ...props } = this.props;
    if (isLoading) {
      return null;
    }
    return <AdminParticipantsDrinks {...props} />;
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  return {
    event,
    participants: getParticipantsList(event)
      .filter(participant => participant.drinks > 0)
      .sort(sortByAttrs(["drinks"])),
    isLoading: state.ui.participant.isLoading
  };
};

export default connect(mapStateToProps)(AdminParticipantsDrinksContainer);

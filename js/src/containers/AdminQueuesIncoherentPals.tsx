import React, { Component } from "react";
import { connect } from "react-redux";
import { IEvent, IParticipant } from "../api/types";
import { sortByAttr } from "../api/utils";
import { AdminQueuesIncoherentPals } from "../components/AdminQueuesIncoherentPals";
import { IState } from "../reducers";
import {
  getEvent,
  getParticipantsList,
  getParticipantsNonMutualPals,
  getParticipantsWithoutPal
} from "../store/utils";

interface Props {
  event: IEvent;
  participantsWithoutPal: Array<IParticipant>;
  participantsNonMutualPals: Array<IParticipant>;
  isLoading: boolean;
}

class AdminQueuesIncoherentPalsContainer extends Component<Props> {
  render() {
    if (this.props.isLoading) {
      return null;
    }
    return (
      <AdminQueuesIncoherentPals
        event={this.props.event}
        participantsWithoutPal={this.props.participantsWithoutPal}
        participantsNonMutualPals={this.props.participantsNonMutualPals}
      />
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  const participants = getParticipantsList(event);
  return {
    event,
    participantsWithoutPal: getParticipantsWithoutPal(participants).sort(
      sortByAttr("profile.date_registered")
    ),
    participantsNonMutualPals: getParticipantsNonMutualPals(participants).sort(
      sortByAttr("profile.date_registered")
    ),
    isLoading: state.ui.participant.isLoading
  };
};

export default connect(mapStateToProps)(AdminQueuesIncoherentPalsContainer);

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  RegistrationClosedValidParticipant,
  RegistrationClosedInvalidParticipant
} from "../components/RegistrationClosed";
import Header from "../components/Header";
import PalsFormContainer from "./PalsFormContainer";
import StatusSwitch from "./StatusSwitch";
import { IEvent, IParticipant } from "../api/types";
import { IState } from "../reducers";
import { getCurrentParticipant, getEvent } from "../store/utils";

interface Props {
  event: IEvent;
  participant: IParticipant;
}

class RegistrationOpenSwitch extends Component<Props> {
  render() {
    if (!this.props.event.is_registration_open) {
      if (!this.props.participant.valid_pal_group) {
        return <RegistrationClosedInvalidParticipant />;
      } else {
        return (
          <RegistrationClosedValidParticipant
            pals={this.props.participant.pals}
          />
        );
      }
    }
    return (
      <div>
        <Header maxDate={this.props.event.date_reg_close} />
        <PalsFormContainer />
        <StatusSwitch />
      </div>
    );
  }
}

const mapStateToProps = (state: IState) => {
  const participant = getCurrentParticipant(state)!;
  return {
    event: getEvent(state)!,
    participant
  };
};

export default connect(mapStateToProps)(RegistrationOpenSwitch);

import React, { Component } from "react";
import { Route, RouteComponentProps, Switch } from "react-router";
import AdminEventInfo from "./AdminEventInfo";
import AdminHistory from "./AdminHistory";
import AdminParticipants from "./AdminParticipants";
import AdminParticipantsCheckin from "./AdminParticipantsCheckIn";
import AdminParticipantsDrinks from "./AdminParticipantsDrinks";
import AdminParticipantsExport from "./AdminParticipantsExport";
import AdminParticipantsProfile from "./AdminParticipantsProfile";
import AdminParticipantsProfiles from "./AdminParticipantsProfiles";
import AdminParticipantsRejected from "./AdminParticipantsRejected";
import AdminQueuesIncoherentPals from "./AdminQueuesIncoherentPals";
import AdminQueuesNoTicket from "./AdminQueuesNoTicket";
import AdminQueuesNotification from "./AdminQueuesNotification";
import AdminQueuesNotifications from "./AdminQueuesNotifications";
import AdminQueuesValidation from "./AdminQueuesValidation";
import EventContainer from "./EventContainer";
import NotificationsLoader from "./NotificationsLoader";
import ParticipantsContainer from "./ParticipantsContainer";

interface urlParams {
  eventcode: string;
  participant?: string;
  notification?: string;
}

export default class AdminPage extends Component<
  RouteComponentProps<urlParams>
> {
  render() {
    return (
      <EventContainer>
        <ParticipantsContainer>
          <Switch>
            <Route
              exact
              path={`${this.props.match.url}`}
              component={AdminEventInfo}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/participants/`}
              component={AdminParticipants}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/participants/profiles/`}
              component={AdminParticipantsProfiles}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/participants/profiles/:participant/`}
              component={AdminParticipantsProfile}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/participants/export/`}
              component={AdminParticipantsExport}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/participants/drinks/`}
              component={AdminParticipantsDrinks}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/participants/check-in/`}
              component={AdminParticipantsCheckin}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/queues/validation/`}
              component={AdminQueuesValidation}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/queues/validation/:participant/`}
              component={AdminQueuesValidation}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/queues/incoherent-pals/`}
              component={AdminQueuesIncoherentPals}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/queues/no-ticket/`}
              component={AdminQueuesNoTicket}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/queues/notifications/`}
              component={AdminQueuesNotifications}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/queues/notifications/:notification/`}
              component={AdminQueuesNotification}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/rejected-participants/`}
              component={AdminParticipantsRejected}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
            <Route
              exact
              path={`${this.props.match.url}/history/`}
              component={AdminHistory}
              //eventCode={this.props.match.params.eventcode}
              //requireOrg={true}
            />
          </Switch>
          <NotificationsLoader />
        </ParticipantsContainer>
      </EventContainer>
    );
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import { IEvent, IParticipant } from "../api/types";
import { sortByAttr } from "../api/utils";
import { AdminParticipantsProfiles } from "../components/AdminParticipantsProfiles";
import { IState } from "../reducers";
import { getEvent, getParticipantsList } from "../store/utils";

interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
  isLoading: boolean;
}

class AdminParticipantsProfilesContainer extends Component<Props> {
  render() {
    if (this.props.isLoading) {
      return null;
    }
    return (
      <AdminParticipantsProfiles
        event={this.props.event}
        participants={this.props.participants}
      />
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  return {
    event,
    participants: getParticipantsList(event)
      .filter(participant => participant.has_ticket)
      .sort(sortByAttr("profile.full_name")),
    isLoading: state.ui.participant.isLoading
  };
};

export default connect(mapStateToProps)(AdminParticipantsProfilesContainer);

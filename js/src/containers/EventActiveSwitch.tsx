import * as React from "react";
import { connect } from "react-redux";
import {
  EventPassed,
  RegistrationNotOpenedYet
} from "../components/RegistrationClosed";
import { IState } from "../reducers";
import { IEvent } from "../api/types";
import { getEvent } from "../store/utils";

interface Props {
  event: IEvent;
  children?: React.ReactNode;
}

class EventActiveSwitch extends React.Component<Props> {
  render() {
    if (!this.props.event.is_active) {
      return <EventPassed event={this.props.event} />;
    }
    if (!this.props.event.is_registration_open) {
      const dateRegOpen = new Date(this.props.event.date_reg_open);
      const now = new Date();
      if (now < dateRegOpen) {
        return <RegistrationNotOpenedYet event={this.props.event} />;
      }
    }
    return this.props.children;
  }
}

const mapStateToProps = (state: IState) => ({
  event: getEvent(state)!
});

export default connect(mapStateToProps)(EventActiveSwitch);

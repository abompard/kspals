import * as React from "react";
import { connect } from "react-redux";
import { logout } from "../actions/user";
import { IEvent, IUser } from "../api/types";
import TopBar from "../components/TopBar";
import { IState } from "../reducers";
import { getEvent, getUser } from "../store/utils";

interface Props {
  event: IEvent | null;
  user: IUser | null;
  logoUrl: string;
  isOrganizer: boolean;
  logout(): void;
}

class TopBarContainer extends React.Component<Props> {
  render() {
    let links = [];
    if (this.props.event) {
      links.push({
        text: "Mes PALs",
        href: `/${this.props.event.code}/`,
        exact: true
      });
      links.push({
        text: "Mon profil",
        href: `/${this.props.event.code}/profile/`,
        exact: true
      });
      if (this.props.isOrganizer) {
        links.push({
          text: "Administration",
          href: `/admin/${this.props.event.code}/`
        });
      }
    }
    return (
      <TopBar
        user={this.props.user}
        event={this.props.event}
        logoUrl={this.props.logoUrl}
        onLogout={this.props.logout}
        links={links}
      />
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  const user = getUser(state);
  const organizers = (event && event.users_can_view) || [];
  const isOrganizer = (event &&
    user &&
    organizers.map(o => o.id).includes(user.id)) as boolean;
  return {
    event,
    user,
    logoUrl: state.config.logoUrl,
    isOrganizer
  };
};
export default connect(mapStateToProps, {
  logout
})(TopBarContainer);

import React, { Component } from "react";
import { connect } from "react-redux";
import Footer, { Props } from "../components/Footer";
import { IState } from "../reducers";

class FooterContainer extends Component<Props> {
  render() {
    return (
      <Footer
        matomoUrl={this.props.matomoUrl}
        matomoId={this.props.matomoId}
        history={this.props.history}
      />
    );
  }
}

const mapStateToProps = (state: IState) => ({
  matomoUrl: state.config.matomoUrl,
  matomoId: state.config.matomoId
});
export default connect(mapStateToProps)(FooterContainer);

import * as React from "react";
import { connect } from "react-redux";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { Dispatch } from "redux";
import AccessForbidden from "../components/AccessForbidden";
import { IState } from "../reducers";
import { getEvent, getUser } from "../store/utils";

interface Args {
  component: React.ComponentType<any>;
  isAuthenticated: boolean;
  isAuthorized: boolean;
  dispatch: Dispatch;
  rest?: RouteProps; // There can be other custom properties but there will always be at least the Route properties
}

function PrivateRoute({
  component: Component,
  isAuthenticated,
  isAuthorized,
  dispatch,
  ...rest
}: Args): React.ReactElement<Route> {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          isAuthorized ? (
            <Component {...props} />
          ) : (
            <AccessForbidden />
          )
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}

interface OwnProps extends RouteProps {
  component: React.ComponentType<any>;
  requireOrg?: boolean;
}

const mapStateToProps = (state: IState, ownProps: OwnProps) => {
  const getAuth = () => {
    if (!state.current.user) {
      return false;
    }
    const user = getUser(state);
    if (!user || user.token === null) {
      return false;
    }
    return true;
  };
  const isAuth = getAuth();
  let isAuthz = true;
  if (isAuth && ownProps.requireOrg) {
    const user = getUser(state);
    const event = getEvent(state);
    const organizers = (event && event.users_can_view) || [];
    if (user && event && !organizers.map(o => o.id).includes(user.id)) {
      isAuthz = false;
    }
  }
  return {
    isAuthenticated: isAuth,
    isAuthorized: isAuthz
  };
};

export default connect(mapStateToProps)(PrivateRoute);

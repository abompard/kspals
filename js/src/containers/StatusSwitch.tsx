import * as React from "react";
import { connect } from "react-redux";
import Status from "../components/Status";
import { IEvent, IParticipant } from "../api/types";
import { IState } from "../reducers";
import { getEvent, getCurrentParticipant } from "../store/utils";

type StatusCode =
  | "MUTUAL_MISSING"
  | "OTHER_PALS_NOT_MUTUAL"
  | "WAITING_LIST"
  | "APPROVAL_MISSING"
  | "PAL_APPROVAL_MISSING"
  | "TICKET_MISSING"
  | "PAL_TICKETS_MISSING"
  | "ALL_TICKETS"
  | null;

interface Props {
  status: StatusCode;
  participant: IParticipant;
  event: IEvent;
}

const getStatusCode = (
  participant: IParticipant,
  event: IEvent
): StatusCode => {
  if (!participant.mutual_pal_group) {
    const nonMutualPals = participant.pals.filter(pal => !pal.is_mutual);
    if (participant.pals.length > 0 && nonMutualPals.length === 0) {
      // XXX: if we ever change the max pal number, this must be adapted.
      if (participant.pals.length === 1) {
        //return "OTHER_PAL_MISSING";
        return "MUTUAL_MISSING";
      } else {
        return "OTHER_PALS_NOT_MUTUAL";
      }
    } else {
      return "MUTUAL_MISSING";
    }
  }
  if ((event.is_on_waiting_list || event.is_full) && !participant.approved) {
    return "WAITING_LIST";
  }
  if (!participant.approved) {
    return "APPROVAL_MISSING";
  }
  if (!participant.approved_pal_group) {
    return "PAL_APPROVAL_MISSING";
  }
  if (!participant.has_ticket) {
    return "TICKET_MISSING";
  }
  const pals_without_tickets = participant.pals.filter(pal => !pal.has_ticket);
  if (pals_without_tickets.length > 0) {
    return "PAL_TICKETS_MISSING";
  }
  return "ALL_TICKETS";
};

class StatusSwitch extends React.Component<Props> {
  getColorAndContent() {
    switch (this.props.status) {
      // case "INVALID_EMAIL":
      //   return {
      //     color: "danger",
      //     //content: "Adresse email invalide, veuillez corriger.",
      //     content: null
      //   };
      // case "OTHER_PAL_MISSING":
      //   return {
      //     color: "warning",
      //     content:
      //       "L'inscription ne sera valide que lorsque tous les PALs se seront désigné⋅es les un⋅es les autres. Votre PAL vous a bien désigné⋅e, mais a aussi désigné une autre personne, que vous devez donc vous aussi désigner."
      //   };
      case "OTHER_PALS_NOT_MUTUAL":
        return {
          color: "warning",
          content:
            "L'inscription ne sera valide que lorsque tous les PALs se seront désigné⋅es les un⋅es les autres. Vos PALs vous ont bien désigné⋅e, mais ne se sont pas désigné⋅es entre elleux."
        };
      case "MUTUAL_MISSING":
        return {
          color: "warning",
          content:
            "L'inscription ne sera valide que lorsque tous les PALs se seront désigné⋅es les un⋅es les autres."
        };
      case "WAITING_LIST":
        return {
          color: "primary",
          content: (
            <span>
              <strong>Votre groupe de PALs est valide !</strong> L'évènement est
              complet, vous êtes donc sur liste d'attente. Vous aurez accès à la
              billetterie dès que des places se libèreront.
            </span>
          )
        };
      case "APPROVAL_MISSING":
        return {
          color: "primary",
          content:
            "Vous aurez accès à la billetterie dès que l'organisation du Kinky Salon aura validé votre pré-inscription."
        };
      case "PAL_APPROVAL_MISSING":
        return {
          color: "primary",
          content:
            "Vous aurez accès à la billetterie dès que l'organisation du Kinky Salon aura validé la pré-inscription de vos PALs."
        };
      case "TICKET_MISSING":
        const ticket_url = this.props.participant.ticket_url;
        if (!ticket_url) {
          return {
            color: "warning",
            content:
              "Votre groupe de PALs est valide, mais la billetterie n'est pas encore prête. Vous pourrez acheter vos billets dès qu'elle le sera."
          };
        }
        return {
          color: "primary",
          content: (
            <React.Fragment>
              <p className="mb-0">
                <strong>Votre groupe de PALs est valide !</strong> Vous pouvez
                maintenant acheter vos billets{" "}
                <a href={ticket_url} className="alert-link">
                  en cliquant sur ce lien
                </a>
                .
              </p>
              {this.props.participant.ticket_coupon ? (
                <p className="mb-0">
                  Vous bénéficiez d'un tarif préférentiel, utilisez le code{" "}
                  <code style={{ fontWeight: "bold", fontSize: "105%" }}>
                    {this.props.participant.ticket_coupon}
                  </code>{" "}
                  pour obtenir votre réduction.
                </p>
              ) : null}
            </React.Fragment>
          )
        };
      case "PAL_TICKETS_MISSING":
        return {
          color: "primary",
          content: (
            <span>
              <strong>Votre groupe de PALs est valide !</strong> Vous avez bien
              acheté votre ticket, mais au moins un de vos PALs{" "}
              <strong>ne l'a pas fait</strong>. Rappelez-lui de le faire au plus
              vite.
            </span>
          )
        };
      case "ALL_TICKETS":
        return {
          color: "success",
          content: (
            <span>
              <strong>
                Votre groupe de PALs est valide, et vos tickets on bien été
                achetés !
              </strong>{" "}
              N'oubliez pas que vous devez vous présenter avec votre PAL pour
              pouvoir entrer, et vous devez partir ensemble.
            </span>
          )
        };
      default:
        return {
          color: "info",
          content: null
        };
    }
  }

  render() {
    if (!this.props.status) {
      return null;
    }
    const { color, content } = this.getColorAndContent();
    if (!content) {
      return null;
    }
    return <Status color={color} content={content} />;
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  const participant = getCurrentParticipant(state)!;
  return {
    status: getStatusCode(participant, event),
    participant,
    event
  };
};

export default connect(mapStateToProps)(StatusSwitch);

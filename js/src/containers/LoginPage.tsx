import * as React from "react";
import { connect } from "react-redux";
import { Redirect, RouteComponentProps, useLocation } from "react-router-dom";
import { requestLoginCode, requestToken } from "../actions/user";
import LoginForms from "../components/LoginForms";
import { IState } from "../reducers";
import { getUserToken } from "../store/utils";

type Params = { from: string };

interface Props extends RouteComponentProps<Params> {
  loginCodeSending: boolean;
  loginCodeSent: boolean;
  loginCodeEntering: boolean;
  loginCodeEntered: boolean;
  isAuthenticated: boolean;
  requestLoginCode(email: string): void;
  requestToken(email: string, code: string): void;
  loginCodeError: string | null;
  tokenError: string | null;
}

function LoginPage(props: Props) {
  const location = useLocation<Params>();
  const { from } = location.state || { from: { pathname: "/" } };
  //const { from } = this.props.location.state || { from: { pathname: "/" } };
  if (props.isAuthenticated || props.loginCodeEntered) {
    return <Redirect to={from} />;
  }
  return (
    <LoginForms
      onEmailSubmit={props.requestLoginCode}
      onCodeSubmit={props.requestToken}
      {...props}
    />
  );
}

const mapStateToProps = (state: IState) => ({
  loginCodeSending: state.login.loginCodeSending,
  loginCodeSent: state.login.loginCodeSent,
  loginCodeEntering: state.login.loginCodeEntering,
  loginCodeEntered: state.login.loginCodeEntered,
  loginCodeError: state.login.requestLoginCodeError,
  tokenError: state.login.requestTokenError,
  isAuthenticated: getUserToken(state) !== null
});

export default connect(mapStateToProps, {
  requestLoginCode,
  requestToken
})(LoginPage);

import React, { Component } from "react";
import { connect } from "react-redux";
import { IState } from "../reducers";
import { getEvent, getNotification, isAdminReadOnly } from "../store/utils";

import { match } from "react-router-dom";
import { validateNotification, loadNotifications } from "../actions/admin";
import {
  AdminQueuesNotification,
  Props as ComponentProps
} from "../components/AdminQueuesNotification";

interface Props extends ComponentProps {
  loadNotifications(): void;
}

class AdminQueuesNotificationContainer extends Component<Props> {
  componentDidMount() {
    if (!this.props.notification && !this.props.isLoading) {
      this.props.loadNotifications();
    }
  }
  render() {
    if (!this.props.notification && this.props.isLoading) {
      return null;
    }
    return (
      <AdminQueuesNotification
        event={this.props.event}
        notification={this.props.notification}
        validateNotification={this.props.validateNotification}
        error={this.props.error}
        fieldErrors={this.props.fieldErrors}
        isLoading={this.props.isLoading}
        readOnly={this.props.readOnly}
      />
    );
  }
}

const mapStateToProps = (
  state: IState,
  ownProps: { match: match<{ notification: string }> }
) => {
  const notificationId = parseInt(ownProps.match.params.notification, 10);
  return {
    event: getEvent(state)!,
    notification: getNotification(state, notificationId),
    isLoading: state.ui.notification.isLoading,
    error: state.ui.notification.error,
    fieldErrors: state.ui.notification.fieldErrors,
    readOnly: isAdminReadOnly(state)
  };
};

export default connect(
  mapStateToProps,
  {
    validateNotification,
    loadNotifications
  }
)(AdminQueuesNotificationContainer);

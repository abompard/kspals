import React, { Component } from "react";
import ParticipantContainer from "./ParticipantContainer";
import RegistrationOpenSwitch from "./RegistrationOpenSwitch";
import { RouteComponentProps } from "react-router";

export default class EventPage extends Component<RouteComponentProps> {
  render() {
    return (
      <ParticipantContainer>
        <RegistrationOpenSwitch />
      </ParticipantContainer>
    );
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  registerParticipant,
  loadParticipant,
  cancelParticipant
} from "../actions/participant";
import ParticipantForm, {
  Props as ComponentProps
} from "../components/ParticipantForm";
import { IState } from "../reducers";
import { getUser, getEvent, getCurrentParticipant } from "../store/utils";
import { EventFull } from "../components/RegistrationClosed";
import { addFlashMessage } from "../actions/flashMessages";

interface Props extends ComponentProps {
  loadParticipant(): void;
}

class ParticipantProfile extends Component<Props> {
  componentDidMount() {
    this.props.loadParticipant();
  }
  render() {
    if (
      this.props.event.is_full &&
      this.props.participant === null &&
      !this.props.isParticipantLoading
    ) {
      return <EventFull />;
    }
    const { loadParticipant, ...props } = this.props;
    /*
      The key prop below is used to recreate the component when the participant is loaded.
      https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html#recommendation-fully-uncontrolled-component-with-a-key
     */
    return (
      <ParticipantForm
        {...props}
        key={this.props.participant ? this.props.participant.id : undefined}
      />
    );
  }
}

const mapStateToProps = (state: IState) => ({
  event: getEvent(state)!, // We're behind a EventContainer
  user: getUser(state)!, // We're behind a PrivateRoute
  participant: getCurrentParticipant(state),
  isParticipantLoading: state.ui.participant.isLoading
});

export default connect(
  mapStateToProps,
  {
    registerParticipant,
    addFlashMessage,
    loadParticipant,
    cancelParticipant
  }
)(ParticipantProfile);

import React, { Component } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { updateParticipant } from "../actions/admin";
import { IParticipant } from "../api/types";
import { sortByAttr, sortByAttrs } from "../api/utils";
import {
  AdminQueuesValidationProfile,
  CommonProps
} from "../components/AdminQueuesValidationProfile";
import { IState } from "../reducers";
import {
  getEvent,
  getParticipantsList,
  getUserToken,
  isAdminReadOnly
} from "../store/utils";

interface Props extends CommonProps, RouteComponentProps<{}> {
  participant: IParticipant;
  isLoading: boolean;
  approvalQueue: Array<IParticipant>;
  fullApprovalQueue: Array<IParticipant>;
  allParticipants: Array<IParticipant>;
  validationMax: number;
}

class AdminQueuesValidationProfileContainer extends Component<Props> {
  timeout: number | null = null;

  componentWillUnmount = () => {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  };

  render() {
    if (this.props.isLoading) {
      return null;
    }
    if (!this.props.participant) {
      return <p className="text-danger">This participant does not exist!</p>;
    }

    // Find next and prev
    let prevParticipant: IParticipant | null = null;
    let nextParticipant: IParticipant | null = null;
    const approvalQueueIds = this.props.approvalQueue.map(
      participant => participant.id
    );
    const fullApprovalQueueIds = this.props.fullApprovalQueue.map(
      participant => participant.id
    );
    const allParticipantIds = this.props.allParticipants.map(
      participant => participant.id
    );
    const currentIndex = allParticipantIds.indexOf(this.props.participant.id);
    const isInApprovalQueue = (p: IParticipant): boolean =>
      approvalQueueIds.indexOf(p.id) !== -1;
    if (currentIndex !== -1) {
      const prevList = this.props.allParticipants
        .slice(0, currentIndex)
        .filter(isInApprovalQueue);
      const nextList = this.props.allParticipants
        .slice(currentIndex + 1)
        .filter(isInApprovalQueue);
      prevParticipant = prevList[prevList.length - 1] || null;
      nextParticipant = nextList[0] || null;
    }

    // Define the PAL group
    const palGroup = [
      this.props.participant,
      ...this.props.participant.pals.map(pal => pal.pal)
    ].filter(participant => participant); // Pals can be undefined if not fully loaded.
    // If everybody is approved, move next or back.
    const nonApproved = palGroup.filter(
      participant => participant.approved === null
    );
    if (nonApproved.length === 0) {
      let redirectUrl = `/admin/${this.props.event.code}/queues/validation/`;
      if (nextParticipant) {
        redirectUrl += nextParticipant.id + "/";
      }
      this.timeout = window.setTimeout(
        () => this.props.history.push(redirectUrl),
        1000
      );
    }
    return (
      <AdminQueuesValidationProfile
        {...this.props}
        palGroup={palGroup}
        prevParticipant={prevParticipant}
        nextParticipant={nextParticipant}
        approvalQueueIds={fullApprovalQueueIds}
        willOverflow={palGroup.length > this.props.validationMax}
      />
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state);
  return {
    allParticipants: getParticipantsList(event).sort(
      sortByAttrs(["pal_group_registration_date", "profile.date_registered"])
    ),
    roles: (event ? event.roles || [] : [])
      .sort(sortByAttr("default"))
      .reverse(),
    ticketRates: (event ? event.ticket_rates || [] : [])
      .sort(sortByAttr("default"))
      .reverse(),
    isLoading: state.ui.participant.isLoading,
    readOnly: isAdminReadOnly(state),
    userToken: getUserToken(state)
  };
};

export default withRouter(
  connect(mapStateToProps, {
    updateParticipant
  })(AdminQueuesValidationProfileContainer)
);

import React, { lazy, Suspense } from "react";
import { IntlProvider } from "react-intl";
import { Provider } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { Store } from "redux";
import AdminTopBarContainer from "./AdminTopBarContainer";
import EventPage from "./EventPage";
import FlashMessagesContainer from "./FlashMessagesContainer";
import FooterContainer from "./FooterContainer";
import Home from "./Home";
import InitialStateContainer from "./InitialStateContainer";
import LoginPage from "./LoginPage";
import PrivateRoute from "./PrivateRoute";
import ScrollToTop from "./Scroll";
import SocketIOProvider from "./SocketIOProvider";
import TopBarContainer from "./TopBarContainer";

// Code splitting
const AdminEventPage = lazy(() => import("./AdminEventPage"));
const PersonalData = lazy(() => import("../components/PersonalData"));

const Root = ({ store }: { store: Store }) => (
  <Provider store={store}>
    <IntlProvider locale={navigator.language}>
      <InitialStateContainer>
        <SocketIOProvider>
          <Route
            children={({ location }) => (
              <div
                className={`container${
                  location.pathname.indexOf("/admin/") === 0 ? "-fluid" : ""
                } bg-white pb-2`}
              >
                <Suspense
                  fallback={
                    <div className="text-center mt-3">
                      <i className="fa fa-spinner fa-pulse fa-lg" />
                    </div>
                  }
                >
                  <Switch>
                    <Route
                      path="/admin/:eventcode/"
                      component={AdminTopBarContainer}
                    />
                    <Route path="/" component={TopBarContainer} />
                  </Switch>
                  <FlashMessagesContainer />
                  <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/login" component={LoginPage} />
                    <Route path="/personal-data/" component={PersonalData} />
                    <PrivateRoute
                      path="/admin/:eventcode/"
                      component={AdminEventPage}
                      requireOrg={true}
                    />
                    <PrivateRoute path="/:eventcode" component={EventPage} />
                  </Switch>
                </Suspense>
                <Route path="/" component={FooterContainer} />
                <ScrollToTop />
              </div>
            )}
          />
        </SocketIOProvider>
      </InitialStateContainer>
    </IntlProvider>
  </Provider>
);

export default Root;

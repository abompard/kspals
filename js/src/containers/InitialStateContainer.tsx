import * as React from "react";
import { connect } from "react-redux";
import { requestInitialState } from "../actions/initialstate";
import { APIError } from "../api/types";
import { IState } from "../reducers";

interface Props {
  error: APIError | null;
  isLoading: boolean;
  requestInitialState(): void;
  children?: React.ReactNode;
}

class InitialStateContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.requestInitialState();
  }

  render() {
    if (this.props.error) {
      return (
        <div className="alert alert-danger">
          Impossible de charger l'application :
          <code className="ms-2">{this.props.error.detail}</code>
        </div>
      );
    }
    if (this.props.isLoading) {
      return (
        <div className="text-center mt-3">
          <i className="fa fa-spinner fa-pulse fa-lg" />
        </div>
      );
    }
    return this.props.children;
  }
}

const mapStateToProps = (state: IState) => ({
  error: state.ui.initialState.error,
  isLoading: state.ui.initialState.isLoading
});

export default connect(mapStateToProps, {
  requestInitialState
})(InitialStateContainer);

import { connect } from "react-redux";
import { updateEventSuccess } from "../actions/admin";
import { AdminEventInfo } from "../components/AdminEventInfo";
import { IState } from "../reducers";
import {
  getEvent,
  getParticipantsList,
  getUserToken,
  isAdminReadOnly
} from "../store/utils";

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  return {
    event,
    drinks_count: getParticipantsList(event)
      .map(participant => participant.drinks)
      .reduce((prev, current) => prev + current, 0),
    participantsIsLoading: state.ui.participant.isLoading,
    readOnly: isAdminReadOnly(state),
    userToken: getUserToken(state)
  };
};

export default connect(mapStateToProps, { updateEventSuccess })(AdminEventInfo);

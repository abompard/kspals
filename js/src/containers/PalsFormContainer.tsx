import { connect } from "react-redux";
import { checkPal } from "../actions/participant";
import PalsForm from "../components/PalsForm";
import { IState } from "../reducers";
import { getCurrentParticipant } from "../store/utils";

const mapStateToProps = (state: IState) => {
  const participant = getCurrentParticipant(state)!;
  return {
    participant
  };
};

export default connect(
  mapStateToProps,
  {
    onPalChange: checkPal
  }
)(PalsForm);

import * as React from "react";
import { connect } from "react-redux";
import { loadEvents } from "../actions/event";
import { sortByAttr } from "../api/utils";
import Home, { Props as ComponentProps } from "../components/Home";
import { IState } from "../reducers";

interface Props extends ComponentProps {
  error: string | null;
  initialStateIsLoading: boolean;
  loadEvents(): void;
}

class HomeContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.initialStateIsLoading || this.props.loadEvents();
  }

  render() {
    if (this.props.initialStateIsLoading) {
      return null;
    }
    if (this.props.error) {
      return (
        <div className="alert alert-danger">
          Impossible de charger la liste des évènements :
          <code className="ms-2">{this.props.error}</code>
        </div>
      );
    }
    return <Home events={this.props.events} isLoading={this.props.isLoading} />;
  }
}

const mapStateToProps = (state: IState) => ({
  events: Object.values(state.entities.events)
    .filter(event => event.is_active)
    .sort(sortByAttr("date_open")),
  isLoading: state.ui.event.isLoading,
  initialStateIsLoading:
    state.ui.initialState.isLoading ||
    !state.ui.initialState.hasInitiallyLoaded,
  error: state.ui.event.error
});

export default connect(mapStateToProps, {
  loadEvents
})(HomeContainer);

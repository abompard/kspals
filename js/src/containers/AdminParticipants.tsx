import React, { Component } from "react";
import { connect } from "react-redux";
import { addParticipant } from "../actions/admin";
import { addFlashMessage } from "../actions/flashMessages";
import { sortByAttrs } from "../api/utils";
import {
  AdminAddParticipant,
  Props as AddComponentProps
} from "../components/AdminAddParticipant";
import {
  AdminParticipants,
  Props as ComponentProps
} from "../components/AdminParticipants";
import { IState } from "../reducers";
import { getEvent, getParticipantsList, isAdminReadOnly } from "../store/utils";

interface Props extends ComponentProps, AddComponentProps {}

class AdminParticipantsContainer extends Component<Props> {
  render() {
    return (
      <AdminParticipants
        event={this.props.event}
        participants={this.props.participants}
        readOnly={this.props.readOnly}
        isLoading={this.props.isLoading}
        addParticipantComponent={
          <AdminAddParticipant
            readOnly={this.props.readOnly}
            addParticipant={this.props.addParticipant}
            addFlashMessage={this.props.addFlashMessage}
          />
        }
      />
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  return {
    event,
    participants: getParticipantsList(event)
      .filter(participant => participant.approved)
      .sort(
        sortByAttrs(["pal_group_registration_date", "profile.date_registered"])
      ),
    isLoading: state.ui.participant.isLoading,
    readOnly: isAdminReadOnly(state)
  };
};

export default connect(mapStateToProps, {
  addParticipant,
  addFlashMessage
})(AdminParticipantsContainer);

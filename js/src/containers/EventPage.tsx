import React, { Component } from "react";

import EventContainer from "./EventContainer";
import PalsPage from "./PalsPage";
import { Switch, Route } from "react-router";
import EventActiveSwitch from "./EventActiveSwitch";
import ParticipantProfile from "./ParticipantProfile";

export default class EventPage extends Component<{}> {
  render() {
    return (
      <EventContainer>
        <EventActiveSwitch>
          <Switch>
            <Route path="/:eventcode/profile/" component={ParticipantProfile} />
            <Route path="/:eventcode" component={PalsPage} />
          </Switch>
        </EventActiveSwitch>
      </EventContainer>
    );
  }
}

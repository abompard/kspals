import * as React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { addFlashMessage } from "../actions/flashMessages";
import { loadParticipant } from "../actions/participant";
import { FlashMessageProps } from "../actions/types";
import { IEvent, IParticipant } from "../api/types";
import FAIcon from "../components/FAIcon";
import { IState } from "../reducers";
import { getCurrentParticipant, getEvent } from "../store/utils";
import SocketIOWatcher from "./SocketIOWatcher";
import withSocketIO from "./WithSocketIO";

type Props = {
  participant: IParticipant | null;
  event: IEvent;
  isLoading: boolean;
  error: string | null;
  mustRegister: boolean;
  socketio: SocketIO.Socket;
  loadParticipant(): void;
  addFlashMessage(message: FlashMessageProps): void;
  children?: React.ReactNode;
};

class ParticipantContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.loadParticipant();
  }

  onParticipantUpdated = (data: any) => {
    if (this.props.participant !== null) {
      this.props.loadParticipant();
    }
  };

  render() {
    if (this.props.error) {
      return (
        <div className="alert alert-danger">
          Erreur:
          <code className="ms-2">{this.props.error}</code>
        </div>
      );
    }
    if (this.props.mustRegister) {
      this.props.addFlashMessage({
        color: "info",
        body:
          "Vous devez vous enregistrer avant de pouvoir déclarer votre ou vos PAL(s)."
      });
      return (
        <Redirect
          to={{
            pathname: `/${this.props.event.code}/profile/`
          }}
        />
      );
    }
    if (this.props.participant === null) {
      return null;
    }
    return (
      <div className="position-relative">
        {this.props.isLoading && (
          <div className="position-absolute text-muted" style={{ right: 0 }}>
            <FAIcon name="spinner" className="fa-spin me-2" />
            Loading...
          </div>
        )}
        <SocketIOWatcher
          model="participant"
          instanceIds={[this.props.participant.id]}
          onUpdate={this.onParticipantUpdated}
        />
        {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = (state: IState) => ({
  participant: getCurrentParticipant(state),
  isLoading: state.ui.participant.isLoading,
  error: state.ui.participant.error,
  mustRegister: state.ui.participant.mustRegister,
  event: getEvent(state)!
});

export default withSocketIO(
  connect(mapStateToProps, {
    loadParticipant,
    addFlashMessage
  })(ParticipantContainer)
);

import React, { Component } from "react";
import { connect } from "react-redux";
import { loadNotifications } from "../actions/admin";
import { IEvent, INotification } from "../api/types";
import { sortByAttr } from "../api/utils";
import { AdminQueuesNotifications } from "../components/AdminQueuesNotifications";
import FAIcon from "../components/FAIcon";
import { IState } from "../reducers";
import { getEvent } from "../store/utils";

interface Props {
  event: IEvent;
  isLoading: boolean;
  notifications: Array<INotification>;
  error: string | null;
  loadNotifications(): void;
}

class AdminQueuesNotificationsContainer extends Component<Props> {
  componentDidMount() {
    if (!this.props.isLoading) {
      this.props.loadNotifications();
    }
  }

  render() {
    if (this.props.error) {
      return (
        <div className="alert alert-danger">
          Erreur:
          <code className="ms-2">{this.props.error}</code>
        </div>
      );
    }
    if (this.props.isLoading) {
      return (
        <div className="text-center">
          <FAIcon name="spinner" className="fa-spin me-2" />
        </div>
      );
    }
    return (
      <AdminQueuesNotifications
        event={this.props.event}
        notifications={this.props.notifications}
      />
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  return {
    event,
    notifications: (event.notifications || []).sort(sortByAttr("date_created")),
    isLoading: state.ui.notification.isLoading,
    error: state.ui.notification.error
  };
};

export default connect(mapStateToProps, {
  loadNotifications
})(AdminQueuesNotificationsContainer);

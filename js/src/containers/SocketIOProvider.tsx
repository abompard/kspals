import React, { Component, createContext } from "react";
import { connect } from "react-redux";
import { io, Socket } from "socket.io-client";
import { setSocketIOActive } from "../actions/socketio";
import { IState } from "../reducers";
import { getUserToken } from "../store/utils";

interface Props {
  endpoint: string | null;
  token: string | null;
  socketIOActive: boolean;
  setSocketIOActive(status: boolean): void;
  children: React.ReactNode;
}

type SocketOrNull = null | Socket;

interface State {
  socket: SocketOrNull;
}

export const SocketIOContext = createContext(null as SocketOrNull);

class SocketIOProvider extends Component<Props, State> {
  state: State = { socket: null };

  componentDidMount() {
    if (!this.props.endpoint) {
      return;
    }
    this.setState({ socket: io(this.props.endpoint) });
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (prevState.socket === null && this.state.socket !== null) {
      this.setupEvents();
    }
    if (prevProps.token !== this.props.token && this.state.socket !== null) {
      this.props.setSocketIOActive(false);
      this.state.socket.emit("logout", () => {
        this.doLogin();
      });
    }
  }

  setupEvents() {
    if (this.state.socket === null) {
      return;
    }
    // Login on connect and reconnect
    this.state.socket.on("connect", this.doLogin);
    // Set ready state
    this.state.socket.on("disconnect", () => {
      this.props.setSocketIOActive(false);
    });
    this.state.socket.on("ready", () => {
      this.props.setSocketIOActive(true);
    });
  }

  doLogin = () => {
    if (this.state.socket === null) {
      return;
    }
    if (!this.props.token) {
      return;
    }
    this.state.socket.emit("login", { token: this.props.token });
  };

  componentWillUnmount() {
    if (this.state.socket !== null && this.state.socket.connected) {
      this.state.socket.close();
    }
  }

  render() {
    return (
      <SocketIOContext.Provider value={this.state.socket}>
        {this.props.children}
      </SocketIOContext.Provider>
    );
  }
}

const mapStateToProps = (state: IState) => {
  return {
    endpoint: state.config.socketIOEndpoint,
    token: getUserToken(state),
    socketIOActive: state.ui.socketIOActive
  };
};

export default connect(
  mapStateToProps,
  { setSocketIOActive }
)(SocketIOProvider);

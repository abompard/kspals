import * as React from "react";
import { connect } from "react-redux";
import { logout } from "../actions/user";
import { IEvent, IParticipant, IUser } from "../api/types";
import TopBar from "../components/TopBar";
import { IState } from "../reducers";
import {
  getEvent,
  getParticipantsList,
  getParticipantsNonMutualPals,
  getParticipantsNoTicket,
  getParticipantsPendingApproval,
  getParticipantsTicketingReady,
  getParticipantsWithoutPal,
  getUser
} from "../store/utils";

interface Props {
  event: IEvent | null;
  user: IUser | null;
  logoUrl: string;
  participants: Array<IParticipant>;
  isNotificationsLoading: boolean;
  toValidateCount: number;
  incoherentPalsCount: number;
  ticketingToSendCount: number;
  noTicketCount: number;
  notificationsCount: number;
  logout(): void;
}

class AdminTopBarContainer extends React.Component<Props> {
  render() {
    let links,
      userLinks = undefined;
    if (this.props.event) {
      links = [
        {
          text: "Info",
          href: `/admin/${this.props.event.code}/`,
          exact: true
        },
        {
          text: "Participants",
          links: [
            {
              text: "List",
              href: `/admin/${this.props.event.code}/participants/`
            },
            {
              text: "Profiles",
              href: `/admin/${this.props.event.code}/participants/profiles/`
            },
            {
              text: "Drinks",
              href: `/admin/${this.props.event.code}/participants/drinks/`
            },
            {
              text: "Export",
              href: `/admin/${this.props.event.code}/participants/export/`
            },
            {
              text: "Check In",
              href: `/admin/${this.props.event.code}/participants/check-in/`
            }
          ]
        },
        {
          text: "To do",
          links: [
            {
              text: "To approve",
              href: `/admin/${this.props.event.code}/queues/validation/`,
              count: this.props.toValidateCount,
              countColor: "info"
            },
            {
              text: "Incoherent PALs",
              href: `/admin/${this.props.event.code}/queues/incoherent-pals/`,
              count: this.props.incoherentPalsCount,
              countColor: "info"
            },
            {
              text: "Ticket not purchased",
              href: `/admin/${this.props.event.code}/queues/no-ticket/`,
              count: this.props.noTicketCount,
              countColor: "warning"
            },
            {
              text: "Email notifications",
              href: `/admin/${this.props.event.code}/queues/notifications/`,
              count: this.props.notificationsCount,
              countColor: "info"
            }
          ]
        },
        {
          text: "Other",
          links: [
            {
              text: "Rejected participants",
              href: `/admin/${this.props.event.code}/rejected-participants/`
            },
            {
              text: "History",
              href: `/admin/${this.props.event.code}/history/`
            }
          ]
        }
      ];
      userLinks = [
        {
          text: "Mes PALs",
          href: `/${this.props.event.code}/`,
          exact: true
        },
        {
          text: "Mon profil",
          href: `/${this.props.event.code}/profile/`,
          exact: true
        }
      ];
    }
    return (
      <TopBar
        user={this.props.user}
        event={this.props.event}
        logoUrl={this.props.logoUrl}
        onLogout={this.props.logout}
        links={links}
        userLinks={userLinks}
      />
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state);
  const participants = getParticipantsList(event);
  const ticketsLeft = Math.max(
    0,
    event && event.full_limit
      ? event.full_limit - event.ticketing_sent_count
      : 0
  );
  const toValidateCount = Math.min(
    getParticipantsPendingApproval(participants).length,
    ticketsLeft
  );
  return {
    event,
    user: getUser(state),
    logoUrl: state.config.logoUrl,
    participants,
    isNotificationsLoading: state.ui.notification.isLoading,
    toValidateCount,
    incoherentPalsCount:
      getParticipantsWithoutPal(participants).length +
      getParticipantsNonMutualPals(participants).length,
    ticketingToSendCount: getParticipantsTicketingReady(state).length,
    noTicketCount: getParticipantsNoTicket(participants).length,
    notificationsCount: (event ? event.notifications || [] : []).filter(
      notif => notif.status === "DRAFT" || notif.status === "TO_SEND"
    ).length
  };
};
export default connect(mapStateToProps, {
  logout
})(AdminTopBarContainer);

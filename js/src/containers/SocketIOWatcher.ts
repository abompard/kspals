import _ from "lodash";
import * as React from "react";
import { Socket } from "socket.io-client";
import withSocketIO from "./WithSocketIO";

type InstanceIds = Array<string | number>;

type Props = {
  model: string;
  instanceIds: InstanceIds;
  socketIO: Socket | null;
  socketIOActive: boolean;
  onUpdate(data: any): void;
};

class SocketIOWatcher extends React.Component<Props> {
  componentDidMount() {
    if (!this.props.socketIO) {
      return;
    }
    this.setupEvent();
    this.props.socketIO.on("ready", () => this.serverSubscribe());
    if (this.props.socketIOActive) {
      this.serverSubscribe();
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (!this.props.socketIO) {
      return;
    }
    if (prevProps.model !== this.props.model) {
      // Client-side event
      this.cancelEvent(prevProps.model);
      this.setupEvent();
      // Server-side subscription
      this.serverUnsubscribe(prevProps.model, prevProps.instanceIds);
      this.serverSubscribe();
    } else if (!_.isEqual(prevProps.instanceIds, this.props.instanceIds)) {
      // Server-side subscription only
      this.serverUnsubscribe(prevProps.model, prevProps.instanceIds);
      this.serverSubscribe();
    }
  }

  componentWillUnount() {
    if (!this.props.socketIO) {
      return;
    }
    this.cancelEvent();
    this.serverUnsubscribe();
  }

  setupEvent = (model?: string) => {
    // Client-side event: this persists through SIO client reconnections
    model = model || this.props.model;
    this.props.socketIO!.on(`${model}_updated`, this.onUpdate);
  };

  cancelEvent = (model?: string) => {
    // Client-side event: this persists through SIO client reconnections
    model = model || this.props.model;
    this.props.socketIO!.off(`${this.props.model}_updated`, this.onUpdate);
  };

  serverSubscribe = (model?: string, instanceIds?: InstanceIds) => {
    // Server-side subscription: this needs to be re-emitted after each SIO client connect+login event
    model = model || this.props.model;
    instanceIds = instanceIds || this.props.instanceIds;
    instanceIds.forEach(instanceId => {
      this.props.socketIO!.emit(`watch_${this.props.model}`, {
        id: instanceId
      });
    });
  };

  serverUnsubscribe = (model?: string, instanceIds?: InstanceIds) => {
    // Server-side subscription: this needs to be re-emitted after each SIO client connect+login event
    model = model || this.props.model;
    instanceIds = instanceIds || this.props.instanceIds;
    instanceIds.forEach(instanceId => {
      this.props.socketIO!.emit(`unwatch_${this.props.model}`, {
        id: instanceId
      });
    });
  };

  onUpdate = (data: any) => {
    this.props.onUpdate(data);
  };

  render() {
    return null;
  }
}

export default withSocketIO(SocketIOWatcher);

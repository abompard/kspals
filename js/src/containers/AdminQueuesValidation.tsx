import React, { Component } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { loadParticipants } from "../actions/admin";
import { IParticipant } from "../api/types";
import { filterParticipants, sortByAttrs } from "../api/utils";
import {
  AdminQueuesValidation,
  BaseProps as ListComponentProps
} from "../components/AdminQueuesValidation";
import { IState } from "../reducers";
import {
  getEvent,
  getParticipant,
  getParticipantsList,
  getParticipantsPendingApproval
} from "../store/utils";
import AdminQueuesValidationProfileContainer from "./AdminQueuesValidationProfile";

interface Props extends Omit<ListComponentProps, "validationMax"> {
  participant: IParticipant | null;
  isParticipantsLoading: boolean;
  loadParticipants(): Promise<void>;
}

interface State {
  onlyMutual: boolean;
  filter: string;
}

class AdminQueuesValidationContainer extends Component<Props, State> {
  state = { onlyMutual: false, filter: "" };

  handleOnlyMutualToggle = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const target = e.currentTarget;
    this.setState({
      onlyMutual: target.checked
    });
  };

  handleFilterChange = (value: string): void => {
    this.setState({ filter: value });
  };

  render() {
    const {
      isParticipantsLoading,
      participant,
      participants,
      ...props
    } = this.props;
    if (isParticipantsLoading) {
      return null;
    }
    // Compute how many tickets are left
    const ticketsLeft = Math.max(
      0,
      this.props.event.full_limit
        ? this.props.event.full_limit - this.props.event.ticketing_sent_count
        : 0
    );
    // Filters
    let filteredParticipants = filterParticipants(
      this.props.participants,
      this.state.filter
    );
    if (this.state.onlyMutual) {
      filteredParticipants = filteredParticipants.filter(
        p => p.mutual_pal_group
      );
    }
    // Route to the proper sub-component
    if (participant) {
      return (
        <AdminQueuesValidationProfileContainer
          event={this.props.event}
          participant={participant}
          approvalQueue={filteredParticipants}
          fullApprovalQueue={this.props.participants}
          validationMax={ticketsLeft}
        />
      );
    } else {
      return (
        <AdminQueuesValidation
          participants={filteredParticipants}
          validationMax={ticketsLeft}
          onlyMutual={this.state.onlyMutual}
          filter={this.state.filter}
          onFilterChange={this.handleFilterChange}
          onOnlyMutualToggle={this.handleOnlyMutualToggle}
          {...props}
        />
      );
    }
  }
}

const mapStateToProps = (
  state: IState,
  ownProps: RouteComponentProps<{ participant: string }>
) => {
  const event = getEvent(state)!;
  const participants = getParticipantsPendingApproval(
    getParticipantsList(event)
  ).sort(
    sortByAttrs(["pal_group_registration_date", "profile.date_registered"])
  );
  let participant = null;
  if (ownProps.match.params.participant) {
    const participantId = parseInt(ownProps.match.params.participant, 10);
    participant = getParticipant(state, participantId);
  }
  return {
    event,
    participants,
    participant,
    isParticipantsLoading: state.ui.participant.isLoading
  };
};

export default connect(mapStateToProps, { loadParticipants })(
  AdminQueuesValidationContainer
);

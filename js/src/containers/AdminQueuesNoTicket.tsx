import React, { Component } from "react";
import { connect } from "react-redux";
import { IEvent, IParticipant } from "../api/types";
import { sortByAttr } from "../api/utils";
import { AdminQueuesNoTicket } from "../components/AdminQueuesNoTicket";
import { IState } from "../reducers";
import {
  getEvent,
  getParticipantsList,
  getParticipantsNoTicket
} from "../store/utils";

interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
  isParticipantsLoading: boolean;
}

class AdminQueuesNoTicketContainer extends Component<Props> {
  render() {
    if (this.props.isParticipantsLoading) {
      return null;
    }
    return (
      <AdminQueuesNoTicket
        event={this.props.event}
        participants={this.props.participants}
      />
    );
  }
}

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  return {
    event,
    participants: getParticipantsNoTicket(getParticipantsList(event)).sort(
      sortByAttr("profile.date_registered")
    ),
    isParticipantsLoading: state.ui.participant.isLoading
  };
};

export default connect(mapStateToProps)(AdminQueuesNoTicketContainer);

import React, { Component } from "react";
import { connect } from "react-redux";
import { IEvent } from "../api/types";
import { IState } from "../reducers";
import { getEvent } from "../store/utils";

interface Props {
  event: IEvent;
}

class AdminParticipantsExport extends Component<Props> {
  render() {
    return (
      <React.Fragment>
        <h3>{this.props.event.name}: participant export</h3>
        <ul>
          <li>
            <a href={`/${this.props.event.code}/participants.csv`}>
              CSV format
            </a>
          </li>
          <li>
            <a href={`/${this.props.event.code}/participants.csv?oldmac=1`}>
              CSV format for MacOS
            </a>
          </li>
          <li>
            <a
              href={`/${this.props.event.code}/participants.csv?waitinglist=1`}
            >
              Waiting list in CSV format
            </a>
          </li>
        </ul>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: IState) => ({
  event: getEvent(state)!
});

export default connect(mapStateToProps)(AdminParticipantsExport);

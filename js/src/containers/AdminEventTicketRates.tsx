import { connect } from "react-redux";
import { updateTicketRate } from "../actions/admin";
import { AdminEventTicketRates } from "../components/AdminEventTicketRates";
import { IState } from "../reducers";
import { getEvent, getParticipantsList, isAdminReadOnly } from "../store/utils";

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  const ticketRates = event.ticket_rates || [];
  const participants = getParticipantsList(event).filter(
    participant => participant.approved
  );
  const participantsCount: { [key: string]: number } = {};
  const ticketsCount: { [key: string]: number } = {};
  ticketRates.forEach(ticketRate => {
    participantsCount[ticketRate.name] = participants.filter(
      participant => participant.ticket_rate === ticketRate.name
    ).length;
    ticketsCount[ticketRate.name] = participants.filter(
      participant =>
        participant.ticket_rate === ticketRate.name &&
        participant.ticket_status === "BOUGHT"
    ).length;
  });
  return {
    ticketRates,
    participantsCount,
    ticketsCount,
    readOnly: isAdminReadOnly(state)
  };
};

export default connect(mapStateToProps, { update: updateTicketRate })(
  AdminEventTicketRates
);

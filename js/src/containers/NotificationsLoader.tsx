import * as React from "react";
import { connect } from "react-redux";
import { IEvent } from "../api/types";
import { IState } from "../reducers";
import { getEvent } from "../store/utils";
import { loadNotifications } from "../actions/admin";

type Props = {
  event: IEvent;
  isLoading: boolean;
  hasInitiallyLoaded: boolean;
  loadNotifications(): void;
};

class NotificationsLoader extends React.Component<Props> {
  componentDidMount() {
    // Delay loading of the notifications in case we're showing the notifications page, which has its own loader.
    setTimeout(() => {
      if (!this.props.isLoading && !this.props.hasInitiallyLoaded) {
        this.props.loadNotifications();
      }
    }, 1000);
  }

  render() {
    return null;
  }
}

const mapStateToProps = (state: IState) => ({
  isLoading: state.ui.notification.isLoading,
  hasInitiallyLoaded: state.ui.notification.hasInitiallyLoaded,
  event: getEvent(state)!
});

export default connect(
  mapStateToProps,
  {
    loadNotifications
  }
)(NotificationsLoader);

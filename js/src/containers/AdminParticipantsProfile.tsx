import React, { Component } from "react";
import { connect } from "react-redux";
import { match } from "react-router-dom";
import {
  buyTicket,
  loadParticipantHistory,
  loadParticipants,
  sellTicket,
  updateParticipant
} from "../actions/admin";
import { sortByAttr } from "../api/utils";
import {
  AdminParticipantsProfile,
  Props as ComponentProps
} from "../components/AdminParticipantsProfile";
import { IState } from "../reducers";
import {
  AddNull,
  getEvent,
  getParticipant,
  getParticipantsList,
  isAdminReadOnly
} from "../store/utils";

interface Props extends AddNull<ComponentProps, "participant"> {
  isLoading: boolean;
}

class AdminParticipantsProfileContainer extends Component<Props> {
  render() {
    const { isLoading, participant, ...props } = this.props;
    if (!participant) {
      if (isLoading) {
        return null;
      } else {
        return <p className="text-danger">This participant does not exist!</p>;
      }
    }
    return <AdminParticipantsProfile {...props} participant={participant} />;
  }
}

const mapStateToProps = (
  state: IState,
  ownProps: { eventCode: string; match: match<{ participant: string }> }
) => {
  const participantId = parseInt(ownProps.match.params.participant, 10);
  const participant = getParticipant(state, participantId, true);
  const event = getEvent(state)!;
  return {
    event,
    participant,
    isLoading: state.ui.participant.isLoading,
    isHistoryLoading: state.ui.history.isLoading,
    readOnly: isAdminReadOnly(state),
    participantsWithoutTicket: getParticipantsList(event)
      .filter(participant => !participant.has_ticket)
      .sort(sortByAttr("profile.full_name")),
    socketIOActive: state.ui.socketIOActive
  };
};

export default connect(mapStateToProps, {
  updateParticipant,
  loadParticipantHistory,
  sellTicket,
  buyTicket,
  loadParticipants
})(AdminParticipantsProfileContainer);

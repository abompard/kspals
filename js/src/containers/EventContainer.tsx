import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { loadEvent } from "../actions/event";
import { IEvent } from "../api/types";
import { IState } from "../reducers";

interface urlParams {
  eventcode: string;
}

interface Props extends RouteComponentProps<urlParams> {
  event: IEvent | null;
  isLoading: boolean;
  error: string | null;
  loadEvent(code: string): any;
  children?: React.ReactNode;
}

const loadData = (props: Props) => {
  if (props.event && props.match.params.eventcode === props.event.code) {
    return; // Already loaded
  }
  props.loadEvent(props.match.params.eventcode);
};

class EventContainer extends React.Component<Props> {
  componentDidMount() {
    loadData(this.props);
  }

  componentDidUpdate(prevProps: Props) {
    if (
      prevProps.match.params.eventcode !== this.props.match.params.eventcode
    ) {
      loadData(this.props);
    }
  }

  render() {
    if (this.props.error) {
      return (
        <div className="alert alert-danger">
          Impossible d'afficher cet évènement :
          <code className="ms-2">{this.props.error}</code>
        </div>
      );
    }
    if (this.props.isLoading) {
      return (
        <div className="text-center mt-3">
          <i className="fa fa-spinner fa-pulse fa-lg" />
        </div>
      );
    }
    if (!this.props.event) {
      return null;
    }
    return this.props.children;
  }
}

const mapStateToProps = (state: IState) => ({
  event: state.current.eventCode
    ? state.entities.events[state.current.eventCode]
    : null,
  isLoading: state.ui.event.isLoading,
  error: state.ui.event.error
});

export default withRouter(
  connect(mapStateToProps, {
    loadEvent
  })(EventContainer)
);

import { connect } from "react-redux";
import { updateRole } from "../actions/admin";
import { AdminEventRoles } from "../components/AdminEventRoles";
import { IState } from "../reducers";
import { getEvent, getParticipantsList, isAdminReadOnly } from "../store/utils";

const mapStateToProps = (state: IState) => {
  const event = getEvent(state)!;
  const roles = event.roles || [];
  const participants = getParticipantsList(event).filter(
    participant => participant.approved
  );
  const participantsCount: { [key: string]: number } = {};
  roles.forEach(role => {
    participantsCount[role.code] = participants.filter(
      participant => participant.role === role.code
    ).length;
  });
  return {
    roles,
    participantsCount,
    readOnly: isAdminReadOnly(state)
  };
};

export default connect(mapStateToProps, { updateRole })(AdminEventRoles);

import React, { Component } from "react";
import { connect } from "react-redux";
import { IEvent, IParticipant } from "../api/types";
import { sortByAttr } from "../api/utils";
import { AdminParticipantsCheckIn } from "../components/AdminParticipantsCheckIn";
import { IState } from "../reducers";
import { getEvent, getParticipantsList } from "../store/utils";

interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
  isLoading: boolean;
}

class AdminParticipantsCheckInContainer extends Component<Props> {
  render() {
    if (this.props.isLoading) {
      return null;
    }
    return (
      <AdminParticipantsCheckIn
        event={this.props.event}
        participants={this.props.participants}
      />
    );
  }
}

const mapStateToProps = (state: IState, ownProps: { eventCode: string }) => {
  const event = getEvent(state)!;
  return {
    event,
    participants: getParticipantsList(event).sort(
      sortByAttr("profile.full_name")
    ),
    isLoading: state.ui.participant.isLoading
  };
};

export default connect(mapStateToProps)(AdminParticipantsCheckInContainer);

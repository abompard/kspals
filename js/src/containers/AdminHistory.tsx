import { connect } from "react-redux";
import { getEvent } from "../store/utils";
import { loadHistory } from "../actions/admin";
import { AdminHistory } from "../components/AdminHistory";
import { IState } from "../reducers";
import { RouteComponentProps } from "react-router";
import querystring from "querystring";
import { getPage, Page } from "../store/pagination";
import { IHistory } from "../api/types";

const mapStateToProps = (state: IState, ownProps: RouteComponentProps<{}>) => {
  const queryString = querystring.parse(ownProps.location.search.slice(1));
  const pageNr = Number(queryString.page || "1");
  const filters = {
    ...queryString
  };
  delete filters.page;
  return {
    event: getEvent(state)!,
    pageNr,
    filters,
    page: getPage(state, "history", pageNr) as Page<IHistory>,
    isLoading: state.ui.history.isLoading,
    error: state.ui.history.error
  };
};

export default connect(
  mapStateToProps,
  {
    loadHistory
  }
)(AdminHistory);

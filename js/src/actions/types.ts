// Inspired by:
// https://github.com/fbsamples/f8app/blob/master/js/actions/types.js

import { Action, ActionCreator } from "redux";
import { ThunkAction } from "redux-thunk";
import { APIError } from "../api/types";
import { IState } from "../reducers";
import { EntitiesState } from "../reducers/entities";
import { FilterDict } from "../types";

export interface ActionFailure extends Action {
  //type: string;
  error: APIError;
}

export type EntitiesResponse<PkType = number> = {
  result: PkType;
  entities: EntitiesState;
};

type EntitiesListResponse<PkType = number> = {
  result: Array<PkType>;
  entities: EntitiesState;
};

type PagedEntitiesListResponse<PkType = number> = EntitiesListResponse<
  PkType
> & {
  page: {
    count: number;
    number: number;
    page_size: number;
    filters: FilterDict;
  };
};

export interface EntitiesAction<PkType = number> extends Action {
  response: EntitiesResponse<PkType>;
}

export interface EntitiesListAction<PkType = number> extends Action {
  response: EntitiesListResponse<PkType>;
}
export interface PagedEntitiesListAction<PkType = number> extends Action {
  response: PagedEntitiesListResponse<PkType>;
}

// Initial statemethod?: RequestInit["method"];
// body?: RequestInit["body"];
export interface InitialStateRequest extends Action {
  type: "INITIAL_STATE_REQUEST";
}
export interface InitialStateSuccess extends Action {
  type: "INITIAL_STATE_SUCCESS";
  response: IState;
}
export interface InitialStateFailure extends ActionFailure {
  type: "INITIAL_STATE_FAILURE";
}
export type InitialStateAction =
  | InitialStateRequest
  | InitialStateSuccess
  | InitialStateFailure;

// User
export interface LoginCodeRequest extends Action {
  type: "LOGIN_CODE_REQUEST";
}
export interface LoginCodeSuccess extends Action {
  type: "LOGIN_CODE_SUCCESS";
  email: string;
}
export interface LoginCodeFailure extends ActionFailure {
  type: "LOGIN_CODE_FAILURE";
}
export type LoginCodeAction =
  | LoginCodeRequest
  | LoginCodeSuccess
  | LoginCodeFailure;
export interface TokenRequest extends Action {
  type: "TOKEN_REQUEST";
}
export interface TokenSuccess extends EntitiesAction {
  type: "TOKEN_SUCCESS";
}
export interface TokenFailure extends ActionFailure {
  type: "TOKEN_FAILURE";
}
export type TokenAction = TokenRequest | TokenSuccess | TokenFailure;
export interface LogoutRequest extends Action {
  type: "LOGOUT_REQUEST";
}
export interface LogoutSuccess extends Action {
  type: "LOGOUT_SUCCESS";
}
export interface LogoutFailure extends ActionFailure {
  type: "LOGOUT_FAILURE";
}
export type LogoutAction = LogoutRequest | LogoutSuccess | LogoutFailure;

// Event

export interface LoadEventRequest extends Action {
  type: "LOAD_EVENT_REQUEST";
}
export interface LoadEventSuccess extends EntitiesAction<string> {
  type: "LOAD_EVENT_SUCCESS";
}
export interface LoadEventFailure extends ActionFailure {
  type: "LOAD_EVENT_FAILURE";
}
export type LoadEventAction =
  | LoadEventRequest
  | LoadEventSuccess
  | LoadEventFailure;

export interface LoadEventsRequest extends Action {
  type: "LOAD_EVENTS_REQUEST";
}
export interface LoadEventsSuccess extends PagedEntitiesListAction<string> {
  type: "LOAD_EVENTS_SUCCESS";
}
export interface LoadEventsFailure extends ActionFailure {
  type: "LOAD_EVENTS_FAILURE";
}
export type LoadEventsAction =
  | LoadEventsRequest
  | LoadEventsSuccess
  | LoadEventsFailure;

export type EventAction =
  | LoadEventAction
  | LoadEventsAction
  | UpdateEventSuccess
  | UpdateRoleSuccess
  | UpdateTicketRateSuccess;

// Participant
export interface LoadParticipantRequest extends Action {
  type: "LOAD_PARTICIPANT_REQUEST";
}
export interface LoadParticipantSuccess extends EntitiesAction {
  type: "LOAD_PARTICIPANT_SUCCESS";
}
export interface LoadParticipantFailure extends ActionFailure {
  type: "LOAD_PARTICIPANT_FAILURE";
}
export type LoadParticipantAction =
  | LoadParticipantRequest
  | LoadParticipantSuccess
  | LoadParticipantFailure;

export interface RegisterParticipantSuccess extends EntitiesAction {
  type: "REGISTER_PARTICIPANT_SUCCESS";
}

export type ParticipantAction =
  | LoadParticipantAction
  | RegisterParticipantSuccess;

// PALs
export interface CheckPALSuccess extends EntitiesAction {
  type: "CHECK_PAL_SUCCESS";
  index: number;
}
export interface DeletePALSuccess extends Action {
  type: "DELETE_PAL_SUCCESS";
  index: number;
}
export type PALAction = CheckPALSuccess | DeletePALSuccess;

/*
 * Admin
 */

// Event

export interface UpdateEventSuccess extends Action {
  type: "UPDATE_EVENT_SUCCESS";
  response: {
    entities: Pick<EntitiesState, "events">;
  };
}

export interface UpdateRoleSuccess extends Action {
  type: "UPDATE_ROLE_SUCCESS";
  response: {
    entities: Pick<EntitiesState, "roles">;
  };
}

export interface UpdateTicketRateSuccess extends Action {
  type: "UPDATE_TICKET_RATE_SUCCESS";
  response: {
    entities: Pick<EntitiesState, "ticket_rates">;
  };
}

// Participants
export interface LoadParticipantsRequest extends Action {
  type: "LOAD_PARTICIPANTS_REQUEST";
}
export interface LoadParticipantsSuccess
  extends PagedEntitiesListAction<number> {
  type: "LOAD_PARTICIPANTS_SUCCESS";
  date_request: string;
}
export interface LoadParticipantsFailure extends ActionFailure {
  type: "LOAD_PARTICIPANTS_FAILURE";
}
export interface LoadParticipantsDone extends Action {
  type: "LOAD_PARTICIPANTS_DONE";
}
export type LoadParticipantsAction =
  | LoadParticipantsRequest
  | LoadParticipantsSuccess
  | LoadParticipantsFailure
  | LoadParticipantsDone;

export interface UpdateParticipantSuccess extends EntitiesAction {
  type: "UPDATE_PARTICIPANT_SUCCESS";
}

export interface AddParticipantSuccess extends EntitiesAction {
  type: "ADD_PARTICIPANT_SUCCESS";
}

export interface CancelParticipantSuccess extends Action {
  type: "CANCEL_PARTICIPANT_SUCCESS";
}

export type ParticipantsAction =
  | LoadParticipantsAction
  | UpdateParticipantSuccess
  | AddParticipantSuccess
  | CancelParticipantSuccess;

// Notification

export interface LoadNotificationsRequest extends Action {
  type: "LOAD_NOTIFICATIONS_REQUEST";
}
export interface LoadNotificationsSuccess
  extends PagedEntitiesListAction<number> {
  type: "LOAD_NOTIFICATIONS_SUCCESS";
}
export interface LoadNotificationsFailure extends ActionFailure {
  type: "LOAD_NOTIFICATIONS_FAILURE";
}
export interface LoadNotificationsDone extends Action {
  type: "LOAD_NOTIFICATIONS_DONE";
}
export type LoadNotificationsAction =
  | LoadNotificationsRequest
  | LoadNotificationsSuccess
  | LoadNotificationsFailure
  | LoadNotificationsDone;

export interface ValidateNotificationRequest extends Action {
  type: "VALIDATE_NOTIFICATION_REQUEST";
}
export interface ValidateNotificationSuccess extends EntitiesAction {
  type: "VALIDATE_NOTIFICATION_SUCCESS";
}
export interface ValidateNotificationFailure extends ActionFailure {
  type: "VALIDATE_NOTIFICATION_FAILURE";
}
export type ValidateNotificationAction =
  | ValidateNotificationRequest
  | ValidateNotificationSuccess
  | ValidateNotificationFailure;

export type NotificationsAction =
  | LoadNotificationsAction
  | ValidateNotificationAction;

// History

export interface LoadHistoryRequest extends Action {
  type: "LOAD_HISTORY_REQUEST";
}
export interface LoadHistorySuccess extends PagedEntitiesListAction<number> {
  type: "LOAD_HISTORY_SUCCESS";
}
export interface LoadHistoryFailure extends ActionFailure {
  type: "LOAD_HISTORY_FAILURE";
}
export type LoadHistoryAction =
  | LoadHistoryRequest
  | LoadHistorySuccess
  | LoadHistoryFailure;

// Flash messages

export interface FlashMessageProps {
  body: string | React.ReactNode;
  title?: string | React.ReactNode;
  color: string;
}

export interface IFlashMessage extends FlashMessageProps {
  id: string;
  shown: boolean;
}

export interface AddFlashMessage {
  type: "ADD_FLASH_MESSAGE";
  message: IFlashMessage;
}
export interface DeleteFlashMessage {
  type: "DELETE_FLASH_MESSAGE";
  id: string;
}
export interface PruneFlashMessages {
  type: "PRUNE_FLASH_MESSAGES";
}
export interface FlagFlashMessageAsShown {
  type: "FLAG_FLASH_MESSAGE_AS_SHOWN";
  id: string;
}
export type FlashMessageAction =
  | AddFlashMessage
  | DeleteFlashMessage
  | PruneFlashMessages
  | FlagFlashMessageAsShown;

export interface SetSocketIOActiveAction {
  type: "SET_SOCKETIO_ACTIVE";
  status: boolean;
}

/*
 * Action groups
 */

export type EntitiesActionSuccesses =
  | TokenSuccess
  | LoadEventSuccess
  | LoadParticipantSuccess
  | RegisterParticipantSuccess
  | LoadParticipantsSuccess
  | UpdateParticipantSuccess
  | LoadNotificationsSuccess
  | LoadHistorySuccess
  | CheckPALSuccess;

export type PagedListActions =
  | LoadEventsSuccess
  | LoadParticipantsSuccess
  | LoadNotificationsSuccess
  | LoadHistorySuccess;

export type AllActions =
  | InitialStateAction
  | LoginCodeAction
  | TokenAction
  | LogoutAction
  | EventAction
  | ParticipantAction
  | PALAction
  | ParticipantsAction
  | NotificationsAction
  | LoadHistoryAction
  | FlashMessageAction
  | SetSocketIOActiveAction;

export type Thunk<R> = ActionCreator<
  ThunkAction<Promise<R>, IState, null, AllActions>
>;
// export type Thunk = ActionCreator<
//   ThunkAction<Promise<AllActions | void>, IState, null, AllActions>
// >;

// export type GetState = () => Object;
// export type PromiseAction = Promise<Action>;
// // eslint-disable-next-line no-use-before-define
// export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
// export type Dispatch = (
//   action: Action | ThunkAction | PromiseAction | Array<Action>
// ) => any;

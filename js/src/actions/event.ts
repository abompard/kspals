import { apiCall, loadAllPages } from "../api/call";
import { Thunk } from "./types";
import { getUserToken } from "../store/utils";
import { eventSchema, eventsSchema } from "../api/schemas";
import { APIError } from "../api/types";

export const loadEvents: Thunk<void> = () => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  dispatch({
    type: "LOAD_EVENTS_REQUEST"
  });
  return loadAllPages("/api/events/", url =>
    apiCall(url, eventsSchema, token).then(
      (result: any) => {
        dispatch({
          type: "LOAD_EVENTS_SUCCESS",
          response: result
        });
      },
      (error: APIError) => {
        dispatch({
          type: "LOAD_EVENTS_FAILURE",
          error
        });
      }
    )
  );
};

export const loadEvent: Thunk<void> = (eventCode: string) => (
  dispatch,
  getState
) => {
  const state = getState();
  const url = `/api/events/${eventCode}/`;
  const token = getUserToken(state);
  dispatch({
    type: "LOAD_EVENT_REQUEST"
  });
  return apiCall(url, eventSchema, token).then(
    (result: any) => {
      dispatch({
        type: "LOAD_EVENT_SUCCESS",
        response: result
      });
    },
    (error: APIError) => {
      dispatch({
        type: "LOAD_EVENT_FAILURE",
        error
      });
    }
  );
};

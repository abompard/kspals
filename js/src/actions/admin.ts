import { FormikHelpers } from "formik";
import querystring from "querystring";
import { apiCall, loadAllPages } from "../api/call";
import {
  historyListSchema,
  notificationSchema,
  notificationsSchema,
  participantFullSchema,
  participantSchema,
  participantsSchema,
  roleSchema,
  ticketRateSchema
} from "../api/schemas";
import {
  IHistory,
  INotification,
  IParticipant,
  IRole,
  ITicketRate,
  PaginatedResult,
  RoleAttributes,
  TicketRateAttributes
} from "../api/types";
import { FormValues, unique } from "../api/utils";
import { EntitiesState } from "../reducers/entities";
import { getEvent, getEventUrl, getUser, getUserToken } from "../store/utils";
import { FilterDict } from "../types";
import { Thunk, UpdateEventSuccess } from "./types";

export type updateEventResult = { entities: Pick<EntitiesState, "events"> };

export const updateEventSuccess = (
  result: updateEventResult
): UpdateEventSuccess => ({
  type: "UPDATE_EVENT_SUCCESS",
  response: result
});

export const loadParticipants: Thunk<void> = (eventCode: string) => (
  dispatch,
  getState
) => {
  const state = getState();
  const event = getEvent(state)!;
  const token = getUserToken(state);
  const date_request = new Date()
    .toISOString()
    .slice(0, 19)
    .replace("T", " ");
  dispatch({
    type: "LOAD_PARTICIPANTS_REQUEST"
  });
  let baseUrl = `${event.url}participants/`;
  if (state.ui.participant.lastLoad !== null) {
    baseUrl += `?date_modified_after=${state.ui.participant.lastLoad}`;
  }
  return loadAllPages(baseUrl, url =>
    apiCall(url, participantsSchema, token).then(
      result => {
        // Update the event's participants list
        const previousValue = getState().entities.events[event.code]
          .participants;
        result.entities["events"] = {
          [event.code]: {
            participants: unique([...(previousValue || []), ...result.result])
          }
        };
        dispatch({
          type: "LOAD_PARTICIPANTS_SUCCESS",
          response: result,
          date_request
        });
        return result;
      },
      error => {
        dispatch({
          type: "LOAD_PARTICIPANTS_FAILURE",
          error
        });
        return error;
      }
    )
  ).then(result => {
    dispatch({
      type: "LOAD_PARTICIPANTS_DONE"
    });
    return result;
  });
};

export const loadOneParticipant: Thunk<void> = (participant: IParticipant) => (
  dispatch,
  getState
) => {
  const state = getState();
  const token = getUserToken(state);
  dispatch({
    type: "LOAD_PARTICIPANT_REQUEST"
  });
  return apiCall(participant.url, participantSchema, token).then(
    result => {
      dispatch({
        type: "LOAD_PARTICIPANT_SUCCESS",
        response: result
      });
      return result;
    },
    error => {
      dispatch({
        type: "LOAD_PARTICIPANT_FAILURE",
        error
      });
    }
  );
};

export const updateParticipant: Thunk<IParticipant> = (
  participant: IParticipant,
  values: Partial<IParticipant>,
  formActions: FormikHelpers<FormValues<Partial<IParticipant>>>
) => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  const body = JSON.stringify(values);
  return apiCall(participant.url, participantFullSchema, token, {
    method: "PATCH",
    body
  }).then(
    result => {
      formActions.setSubmitting(false);
      dispatch({
        type: "UPDATE_PARTICIPANT_SUCCESS",
        response: result
      });
      return result.entities.participants[result.result];
    },
    error => {
      formActions.setSubmitting(false);
      formActions.setErrors(error.fields || {});
      throw error;
    }
  );
};

type AddParticipantValues = Pick<IParticipant, "email">;

export const addParticipant: Thunk<IParticipant> = (
  values: AddParticipantValues,
  formActions: FormikHelpers<FormValues<AddParticipantValues>>
) => (dispatch, getState) => {
  const state = getState();
  const event = getEvent(state)!;
  const token = getUserToken(state);
  const body = JSON.stringify(values);
  const url = `${getEventUrl(state)}participants/`;
  return apiCall(url, participantFullSchema, token, {
    method: "POST",
    body
  }).then(
    result => {
      formActions.setSubmitting(false);
      // Add the participant to the event's participants list
      result.entities["events"] = {
        [event.code]: {
          participants: [
            ...state.entities.events[event.code].participants,
            result.result
          ]
        }
      };
      dispatch({
        type: "ADD_PARTICIPANT_SUCCESS",
        response: result
      });
      return result.entities.participants[result.result];
    },
    error => {
      formActions.setSubmitting(false);
      formActions.setErrors(error.fields || {});
      throw error;
    }
  );
};

export const sellTicket: Thunk<IParticipant> = (
  participant: IParticipant,
  sell_to: string | null,
  formActions: FormikHelpers<FormValues<{ sell_to: string | null }>>
) => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  const body: { sell_to: string | null; autoapprove?: boolean } = { sell_to };
  // Is it an admin operation?
  const currentUser = getUser(state)!;
  const event = getEvent(state)!;
  const isOrganizer = (event.users_can_change || [])
    .map(o => o.id)
    .includes(currentUser.id);
  if (isOrganizer && participant.email !== currentUser.email) {
    // Admin and selling somebody else's ticket: don't go through the acceptance workflow.
    body.autoapprove = true;
  }
  return apiCall(`${participant.url}sell/`, participantFullSchema, token, {
    method: sell_to !== null ? "POST" : "DELETE",
    body: JSON.stringify(body)
  }).then(
    result => {
      formActions.setSubmitting(false);
      dispatch({
        type: "UPDATE_PARTICIPANT_SUCCESS",
        response: result
      });
      return result.entities.participants[result.result];
    },
    error => {
      formActions.setSubmitting(false);
      formActions.setErrors(error.fields || {});
      throw error;
    }
  );
};

export const buyTicket: Thunk<IParticipant> = (
  participant: IParticipant,
  sold_by: string,
  formActions: FormikHelpers<FormValues<{ sold_by: string | null }>>
) => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  const body = JSON.stringify({ sold_by });
  return apiCall(`${participant.url}buy/`, participantFullSchema, token, {
    method: "POST",
    body
  }).then(
    result => {
      formActions.setSubmitting(false);
      dispatch({
        type: "UPDATE_PARTICIPANT_SUCCESS",
        response: result
      });
      return result.entities.participants[result.result];
    },
    error => {
      formActions.setSubmitting(false);
      formActions.setErrors(error.fields || {});
      throw error;
    }
  );
};

export const loadNotifications: Thunk<void> = (
  includeSent: boolean = false
) => (dispatch, getState) => {
  const state = getState();
  const event = getEvent(state)!;
  let url = `${event.url}notifications/`;
  if (!includeSent) {
    url += "?status__in=DRAFT,TO_SEND";
  }
  const token = getUserToken(state);
  dispatch({
    type: "LOAD_NOTIFICATIONS_REQUEST"
  });
  return loadAllPages(url, url =>
    apiCall(url, notificationsSchema, token).then(
      result => {
        // Update the event's notifications list
        const previousValue = getState().entities.events[event.code]
          .notifications;
        result.entities["events"] = {
          [event.code]: {
            notifications: unique([...(previousValue || []), ...result.result])
          }
        };
        dispatch({
          type: "LOAD_NOTIFICATIONS_SUCCESS",
          response: result
        });
        return result;
      },
      error => {
        dispatch({
          type: "LOAD_NOTIFICATIONS_FAILURE",
          error
        });
        return error;
      }
    )
  ).then(result => {
    dispatch({
      type: "LOAD_NOTIFICATIONS_DONE"
    });
    return result;
  });
};

export const validateNotification: Thunk<void> = (
  notificationId: number,
  status: INotification["status"]
) => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  const notification = state.entities.notifications[notificationId];
  const body = JSON.stringify({ status });
  dispatch({
    type: "VALIDATE_NOTIFICATION_REQUEST"
  });
  return apiCall(notification.url, notificationSchema, token, {
    method: "PATCH",
    body
  }).then(
    result => {
      dispatch({
        type: "VALIDATE_NOTIFICATION_SUCCESS",
        response: result
      });
    },
    error => {
      dispatch({
        type: "VALIDATE_NOTIFICATION_FAILURE",
        error
      });
    }
  );
};

export const loadHistory: Thunk<PaginatedResult<IHistory>> = (
  page: number,
  filters: FilterDict
) => (dispatch, getState) => {
  const state = getState();
  const event = getEvent(state)!;
  const qs = querystring.stringify(filters);
  let url = `${event.url}history/?page=${page}&${qs}`;
  const token = getUserToken(state);
  dispatch({
    type: "LOAD_HISTORY_REQUEST"
  });
  return apiCall(url, historyListSchema, token).then(
    result => {
      // Update the event's history list
      result.entities["events"] = {
        [event.code]: {
          history: result.result
        }
      };
      dispatch({
        type: "LOAD_HISTORY_SUCCESS",
        response: {
          ...result,
          page: {
            ...result.page,
            filters: filters
          }
        }
      });
      return result;
    },
    error => {
      dispatch({
        type: "LOAD_HISTORY_FAILURE",
        error
      });
      return error;
    }
  );
};

export const loadParticipantHistory: Thunk<void> = (
  participant: IParticipant
) => (dispatch, getState) => {
  const state = getState();
  // Only load the last page
  let url = `${participant.url}history/?page=1`;
  const token = getUserToken(state);
  dispatch({
    type: "LOAD_HISTORY_REQUEST"
  });
  return apiCall(url, historyListSchema, token).then(
    result => {
      // Remove pagination information because we don't want to mess up the other history pages (we're re-using the action types here)
      result.page = undefined;
      result.entities.participants = {
        [participant.id]: {
          history: result.result
        }
      };
      dispatch({
        type: "LOAD_HISTORY_SUCCESS",
        response: result
      });
      return result;
    },
    error => {
      dispatch({
        type: "LOAD_HISTORY_FAILURE",
        error
      });
      return error;
    }
  );
};

/*
 * Roles
 */

export const updateRole: Thunk<IRole> = (
  role: IRole,
  values: RoleAttributes,
  formActions: FormikHelpers<RoleAttributes>
) => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  const body = JSON.stringify({
    ...values,
    default: values.default ? true : false
  });
  return apiCall(role.url, roleSchema, token, {
    method: "PATCH",
    body
  }).then(
    result => {
      formActions.setSubmitting(false);
      dispatch({
        type: "UPDATE_ROLE_SUCCESS",
        response: result
      });
      return result.entities.roles[result.result];
    },
    error => {
      formActions.setSubmitting(false);
      formActions.setErrors(error.fields || {});
      throw error;
    }
  );
};

/*
 * Ticket Rates
 */

export const updateTicketRate: Thunk<ITicketRate> = (
  ticketRate: ITicketRate,
  values: TicketRateAttributes,
  formActions: FormikHelpers<TicketRateAttributes>
) => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  const body = JSON.stringify({
    ...values,
    default: values.default ? true : false,
    low_income: values.low_income ? true : false,
    coupon: values.coupon === "" ? null : values.coupon
  });
  return apiCall(ticketRate.url, ticketRateSchema, token, {
    method: "PATCH",
    body
  }).then(
    result => {
      formActions.setSubmitting(false);
      dispatch({
        type: "UPDATE_TICKET_RATE_SUCCESS",
        response: result
      });
      return result.entities.ticket_rates[result.result];
    },
    error => {
      formActions.setSubmitting(false);
      formActions.setErrors(error.fields || {});
      throw error;
    }
  );
};

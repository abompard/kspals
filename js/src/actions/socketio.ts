import { SetSocketIOActiveAction } from "./types";

export const setSocketIOActive = (
  socketIOActive: boolean
): SetSocketIOActiveAction => ({
  type: "SET_SOCKETIO_ACTIVE",
  status: socketIOActive
});

import { FormikHelpers } from "formik";
import { normalize } from "normalizr";
import { apiCall, backendCall } from "../api/call";
import { palSchema, participantSchema } from "../api/schemas";
import { APIError, IParticipant, IParticipantProfile } from "../api/types";
import { FormValues } from "../api/utils";
import {
  getCurrentParticipant,
  getEventUrl,
  getUserToken
} from "../store/utils";
import { EntitiesResponse, Thunk } from "./types";

export const loadParticipant: Thunk<void> = (eventCode: string) => (
  dispatch,
  getState
) => {
  const state = getState();
  const eventUrl = getEventUrl(state);
  const url = `${eventUrl}participants/me/`;
  const token = getUserToken(state);
  dispatch({
    type: "LOAD_PARTICIPANT_REQUEST"
  });
  return apiCall(url, participantSchema, token).then(
    result => {
      dispatch({
        type: "LOAD_PARTICIPANT_SUCCESS",
        response: result
      });
      return result;
    },
    error => {
      dispatch({
        type: "LOAD_PARTICIPANT_FAILURE",
        error
      });
    }
  );
};

export const registerParticipant: Thunk<IParticipant> = (
  formData: FormData,
  formActions: FormikHelpers<FormValues<Partial<IParticipantProfile>>>
) => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  const participant = getCurrentParticipant(state);
  const isNew = participant === null || participant.profile === null;
  const eventUrl = getEventUrl(state);
  let url = `${eventUrl}participants/`;
  if (!isNew) {
    url += `${state.current.participant}/`;
  }
  const options: RequestInit = {
    method: isNew ? "POST" : "PATCH",
    body: formData
    // headers: {
    //   //"Content-Type": "multipart/form-data" // Reset the Content-Type, it's not JSON
    //   //"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8" // Reset the Content-Type, it's not JSON
    //   "Content-Type": undefined // Reset the Content-Type, it's not JSON
    // }
  };
  return backendCall(url, token, options).then(
    result => {
      formActions.setSubmitting(false);
      return result.json().then(json => {
        const normalizedResult = normalize(
          json,
          participantSchema
        ) as EntitiesResponse;
        dispatch({
          type: "REGISTER_PARTICIPANT_SUCCESS",
          response: normalizedResult
        });
        return normalizedResult.entities.participants![json.id];
      });
    },
    error => {
      let jsonPromise;
      if (error instanceof TypeError) {
        jsonPromise = Promise.resolve({
          fields: { profile: { non_field_errors: error.toString() } }
        });
      } else {
        jsonPromise = error.json();
      }
      return jsonPromise.then(
        (json: { fields: { profile: { [key: string]: Array<string> } } }) => {
          formActions.setSubmitting(false);
          formActions.setErrors(json.fields.profile || {});
          throw json;
        }
      );
    }
  );
};

export const checkPal: Thunk<void> = (index: number, email: string) => (
  dispatch,
  getState
) => {
  const state = getState();
  const token = getUserToken(state);
  const participant = getCurrentParticipant(state);
  if (participant === null) {
    throw new Error("No logged-in participant");
  }
  const socketIOActive = state.ui.socketIOActive;
  const body = JSON.stringify({ pal: email });
  let method: string, url: string;
  const pal = participant.pals[index];
  if (pal) {
    method = email ? "PUT" : "DELETE";
    url = pal.url;
  } else {
    method = "POST";
    url = `${participant.url}pals/`;
  }
  return apiCall(url, palSchema, token, { method, body }).then(
    result => {
      dispatch(
        email
          ? {
              type: "CHECK_PAL_SUCCESS",
              index,
              response: result
            }
          : {
              type: "DELETE_PAL_SUCCESS",
              index
            }
      );
      if (!socketIOActive) {
        return dispatch(loadParticipant());
      }
    },
    (error: APIError) => {
      throw error;
    }
  );
};

export const cancelParticipant: Thunk<void> = (
  formActions: FormikHelpers<FormValues<{}>>
) => (dispatch, getState) => {
  const state = getState();
  const token = getUserToken(state);
  const participant = getCurrentParticipant(state);
  if (participant === null) {
    throw new Error("No logged-in participant");
  }
  return apiCall(participant.url, null, token, { method: "DELETE" }).then(
    result => {
      formActions.setSubmitting(false);
      dispatch({
        type: "CANCEL_PARTICIPANT_SUCCESS",
        response: result
      });
    },
    (error: APIError) => {
      formActions.setSubmitting(false);
      formActions.setErrors(error.fields || {});
      throw error;
    }
  );
};

import { backendCall } from "../api/call";
import { Thunk } from "./types";
import { APIError } from "../api/types";

export const requestInitialState: Thunk<void> = () => (dispatch, getState) => {
  const url = "/initial-state";
  dispatch({
    type: "INITIAL_STATE_REQUEST"
  });
  return backendCall(url).then(
    response =>
      response.json().then(result => {
        dispatch({
          type: "INITIAL_STATE_SUCCESS",
          response: result
        });
      }),
    error => {
      const contentType = error.headers.get("content-type");
      const isJson = contentType && contentType.includes("application/json");
      if (isJson) {
        error.json().then((result: APIError) => {
          dispatch({
            type: "INITIAL_STATE_FAILURE",
            error: result
          });
        });
      } else {
        error.text().then((content: string) => {
          dispatch({
            type: "INITIAL_STATE_FAILURE",
            error: {
              detail: content,
              httpStatus: error.status
            }
          });
        });
      }
    }
  );
};

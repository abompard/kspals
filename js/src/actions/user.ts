import { apiCall } from "../api/call";
import { userSchema } from "../api/schemas";
import { getUserToken } from "../store/utils";
import { Thunk } from "./types";

export const requestLoginCode: Thunk<void> = (email: string) => (
  dispatch,
  getState
) => {
  const url = "/api/auth/email/";
  dispatch({
    type: "LOGIN_CODE_REQUEST"
  });
  const body = JSON.stringify({ email });
  return apiCall(url, null, null, { method: "POST", body }).then(
    result => {
      dispatch({
        type: "LOGIN_CODE_SUCCESS",
        email
      });
    },
    error => {
      dispatch({
        type: "LOGIN_CODE_FAILURE",
        error
      });
    }
  );
};

export const requestToken: Thunk<void> = (email: string, code: string) => (
  dispatch,
  getState
) => {
  const url = "/api/callback/auth-and-login/";
  dispatch({
    type: "TOKEN_REQUEST"
  });
  const body = JSON.stringify({
    email: email,
    token: code
  });
  return apiCall(url, userSchema, null, { method: "POST", body }).then(
    result => {
      dispatch({
        type: "TOKEN_SUCCESS",
        response: result
      });
    },
    error => {
      dispatch({
        type: "TOKEN_FAILURE",
        error
      });
    }
  );
};

export const logout: Thunk<void> = () => (dispatch, getState) => {
  const url = "/logout";
  const state = getState();
  const token = getUserToken(state);
  dispatch({
    type: "LOGOUT_REQUEST"
  });
  return apiCall(url, null, token, { method: "POST" }).then(
    result => {
      dispatch({
        type: "LOGOUT_SUCCESS"
      });
    },
    error => {
      dispatch({
        type: "LOGOUT_FAILURE",
        error
      });
    }
  );
};

import { getRandomString } from "../store/utils";
import {
  AddFlashMessage,
  DeleteFlashMessage,
  FlagFlashMessageAsShown,
  FlashMessageProps,
  PruneFlashMessages
} from "./types";

export const addFlashMessage = (
  message: FlashMessageProps
): AddFlashMessage => ({
  type: "ADD_FLASH_MESSAGE",
  message: {
    ...message,
    id: getRandomString(),
    shown: false
  }
});

export const deleteFlashMessage = (id: string): DeleteFlashMessage => ({
  type: "DELETE_FLASH_MESSAGE",
  id
});

export const pruneFlashMessages = (): PruneFlashMessages => ({
  type: "PRUNE_FLASH_MESSAGES"
});

export const flagFlashMessageAsShown = (
  id: string
): FlagFlashMessageAsShown => ({
  type: "FLAG_FLASH_MESSAGE_AS_SHOWN",
  id
});

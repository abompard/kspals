import React, { Component } from "react";
import { Button, Col, Form, FormGroup, Input, Label, Row } from "reactstrap";
import { IEvent, IParticipant } from "../api/types";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import FAIcon from "./FAIcon";
import { ParticipantsFilter } from "./ParticipantsFilter";
import ParticipantsTable from "./ParticipantsTable";

export interface BaseProps {
  event: IEvent;
  participants: Array<IParticipant>;
  validationMax: number;
  loadParticipants(): Promise<void>;
}

interface Props extends BaseProps {
  onlyMutual: boolean;
  filter: string;
  onOnlyMutualToggle(e: React.ChangeEvent<HTMLInputElement>): void;
  onFilterChange(value: string): void;
}

export class AdminQueuesValidation extends Component<Props> {
  render() {
    const baseurl = `/admin/${this.props.event.code}/queues/validation/`;
    const participants = this.props.participants.map((participant, index) => ({
      ...participant,
      registration_order: index + 1
    }));
    const participantsUnderThreshold = participants.slice(
      0,
      this.props.validationMax
    );
    const participantsOverThreshold = participants.slice(
      this.props.validationMax
    );
    return (
      <React.Fragment>
        <BreadcrumbHeader crumbs={["To do", "To approve"]} />

        <Button
          className="float-end"
          size="small"
          color="light"
          onClick={this.props.loadParticipants}
        >
          <FAIcon name="refresh" />
        </Button>
        <h3>
          Participants to approve{" "}
          <small>
            (
            {participantsUnderThreshold.length +
              participantsOverThreshold.length}
            )
          </small>
        </h3>

        <Form
          className="my-4"
          onSubmit={e => {
            e.preventDefault();
          }}
        >
          <Row>
            <Col md="6" lg="4" xl="3">
              <ParticipantsFilter
                value={this.props.filter}
                onChange={this.props.onFilterChange}
              />
              <FormGroup check className="mt-2">
                <Label check>
                  <Input
                    type="checkbox"
                    name="onlymutual"
                    onChange={this.props.onOnlyMutualToggle}
                    checked={this.props.onlyMutual}
                  />{" "}
                  Only mutual PALs
                </Label>
              </FormGroup>
            </Col>
          </Row>
        </Form>
        {this.props.participants.length === 0 ? (
          <p>No participant to approve.</p>
        ) : (
          <React.Fragment>
            {participantsUnderThreshold.length !== 0 ? (
              <React.Fragment>
                <h4>
                  Below the event's threshold{" "}
                  <small>({participantsUnderThreshold.length})</small>
                </h4>
                <ValidationParticipantsTable
                  event={this.props.event}
                  participants={participantsUnderThreshold}
                  baseUrl={baseurl}
                />
              </React.Fragment>
            ) : this.props.filter || this.props.onlyMutual ? null : (
              <p className="text-warning">
                The event's threshold has already been reached!
              </p>
            )}
            {participantsOverThreshold.length !== 0 ? (
              <React.Fragment>
                <h4>
                  Over the event's threshold{" "}
                  <small>({participantsOverThreshold.length})</small>
                </h4>
                <ValidationParticipantsTable
                  event={this.props.event}
                  participants={participantsOverThreshold}
                  baseUrl={baseurl}
                />
              </React.Fragment>
            ) : null}
            <hr className="my-5" />
            <p>
              <FAIcon name="save" className="me-2" />
              <a
                href={`/${this.props.event.code}/participants.csv?waitinglist=1`}
              >
                Export the waiting list
              </a>{" "}
              in CSV format.
            </p>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

interface ValidationParticipantsTableProps {
  event: IEvent;
  participants: Array<IParticipant>;
  baseUrl: string;
}

class ValidationParticipantsTable extends Component<
  ValidationParticipantsTableProps
> {
  render() {
    return (
      <ParticipantsTable
        event={this.props.event}
        participants={this.props.participants}
        baseParticipantsUrl={this.props.baseUrl}
        sortBy="registration_order"
        columns={[
          {
            attribute: "registration_order",
            header: "#"
          },
          {
            attribute: "profile.full_name",
            header: "Name"
          },
          {
            attribute: "email",
            header: "Email"
          },
          {
            attribute: "profile.date_registered",
            header: "Registered on"
          },
          {
            attribute: "mutual_pal_group",
            header: "Mutual PALs"
          }
        ]}
      />
    );
  }
}

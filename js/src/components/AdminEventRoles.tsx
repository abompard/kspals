import { ErrorMessage, Field, Formik, FormikHelpers } from "formik";
import React from "react";
import {
  Alert,
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap";
import { IRole, RoleAttributes } from "../api/types";
import FAIcon from "./FAIcon";
import { FormInput } from "./FormInput";

interface CommonProps {
  readOnly: boolean;
  updateRole: (
    role: IRole,
    values: RoleAttributes,
    actions: FormikHelpers<RoleAttributes>
  ) => Promise<IRole>;
}

interface AdminEventRolesProps extends CommonProps {
  roles: Array<IRole>;
  participantsCount: { [key: string]: number };
}

export const AdminEventRoles: React.FunctionComponent<AdminEventRolesProps> = props => {
  if (!props.roles) {
    return null;
  }

  return (
    <React.Fragment>
      {props.roles.map((role, index) => (
        <EventRoleForm
          key={role.id}
          role={role}
          showLabel={index === 0}
          participantsCount={props.participantsCount[role.code]}
          updateRole={props.updateRole}
          readOnly={props.readOnly}
        />
      ))}
    </React.Fragment>
  );
};

interface EventRoleFormProps extends CommonProps {
  role: IRole;
  showLabel: boolean;
  participantsCount: number;
}

const EventRoleForm: React.FunctionComponent<EventRoleFormProps> = props => {
  const handleSubmit = (
    values: RoleAttributes,
    actions: FormikHelpers<RoleAttributes>
  ) => {
    if (props.readOnly || !props.role || Object.keys(values).length === 0) {
      actions.setSubmitting(false);
      return;
    }
    props
      .updateRole(props.role, values, actions)
      .then(
        role => {
          actions.setStatus({
            color: "success",
            body: `The role ${role.name} has been updated.`
          });
        },
        error => {
          actions.setErrors(error.fields || {});
          actions.setStatus({
            color: "danger",
            body: "There was an error updating the role."
          });
        }
      )
      .then(() => {
        actions.setSubmitting(false);
      });
  };

  const initialValues: RoleAttributes = {
    code: props.role.code,
    default: props.role.default,
    name: props.role.name
  };

  const labelClass = props.showLabel ? "" : "d-lg-none";

  return (
    <Formik initialValues={initialValues} onSubmit={handleSubmit}>
      {({ handleSubmit, handleReset, isSubmitting, status, setStatus }) => (
        <React.Fragment>
          <Form onSubmit={handleSubmit} onReset={handleReset}>
            <Row className="form-row">
              <Col md={6} lg={3}>
                <FormGroup>
                  <Label for="code" className={labelClass}>
                    Code
                  </Label>
                  <Field
                    component={FormInput}
                    type="text"
                    name="code"
                    readOnly={props.readOnly}
                  />
                </FormGroup>
              </Col>
              <Col md={6} lg={3}>
                <FormGroup>
                  <Label for="name" className={labelClass}>
                    Name
                  </Label>
                  <Field
                    component={FormInput}
                    type="text"
                    name="name"
                    readOnly={props.readOnly}
                  />
                </FormGroup>
              </Col>
              <Col md={3} lg={2} className="d-flex align-items-end">
                <FormGroup check className="mb-3">
                  <Field
                    component={FormInput}
                    type="checkbox"
                    name="default"
                    readOnly={props.readOnly}
                    label="Default"
                  />
                </FormGroup>
              </Col>
              <Col md={3} lg={1} className="d-flex align-items-end">
                <FormGroup>
                  <Label className={labelClass}>Participants</Label>
                  <Input plaintext value={props.participantsCount} readOnly />
                </FormGroup>
              </Col>
              <Col md={6} lg={3} className="d-flex align-items-end">
                <FormGroup>
                  <Button color="primary" type="submit" disabled={isSubmitting}>
                    Update
                    {isSubmitting && (
                      <FAIcon name="spinner" className="fa-spin ms-3" />
                    )}
                  </Button>
                  <ErrorMessage name="non_field_errors" />
                </FormGroup>
              </Col>
            </Row>
            {status && status.body && (
              <Row>
                <Col>
                  <Alert
                    color={status.color}
                    className="text-center mb-4"
                    toggle={() => setStatus(null)}
                  >
                    {status.body}
                  </Alert>
                </Col>
              </Row>
            )}
          </Form>
          <hr className="d-lg-none my-3" />
        </React.Fragment>
      )}
    </Formik>
  );
};

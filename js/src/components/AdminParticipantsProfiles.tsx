import React, { Component } from "react";
import { Row, Col, CardText } from "reactstrap";
import { IEvent, IParticipant } from "../api/types";
import { ParticipantCard } from "./ParticipantCard";
import { BreadcrumbHeader } from "./BreadcrumbHeader";

interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
}

export class AdminParticipantsProfiles extends Component<Props> {
  render() {
    return (
      <React.Fragment>
        <BreadcrumbHeader crumbs={["Participants", "Profiles"]} />

        <h3>
          Participant profiles <small>({this.props.participants.length})</small>
        </h3>
        {this.props.participants.length === 0 ? (
          <p>No participants yet.</p>
        ) : (
          <Row>
            {this.props.participants.map(participant => (
              <ParticipantProfile
                key={participant.id}
                participant={participant}
                event={this.props.event}
              />
            ))}
          </Row>
        )}
      </React.Fragment>
    );
  }
}

interface ParticipantProfileProps {
  participant: IParticipant;
  event: IEvent;
}

class ParticipantProfile extends Component<ParticipantProfileProps> {
  render() {
    const palsZone = (
      <React.Fragment>
        <CardText className="mb-0">
          <strong>Pals:</strong>
        </CardText>
        <CardText tag="ul">
          {this.props.participant.pals.map(pal => (
            <li key={pal.id}>
              {pal.pal && pal.pal.profile ? (
                <React.Fragment>
                  <a href={`#p-${pal.pal.id}`}>{pal.pal.profile.full_name}</a> (
                  {pal.email})
                </React.Fragment>
              ) : (
                pal.email
              )}
            </li>
          ))}
        </CardText>
      </React.Fragment>
    );
    return (
      <Col lg="6" xl="4">
        <ParticipantCard
          participant={this.props.participant}
          event={this.props.event}
          id={`p-${this.props.participant.id}`}
          bottom={palsZone}
        />
      </Col>
    );
  }
}

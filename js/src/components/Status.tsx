import * as React from "react";
import { Alert } from "reactstrap";
import "./Status.css";

export default class Status extends React.Component<{
  color: string;
  content: React.ReactNode;
}> {
  static defaultProps = {
    color: "info"
  };

  render() {
    return (
      <Alert color={this.props.color} className="Status mx-auto mt-4">
        {this.props.content}
      </Alert>
    );
  }
}

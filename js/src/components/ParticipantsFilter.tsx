import React, { Component } from "react";
import { Button, Input, InputGroup, InputGroupText } from "reactstrap";
import FAIcon from "./FAIcon";

interface Props {
  onChange: (value: string) => void;
  value: string;
  inputProps?: object;
  className?: string;
  focus?: boolean;
}

export class ParticipantsFilter extends Component<Props> {
  input: React.RefObject<HTMLInputElement> = React.createRef();

  componentDidMount = () => {
    this.props.focus && this.input.current && this.input.current.focus();
  };

  handleFilterChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const value = e.target.value;
    this.props.onChange(value);
  };

  handleClear = (e: React.MouseEvent<HTMLButtonElement>): void => {
    this.props.onChange("");
    this.input.current && this.input.current.focus();
  };

  render() {
    return (
      <InputGroup>
        <InputGroupText>
          <FAIcon name="search" />
        </InputGroupText>

        <Input
          name="filter"
          id="filter"
          placeholder="Filter by name or email"
          onChange={this.handleFilterChange}
          value={this.props.value}
          bsSize="sm"
          style={{ width: "auto" }}
          autoComplete="off"
          {...this.props.inputProps}
          innerRef={this.input}
        />

        <Button onClick={this.handleClear} size="sm" outline>
          <FAIcon name="times-circle" />
        </Button>
      </InputGroup>
    );
  }
}

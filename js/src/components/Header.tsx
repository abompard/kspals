import * as React from "react";
import { FormattedDate, FormattedRelative, FormattedTime } from "react-intl";
import { UncontrolledTooltip } from "reactstrap";
import "./Header.css";

interface Props {
  maxDate: string;
}

export default class Header extends React.Component<Props> {
  render() {
    return (
      <div className="Header">
        <h1>Gérer les PALs</h1>
        <p>
          Précisez ici l'adresse email de la ou des personnes avec qui vous
          formerez votre{" "}
          <a href="http://www.kinkysalon.fr/le-systeme-pal/">groupe de PALs</a>{" "}
          pour le Kinky Salon. Vous devrez arriver avec ces personnes, et vous
          serez responsables les un⋅es des autres durant toute la soirée.
        </p>
        <p>
          Ce site web <strong> n'enverra pas de mail </strong> aux adresses de
          vos PALs : vous devrez vous coordonner avec elleux pour que chacun⋅e
          précise l'adresse email des autres dans son formulaire.
          Communiquez-vous clairement les adresses que vous utilisez !
        </p>
        <p>
          Quand nous constaterons que votre groupe est complet, nous pourrons le
          valider, et un email avec le lien vers la billetterie sera envoyé à
          chacun⋅e de vous. Vous pourrez toutefois revenir sur cette page pour
          modifier vos PALs jusqu'à la date limite. Mais{" "}
          <strong> attention </strong> : si votre groupe de PALs n'est pas
          cohérent (c'est à dire qu'iels se sont toustes désigné⋅es les un⋅es
          les autres) à la date limite, vous ne pourrez pas entrer !
        </p>
        <p>
          La date limite de modification de votre ou de vos PAL(s) est :{" "}
          <strong id="timeleft">
            <FormattedRelative value={this.props.maxDate} />
          </strong>
          <UncontrolledTooltip placement="top" target="timeleft">
            <FormattedDate
              value={this.props.maxDate}
              month="long"
              day="numeric"
              weekday="long"
            />{" "}
            <FormattedTime value={this.props.maxDate} />
          </UncontrolledTooltip>
          .
        </p>
      </div>
    );
  }
}

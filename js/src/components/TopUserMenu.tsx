import * as React from "react";
import { Link, NavLink } from "react-router-dom";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  NavItem,
  UncontrolledDropdown
} from "reactstrap";
import { IEvent, IUser } from "../api/types";
import { ILink } from "./TopBar";

interface Props {
  user: IUser | null;
  event: IEvent | null;
  links?: Array<ILink>;
  onLogout(): void;
}

export default class TopUserMenu extends React.Component<Props> {
  handleLogout = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    this.props.onLogout();
  };

  render() {
    if (this.props.user === null) {
      return (
        <NavItem>
          <Link to="/login" className="nav-link">
            Se connecter
          </Link>
        </NavItem>
      );
    } else {
      return (
        <UncontrolledDropdown nav>
          <DropdownToggle nav caret>
            {this.props.user.email}
          </DropdownToggle>
          <DropdownMenu>
            {(this.props.links || []).map(link => (
              <DropdownItem key={link.href} tag={NavLink} to={link.href} exact>
                {link.text}
              </DropdownItem>
            ))}
            <DropdownItem onClick={this.handleLogout} tag="a" href="/logout">
              Déconnexion
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      );
    }
  }
}

import { ErrorMessage, Field, Formik, FormikHelpers } from "formik";
import React, { Component } from "react";
import { FormattedDate, FormattedTime } from "react-intl";
import { Link } from "react-router-dom";
import { Button, Col, Form, FormGroup, Label, Row, Table } from "reactstrap";
import { IEvent, IParticipant } from "../api/types";
import { FormValues, nullBoolToString, stringToNullBool } from "../api/utils";
import { AdminParticipantPals } from "./AdminParticipantPals";
import {
  AdminParticipantSellTicket,
  BuyTicketFormValues,
  SellTicketFormValues
} from "./AdminParticipantSellTicket";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import FAIcon from "./FAIcon";
import { FormInput } from "./FormInput";
import { ParticipantCard } from "./ParticipantCard";

export interface Props {
  event: IEvent;
  participant: IParticipant;
  readOnly: boolean;
  isHistoryLoading: boolean;
  participantsWithoutTicket: Array<IParticipant>;
  socketIOActive: boolean;
  updateParticipant: (
    participant: IParticipant,
    values: ParticipantAttributes,
    actions: FormikHelpers<ParticipantFormValues>
  ) => Promise<IParticipant>;
  loadParticipantHistory: (participant: IParticipant) => Promise<void>;
  loadParticipants(): Promise<void>;
  sellTicket: (
    participant: IParticipant,
    sell_to: string | null,
    actions: FormikHelpers<SellTicketFormValues>
  ) => Promise<IParticipant>;
  buyTicket: (
    participant: IParticipant,
    sold_by: string,
    actions: FormikHelpers<BuyTicketFormValues>
  ) => Promise<IParticipant>;
}

export class AdminParticipantsProfile extends Component<Props> {
  render() {
    return (
      <React.Fragment>
        <BreadcrumbHeader
          crumbs={[
            "Participants",
            <Link to={`/admin/${this.props.event.code}/participants/`}>
              List
            </Link>,
            this.props.participant.profile.full_name
          ]}
        />

        <h3>Participant: {this.props.participant.profile.full_name}</h3>

        <Row className="justify-content-center">
          <Col lg="10" xl="8">
            <ParticipantCard
              participant={this.props.participant}
              event={this.props.event}
            />
          </Col>
        </Row>

        <Row className="justify-content-center mb-4">
          <Col lg="8" xl="6">
            <UpdateForm key={this.props.participant.id} {...this.props} />
          </Col>
        </Row>

        <AdminParticipantPals
          event={this.props.event}
          participant={this.props.participant}
        />

        <AdminParticipantSellTicket {...this.props} />

        <ParticipantHistory
          participant={this.props.participant}
          loadParticipantHistory={this.props.loadParticipantHistory}
          isHistoryLoading={this.props.isHistoryLoading}
        />
      </React.Fragment>
    );
  }
}

type ParticipantAttributes = Pick<
  IParticipant,
  | "approved"
  | "role"
  | "ticket_rate"
  | "ticket_name"
  | "ticket_status"
  | "drinks"
>;
type ParticipantFormValues = FormValues<ParticipantAttributes>;

class UpdateForm extends React.Component<Props> {
  handleSubmit = (
    values: ParticipantFormValues,
    actions: FormikHelpers<ParticipantFormValues>
  ) => {
    if (
      this.props.readOnly ||
      !this.props.participant ||
      Object.keys(values).length === 0
    ) {
      actions.setSubmitting(false);
      return;
    }
    const realValues: ParticipantAttributes = {
      ...values,
      approved: stringToNullBool(values.approved),
      drinks: parseInt(values.drinks, 10),
      ticket_status: values.ticket_status as IParticipant["ticket_status"]
    };
    this.props
      .updateParticipant(this.props.participant, realValues, actions)
      .then(
        () => {
          actions.setStatus({
            color: "success",
            body: `Participant ${this.props.participant.profile.full_name} has been updated.`
          });
        },
        () => {
          actions.setStatus({
            color: "danger",
            body: "There was an error updating the participant."
          });
        }
      );
  };

  render() {
    if (this.props.readOnly) {
      return null;
    }
    const leftWidth = { xs: 5, sm: 3 },
      rightWidth = { xs: 12 - leftWidth["xs"], sm: 12 - leftWidth["sm"] };
    const initialValues: ParticipantFormValues = {
      approved: nullBoolToString(this.props.participant.approved),
      role: this.props.participant.role || "",
      ticket_rate: this.props.participant.ticket_rate,
      ticket_status: this.props.participant.ticket_status || "",
      ticket_name: this.props.participant.ticket_name || "",
      drinks: this.props.participant.drinks.toString()
    };
    return (
      <Formik initialValues={initialValues} onSubmit={this.handleSubmit}>
        {({ values, status, handleSubmit, handleReset, isSubmitting }) => (
          <Form onSubmit={handleSubmit} onReset={handleReset}>
            <FormGroup row>
              <Label for="approved" {...leftWidth}>
                Approved
              </Label>
              <Col {...rightWidth}>
                <Field component={FormInput} type="select" name="approved">
                  <option value="true">Oui</option>
                  <option value="">Indéterminé</option>
                  <option value="false">Non</option>
                </Field>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="role" {...leftWidth}>
                Role
              </Label>
              <Col {...rightWidth}>
                <Field component={FormInput} type="select" name="role">
                  {this.props.event.roles &&
                    this.props.event.roles.map(role => (
                      <option key={role.code} value={role.code}>
                        {role.name}
                      </option>
                    ))}
                </Field>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="ticket_rate" {...leftWidth}>
                Ticket rate
              </Label>
              <Col {...rightWidth}>
                <Field component={FormInput} type="select" name="ticket_rate">
                  {this.props.event.ticket_rates &&
                    this.props.event.ticket_rates.map(rate => (
                      <option key={rate.id}>{rate.name}</option>
                    ))}
                </Field>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="ticket_status" {...leftWidth}>
                Ticket status
              </Label>
              <Col {...rightWidth}>
                <Field component={FormInput} type="select" name="ticket_status">
                  {this.props.event.ticket_statuses &&
                    this.props.event.ticket_statuses.map(status => (
                      <option key={status.code} value={status.code}>
                        {status.name}
                      </option>
                    ))}
                </Field>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="ticket_name" {...leftWidth}>
                Ticket name
              </Label>
              <Col {...rightWidth}>
                <Field
                  component={FormInput}
                  type="text"
                  name="ticket_name"
                  value={values.ticket_name || ""}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="drinks" {...leftWidth}>
                Drinks
              </Label>
              <Col {...rightWidth}>
                <Field component={FormInput} type="number" name="drinks" />
              </Col>
            </FormGroup>
            {status && status.body && (
              <FormGroup row>
                <Col>
                  <div
                    className={`alert alert-${status.color} text-center mt-3`}
                  >
                    {status.body}
                  </div>
                </Col>
              </FormGroup>
            )}
            <FormGroup row>
              <Col
                xs={{ size: rightWidth["xs"], offset: leftWidth["xs"] }}
                sm={{ size: rightWidth["sm"], offset: leftWidth["sm"] }}
              >
                <Button color="primary" type="submit" disabled={isSubmitting}>
                  Update
                  {isSubmitting && (
                    <FAIcon name="spinner" className="fa-spin ms-3" />
                  )}
                </Button>
                <ErrorMessage name="non_field_errors" />
              </Col>
            </FormGroup>
          </Form>
        )}
      </Formik>
    );
  }
}

type ParticipantHistoryProps = Pick<
  Props,
  "participant" | "loadParticipantHistory" | "isHistoryLoading"
>;

class ParticipantHistory extends React.Component<ParticipantHistoryProps> {
  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    this.props.loadParticipantHistory(this.props.participant);
  };

  componentDidUpdate(prevProps: Props) {
    if (prevProps.participant.id !== this.props.participant.id) {
      this.loadData();
    }
  }
  render() {
    return (
      <React.Fragment>
        <h4 className="mt-4">History</h4>
        {this.props.isHistoryLoading ? (
          <FAIcon name="spinner" className="fa-spin" />
        ) : !this.props.participant.history ? (
          <p>No history yet.</p>
        ) : (
          <Row className="justify-content-center">
            <Col lg="10" xl="8">
              <Table hover size="sm">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Action</th>
                    <th>Data</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.participant.history.map(history => (
                    <tr key={history.id}>
                      <td>
                        <FormattedDate value={history.date} />{" "}
                        <FormattedTime value={history.date} />
                      </td>
                      <td>{history.action}</td>
                      <td>{history.data}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Col>
          </Row>
        )}
      </React.Fragment>
    );
  }
}

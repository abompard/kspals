import { FieldProps } from "formik";
import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { FormFeedback, Input, InputGroup, Label } from "reactstrap";
import InputSpinner from "./InputSpinner";

type PossibleValues = string | number | string[];

interface ComponentArgs extends FieldProps<PossibleValues> {
  readOnly: boolean;
  rightButton?: React.ReactNode;
  withSpinner?: boolean;
  label?: string;
}

export const FormInput = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, isSubmitting, values }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  rightButton,
  withSpinner,
  label,
  ...props
}: ComponentArgs) => {
  const input = (
    <Input
      id={field.name}
      invalid={touched[field.name] && errors[field.name] ? true : false}
      {...field}
      {...props}
    />
  );
  const feedback = touched[field.name] && errors[field.name] && (
    <FormFeedback>{errors[field.name]}</FormFeedback>
  );
  if (label) {
    // this is a checkbox
    return (
      <Label check>
        {input}
        {label && ` ${label}`}
        {feedback}
      </Label>
    );
  } else {
    return (
      <InputGroup>
        {input}
        {withSpinner && <InputSpinner isLoading={isSubmitting || true} />}
        {rightButton}
        {feedback}
      </InputGroup>
    );
  }
};

export const DateTimeFormInput = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, values, setValues }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  meta,
  ...props
}: ComponentArgs) => {
  const dateValue = new Date((meta && meta.value) || values[field.name]);
  const onChange = (value: Date | null) => {
    if (!value) {
      return;
    }
    setValues({ ...values, [field.name]: value.toISOString() });
  };

  class CustomInput extends React.Component<{ value?: string; onClick?: any }> {
    render() {
      const { value, onClick } = this.props;
      return (
        <Input
          id={field.name}
          name={field.name}
          type="text"
          invalid={touched[field.name] && errors[field.name] ? true : false}
          value={value}
          onClick={onClick}
          onChange={() => null}
          readOnly={props.readOnly}
        />
      );
    }
  }

  return (
    <React.Fragment>
      <DatePicker
        selected={dateValue}
        onChange={onChange}
        onBlur={field.onBlur}
        showTimeSelect
        timeFormat="HH:mm"
        timeIntervals={30}
        timeCaption="time"
        dateFormat="Pp"
        locale={navigator.language}
        customInput={<CustomInput />}
      />
      <FormFeedback>{errors[field.name]}</FormFeedback>
    </React.Fragment>
  );
};

import { Formik, FormikHelpers } from "formik";
import { History } from "history";
import * as React from "react";
import { Alert, Button, Form, Modal, ModalBody, ModalHeader } from "reactstrap";
import { FlashMessageProps } from "../actions/types";
import { IEvent, IParticipant } from "../api/types";
import FAIcon from "./FAIcon";

export interface Props {
  event: IEvent;
  participant: IParticipant | null;
  cancelParticipant(actions: FormikHelpers<{}>): Promise<void>;
  history: History;
  addFlashMessage(message: FlashMessageProps): void;
}

interface State {
  modalOpen: boolean;
}

export default class CancelParticipation extends React.Component<Props, State> {
  state = { modalOpen: false };
  toggle = () => {
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  handleSubmit = (values: {}, actions: FormikHelpers<{}>) => {
    this.props.cancelParticipant(actions).then(
      () => {
        this.props.addFlashMessage({
          color: "success",
          body: `Votre participation a été annulée.`
        });
        this.setState({ modalOpen: false });
        //this.props.history.push(`/${this.props.event.code}/`);
      },
      () => {
        actions.setStatus({
          color: "danger",
          body: "Il y a eu une erreur à la suppression de votre participation."
        });
      }
    );
  };

  render() {
    const title = "Annuler ma participation";
    if (
      this.props.participant === null ||
      this.props.participant.profile === null
    ) {
      return null;
    }
    return (
      <React.Fragment>
        <Button color="danger" onClick={this.toggle}>
          <FAIcon name="exclamation-triangle" className="me-2" />
          {title}…
        </Button>
        <Modal isOpen={this.state.modalOpen} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>{title}</ModalHeader>
          <ModalBody>
            <p>
              Attention, si vous annulez votre participation et que vous changez
              d'avis, il faudra re-remplir le formulaire d'inscription.
            </p>
            {this.props.participant.ticket_status === "NOT_READY" && (
              <p>
                Si vous étiez en liste d'attente, vous perdrez votre place sur
                cette liste.
              </p>
            )}
            {this.props.participant.has_ticket && (
              <p>
                Vous avez déjà acheté un billet, contactez l'équipe pour savoir
                s'il est possible de vous le rembourser.
              </p>
            )}
            <p>Êtes-vous sûr⋅e ?</p>
            <Formik initialValues={{}} onSubmit={this.handleSubmit}>
              {({ status, handleSubmit, handleReset, isSubmitting }) => (
                <Form onSubmit={handleSubmit} onReset={handleReset}>
                  {status && status.body && (
                    <Alert color={status.color} className="text-center mt-3">
                      {status.body}
                    </Alert>
                  )}
                  <p className="mb-0 text-center">
                    <Button
                      color="danger"
                      type="submit"
                      disabled={isSubmitting}
                    >
                      {title}
                    </Button>
                  </p>
                </Form>
              )}
            </Formik>
          </ModalBody>
        </Modal>
      </React.Fragment>
    );
  }
}

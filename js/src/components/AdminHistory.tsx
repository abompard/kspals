import _ from "lodash";
import querystring from "querystring";
import React, { Component } from "react";
import { FormattedDate, FormattedTime } from "react-intl";
import { Link, RouteComponentProps } from "react-router-dom";
import {
  Button,
  Form,
  FormGroup,
  Input,
  InputGroup,
  Label,
  Table
} from "reactstrap";
import { IEvent, IHistory, PaginatedResult } from "../api/types";
import { Page } from "../store/pagination";
import { FilterDict } from "../types";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import FAIcon from "./FAIcon";
import { PaginationBar } from "./PaginationBar";

interface Props extends RouteComponentProps {
  event: IEvent;
  pageNr: number;
  filters: FilterDict;
  page: Page<IHistory>;
  isLoading: boolean;
  error: string | null;
  loadHistory(
    page: number,
    filters: FilterDict
  ): Promise<PaginatedResult<IHistory>>;
}

export class AdminHistory extends Component<Props> {
  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.pageNr !== this.props.pageNr) {
      this.loadData();
    }
    if (!_.isEqual(prevProps.filters, this.props.filters)) {
      this.loadData();
    }
  }

  loadData = () => {
    if (
      this.props.page.objects.length === 0 ||
      !_.isEqual(this.props.page.filters, this.props.filters)
    ) {
      this.props.loadHistory(this.props.pageNr, this.props.filters);
    }
  };

  onFilterChange = (filters: FilterDict) => {
    this.props.history.push({
      search: `?${querystring.stringify({
        page: "1",
        ...this.props.filters,
        ...filters
      })}`
    });
  };

  render() {
    const baseUrl = `/admin/${this.props.event.code}/participants/profiles/`;

    return (
      <React.Fragment>
        <BreadcrumbHeader crumbs={["Other", "History"]} />

        <Button
          className="float-end"
          size="small"
          color="light"
          onClick={() =>
            this.props.loadHistory(this.props.pageNr, this.props.filters)
          }
        >
          <FAIcon name="refresh" />
        </Button>

        <h3>
          History
          <small className="ms-2">
            {this.props.page.count && `(${this.props.page.count})`}
          </small>
        </h3>

        {this.props.error && (
          <p className="text-danger">
            Error loading the history: {this.props.error}.
          </p>
        )}

        {!this.props.isLoading && this.props.page.objects.length === 0 ? (
          <p>No history yet.</p>
        ) : (
          <React.Fragment>
            <HistoryFilter
              filters={this.props.filters}
              onChange={this.onFilterChange}
              key={querystring.stringify(this.props.filters)}
            />
            <PaginationBar
              page={this.props.page}
              queryString={this.props.filters}
            />
            {this.props.isLoading && (
              <div className="text-center py-3">
                <FAIcon name="spinner" className="fa-spin text-muted" />
              </div>
            )}
            {this.props.page.objects.length === 0 ? null : (
              <Table hover size="sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Participant</th>
                    <th>Action</th>
                    <th>Data</th>
                    <th>Happened on</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.page.objects.map(history => (
                    <tr key={history.id}>
                      <td>{history.id}</td>
                      <td>
                        {history.participant ? (
                          <Link to={`${baseUrl}${history.participant.id}/`}>
                            {history.participant.profile
                              ? history.participant.profile.full_name
                              : history.participant.email}
                          </Link>
                        ) : (
                          ""
                        )}
                      </td>
                      <td>{history.action}</td>
                      <td>{history.data}</td>
                      <td>
                        <FormattedDate value={history.date} />{" "}
                        <FormattedTime value={history.date} />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            )}
            <PaginationBar page={this.props.page} />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

interface HistoryProps {
  filters: FilterDict;
  onChange(filters: FilterDict): void;
}

const HistoryFilter: React.FunctionComponent<HistoryProps> = props => {
  const [participant, setParticipant] = React.useState(
    props.filters.participant || ""
  );
  const [email, setEmail] = React.useState(props.filters.email || "");
  const [action, setAction] = React.useState(props.filters.action || "");
  const [dateMin, setDateMin] = React.useState(props.filters.date__gt || "");
  const [dateMax, setDateMax] = React.useState(props.filters.date__lt || "");

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const name = e.currentTarget.name;
    const value = e.currentTarget.value;
    switch (name) {
      case "date_min":
        setDateMin(value);
        break;
      case "date_max":
        setDateMax(value);
        break;
      case "email":
        setEmail(value);
        break;
      case "participant":
        setParticipant(value);
        break;
      case "action":
        setAction(value);
        break;
    }
  };

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    props.onChange({
      participant: participant,
      email: email,
      action: action,
      date__gt: dateMin,
      date__lt: dateMax
    });
  };

  return (
    <React.Fragment>
      <h5 className="mt-3">Filters</h5>
      <Form
        inline
        onSubmit={onSubmit}
        className="mb-3 row row-cols-sm-auto g-1 align-items-center"
      >
        <FormGroup>
          <Label className="sr-only" for="participant">
            Participant ID
          </Label>
          <ClearableInput
            type="text"
            bsSize="sm"
            name="participant"
            value={participant}
            placeholder="Participant ID"
            onChange={onChange}
          />
        </FormGroup>
        <FormGroup>
          <Label className="sr-only" for="email">
            Email
          </Label>
          <ClearableInput
            type="text"
            bsSize="sm"
            name="email"
            value={email}
            placeholder="Email"
            onChange={onChange}
          />
        </FormGroup>
        <FormGroup>
          <Label className="sr-only" for="action">
            Action
          </Label>
          <ClearableInput
            type="text"
            bsSize="sm"
            name="action"
            value={action}
            placeholder="Action"
            onChange={onChange}
          />
        </FormGroup>
        <FormGroup>
          <Label className="sr-only" for="date_min">
            Date min
          </Label>
          <Input
            type="datetime-local"
            bsSize="sm"
            name="date_min"
            value={dateMin}
            placeholder="Date min"
            onChange={onChange}
          />
        </FormGroup>
        <FormGroup>
          <Label className="sr-only" for="date_max">
            Date max
          </Label>
          <Input
            type="datetime-local"
            bsSize="sm"
            name="date_max"
            value={dateMax}
            placeholder="Date max"
            onChange={onChange}
          />
        </FormGroup>
        <FormGroup>
          <Button size="sm" type="submit">
            Filter
          </Button>
        </FormGroup>
      </Form>
    </React.Fragment>
  );
};

const ClearableInput: React.FunctionComponent<any> = props => {
  const ref = React.createRef();
  const triggerClear = () => {
    const input = ref.current as HTMLInputElement;
    props.onChange({
      currentTarget: {
        name: props.name,
        value: ""
      }
    } as React.ChangeEvent<HTMLInputElement>);
    input.focus();
  };
  return (
    <InputGroup>
      <Input {...props} innerRef={ref} />
      <Button color="secondary" onClick={triggerClear} size={props.bsSize}>
        <FAIcon name="times-circle" />
      </Button>
    </InputGroup>
  );
};

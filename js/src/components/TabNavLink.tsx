import React from "react";
import { NavLink } from "reactstrap";

interface NavLinkProps {
    activeTab: string;
    tabId: string;
    onTabChange: React.MouseEventHandler<HTMLAnchorElement>;
    children: React.ReactNode;
  }

export const TabNavLink = (props: NavLinkProps) => (
    <NavLink
      className={props.activeTab === props.tabId ? "active" : undefined}
      onClick={props.onTabChange}
      data-tabid={props.tabId}
      style={{ cursor: "pointer" }}
    >
      {props.children}
    </NavLink>
  );

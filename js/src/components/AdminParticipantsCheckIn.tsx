import React, { Component } from "react";
import { Row, Col, Form, FormGroup, Label, Input } from "reactstrap";
import { IEvent, IParticipant } from "../api/types";
import { ParticipantCard } from "./ParticipantCard";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import { ParticipantsFilter } from "./ParticipantsFilter";
import { filterParticipants } from "../api/utils";

interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
}

interface State {
  filter: string;
  onlyValid: boolean;
}

export class AdminParticipantsCheckIn extends Component<Props, State> {
  state = { filter: "", onlyValid: true };

  handleFilterChange = (value: string): void => {
    this.setState({ filter: value });
  };

  handleOnlyValidChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.currentTarget.checked;
    this.setState({
      onlyValid: value
    });
  };

  render() {
    const participants = filterParticipants(
      this.props.participants,
      this.state.filter
    ).filter(participant => {
      if (!this.state.onlyValid) {
        return true;
      }
      return participant.valid_pal_group && participant.has_ticket;
    });
    const title = <h3>Check In for {this.props.event.name}</h3>;
    const breadcrumbs = (
      <BreadcrumbHeader crumbs={["Participants", "Check In"]} />
    );
    const filterElement = (
      <Form
        className="my-4"
        onSubmit={e => {
          e.preventDefault();
        }}
      >
        <Row>
          <Col md="6" lg="4" xl="3">
            <ParticipantsFilter
              value={this.state.filter}
              onChange={this.handleFilterChange}
              focus
            />
            <FormGroup check className="mt-2">
              <Label check>
                <Input
                  type="checkbox"
                  name="onlyvalid"
                  onChange={this.handleOnlyValidChange}
                  checked={this.state.onlyValid}
                />{" "}
                Only valid participants
              </Label>
            </FormGroup>
          </Col>
        </Row>
      </Form>
    );
    const errorMsg = (msg: string) => (
      <React.Fragment>
        {breadcrumbs}
        {title}
        {filterElement}
        <p>{msg}</p>
      </React.Fragment>
    );
    if (!this.state.filter) {
      return errorMsg("You must select a participant.");
    }
    if (this.state.filter.length < 3) {
      return errorMsg("At least 3 characters please...");
    }
    if (this.state.filter && participants.length === 0) {
      return errorMsg("Participant not found.");
    }
    return (
      <React.Fragment>
        {breadcrumbs}
        {title}
        {filterElement}

        {participants.map((participant, index) => (
          <ShowPalGroup
            key={participant.id}
            participant={participant}
            event={this.props.event}
            className={`my-3 pt-3 ${index === 0 ? "" : "border-top"}`}
          />
        ))}
      </React.Fragment>
    );
  }
}

interface ShowPalGroupProps {
  participant: IParticipant;
  event: IEvent;
  className?: string;
}

class ShowPalGroup extends Component<ShowPalGroupProps> {
  render() {
    const palGroup: Array<IParticipant> = [
      this.props.participant,
      ...this.props.participant.pals
        .map(pal => pal.pal)
        .filter(participant => participant)
    ];
    const className = this.props.className || "my-4";
    return (
      <Row className={className}>
        {palGroup.map((participant: IParticipant) => (
          <Col
            lg="6"
            xl={palGroup.length >= 3 ? "4" : "6"}
            key={participant.id}
          >
            <ParticipantCard
              participant={participant}
              event={this.props.event}
              noQuestions
              checkValid
              linkToProfile
            />
          </Col>
        ))}
      </Row>
    );
  }
}

import * as React from "react";
import { IPal, IParticipant } from "../api/types";
import FAIcon from "./FAIcon";

interface Props {
  className: string;
  pal: IPal | IParticipant | null;
  iconOnly: boolean;
  isSelf: boolean;
}

export default class ApprovedStatus extends React.Component<Props> {
  static defaultProps = {
    className: "",
    isSelf: false,
    iconOnly: false
  };

  render() {
    if (!this.props.pal) {
      return null;
    }
    if ("is_mutual" in this.props.pal && !this.props.pal.is_mutual) {
      return null; // Don't display the approval status on random pals
    }
    let color, text, icon;
    if (this.props.pal.approved) {
      color = "success";
      icon = "check";
      if (this.props.isSelf) {
        text = "Votre pré-inscription est validée par l'organisation.";
      } else {
        text =
          "La pré-inscription de votre PAL est validée par l'organisation.";
      }
    } else {
      color = "warning";
      icon = "hourglass-o";
      if (this.props.isSelf) {
        text =
          "Votre pré-inscription n'est pas encore validée par l'organisation.";
      } else {
        text =
          "La pré-inscription de votre PAL n'est pas encore validée par l'organisation.";
      }
    }
    const {
      className,
      pal,
      isSelf,
      iconOnly,
      children,
      ...otherProps
    } = this.props;
    if (this.props.iconOnly) {
      const newClassName = `fa-lg fa-fw text-${color} ${className}`;
      return (
        <FAIcon
          name={icon}
          className={newClassName}
          title={text}
          {...otherProps}
        />
      );
    } else {
      const newClassName = `mt-1 mb-0 text-${color} ${className}`;
      return (
        <p className={newClassName} {...otherProps}>
          <FAIcon name={icon} className="me-2 d-md-none" />
          {text}
        </p>
      );
    }
  }
}

import React, { Component } from "react";
import { FormattedDate, FormattedTime } from "react-intl";
import { Link } from "react-router-dom";
import { Table } from "reactstrap";
import { IEvent, INotification } from "../api/types";
import { BreadcrumbHeader } from "./BreadcrumbHeader";

interface Props {
  event: IEvent;
  notifications: Array<INotification>;
}

export class AdminQueuesNotifications extends Component<Props> {
  render() {
    const pending = this.props.notifications.filter(
      notif => notif.status === "TO_SEND"
    );
    const draft = this.props.notifications.filter(
      notif => notif.status === "DRAFT"
    );
    const sent = this.props.notifications.filter(
      notif => notif.status === "SENT"
    );
    return (
      <React.Fragment>
        <BreadcrumbHeader crumbs={["To do", "Notifications"]} />

        <h3>
          Notifications <small>({this.props.notifications.length})</small>
        </h3>

        {this.props.notifications.length === 0 && <p>No notification yet.</p>}

        {pending.length !== 0 && (
          <div className="mt-3">
            <h4>
              To send ASAP <small>({pending.length})</small>
            </h4>
            <NotificationsTable
              event={this.props.event}
              notifications={pending}
            />
          </div>
        )}

        {draft.length !== 0 && (
          <div className="mt-3">
            <h4>
              Waiting for approval <small>({draft.length})</small>
            </h4>
            <NotificationsTable
              event={this.props.event}
              notifications={draft}
            />
          </div>
        )}

        {sent.length !== 0 && (
          <div className="mt-3">
            <h4>
              Already sent <small>({sent.length})</small>
            </h4>
            <NotificationsTable event={this.props.event} notifications={sent} />
          </div>
        )}
      </React.Fragment>
    );
  }
}

class NotificationsTable extends Component<Props> {
  render() {
    const baseurl = `/admin/${this.props.event.code}/queues/notifications/`;
    return (
      <Table hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Type</th>
            <th>Participant</th>
            <th>Recipient(s)</th>
            <th>Created on</th>
            <th>Sent on</th>
          </tr>
        </thead>
        <tbody>
          {this.props.notifications.map(notif => (
            <tr key={notif.id}>
              <th scope="row">
                <Link to={`${baseurl}${notif.id}/`}>{notif.id}</Link>
              </th>
              <td>
                <Link to={`${baseurl}${notif.id}/`}>{notif.name_human}</Link>
              </td>
              <td>{notif.participant.profile.full_name}</td>
              <td>{notif.recipient.replace(",", ", ")}</td>
              <td>
                <FormattedDate value={notif.date_created} />{" "}
                <FormattedTime value={notif.date_created} />
              </td>
              <td>
                {notif.date_sent === null ? (
                  "Not sent yet."
                ) : (
                  <span>
                    <FormattedDate value={notif.date_sent} />{" "}
                    <FormattedTime value={notif.date_sent} />
                  </span>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}

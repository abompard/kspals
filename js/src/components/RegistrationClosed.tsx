import * as React from "react";
import { FormattedDate, FormattedRelative, FormattedTime } from "react-intl";
import { IEvent, IPal } from "../api/types";
import FAIcon from "./FAIcon";

export class RegistrationNotOpenedYet extends React.Component<{
  event: IEvent;
}> {
  render() {
    return (
      <div className="alert alert-warning">
        <p>
          <strong>Les pré-inscriptions ne sont pas encore ouvertes !</strong>
        </p>
        <p className="mb-0">
          Les pré-inscriptions pour le {this.props.event.name} ouvriront{" "}
          <strong>
            <FormattedRelative value={this.props.event.date_reg_open} />
          </strong>{" "}
          (le <FormattedDate value={this.props.event.date_reg_open} /> à{" "}
          <FormattedTime value={this.props.event.date_reg_open} />
          ).
        </p>
      </div>
    );
  }
}

export class RegistrationClosed extends React.Component<{
  children?: React.ReactNode;
}> {
  render() {
    return (
      <div className="alert alert-warning">
        <strong>Les pré-inscriptions sont fermées !</strong>
        {this.props.children}
      </div>
    );
  }
}

export class RegistrationClosedInvalidParticipant extends React.Component<{}> {
  render() {
    return (
      <RegistrationClosed>
        <p>
          Nous sommes désolé⋅e⋅s, mais il n'est plus possible de venir au Kinky
          Salon.
        </p>
      </RegistrationClosed>
    );
  }
}

export class RegistrationClosedValidParticipant extends React.Component<{
  pals: Array<IPal>;
}> {
  render() {
    const memoryMessage =
      this.props.pals.length === 1
        ? "Pour mémoire, votre PAL est :"
        : "Pour mémoire, vos PALs sont :";
    return (
      <div>
        <RegistrationClosed>
          <span className="ms-1">
            Il n'est plus possible de changer vos PALs.
          </span>
        </RegistrationClosed>
        <p>{memoryMessage}</p>
        <ul className="fa-ul">
          {this.props.pals.map(pal => (
            <li key={pal.email}>
              <i className="fa fa-li fa-hand-o-right" />
              <strong className="me-2">{pal.email}</strong>
              {pal.name && `(${pal.name})`}
            </li>
          ))}
        </ul>
        <p>
          N'oubliez pas que vous devez vous présenter avec tous vos PALs pour
          pouvoir entrer, et vous devez partir ensemble.
        </p>
      </div>
    );
  }
}

export class EventFull extends React.Component<{}> {
  render() {
    return (
      <RegistrationClosed>
        <p className="mb-0">
          Nous sommes désolé⋅e⋅s mais l'évènement est déjà à son nombre maximum
          de participant⋅e⋅s
          <FAIcon name="frown-o" className="ms-2" />
        </p>
        <p>Nous espérons vous voir la prochaine fois.</p>
      </RegistrationClosed>
    );
  }
}

interface EventPassedProps {
  event: IEvent;
}

export class EventPassed extends React.Component<EventPassedProps> {
  render() {
    const date_open = Date.parse(this.props.event.date_open);
    const date_end = date_open + 12 * 60 * 1000;
    const now = Date.now();
    return (
      <div className="alert alert-warning">
        <strong className="me-2">
          {now > date_end
            ? "L'évènement est maintenant terminé !"
            : "L'évènement est en cours !"}
        </strong>
        Impossible de modifier ses PALs.
      </div>
    );
  }
}

import React, { Component } from "react";
import { IEvent, IParticipant } from "../api/types";
import { ParticipantCard } from "./ParticipantCard";
import { Card, CardBody, CardText, Row, Col } from "reactstrap";
import { Link } from "react-router-dom";

interface Props {
  event: IEvent;
  participant: IParticipant;
}

export class AdminParticipantPals extends Component<Props> {
  render() {
    const profileUrl = `/admin/${this.props.event.code}/participants/profiles/`;
    if (this.props.participant.pals.length === 0) {
      return (
        <React.Fragment>
          <h4>PALs</h4>
          <p>This participant has not selected any PAL yet.</p>
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <h4>PALs</h4>
        <p>
          {this.props.participant.mutual_pal_group ? (
            <span className="text-success">The PAL group is mutual</span>
          ) : (
            <strong className="text-warning">
              The PAL group is not coherent
            </strong>
          )}
        </p>
        <p>
          {this.props.participant.profile.full_name} has currently selected the
          following PAL(s):
        </p>
        <Row className="justify-content-center">
          {this.props.participant.pals.map(pal => {
            let content;
            if (!pal.pal || !pal.pal.profile) {
              content = (
                <Card className="my-3">
                  <CardBody>
                    <CardText>
                      <strong>{pal.email}</strong> (they haven't registered yet)
                    </CardText>
                  </CardBody>
                </Card>
              );
            } else {
              const mutualMessage = pal.is_mutual ? (
                <CardText>
                  <Link to={`${profileUrl}${pal.pal.id}/`}>
                    {pal.pal.profile.full_name}
                  </Link>{" "}
                  <strong className="text-success">has also</strong> selected
                  them.
                </CardText>
              ) : (
                <CardText>
                  <Link to={`${profileUrl}${pal.pal.id}/`}>
                    {pal.pal.profile.full_name}
                  </Link>{" "}
                  <strong className="text-warning">has NOT</strong> selected
                  them back.
                </CardText>
              );
              content = (
                <ParticipantCard
                  participant={pal.pal}
                  event={this.props.event}
                  top={mutualMessage}
                />
              );
            }
            return (
              <Col
                lg="10"
                xl={this.props.participant.pals.length > 1 ? "6" : "8"}
                key={pal.id}
              >
                {content}
              </Col>
            );
          })}
        </Row>
      </React.Fragment>
    );
  }
}

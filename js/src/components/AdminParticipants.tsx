import React, { Component } from "react";
import { Col, Form, Nav, NavItem, Row, TabContent, TabPane } from "reactstrap";
import { IEvent, IPal, IParticipant, TicketStatus } from "../api/types";
import { filterParticipants } from "../api/utils";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import FAIcon from "./FAIcon";
import { ParticipantsFilter } from "./ParticipantsFilter";
import ParticipantsTable from "./ParticipantsTable";
import { SimplePalsList } from "./SimplePalsList";
import { TabNavLink } from "./TabNavLink";

export interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
  readOnly: boolean;
  addParticipantComponent: React.ReactNode;
  isLoading: boolean;
}

interface State {
  filter: string;
  activeTab: string;
}

export class AdminParticipants extends Component<Props, State> {
  state = { filter: "", activeTab: "1" };

  handleFilterChange = (value: string): void => {
    this.setState({ filter: value });
  };

  handleTabChange: React.MouseEventHandler<HTMLAnchorElement> = e => {
    const tabId = e.currentTarget.dataset.tabid || "1";
    if (this.state.activeTab !== tabId) {
      this.setState({
        activeTab: tabId
      });
    }
  };

  render() {
    const breadcrumbs = <BreadcrumbHeader crumbs={["Participants", "List"]} />;
    if (this.props.participants.length === 0 && !this.props.isLoading) {
      return (
        <React.Fragment>
          {breadcrumbs}
          <h3>Participants</h3>
          <p>No participants yet.</p>
        </React.Fragment>
      );
    }

    const participants = filterParticipants(
      this.props.participants,
      this.state.filter
    ).map((participant, index) => ({
      ...participant,
      registration_order: index + 1
    }));
    const issues = participants.filter(
      participant => participant.has_ticket && !participant.valid_pal_group
    );
    const confirmed = participants.filter(
      participant => participant.has_ticket && participant.valid_pal_group
    );
    const noTicket = participants.filter(
      participant => !participant.has_ticket
    );
    return (
      <React.Fragment>
        {breadcrumbs}

        {/* New participant */
        this.props.readOnly ? null : this.props.addParticipantComponent}

        <h3 className="mb-3">
          Participants <small>({this.props.participants.length})</small>
        </h3>

        <Form
          onSubmit={e => {
            e.preventDefault();
          }}
        >
          <Row>
            <Col md="6" lg="4" xl="3">
              <ParticipantsFilter
                value={this.state.filter}
                onChange={this.handleFilterChange}
              />
            </Col>
          </Row>
        </Form>

        <Nav tabs className="mt-4 mb-3">
          <NavItem>
            <TabNavLink
              activeTab={this.state.activeTab}
              tabId="1"
              onTabChange={this.handleTabChange}
            >
              Confirmed <small>({confirmed.length})</small>
            </TabNavLink>
          </NavItem>

          <NavItem>
            <TabNavLink
              activeTab={this.state.activeTab}
              tabId="2"
              onTabChange={this.handleTabChange}
            >
              {issues.length > 0 && (
                <FAIcon
                  name="exclamation-triangle"
                  className="me-2 text-danger"
                />
              )}
              Participants with issues <small>({issues.length})</small>
            </TabNavLink>
          </NavItem>

          <NavItem>
            <TabNavLink
              activeTab={this.state.activeTab}
              tabId="3"
              onTabChange={this.handleTabChange}
            >
              Participants without tickets <small>({noTicket.length})</small>
            </TabNavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <ConfirmedParticipants {...this.props} participants={confirmed} />
          </TabPane>

          <TabPane tabId="2">
            <ParticipantsWithIssues {...this.props} participants={issues} />
          </TabPane>

          <TabPane tabId="3">
            <ParticipantsWithoutTickets
              {...this.props}
              participants={noTicket}
            />
          </TabPane>
        </TabContent>
      </React.Fragment>
    );
  }
}

class ParticipantsWithIssues extends Component<Props> {
  render() {
    if (this.props.participants.length === 0) {
      if (this.props.isLoading) {
        return <p>Loading...</p>;
      }
      return <p>There are no participants with issues.</p>;
    }
    return (
      <ParticipantsTable
        event={this.props.event}
        participants={this.props.participants}
        sortBy="profile.full_name"
        columns={[
          {
            attribute: "registration_order",
            header: "#"
          },
          {
            attribute: "profile.full_name",
            header: "Name"
          },
          {
            attribute: "email",
            header: "Email"
          },
          {
            attribute: "profile.date_registered",
            header: "Registered on"
          },
          {
            attribute: "approved",
            header: "Approved"
          },
          {
            attribute: "mutual_pal_group",
            header: "Mutual PALs",
            render: (value: boolean): string | React.ReactNode =>
              value ? "Yes" : <strong className="text-danger">No</strong>
          },
          {
            attribute: "valid_pal_group",
            header: "Approved PALs",
            render: (value: boolean): string | React.ReactNode =>
              value ? "Yes" : <strong className="text-danger">No</strong>
          },
          {
            attribute: "has_ticket",
            header: "Ticket bought",
            render: (value: boolean): string | React.ReactNode =>
              value ? <strong className="text-danger">Yes</strong> : "No"
          }
        ]}
      />
    );
  }
}

class ConfirmedParticipants extends Component<Props> {
  render() {
    if (this.props.participants.length === 0) {
      if (this.props.isLoading) {
        return <p>Loading...</p>;
      }
      return <p>There are no confirmed participants yet.</p>;
    }
    return (
      <ParticipantsTable
        event={this.props.event}
        participants={this.props.participants}
        sortBy="profile.full_name"
        columns={[
          {
            attribute: "registration_order",
            header: "#"
          },
          {
            attribute: "profile.full_name",
            header: "Name"
          },
          {
            attribute: "email",
            header: "Email"
          },
          {
            attribute: "profile.date_registered",
            header: "Registered on"
          },
          {
            attribute: "pals",
            header: "PALs",
            render: (value: Array<IPal>): React.ReactNode => (
              <SimplePalsList pals={value} event={this.props.event} />
            )
          }
        ]}
      />
    );
  }
}

class ParticipantsWithoutTickets extends Component<Props> {
  render() {
    if (this.props.participants.length === 0) {
      if (this.props.isLoading) {
        return <p>Loading...</p>;
      }
      return <p>There are no participants without tickets.</p>;
    }
    return (
      <ParticipantsTable
        event={this.props.event}
        participants={this.props.participants}
        sortBy="profile.full_name"
        columns={[
          {
            attribute: "registration_order",
            header: "#"
          },
          {
            attribute: "profile.full_name",
            header: "Name"
          },
          {
            attribute: "email",
            header: "Email"
          },
          {
            attribute: "profile.date_registered",
            header: "Registered on"
          },
          {
            attribute: "approved",
            header: "Approved"
          },
          {
            attribute: "mutual_pal_group",
            header: "Mutual PALs"
          },
          {
            attribute: "valid_pal_group",
            header: "Approved PALs"
          },
          {
            attribute: "ticket_status",
            header: "Ticket status",
            render: (value: TicketStatus): string => {
              if (value === "NOT_READY") {
                return "URL not sent";
              } else if (value === "URL_SENT") {
                return "URL sent";
              } else if (value === "BOUGHT") {
                return "Bought";
              } else {
                return value;
              }
            }
          }
        ]}
      />
    );
  }
}

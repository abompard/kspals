import * as React from "react";
import FAIcon from "./FAIcon";
import "./InputSpinner.css";

export default class InputSpinner extends React.Component<{
  isLoading: boolean;
}> {
  render() {
    if (!this.props.isLoading) {
      return null;
    }
    return (
      <FAIcon name="spinner" className="InputSpinner text-muted fa-spin" />
    );
  }
}

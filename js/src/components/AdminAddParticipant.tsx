import { ErrorMessage, Field, Formik, FormikHelpers } from "formik";
import React, { Component } from "react";
import {
  Alert,
  Button,
  Col,
  Form,
  FormGroup,
  Label,
  Modal,
  ModalBody,
  ModalHeader
} from "reactstrap";
import { FlashMessageProps } from "../actions/types";
import { IParticipant } from "../api/types";
import { FormValues } from "../api/utils";
import FAIcon from "./FAIcon";
import { FormInput } from "./FormInput";

export interface Props {
  readOnly: boolean;
  addParticipant(
    values: ParticipantFormValues,
    actions: FormikHelpers<ParticipantFormValues>
  ): Promise<IParticipant>;
  addFlashMessage(message: FlashMessageProps): void;
}

type ParticipantAttributes = Pick<IParticipant, "email">;
type ParticipantFormValues = FormValues<ParticipantAttributes>;

interface State {
  panelOpen: boolean;
}

export class AdminAddParticipant extends Component<Props, State> {
  state = { panelOpen: false };
  togglePanel = () => {
    this.setState(prevState => ({
      panelOpen: !prevState.panelOpen
    }));
  };

  validateEmail(value: string): string | undefined {
    if (value && !/\S+@\S+\.\S+/.test(value)) {
      return "Ce n'est pas un email valide.";
    }
  }

  handleSubmit = (
    values: ParticipantFormValues,
    actions: FormikHelpers<ParticipantFormValues>
  ) => {
    if (this.props.readOnly || Object.keys(values).length === 0) {
      actions.setSubmitting(false);
      return;
    }
    this.props.addParticipant(values, actions).then(
      (participant: IParticipant) => {
        this.props.addFlashMessage({
          color: "success",
          body: `Participant ${participant.email} has been added.`
        });
        this.setState({ panelOpen: false });
      },
      () => {
        actions.setStatus({
          color: "danger",
          body: "There was an error updating the participant."
        });
      }
    );
  };

  render() {
    const leftWidth = 2,
      rightWidth = 12 - leftWidth;
    return (
      <React.Fragment>
        <div className="float-end">
          <Button color="secondary" onClick={this.togglePanel}>
            <FAIcon name="plus" /> Add
          </Button>
        </div>

        <Modal
          isOpen={this.state.panelOpen}
          toggle={this.togglePanel}
          size="lg"
          backdrop
        >
          <ModalHeader toggle={this.togglePanel}>
            Add a new participant
          </ModalHeader>
          <ModalBody>
            <Formik
              initialValues={{
                email: ""
              }}
              onSubmit={this.handleSubmit}
            >
              {({ status, handleSubmit, handleReset, isSubmitting }) => (
                <Form onSubmit={handleSubmit} onReset={handleReset}>
                  <FormGroup row>
                    <Label for="email" sm={leftWidth}>
                      Email
                    </Label>
                    <Col sm={rightWidth}>
                      <Field
                        component={FormInput}
                        type="email"
                        name="email"
                        validate={this.validateEmail}
                      />
                    </Col>
                  </FormGroup>
                  {status && status.body && (
                    <FormGroup>
                      <Alert color={status.color} className="text-center mt-3">
                        {status.body}
                      </Alert>
                    </FormGroup>
                  )}
                  <FormGroup check row>
                    <Col sm={{ size: rightWidth, offset: leftWidth }}>
                      <Button
                        color="primary"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        Add
                        {isSubmitting && (
                          <FAIcon name="spinner" className="fa-spin ms-3" />
                        )}
                      </Button>
                      <ErrorMessage name="non_field_errors" />
                    </Col>
                  </FormGroup>
                </Form>
              )}
            </Formik>
          </ModalBody>
        </Modal>
      </React.Fragment>
    );
  }
}

import * as React from "react";
import { Button, Col, Form, Input, Label } from "reactstrap";
import FAIcon from "./FAIcon";

interface Props {
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  onSubmit: React.ChangeEventHandler<HTMLFormElement>;
  value: string;
  error: string | null;
  isLoading: boolean;
  isDone: boolean;
  disabled?: boolean;
}

interface State {
  code: string;
}

export default class LoginFormCode extends React.Component<Props, State> {
  render() {
    return (
      <div>
        <p className={"card-text" + (this.props.disabled ? " text-muted" : "")}>
          <strong>Étape 2</strong>: entrez le code à 6 chiffres que vous avez
          reçu par email.
        </p>
        <Form
          className="row row-cols-1 row-cols-sm-auto g-2 align-items-center"
          onSubmit={this.props.onSubmit}
        >
          <Col>
            <Label
              htmlFor="code"
              className={this.props.disabled ? " text-muted" : ""}
            >
              Code :
            </Label>
          </Col>
          <Col>
            <Input
              type="number"
              name="code"
              id="code"
              placeholder="------"
              value={this.props.value}
              onChange={this.props.onChange}
              disabled={this.props.disabled}
              valid={this.props.error ? false : undefined}
            />
          </Col>
          <Col>
            <Button color="primary" disabled={this.props.disabled}>
              Vérifier le code
              {this.props.isLoading && (
                <FAIcon name="spinner" className="fa-spin ms-3" />
              )}
            </Button>
          </Col>
        </Form>
        {this.props.isDone && (
          <p className="text-success">
            Le code est valide, vous êtes maintenant connecté⋅e.
          </p>
        )}
        {this.props.error && (
          <React.Fragment>
            <p className="text-danger">{this.props.error} Please try again.</p>
          </React.Fragment>
        )}
      </div>
    );
  }
}

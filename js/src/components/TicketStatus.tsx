import * as React from "react";
import { IPal, IParticipant } from "../api/types";
import FAIcon from "./FAIcon";

type Props = {
  className: string;
  pal: IPal | IParticipant | null;
  iconOnly: boolean;
  isSelf: boolean;
};

export default class TicketStatus extends React.Component<Props> {
  static defaultProps = {
    isSelf: false,
    iconOnly: false
  };

  render() {
    if (!this.props.pal || this.props.pal.has_ticket === null) {
      return null;
    }
    if ("is_mutual" in this.props.pal && !this.props.pal.is_mutual) {
      return null; // Don't display the ticket status on random pals
    }
    if (!this.props.pal.approved) {
      return null; // Don't display the ticket status if they are not approved yet
    }
    let color, text, icon;
    icon = "ticket";
    if (this.props.pal.has_ticket) {
      color = "success";
      if (this.props.isSelf) {
        text = "Vous avez bien pris votre billet.";
      } else {
        text = "Votre PAL a bien pris son billet.";
      }
    } else {
      color = "warning";
      if (this.props.isSelf) {
        text = "Vous n'avez pas encore pris votre billet.";
      } else {
        text = "Votre PAL n'a pas encore pris son billet.";
      }
    }
    const {
      className,
      pal,
      isSelf,
      iconOnly,
      children,
      ...otherProps
    } = this.props;
    if (this.props.iconOnly) {
      const newClassName = `fa-lg fa-fw text-${color} ${className}`;
      return (
        <FAIcon
          name={icon}
          className={newClassName}
          title={text}
          {...otherProps}
        />
      );
    } else {
      const newClassName = `mt-1 mb-0 text-${color} ${className}`;
      return (
        <p className={newClassName} {...otherProps}>
          <FAIcon name={icon} className="me-2 d-md-none" />
          {text}
        </p>
      );
    }
  }
}

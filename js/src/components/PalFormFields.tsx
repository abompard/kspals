import { Field, Formik, FormikHelpers } from "formik";
import * as React from "react";
import { Button, Col, Form, FormGroup, Label } from "reactstrap";
import { APIError, IPal } from "../api/types";
import { FormValues } from "../api/utils";
import ApprovedStatus from "./ApprovedStatus";
import FAIcon from "./FAIcon";
import { FormInput } from "./FormInput";
import MutualStatus from "./MutualStatus";
import TicketStatus from "./TicketStatus";

interface Props {
  index: number;
  pal: IPal | null;
  label: React.ReactNode | undefined;
  onChange(index: number, email: string): Promise<void>;
  disabled?: boolean;
}

type PALFormValues = FormValues<{ email: string }>;

export default class PalFormFields extends React.Component<Props> {
  handleSubmit = (
    values: PALFormValues,
    actions: FormikHelpers<PALFormValues>
  ) => {
    this.props.onChange(this.props.index, values.email).then(
      () => {
        actions.setSubmitting(false);
      },
      (error: APIError) => {
        console.log("Got an error", error);
        actions.setSubmitting(false);
        if (error.fields && error.fields.pal) {
          actions.setErrors({ email: error.fields.pal[0] });
        } else {
          actions.setErrors({ email: error.detail });
        }
      }
    );
  };

  validateEmail(value: string): string | undefined {
    if (value && !/\S+@\S+\.\S+/.test(value)) {
      return "Ce n'est pas un email valide.";
    }
  }

  handleChange = (
    setFieldValue: (field: string, value: any) => void,
    setFieldTouched: (field: string) => void
  ) => (e: React.ChangeEvent<HTMLInputElement>) => {
    setFieldValue("email", e.currentTarget.value.toLowerCase());
    setFieldTouched("email");
  };

  render() {
    const storedEmail = this.props.pal ? this.props.pal.email : null;
    return (
      <Formik
        initialValues={{
          email: (this.props.pal && this.props.pal.email) || ""
        }}
        onSubmit={this.handleSubmit}
        isInitialValid
      >
        {({
          values,
          handleSubmit,
          handleReset,
          isSubmitting,
          isValid,
          setFieldValue,
          setFieldTouched,
          submitForm
        }) => (
          <Form onSubmit={handleSubmit} onReset={handleReset}>
            <FormGroup row className="mt-3">
              <Label
                htmlFor="email"
                md={2}
                className={this.props.disabled ? "text-muted" : ""}
              >
                {this.props.label}
              </Label>
              <Col md={7}>
                <Field
                  component={FormInput}
                  type="email"
                  name="email"
                  validate={this.validateEmail}
                  onChange={this.handleChange(setFieldValue, setFieldTouched)}
                  placeholder="email de votre PAL"
                  disabled={this.props.disabled}
                  rightButton={
                    <SubmitButton
                      disabled={this.props.disabled || !isValid || isSubmitting}
                      isSubmitting={isSubmitting}
                      value={values.email}
                      storedValue={storedEmail}
                      handleDelete={() => {
                        setFieldValue("email", "");
                        submitForm();
                      }}
                    />
                  }
                />
                {values.email !== storedEmail ? null : (
                  <React.Fragment>
                    <MutualStatus pal={this.props.pal} className="d-md-none" />
                    <ApprovedStatus
                      pal={this.props.pal}
                      className="d-md-none"
                    />
                    <TicketStatus pal={this.props.pal} className="d-md-none" />
                  </React.Fragment>
                )}
              </Col>
              {values.email !== storedEmail ? null : (
                <Col
                  md={3}
                  xs={3}
                  className="d-none d-md-flex align-items-center"
                >
                  <MutualStatus pal={this.props.pal} iconOnly={true} />
                  <ApprovedStatus
                    pal={this.props.pal}
                    iconOnly={true}
                    className="ms-md-3"
                  />
                  <TicketStatus
                    pal={this.props.pal}
                    iconOnly={true}
                    className="ms-md-3"
                  />
                </Col>
              )}
            </FormGroup>
          </Form>
        )}
      </Formik>
    );
  }
}

interface ButtonProps {
  disabled: boolean;
  onClick?: () => void;
}

interface SubmitButtonProps {
  isSubmitting: boolean;
  disabled: boolean;
  value: string;
  storedValue: string | null;
  handleDelete(): void;
}

const SubmitButton: React.FunctionComponent<SubmitButtonProps> = props => {
  if (!props.storedValue && !props.value) {
    return null;
  }
  if (props.value && props.value !== props.storedValue) {
    return <ChooseButton disabled={props.disabled} />;
  } else {
    return (
      <DeleteButton disabled={props.disabled} onClick={props.handleDelete} />
    );
  }
};

const ChooseButton: React.FunctionComponent<ButtonProps> = props => (
  <Button
    type="submit"
    color="primary"
    onClick={props.onClick}
    disabled={props.disabled}
  >
    <FAIcon name="hand-o-left" title="Choisir ce PAL" />
    <span className="ms-2 d-none d-lg-inline">Choisir</span>
  </Button>
);

const DeleteButton: React.FunctionComponent<ButtonProps> = props => (
  <Button
    type="submit"
    color="danger"
    onClick={props.onClick}
    disabled={props.disabled}
  >
    <FAIcon name="times" title="Supprimer ce PAL" />
    <span className="ms-2 d-none d-lg-inline">Supprimer</span>
  </Button>
);

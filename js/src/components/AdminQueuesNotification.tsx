import React, { Component } from "react";
import { IEvent, INotification, BackendValidationError } from "../api/types";
import { Button, Card, CardBody, CardText } from "reactstrap";
import { Link } from "react-router-dom";
import { FormattedDate, FormattedTime } from "react-intl";
import { FieldErrors } from "./FieldErrors";
import { BreadcrumbHeader } from "./BreadcrumbHeader";

export interface Props {
  event: IEvent;
  notification: INotification | null;
  error: string | null;
  fieldErrors?: BackendValidationError;
  isLoading: boolean;
  readOnly: boolean;
  validateNotification: (participantId: number, status: string) => void;
}

export class AdminQueuesNotification extends Component<Props> {
  handleSend = (): void => {
    if (!this.props.notification || this.props.readOnly) {
      return;
    }
    this.props.validateNotification(this.props.notification.id, "TO_SEND");
  };

  handleCancel = (): void => {
    if (!this.props.notification || this.props.readOnly) {
      return;
    }
    this.props.validateNotification(this.props.notification.id, "DRAFT");
  };

  handleDelete = (): void => {
    if (!this.props.notification || this.props.readOnly) {
      return;
    }
    this.props.validateNotification(this.props.notification.id, "TRASH");
  };

  render() {
    if (!this.props.notification) {
      return <p className="text-danger">This notification does not exist!</p>;
    }
    let statusText: string = this.props.notification.status;
    if (statusText === "DRAFT") {
      statusText = "Waiting for approval";
    } else if (statusText === "TO_SEND") {
      statusText = "Ready to be sent";
    } else if (statusText === "SENT") {
      statusText = "Sent";
    } else if (statusText === "TRASH") {
      statusText = "In the trash";
    }
    return (
      <React.Fragment>
        <BreadcrumbHeader
          crumbs={[
            "To do",
            <Link to={`/admin/${this.props.event.code}/queues/notifications/`}>
              Notifications
            </Link>,
            `${this.props.notification.name_human} for ${
              this.props.notification.participant.profile.full_name
            }`
          ]}
        />

        <h3>
          Notification to validate: {this.props.notification.name_human} for{" "}
          {this.props.notification.participant.profile.full_name}
        </h3>

        <Card className="my-3">
          <CardBody>
            <CardText>
              <strong>About: </strong>
              <Link
                to={`/admin/${this.props.event.code}/participants/profiles/${
                  this.props.notification.participant.id
                }/`}
              >
                {this.props.notification.participant.profile.full_name}
              </Link>
            </CardText>
            <CardText>
              <strong>Status: </strong>
              {statusText}
            </CardText>
            <CardText>
              <strong>Created: </strong>
              <FormattedDate
                value={this.props.notification.date_created}
              />{" "}
              <FormattedTime value={this.props.notification.date_created} />
            </CardText>
            <CardText>
              <strong>Sent: </strong>
              {this.props.notification.date_sent === null ? (
                "Not sent yet."
              ) : (
                <span>
                  <FormattedDate value={this.props.notification.date_sent} />{" "}
                  <FormattedTime value={this.props.notification.date_sent} />
                </span>
              )}
            </CardText>
            <CardText>
              <strong>Recipient: </strong>
              {this.props.notification.recipient}
            </CardText>
            <CardText>
              <strong>Subject: </strong>
              {this.props.notification.subject}
            </CardText>
            <CardText className="mb-0">
              <strong>Text body:</strong>
            </CardText>
            <Card>
              <CardBody>
                <CardText tag="pre" style={{ lineHeight: "1" }}>
                  {this.props.notification.body_text}
                </CardText>
              </CardBody>
            </Card>
            {this.props.notification.body_html !== null && (
              <div>
                <CardText className="mb-0 mt-3">
                  <strong>HTML body:</strong>
                </CardText>
                <Card>
                  <CardBody>
                    <CardText>
                      <span
                        dangerouslySetInnerHTML={{
                          __html: this.props.notification.body_html
                        }}
                      />
                    </CardText>
                  </CardBody>
                </Card>
              </div>
            )}
          </CardBody>
        </Card>

        {this.props.readOnly ? null : (
          <React.Fragment>
            <div className="text-center">
              {this.props.notification.status === "DRAFT" && (
                <React.Fragment>
                  <Button
                    color="success"
                    onClick={this.handleSend}
                    disabled={this.props.isLoading}
                    className="mx-3"
                  >
                    Send
                  </Button>
                  <Button
                    color="danger"
                    onClick={this.handleDelete}
                    disabled={this.props.isLoading}
                    className="mx-3"
                  >
                    Delete
                  </Button>
                </React.Fragment>
              )}
              {this.props.notification.status === "TO_SEND" && (
                <Button
                  color="warning"
                  onClick={this.handleCancel}
                  disabled={this.props.isLoading}
                >
                  Cancel
                </Button>
              )}
              {this.props.notification.status === "SENT" && (
                <Button
                  color="info"
                  onClick={this.handleCancel}
                  disabled={this.props.isLoading}
                >
                  Reset to "draft"
                </Button>
              )}
              {this.props.notification.status === "TRASH" && (
                <Button
                  color="info"
                  onClick={this.handleCancel}
                  disabled={this.props.isLoading}
                >
                  Restore
                </Button>
              )}
            </div>

            {this.props.error && (
              <p className="text-danger">{this.props.error}</p>
            )}
            <FieldErrors errors={this.props.fieldErrors} />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

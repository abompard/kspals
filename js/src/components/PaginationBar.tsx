import React, { Component } from "react";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import { Link } from "react-router-dom";
import { Page } from "../store/pagination";
import { PaginatedEntity } from "../api/types";
import querystring from "querystring";

interface Props {
  page: Page<PaginatedEntity>;
  queryString?: {
    [key: string]: string | string[];
  };
}

export class PaginationBar extends Component<Props> {
  render() {
    if (this.props.page.count === null || this.props.page.pageSize === null) {
      return null;
    }
    const pageSize = this.props.page.pageSize;
    const pageNumber = this.props.page.number;
    const pageCount = Math.ceil(this.props.page.count / pageSize);
    if (pageCount <= 1) {
      return null;
    }
    const firstDisabled = pageNumber <= 1;
    const lastDisabled = pageNumber >= pageCount;
    const getQS = (pageNr: number) =>
      "?" +
      querystring.stringify({
        ...(this.props.queryString || {}),
        page: pageNr
      });
    return (
      <Pagination
        aria-label="Page navigation"
        className="mt-3"
        listClassName="justify-content-center mt-3"
      >
        <PaginationItem disabled={firstDisabled}>
          <PaginationLink first tag={Link} to={getQS(1)} />
        </PaginationItem>
        <PaginationItem disabled={firstDisabled}>
          <PaginationLink previous tag={Link} to={getQS(pageNumber - 1)} />
        </PaginationItem>
        {pageCount > 1 &&
          Array.from(Array(pageCount).keys()).map(index => {
            const n = index + 1;
            return (
              <PaginationItem key={n} active={n === pageNumber}>
                <PaginationLink tag={Link} to={getQS(n)}>
                  {n}
                </PaginationLink>
              </PaginationItem>
            );
          })}
        <PaginationItem disabled={lastDisabled}>
          <PaginationLink next tag={Link} to={getQS(pageNumber + 1)} />
        </PaginationItem>
        <PaginationItem disabled={lastDisabled}>
          <PaginationLink last tag={Link} to={getQS(pageCount)} />
        </PaginationItem>
      </Pagination>
    );
  }
}

import * as React from "react";

export default class AccessForbidden extends React.Component<{}> {
  render() {
    return <p className="text-danger">Vous n'avez pas accès à cette page.</p>;
  }
}

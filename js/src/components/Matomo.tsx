import * as React from "react";
import ReactPiwik from "react-piwik";
import { History } from "history";

interface Props {
  matomoUrl: string;
  matomoId: number;
  history: History;
}

export default class Matomo extends React.PureComponent<Props> {
  matomo: ReactPiwik | null = null;

  componentDidMount = () => {
    this.setupMatomo(this.props);
  };

  componentWillUnmount = () => {
    ReactPiwik.disconnectFromHistory();
  };

  setupMatomo = (props: Props): void => {
    if (!this.props.matomoUrl || !this.props.matomoId) {
      return;
    }
    this.matomo = new ReactPiwik({
      url: props.matomoUrl,
      siteId: props.matomoId,
      trackErrors: true,
      jsFilename: "js/",
      phpFilename: "js/"
    });
    // Subscribe to history changes
    this.matomo!.connectToHistory(this.props.history);
    // track the initial pageview
    ReactPiwik.push(["trackPageView"]);
  };

  render() {
    return null;
  }
}

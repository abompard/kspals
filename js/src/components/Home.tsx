import * as React from "react";
import { FormattedRelative } from "react-intl";
import { Link } from "react-router-dom";
import { Card, CardText, CardTitle, Col, Row } from "reactstrap";
import { IEvent } from "../api/types";

export interface Props {
  events: Array<IEvent>;
  isLoading: boolean;
}

export default class Home extends React.Component<Props> {
  render() {
    const now = new Date().toISOString();
    return (
      <div className="Home">
        <h4 className="mb-5">
          Choisissez l'évènement auquel vous voulez participer
        </h4>
        {this.props.isLoading && (
          <div className="text-center mt-3">
            <i className="fa fa-spinner fa-pulse fa-lg" />
          </div>
        )}
        {this.props.events.length === 0 && !this.props.isLoading ? (
          <p>Pas d'évènement actuellement</p>
        ) : (
          <Row>
            {this.props.events.map(event => {
              let color, message;
              if (event.is_registration_open) {
                if (event.is_full) {
                  color = "danger";
                  message = "L'évènement est complet !";
                } else if (event.is_on_waiting_list) {
                  color = "warning";
                  message =
                    "L'évènement est complet, mais la liste d'attente est ouverte.";
                } else {
                  color = "success";
                }
              } else {
                color = "secondary";
                message = "Les inscriptions ne sont pas ouvertes.";
              }
              return (
                <Col
                  key={event.code}
                  md="6"
                  lg={{ size: 5, offset: 1 }}
                  xl={{ size: 4, offset: 0 }}
                  className="mb-3"
                >
                  <Card body outline color={color}>
                    <CardTitle className="text-center" tag="h5">
                      <Link to={`${event.code}/`}>{event.name}</Link>
                    </CardTitle>
                    {message && (
                      <CardText className="font-italic">{message}</CardText>
                    )}
                    <CardText className="mb-0">
                      Lieu : {event.location}
                    </CardText>
                    <CardText className="mb-0">
                      Date : <FormattedRelative value={event.date_open} />
                    </CardText>
                    {now < event.date_reg_open ? (
                      <CardText className="mb-0">
                        Début des inscriptions :{" "}
                        <FormattedRelative value={event.date_reg_open} />
                      </CardText>
                    ) : (
                      <CardText className="mb-0">
                        Fin des inscriptions :{" "}
                        <FormattedRelative value={event.date_reg_close} />
                      </CardText>
                    )}
                  </Card>
                </Col>
              );
            })}
          </Row>
        )}
      </div>
    );
  }
}

import * as React from "react";
import { Link } from "react-router-dom";
import { Alert } from "reactstrap";
import LoginFormCode from "./LoginFormCode";
import LoginFormEmail from "./LoginFormEmail";

interface Props {
  onEmailSubmit(email: string): any;
  onCodeSubmit(email: string, code: string): any;
  loginCodeSending: boolean;
  loginCodeSent: boolean;
  loginCodeEntering: boolean;
  loginCodeEntered: boolean;
  loginCodeError: string | null;
  tokenError: string | null;
}

export default function LoginForms(props: Props) {
  const [email, setEmail] = React.useState("");
  const [code, setCode] = React.useState("");

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const name = e.currentTarget.name;
    let value = e.currentTarget.value;
    if (name === "email") {
      setEmail(value.toLowerCase());
    } else if (name === "code") {
      setCode(value);
    }
  };

  const handleEmailSubmit: React.ChangeEventHandler<HTMLFormElement> = e => {
    e.preventDefault();
    props.onEmailSubmit(email);
  };

  const handleCodeSubmit: React.ChangeEventHandler<HTMLFormElement> = e => {
    e.preventDefault();
    props.onCodeSubmit(email, code);
  };

  return (
    <div className="LoginForms">
      <div className="card">
        <div className="card-header">
          Pré-enregistrement pour le Kinky Salon
        </div>
        <div className="card-body">
          <LoginFormEmail
            onChange={handleChange}
            onSubmit={handleEmailSubmit}
            value={email}
            error={props.loginCodeError}
            isLoading={props.loginCodeSending}
            isDone={props.loginCodeSent}
          />
        </div>
        <div className="card-body">
          <LoginFormCode
            onChange={handleChange}
            onSubmit={handleCodeSubmit}
            value={code}
            error={props.tokenError}
            isLoading={props.loginCodeEntering}
            isDone={props.loginCodeEntered}
          />
        </div>
      </div>
      <Alert color="secondary" className="mt-4" style={{ fontSize: "small" }}>
        En utilisant cette application, vous acceptez notre{" "}
        <Link to="/personal-data/">politique de confidentialité</Link>.
      </Alert>
    </div>
  );
}

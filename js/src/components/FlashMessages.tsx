import * as React from "react";
import { RouteComponentProps } from "react-router-dom";
import { Alert } from "reactstrap";
import { IFlashMessage } from "../actions/types";
//import { Toast, ToastBody, ToastHeader } from "reactstrap";

export interface Props extends RouteComponentProps {
  messages: Array<IFlashMessage>;
  deleteFlashMessage(id: string): void;
  flagFlashMessageAsShown(id: string): void;
  pruneFlashMessages(): void;
}

export default class FlashMessages extends React.Component<Props> {
  componentDidUpdate(prevProps: RouteComponentProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.props.pruneFlashMessages();
    }
  }

  /*render() {
    return (
      <div className="FlashMessages">
        {this.props.messages.map((message, index) => (
          <Toast
            className={message.color ? `border-${message.color}` : undefined}
            key={index}
          >
            {message.title ? (
              <ToastHeader icon={message.color}>{message.title}</ToastHeader>
            ) : null}
            <ToastBody>{message.body}</ToastBody>
          </Toast>
        ))}
      </div>
    );
  }*/
  render() {
    if (this.props.messages.length === 0) {
      return null;
    }
    return (
      <>
        {this.props.messages.map(message => (
          <FlashMessage
            key={message.id}
            message={message}
            onDelete={this.props.deleteFlashMessage}
            onShown={this.props.flagFlashMessageAsShown}
          />
        ))}
      </>
    );
  }
}

interface MessageProps {
  message: IFlashMessage;
  onDelete(id: string): void;
  onShown(id: string): void;
}

class FlashMessage extends React.Component<MessageProps> {
  componentDidMount = () => {
    window.setTimeout(
      () => this.props.onShown(this.props.message.id),
      1 * 1000
    );
  };
  handleDismiss = (): void => {
    this.props.onDelete(this.props.message.id);
  };
  render() {
    return (
      <div className="d-flex justify-content-center">
        <Alert
          color={this.props.message.color}
          toggle={this.handleDismiss}
          className="mb-3"
        >
          {this.props.message.body}
        </Alert>
      </div>
    );
  }
}

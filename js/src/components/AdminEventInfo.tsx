import { ErrorMessage, Field, Formik, FormikHelpers } from "formik";
import React, { Component } from "react";
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Nav,
  NavItem,
  Row,
  TabContent,
  TabPane
} from "reactstrap";
import { updateEventResult } from "../actions/admin";
import { apiCall } from "../api/call";
import { eventSchema } from "../api/schemas";
import { APIError, IEvent } from "../api/types";
import {
  FormValues,
  nullNumberToString,
  stringToNullNumber
} from "../api/utils";
import AdminEventRoles from "../containers/AdminEventRoles";
import AdminEventTicketRates from "../containers/AdminEventTicketRates";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import FAIcon from "./FAIcon";
import { DateTimeFormInput, FormInput } from "./FormInput";
import { TabNavLink } from "./TabNavLink";

export interface Props {
  event: IEvent;
  drinks_count: number;
  participantsIsLoading: boolean;
  userToken: string | null;
  updateEventSuccess: (result: updateEventResult) => void;
  readOnly: boolean;
}

interface State {
  activeTab: string;
}

export class AdminEventInfo extends Component<Props, State> {
  state = { activeTab: "1" };
  handleTabChange: React.MouseEventHandler<HTMLAnchorElement> = e => {
    const tabId = e.currentTarget.dataset.tabid || "1";
    if (this.state.activeTab !== tabId) {
      this.setState({
        activeTab: tabId
      });
    }
  };
  render() {
    return (
      <React.Fragment>
        <BreadcrumbHeader crumbs={["Event", this.props.event.name]} />
        <Nav tabs className="mt-4 mb-3">
          <NavItem>
            <TabNavLink
              activeTab={this.state.activeTab}
              tabId="1"
              onTabChange={this.handleTabChange}
            >
              General
            </TabNavLink>
          </NavItem>

          <NavItem>
            <TabNavLink
              activeTab={this.state.activeTab}
              tabId="2"
              onTabChange={this.handleTabChange}
            >
              Roles
            </TabNavLink>
          </NavItem>

          <NavItem>
            <TabNavLink
              activeTab={this.state.activeTab}
              tabId="3"
              onTabChange={this.handleTabChange}
            >
              Ticket Rates
            </TabNavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row className="justify-content-center">
              <Col lg="10" xl="8">
                <UpdateForm key={this.props.event.code} {...this.props} />
              </Col>
            </Row>
          </TabPane>

          <TabPane tabId="2">
            <AdminEventRoles />
          </TabPane>

          <TabPane tabId="3">
            <AdminEventTicketRates />
          </TabPane>
        </TabContent>
      </React.Fragment>
    );
  }
}

type EventAttributes = Partial<
  Pick<
    IEvent,
    | "name"
    | "location"
    | "date_open"
    | "date_reg_open"
    | "date_reg_close"
    | "full_limit"
    | "waiting_list_size"
  >
>;
type EventFormValues = FormValues<EventAttributes>;

class UpdateForm extends React.Component<Props> {
  handleSubmit = (
    values: EventFormValues,
    actions: FormikHelpers<EventFormValues>
  ) => {
    if (
      this.props.readOnly ||
      !this.props.event ||
      !this.props.userToken ||
      Object.keys(values).length === 0
    ) {
      actions.setSubmitting(false);
      return;
    }
    const realValues: EventAttributes = {
      ...values,
      full_limit: stringToNullNumber(values.full_limit || ""),
      waiting_list_size: Number(values.waiting_list_size || "0")
    };
    const body = JSON.stringify(realValues);
    apiCall(this.props.event.url, eventSchema, this.props.userToken, {
      method: "PATCH",
      body
    })
      .then(
        (result: updateEventResult) => {
          if (!result) {
            return;
          }
          this.props.updateEventSuccess(result);
          actions.setStatus({
            color: "success",
            body: `Event ${this.props.event.name} has been updated.`
          });
        },
        (error: APIError) => {
          actions.setErrors(error.fields || {});
          actions.setStatus({
            color: "danger",
            body: "There was an error updating the event."
          });
        }
      )
      .then(() => {
        actions.setSubmitting(false);
      });
  };

  render() {
    const leftWidth = { xs: 5, sm: 3 },
      rightWidth = { xs: 12 - leftWidth["xs"], sm: 12 - leftWidth["sm"] };
    const initialValues: EventFormValues = {
      name: this.props.event.name,
      location: this.props.event.location,
      date_open: this.props.event.date_open,
      date_reg_open: this.props.event.date_reg_open,
      date_reg_close: this.props.event.date_reg_close,
      full_limit: nullNumberToString(this.props.event.full_limit),
      waiting_list_size: this.props.event.waiting_list_size.toString()
    };
    return (
      <Formik initialValues={initialValues} onSubmit={this.handleSubmit}>
        {({ handleSubmit, handleReset, isSubmitting, status }) => (
          <Form onSubmit={handleSubmit} onReset={handleReset}>
            <FormGroup row>
              <Label for="name" {...leftWidth}>
                Name:
              </Label>
              <Col {...rightWidth}>
                <Field
                  component={FormInput}
                  type="text"
                  name="name"
                  readOnly={this.props.readOnly}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="location" {...leftWidth}>
                Location:
              </Label>
              <Col {...rightWidth}>
                <Field
                  component={FormInput}
                  type="text"
                  name="location"
                  readOnly={this.props.readOnly}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="date_open" {...leftWidth}>
                Opening date:
              </Label>
              <Col {...rightWidth}>
                <Field
                  component={DateTimeFormInput}
                  name="date_open"
                  readOnly={this.props.readOnly}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label {...leftWidth}>Active:</Label>
              <Col {...rightWidth}>
                <BooleanField value={this.props.event.is_active} />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="date_reg_open" {...leftWidth}>
                Registration opening date:
              </Label>
              <Col {...rightWidth}>
                <Field
                  component={DateTimeFormInput}
                  name="date_reg_open"
                  readOnly={this.props.readOnly}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="date_reg_close" {...leftWidth}>
                Registration closing date:
              </Label>
              <Col {...rightWidth}>
                <Field
                  component={DateTimeFormInput}
                  name="date_reg_close"
                  readOnly={this.props.readOnly}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label {...leftWidth}>Registration open:</Label>
              <Col {...rightWidth}>
                <BooleanField value={this.props.event.is_registration_open} />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="full_limit" {...leftWidth}>
                Full limit:
              </Label>
              <Col {...rightWidth}>
                <Field
                  component={FormInput}
                  type="number"
                  name="full_limit"
                  readOnly={this.props.readOnly}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="waiting_list_size" {...leftWidth}>
                Waiting list size:
              </Label>
              <Col {...rightWidth}>
                <Field
                  component={FormInput}
                  type="number"
                  name="waiting_list_size"
                  readOnly={this.props.readOnly}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label {...leftWidth}>On waiting list:</Label>
              <Col {...rightWidth}>
                <BooleanField value={this.props.event.is_on_waiting_list} />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label {...leftWidth}>Number of ticketing links sent:</Label>
              <Col {...rightWidth}>
                <Input
                  plaintext
                  readOnly
                  value={this.props.event.ticketing_sent_count || "0"}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label {...leftWidth}>Number of tickets bought:</Label>
              <Col {...rightWidth}>
                <Input
                  plaintext
                  readOnly
                  value={this.props.event.tickets_bought_count || "0"}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label {...leftWidth}>Drinks bought:</Label>
              <Col {...rightWidth}>
                {this.props.participantsIsLoading ? (
                  <FAIcon name="spinner" className="fa-spin py-2" />
                ) : (
                  <Input plaintext readOnly value={this.props.drinks_count} />
                )}
              </Col>
            </FormGroup>
            {status && status.body && (
              <FormGroup row>
                <Col>
                  <div
                    className={`alert alert-${status.color} text-center mt-3`}
                  >
                    {status.body}
                  </div>
                </Col>
              </FormGroup>
            )}
            {this.props.readOnly ? null : (
              <FormGroup row>
                <Col
                  xs={{ size: rightWidth["xs"], offset: leftWidth["xs"] }}
                  sm={{ size: rightWidth["sm"], offset: leftWidth["sm"] }}
                >
                  <Button color="primary" type="submit" disabled={isSubmitting}>
                    Update
                    {isSubmitting && (
                      <FAIcon name="spinner" className="fa-spin ms-3" />
                    )}
                  </Button>
                  <ErrorMessage name="non_field_errors" />
                </Col>
              </FormGroup>
            )}
          </Form>
        )}
      </Formik>
    );
  }
}

const BooleanField: React.FunctionComponent<{ value: boolean }> = ({
  value
}) => (
  <Input
    plaintext
    readOnly
    className={value ? "text-success" : "text-danger"}
    value={value ? "Yes" : "No"}
  />
);
// const BooleanField = (props: { value: boolean }) => (
//   <div className={props.value ? "text-success" : "text-danger"}>
//     <FAIcon name={props.value ? "check-circle" : "times-circle"} />
//   </div>
// );

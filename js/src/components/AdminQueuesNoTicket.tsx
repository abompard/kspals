import React, { Component } from "react";
import { IEvent, IParticipant } from "../api/types";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import ParticipantsTable from "./ParticipantsTable";

interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
}

export class AdminQueuesNoTicket extends Component<Props> {
  render() {
    return (
      <React.Fragment>
        <BreadcrumbHeader crumbs={["To do", "Ticket not purchased"]} />

        <h3>
          Valid participants who haven't bought their ticket yet{" "}
          <small>({this.props.participants.length})</small>
        </h3>
        {this.props.participants.length === 0 ? (
          <p>No participant in this situation.</p>
        ) : (
          <ParticipantsTable
            event={this.props.event}
            participants={this.props.participants}
            columns={[
              {
                attribute: "id",
                header: "#"
              },
              {
                attribute: "profile.full_name",
                header: "Name"
              },
              {
                attribute: "email",
                header: "Email"
              },
              {
                attribute: "profile.date_registered",
                header: "Registered on"
              }
            ]}
          />
        )}
      </React.Fragment>
    );
  }
}

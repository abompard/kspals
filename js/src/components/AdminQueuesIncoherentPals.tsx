import React, { Component } from "react";
import { Table } from "reactstrap";
import { Link } from "react-router-dom";
import { IEvent, IParticipant } from "../api/types";
import { FormattedTime, FormattedDate } from "react-intl";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import { SimplePalsList } from "./SimplePalsList";

interface Props {
  event: IEvent;
  participantsWithoutPal: Array<IParticipant>;
  participantsNonMutualPals: Array<IParticipant>;
}

export class AdminQueuesIncoherentPals extends Component<Props> {
  render() {
    const breadcrumbs = (
      <BreadcrumbHeader crumbs={["To do", "Incoherent PALs"]} />
    );
    if (
      this.props.participantsWithoutPal.length +
        this.props.participantsNonMutualPals.length ===
      0
    ) {
      return (
        <React.Fragment>
          {breadcrumbs}
          <h3>Incoherent PALs</h3>
          <p>No participants in this situation.</p>
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        {breadcrumbs}

        <h3>
          Incoherent PALs{" "}
          <small>
            (
            {this.props.participantsNonMutualPals.length +
              this.props.participantsWithoutPal.length}
            )
          </small>
        </h3>
        <h4 className="mt-3">
          Participants without PAL{" "}
          <small>({this.props.participantsWithoutPal.length})</small>
        </h4>
        <WithoutPals
          event={this.props.event}
          participants={this.props.participantsWithoutPal}
        />
        <h4 className="mt-3">
          Non-mutual PALs{" "}
          <small>({this.props.participantsNonMutualPals.length})</small>
        </h4>
        <NonMutual
          event={this.props.event}
          participants={this.props.participantsNonMutualPals}
        />
      </React.Fragment>
    );
  }
}

interface SubProps {
  event: IEvent;
  participants: Array<IParticipant>;
}
class WithoutPals extends Component<SubProps> {
  render() {
    if (this.props.participants.length === 0) {
      return <p>No participants without PALs.</p>;
    }
    const baseurl = `/admin/${this.props.event.code}/participants/profiles/`;
    return (
      <Table hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Registered on</th>
            <th>Ticket bought</th>
          </tr>
        </thead>
        <tbody>
          {this.props.participants.map(participant => (
            <tr key={participant.id}>
              <th scope="row">
                <Link to={`${baseurl}${participant.id}/`}>
                  {participant.id}
                </Link>
              </th>
              <td>
                <Link to={`${baseurl}${participant.id}/`}>
                  {participant.profile.first_name}{" "}
                  {participant.profile.last_name}
                </Link>
              </td>
              <td>{participant.email}</td>
              <td>
                <FormattedDate value={participant.profile.date_registered} />{" "}
                <FormattedTime value={participant.profile.date_registered} />
              </td>
              <td>
                {participant.has_ticket ? (
                  <strong className="text-danger">Yes</strong>
                ) : (
                  "No"
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}

class NonMutual extends Component<SubProps> {
  render() {
    if (this.props.participants.length === 0) {
      return <p>All participants have mutual PALs.</p>;
    }
    const baseurl = `/admin/${this.props.event.code}/participants/profiles/`;
    return (
      <Table hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Ticket bought</th>
            <th>PALs</th>
          </tr>
        </thead>
        <tbody>
          {this.props.participants.map(participant => (
            <tr key={participant.id}>
              <th scope="row">
                <Link to={`${baseurl}${participant.id}/`}>
                  {participant.id}
                </Link>
              </th>
              <td>
                <Link to={`${baseurl}${participant.id}/`}>
                  {participant.profile.full_name}
                </Link>
              </td>
              <td>{participant.email}</td>
              <td>
                {participant.has_ticket ? (
                  <strong className="text-danger">Yes</strong>
                ) : (
                  "No"
                )}
              </td>
              <td>
                <SimplePalsList pals={participant.pals} event={this.props.event} />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}

import React, { Component } from "react";
import { IPal, IEvent } from "../api/types";
import { Link } from "react-router-dom";

interface Props {
  event: IEvent;
  pals: Array<IPal>;
}

export class SimplePalsList extends Component<Props> {
  render() {
    if (this.props.pals.length === 0) {
      return <i>No PALs</i>;
    }
    const baseProfileUrl = `/admin/${
      this.props.event.code
    }/participants/profiles/`;

    return (
      <React.Fragment>
        {this.props.pals.map((pal, index) => {
          let result;
          if (pal.pal && pal.pal.profile) {
            result = (
              <Link to={`${baseProfileUrl}${pal.pal.id}/`} key={pal.pal.id}>
                {pal.pal.profile.full_name}
              </Link>
            );
          } else {
            result = <span key={pal.email}>{pal.email}</span>;
          }
          return (
            <React.Fragment key={index}>
              {index === 0 ? null : " – "}
              {result}
            </React.Fragment>
          );
        })}
      </React.Fragment>
    );
  }
}

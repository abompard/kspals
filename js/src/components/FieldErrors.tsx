import React, { Component } from "react";
import { BackendValidationError } from "../api/types";

interface Props {
  errors?: BackendValidationError;
}

export class FieldErrors extends Component<Props> {
  render() {
    if (!this.props.errors || Object.keys(this.props.errors).length === 0) {
      return null;
    }
    return (
      <p className="text-danger">
        {Object.keys(this.props.errors).map(fieldName => {
          const error = (this.props.errors as BackendValidationError)[
            fieldName
          ];
          return fieldName === "non_field_errors"
            ? error
            : `${fieldName}: ${error}`;
        })}
      </p>
    );
  }
}

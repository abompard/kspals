import * as React from "react";

export default class PersonalData extends React.Component<{}> {
  render() {
    return (
      <React.Fragment>
        <h3 className="mb-3">Données personnelles et confidentialité</h3>
        <div className="text-justify">
          <p>
            Pour le bon fonctionnement de cette application de gestion des PALs,
            et pour la validation des participant⋅es à l'entrée et pendant la
            soirée, nous devons enregistrer des données personnelles telles que
            votre nom, votre adresse email et une photo que vous choisirez. À
            l'inscription, nous posons aussi quelques questions pour évaluer
            l'intérêt de nos participant⋅es pour nos activités et adapter notre
            communication et nos actions en conséquence.
          </p>
          <p>
            Après chaque évènement, toutes les données collectées par cette
            application sont <strong>systématiquement supprimées</strong>.
          </p>
          <p>
            Vos données ne sont en aucun cas revendues ou communiquées à des
            tiers. Elle sont stockées sur un serveur hébergé en France, donc
            sous la législation française. Seul⋅es les membres de l'équipe ont
            accès à vos données, il n'y a pas de prestataire externe.
          </p>
          <p>
            Vous pouvez à tout moment nous demander de supprimer vos données
            personnelles (votre inscription à la soirée en cours sera alors
            annulée), de les rectifier, ou de vous envoyer une copie de la
            totalité de ces données. Pour cela, faites-nous en la demande par
            email à l'adresse{" "}
            <a href="mailto:contact@kinkysalon.fr">contact@kinkysalon.fr</a>.
          </p>
          <p>
            Nous collectons aussi des données anonymes sur les visites de ce
            site pour évaluer le traffic réseau et la puissance nécessaires au
            bon fonctionnement de l'application.
          </p>
          <p>
            Vu la nature de nos activités, nous prenons la sécurité de vos
            données personnelles très au sérieux. Nous savons être sérieux quand
            il le faut.
          </p>
        </div>
      </React.Fragment>
    );
  }
}

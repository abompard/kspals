import React, { Component } from "react";
import { FormattedDate, FormattedTime } from "react-intl";
import { Link } from "react-router-dom";
import { Card, CardBody, CardText } from "reactstrap";
import { IEvent, IParticipant } from "../api/types";

interface Props {
  participant: IParticipant;
  event: IEvent;
  top?: React.ReactNode;
  bottom?: React.ReactNode;
  id?: string;
  noQuestions?: boolean;
  checkValid?: boolean;
  linkToProfile?: boolean;
}

export class ParticipantCard extends Component<Props> {
  handlePictureClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    window.open(e.currentTarget.href);
  };

  render() {
    let topClass = "my-3",
      topStyle: { [key: string]: string } = {};
    if (this.props.checkValid) {
      const isValid =
        this.props.participant.valid_pal_group &&
        this.props.participant.has_ticket;
      topStyle["borderWidth"] = "0.1em";
      if (isValid) {
        topClass += " border-success";
      } else {
        topClass += " border-danger";
      }
    }
    const defaultTicketRate = this.props.event.ticket_rates.filter(
      r => r.default
    )[0];
    return (
      <Card className={topClass} style={topStyle} id={this.props.id}>
        <CardBody>
          {this.props.top}
          <a
            href={this.props.participant.profile.picture}
            onClick={this.handlePictureClick}
          >
            <img
              className="float-end ms-3 mb-3"
              src={this.props.participant.profile.picture_thumb}
              alt={this.props.participant.profile.full_name}
            />
          </a>

          <CardText>
            <strong>Name: </strong>
            {this.props.linkToProfile ? (
              <Link
                to={`/admin/${this.props.event.code}/participants/profiles/${this.props.participant.id}/`}
              >
                {this.props.participant.profile.full_name}
              </Link>
            ) : (
              this.props.participant.profile.full_name
            )}
          </CardText>

          <CardText>
            <strong>Email: </strong>
            {this.props.participant.email}
          </CardText>

          {this.props.noQuestions ? null : (
            <React.Fragment>
              <CardText className="mb-0">
                <strong>Reasons to participate:</strong>
              </CardText>
              <CardText>{this.props.participant.profile.introduction}</CardText>
            </React.Fragment>
          )}

          {this.props.noQuestions ? null : (
            <React.Fragment>
              <CardText className="mb-0">
                <strong>Heard of the Kinky Salon through:</strong>
              </CardText>
              <CardText>
                {this.props.participant.profile.learned_about}
              </CardText>
            </React.Fragment>
          )}

          {this.props.participant.profile.low_income ? (
            <CardText>
              <strong>Low income: </strong> Yes
            </CardText>
          ) : null}

          {this.props.participant.drinks !== 0 && (
            <CardText>
              <strong>Drinks: </strong>
              {this.props.participant.drinks}
            </CardText>
          )}

          <CardText className="mb-0">
            <strong>Registration status:</strong>
          </CardText>
          <CardText tag="ul">
            {this.props.participant.approved ? null : (
              <li>
                Approved:{" "}
                <Link
                  to={`/admin/${this.props.event.code}/queues/validation/${this.props.participant.id}/`}
                  className={this.props.checkValid ? "text-danger" : ""}
                >
                  No
                </Link>
              </li>
            )}
            <li>
              Coherent PAL group:{" "}
              {this.props.participant.mutual_pal_group ? (
                "Yes"
              ) : (
                <span className="text-danger">No</span>
              )}
            </li>
            <li>
              Ticket purchased:{" "}
              {this.props.participant.has_ticket ? (
                "Yes"
              ) : (
                <span className={this.props.checkValid ? "text-danger" : ""}>
                  No
                </span>
              )}
            </li>
            {this.props.participant.ticket_name ? (
              <li>Name on the ticket: {this.props.participant.ticket_name}</li>
            ) : null}
            {this.props.participant.has_ticket &&
            this.props.participant.ticket_rate !== defaultTicketRate.name ? (
              <li>Ticket rate: {this.props.participant.ticket_rate}</li>
            ) : null}
            <li>
              Registered on:{" "}
              <FormattedDate
                value={this.props.participant.profile.date_registered}
              />{" "}
              <FormattedTime
                value={this.props.participant.profile.date_registered}
              />
            </li>
          </CardText>

          {this.props.participant.notes.map(note => (
            <CardText key={note.id}>
              <strong>{`${note.title}: `}</strong>
              {note.content}
            </CardText>
          ))}

          {this.props.bottom}
        </CardBody>
      </Card>
    );
  }
}

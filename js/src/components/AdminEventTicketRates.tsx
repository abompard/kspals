import { ErrorMessage, Field, Formik, FormikHelpers } from "formik";
import React from "react";
import {
  Alert,
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap";
import { ITicketRate, TicketRateAttributes } from "../api/types";
import FAIcon from "./FAIcon";
import { FormInput } from "./FormInput";

interface CommonProps {
  readOnly: boolean;
  update: (
    ticketRate: ITicketRate,
    values: TicketRateAttributes,
    actions: FormikHelpers<TicketRateAttributes>
  ) => Promise<ITicketRate>;
}

interface AdminEventTicketRatesProps extends CommonProps {
  ticketRates: Array<ITicketRate>;
  participantsCount: { [key: string]: number };
  ticketsCount: { [key: string]: number };
}

export const AdminEventTicketRates: React.FunctionComponent<AdminEventTicketRatesProps> = props => {
  if (!props.ticketRates) {
    return null;
  }

  return (
    <React.Fragment>
      {props.ticketRates.map((ticketRate, index) => (
        <EventTicketRateForm
          key={ticketRate.id}
          ticketRate={ticketRate}
          showLabel={index === 0}
          participantsCount={props.participantsCount[ticketRate.name]}
          ticketsCount={props.ticketsCount[ticketRate.name]}
          update={props.update}
          readOnly={props.readOnly}
        />
      ))}
    </React.Fragment>
  );
};

interface EventTicketRateFormProps extends CommonProps {
  ticketRate: ITicketRate;
  showLabel: boolean;
  participantsCount: number;
  ticketsCount: number;
}

const EventTicketRateForm: React.FunctionComponent<EventTicketRateFormProps> = props => {
  const handleSubmit = (
    values: TicketRateAttributes,
    actions: FormikHelpers<TicketRateAttributes>
  ) => {
    if (
      props.readOnly ||
      !props.ticketRate ||
      Object.keys(values).length === 0
    ) {
      actions.setSubmitting(false);
      return;
    }
    props
      .update(props.ticketRate, values, actions)
      .then(
        ticketRate => {
          actions.setStatus({
            color: "success",
            body: `The ticket rate ${ticketRate.name} has been updated.`
          });
        },
        error => {
          actions.setErrors(error.fields || {});
          actions.setStatus({
            color: "danger",
            body: "There was an error updating the ticket rate."
          });
        }
      )
      .then(() => {
        actions.setSubmitting(false);
      });
  };

  const initialValues: TicketRateAttributes = {
    name: props.ticketRate.name,
    default: props.ticketRate.default,
    low_income: props.ticketRate.low_income,
    coupon: props.ticketRate.coupon || "",
    amount: props.ticketRate.amount || 0,
    ticket_url: props.ticketRate.ticket_url
  };

  const labelClass = "fw-bold" + (props.showLabel ? "" : " d-lg-none");

  return (
    <Formik initialValues={initialValues} onSubmit={handleSubmit}>
      {({ handleSubmit, handleReset, isSubmitting, status, setStatus }) => (
        <React.Fragment>
          <Form onSubmit={handleSubmit} onReset={handleReset}>
            <Row className="form-row">
              <Col sm={3} md={2} lg={2}>
                <FormGroup>
                  <Label for="name" className={labelClass}>
                    Name
                  </Label>
                  <Field
                    component={FormInput}
                    type="text"
                    name="name"
                    readOnly={props.readOnly}
                  />
                </FormGroup>
              </Col>
              <Col sm={3} md={2} lg={1} className="d-flex align-items-end">
                <FormGroup check className="mb-3">
                  <Field
                    component={FormInput}
                    type="checkbox"
                    name="default"
                    readOnly={props.readOnly}
                    label="Default"
                  />
                </FormGroup>
              </Col>
              <Col sm={3} md={2} lg={2} className="d-flex align-items-end">
                <FormGroup check className="mb-3">
                  <Field
                    component={FormInput}
                    type="checkbox"
                    name="low_income"
                    readOnly={props.readOnly}
                    label="Low Income"
                  />
                </FormGroup>
              </Col>
              <Col sm={3} md={2} lg={2} xxl={1}>
                <FormGroup>
                  <Label for="coupon" className={labelClass}>
                    Coupon
                  </Label>
                  <Field
                    component={FormInput}
                    type="text"
                    name="coupon"
                    readOnly={props.readOnly}
                  />
                </FormGroup>
              </Col>
              <Col sm={3} md={2} lg={1}>
                <FormGroup>
                  <Label for="amount" className={labelClass}>
                    Amount
                  </Label>
                  <Field
                    component={FormInput}
                    type="number"
                    name="amount"
                    readOnly={props.readOnly}
                  />
                </FormGroup>
              </Col>
              <Col sm={3} md={2} lg={2}>
                <FormGroup>
                  <Label for="ticket_url" className={labelClass}>
                    URL
                  </Label>
                  <Field
                    component={FormInput}
                    type="text"
                    name="ticket_url"
                    readOnly={props.readOnly}
                  />
                </FormGroup>
              </Col>
              <Col sm={3} md={2} lg={1} className="d-flex align-items-end">
                <FormGroup>
                  <Label className={labelClass}>Participants</Label>
                  <Input plaintext value={props.participantsCount} readOnly />
                </FormGroup>
              </Col>
              <Col sm={3} md={2} lg={1} className="d-flex align-items-end">
                <FormGroup>
                  <Label className={labelClass}>Tickets</Label>
                  <Input plaintext value={props.ticketsCount} readOnly />
                </FormGroup>
              </Col>
              <Col sm={3} md={2} lg={1} className="d-flex align-items-end">
                <FormGroup>
                  <Button color="primary" type="submit" disabled={isSubmitting}>
                    Update
                    {isSubmitting && (
                      <FAIcon name="spinner" className="fa-spin ms-3" />
                    )}
                  </Button>
                  <ErrorMessage name="non_field_errors" />
                </FormGroup>
              </Col>
            </Row>
            {status && status.body && (
              <Row>
                <Col>
                  <Alert
                    color={status.color}
                    className="text-center mb-4"
                    toggle={() => setStatus(null)}
                  >
                    {status.body}
                  </Alert>
                </Col>
              </Row>
            )}
          </Form>
          <hr className="d-lg-none my-3" />
        </React.Fragment>
      )}
    </Formik>
  );
};

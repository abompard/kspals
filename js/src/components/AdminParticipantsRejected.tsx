import React, { Component } from "react";
import { Form, Row, Col } from "reactstrap";
import { IEvent, IParticipant, IPal } from "../api/types";
import ParticipantsTable from "./ParticipantsTable";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import { ParticipantsFilter } from "./ParticipantsFilter";
import { SimplePalsList } from "./SimplePalsList";

export interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
}

interface State {
  filter: string;
}

export class AdminParticipantsRejected extends Component<Props, State> {
  state = { filter: "" };

  handleFilterChange = (value: string): void => {
    this.setState({ filter: value });
  };

  render() {
    const breadcrumbs = (
      <BreadcrumbHeader crumbs={["Other", "Rejected Participants"]} />
    );
    if (this.props.participants.length === 0) {
      return (
        <React.Fragment>
          {breadcrumbs}
          <h3>Rejected participants</h3>
          <p>No rejected participants.</p>
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        {breadcrumbs}
        <h3 className="mb-3">
          Rejected participants{" "}
          <small>({this.props.participants.length})</small>
        </h3>
        <Form
          onSubmit={e => {
            e.preventDefault();
          }}
          className="mb-3"
        >
          <Row>
            <Col md="6" lg="4" xl="3">
              <ParticipantsFilter
                value={this.state.filter}
                onChange={this.handleFilterChange}
              />
            </Col>
          </Row>
        </Form>
        <ParticipantsTable
          event={this.props.event}
          participants={this.props.participants}
          sortBy="profile.full_name"
          columns={[
            {
              attribute: "registration_order",
              header: "#"
            },
            {
              attribute: "profile.full_name",
              header: "Name"
            },
            {
              attribute: "email",
              header: "Email"
            },
            {
              attribute: "profile.date_registered",
              header: "Registered on"
            },
            {
              attribute: "mutual_pal_group",
              header: "Mutual PALs"
            },
            {
              attribute: "valid_pal_group",
              header: "Approved PALs"
            },
            {
              attribute: "has_ticket",
              header: "Ticket bought",
              render: (value: boolean): string | React.ReactNode =>
                value ? <strong className="text-danger">Yes</strong> : "No"
            },
            {
              attribute: "pals",
              header: "PALs",
              render: (value: Array<IPal>): React.ReactNode => (
                <SimplePalsList pals={value} event={this.props.event} />
              )
            }
          ]}
        />
      </React.Fragment>
    );
  }
}

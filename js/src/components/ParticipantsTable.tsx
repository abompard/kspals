import React, { Component } from "react";
import { FormattedDate, FormattedTime } from "react-intl";
import { Link } from "react-router-dom";
import { Button, Table } from "reactstrap";
import { IEvent, IParticipant } from "../api/types";
import { sortByAttr } from "../api/utils";
import FAIcon from "./FAIcon";

interface TableColumn {
  attribute: string;
  header: string;
  render?: (value: any, participant: IParticipant) => string | React.ReactNode;
}

interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
  columns: Array<TableColumn>;
  baseParticipantsUrl?: string;
  sortBy?: string;
  reverse?: boolean;
}

interface State {
  sortBy: string;
  reverse: boolean;
}

const BOOL_ATTRS = [
  "approved",
  "mutual_pal_group",
  "approved_pal_group",
  "valid_pal_group",
  "has_ticket"
];
const LINK_ATTRS = ["id", "profile.full_name"];
const DATE_ATTRS = ["profile.date_registered"];

const displayBoolNull = (
  value: boolean | null,
  participant: IParticipant
): string => {
  if (value === true) {
    return "Yes";
  }
  if (value === false) {
    return "No";
  }
  return "Unknown";
};

const displayLink = (baseUrl: string) => (
  value: string,
  participant: IParticipant
): React.ReactNode => <Link to={`${baseUrl}${participant.id}/`}>{value}</Link>;

const displayDate = (
  value: string,
  participant: IParticipant
): React.ReactNode => (
  <React.Fragment>
    <FormattedDate value={value} /> <FormattedTime value={value} />
  </React.Fragment>
);

export default class ParticipantsTable extends Component<Props, State> {
  public state: State;

  constructor(props: Props) {
    super(props);
    this.state = {
      sortBy: props.sortBy || "profile.date_registered",
      reverse: props.reverse || false
    };
  }

  handleSortClick: React.MouseEventHandler<HTMLButtonElement> = e => {
    e.preventDefault();
    const sortBy = e.currentTarget.dataset.name;
    if (!sortBy) {
      return;
    }
    if (sortBy === this.state.sortBy) {
      this.setState(prevState => ({
        reverse: !prevState.reverse
      }));
    } else {
      this.setState({
        sortBy
      });
    }
  };

  renderValue = (
    participant: IParticipant,
    column: TableColumn
  ): string | React.ReactNode => {
    let value = participant as { [key: string]: any };
    const baseUrl =
      this.props.baseParticipantsUrl ||
      `/admin/${this.props.event.code}/participants/profiles/`;
    column.attribute.split(".").forEach((attrElem: string): void => {
      value = value[attrElem];
    });
    let render = (
      value: any,
      participant: IParticipant
    ): string | React.ReactNode => value;
    if (column.render) {
      render = column.render;
    } else if (BOOL_ATTRS.indexOf(column.attribute) !== -1) {
      render = displayBoolNull;
    } else if (LINK_ATTRS.indexOf(column.attribute) !== -1) {
      render = displayLink(baseUrl);
    } else if (DATE_ATTRS.indexOf(column.attribute) !== -1) {
      render = displayDate;
    }
    return render(value, participant);
  };

  render() {
    let participants = this.props.participants.sort(
      sortByAttr(this.state.sortBy)
    );
    if (this.state.reverse) {
      participants.reverse();
    }
    const caret = (
      <FAIcon
        className="ms-2 text-muted"
        name={this.state.reverse ? "sort-up" : "sort-down"}
      />
    );
    return (
      <Table hover borderless size="sm">
        <thead>
          <tr>
            {this.props.columns.map(column => (
              <th
                key={column.attribute}
                className={
                  BOOL_ATTRS.indexOf(column.attribute) !== -1
                    ? "text-center"
                    : undefined
                }
              >
                <Button
                  color="link"
                  onClick={this.handleSortClick}
                  data-name={column.attribute}
                  style={{ color: "black", textDecoration: "none" }}
                  className="p-0"
                >
                  {column.header}
                  {this.state.sortBy === column.attribute && caret}
                </Button>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {participants.map(participant => (
            <tr key={participant.id}>
              {this.props.columns.map(column => (
                <td
                  key={column.attribute}
                  className={
                    BOOL_ATTRS.indexOf(column.attribute) !== -1
                      ? "text-center"
                      : undefined
                  }
                >
                  {this.renderValue(participant, column)}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}

import { History } from "history";
import * as React from "react";
import { Link } from "react-router-dom";
import Matomo from "./Matomo";

export interface Props {
  matomoUrl?: string | null;
  matomoId?: number | null;
  history: History;
}

export default class Footer extends React.Component<Props> {
  render() {
    return (
      <div className="Footer">
        <p className="mb-0">
          Cette application de gestion des PALs a été développée pour le{" "}
          <a href="https://www.kinkysalon.fr">Kinky Salon Paris</a>. Merci de
          nous contacter si vous rencontrez une anomalie. Le{" "}
          <a href="https://gitlab.com/abompard/kspals">code source</a> est
          disponible sous licence{" "}
          <a href="http://www.gnu.org/licenses/agpl-3.0.html">AGPL v3</a>.
        </p>
        <p className="mb-0">
          <Link to="/personal-data/">
            Protection des données personnelles et confidentialité
          </Link>
          .
        </p>
        {this.props.matomoUrl && this.props.matomoId && (
          <Matomo
            matomoUrl={this.props.matomoUrl}
            matomoId={this.props.matomoId}
            history={this.props.history}
          />
        )}
      </div>
    );
  }
}

import {
  ErrorMessage,
  Field,
  Formik,
  FormikHelpers,
  FormikProps
} from "formik";
import React, { Component } from "react";
import { Typeahead } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";
import { Link } from "react-router-dom";
import { Button, Col, Form, FormGroup, Label, Row } from "reactstrap";
import { FormValues } from "../api/utils";
import { Props } from "./AdminParticipantsProfile";
import FAIcon from "./FAIcon";

export class AdminParticipantSellTicket extends Component<Props> {
  render() {
    if (this.props.readOnly) {
      return null;
    }
    return (
      <React.Fragment>
        <h4 className="mt-4">Sell ticket</h4>
        {/* Use key to reload the form if we switched participants page */}
        <Row className="justify-content-center" key={this.props.participant.id}>
          <Col lg="10" xl="8">
            <SellTicketForm {...this.props} />
            <BuyTicketForm {...this.props} />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export type SellTicketFormValues = FormValues<{
  sell_to: string | null;
}>;

class SellTicketForm extends Component<Props> {
  handleSubmit = (
    values: SellTicketFormValues,
    actions: FormikHelpers<SellTicketFormValues>
  ) => {
    if (
      this.props.readOnly ||
      !this.props.participant ||
      Object.keys(values).length === 0
    ) {
      actions.setSubmitting(false);
      return;
    }
    this.props.sellTicket(this.props.participant, values.sell_to, actions).then(
      () => {
        const body =
          values.sell_to === null
            ? `Sale has been cancelled.`
            : `Ticket of ${this.props.participant.profile.full_name} has been sold to ${values.sell_to}.`;
        actions.setStatus({
          color: "success",
          body
        });
        if (!this.props.socketIOActive) {
          return this.props.loadParticipants();
        }
      },
      () => {
        actions.setStatus({
          color: "danger",
          body: "There was an error during this operation."
        });
      }
    );
  };

  render() {
    const selling_to = this.props.participant.selling_ticket_to;
    let FormComponent: React.ElementType<SubFormProps>;
    switch (this.props.participant.ticket_status) {
      case "BOUGHT":
        FormComponent = SellForm;
        break;
      case "SELLING":
        FormComponent = CancelForm;
        break;
      default:
        FormComponent = (props: SubFormProps) => (
          <p>
            This participant does not have a ticket, selling is not possible.
          </p>
        );
        break;
    }
    return (
      <Formik
        initialValues={{
          sell_to: selling_to ? selling_to.email : ""
        }}
        onSubmit={this.handleSubmit}
      >
        {formProps => (
          <React.Fragment>
            <Form
              inline
              onSubmit={formProps.handleSubmit}
              onReset={formProps.handleReset}
            >
              <FormComponent
                componentProps={this.props}
                formProps={formProps}
              />
            </Form>
            <ErrorMessage name="non_field_errors" />
            {formProps.status && formProps.status.body && (
              <div
                className={`alert alert-${formProps.status.color} text-center mt-3`}
              >
                {formProps.status.body}
              </div>
            )}
          </React.Fragment>
        )}
      </Formik>
    );
  }
}

interface SubFormProps {
  componentProps: Props;
  formProps: FormikProps<SellTicketFormValues>;
}
const SellForm: React.FunctionComponent<SubFormProps> = ({
  componentProps,
  formProps
}) => (
  <React.Fragment>
    <FormGroup>
      <Label for="sell_to">Sell ticket to:</Label>
      <Field
        component={Typeahead}
        type="text"
        name="sell_to"
        id="sell_to"
        onChange={(selected: Array<{ email: string }>) => {
          formProps.setFieldValue("sell_to", selected[0].email);
        }}
        selected={componentProps.participantsWithoutTicket.filter(
          p => p.email === formProps.values.sell_to
        )}
        className="mx-3"
        options={componentProps.participantsWithoutTicket.map(p => ({
          email: p.email,
          first_name: p.profile.first_name,
          last_name: p.profile.last_name
        }))}
        labelKey="email"
        filterBy={["email", "first_name", "last_name"]}
        flip
        highlightOnlyResult
        minLength={2}
      />
    </FormGroup>

    <Button color="primary" type="submit" disabled={formProps.isSubmitting}>
      Sell
      {formProps.isSubmitting && (
        <FAIcon name="spinner" className="fa-spin ms-3" />
      )}
    </Button>
  </React.Fragment>
);

const CancelForm: React.FunctionComponent<SubFormProps> = ({
  componentProps,
  formProps
}) => {
  const profileUrl = `/admin/${componentProps.event.code}/participants/profiles/`;
  const selling_to = componentProps.participant.selling_ticket_to!;
  return (
    <React.Fragment>
      {componentProps.participant.profile.full_name} is currently selling their
      ticket to{" "}
      <Link to={`${profileUrl}${selling_to.id}/`} className="ms-1">
        {selling_to.profile.full_name}
      </Link>
      .
      <Button
        color="danger"
        type="submit"
        disabled={formProps.isSubmitting}
        size="sm"
        className="ms-3"
        onClick={() => {
          formProps.setFieldValue("sell_to", null);
        }}
      >
        Cancel
        {formProps.isSubmitting && (
          <FAIcon name="spinner" className="fa-spin ms-3" />
        )}
      </Button>
    </React.Fragment>
  );
};

export type BuyTicketFormValues = FormValues<{
  sold_by: string | null;
}>;

class BuyTicketForm extends Component<Props> {
  handleSubmit = (
    values: BuyTicketFormValues,
    actions: FormikHelpers<BuyTicketFormValues>
  ) => {
    if (
      this.props.readOnly ||
      !this.props.participant ||
      Object.keys(values).length === 0
    ) {
      actions.setSubmitting(false);
      return;
    }
    this.props.buyTicket(this.props.participant, values.sold_by, actions).then(
      () => {
        actions.setStatus({
          color: "success",
          body: `Ticket of ${this.props.participant.profile.full_name} has been bought from ${values.sold_by}.`
        });
      },
      () => {
        actions.setStatus({
          color: "danger",
          body: "There was an error selling the ticket."
        });
      }
    );
  };

  render() {
    const profileUrl = `/admin/${this.props.event.code}/participants/profiles/`;
    return (
      <Formik
        initialValues={{
          sold_by: ""
        }}
        onSubmit={this.handleSubmit}
      >
        {({
          values,
          status,
          handleSubmit,
          handleReset,
          setFieldValue,
          isSubmitting
        }) => (
          <React.Fragment>
            {this.props.participant.ticket_offer_by.length > 0 && (
              <React.Fragment>
                <p className="mb-1">This participant is offered a ticket by:</p>
                <ul>
                  {this.props.participant.ticket_offer_by.map(seller => {
                    return (
                      <li key={seller.id}>
                        <Form
                          inline
                          onSubmit={handleSubmit}
                          onReset={handleReset}
                        >
                          <FormGroup>
                            <Link
                              to={`${profileUrl}${seller.id}/`}
                              className="ms-1"
                            >
                              {seller.profile.full_name}
                            </Link>
                          </FormGroup>
                          <Button
                            color="primary"
                            type="submit"
                            disabled={
                              isSubmitting || this.props.participant.has_ticket
                            }
                            size="sm"
                            className="ms-3"
                            onClick={() => {
                              setFieldValue("sold_by", seller.id);
                            }}
                          >
                            Accept and buy
                            {isSubmitting && (
                              <FAIcon name="spinner" className="fa-spin ms-3" />
                            )}
                          </Button>
                          {this.props.participant.has_ticket && (
                            <span>
                              ({this.props.participant.profile.full_name} has a
                              ticket already)
                            </span>
                          )}
                        </Form>
                      </li>
                    );
                  })}
                </ul>
                <ErrorMessage name="non_field_errors" />
              </React.Fragment>
            )}
            {status && status.body && (
              <div className={`alert alert-${status.color} text-center mt-3`}>
                {status.body}
              </div>
            )}
          </React.Fragment>
        )}
      </Formik>
    );
  }
}

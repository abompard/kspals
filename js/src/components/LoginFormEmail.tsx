import * as React from "react";
import { Button, Col, Form, Input, Label } from "reactstrap";
import FAIcon from "./FAIcon";

interface Props {
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  onSubmit: React.ChangeEventHandler<HTMLFormElement>;
  value: string;
  error: string | null;
  isLoading: boolean;
  isDone: boolean;
}

export default class LoginFormEmail extends React.Component<Props> {
  render() {
    return (
      <div>
        <p className="card-text">
          Pour participer au Kinky Salon, entrez votre adresse email. Vous
          recevrez un code à 6 chiffres que vous devrez entrer ci-dessous.
        </p>
        <p className="card-text">
          S'enregistrer sur cette page ne donne pas directement accès au Kinky
          Salon mais permet de constituer votre groupe de PALs. Il faudra aussi
          acheter un billet, le lien vers la billetterie sera fourni aux groupes
          de PALs constitués.
        </p>
        <p className={"card-text" + (this.props.isDone ? " text-muted" : "")}>
          <strong>Étape 1</strong>: entrez votre adresse email pour recevoir le
          code de validation.
        </p>
        <Form
          className="row row-cols-1 row-cols-sm-auto g-2 align-items-center"
          onSubmit={this.props.onSubmit}
        >
          <Col>
            <Label
              htmlFor="email"
              className={this.props.isDone ? "text-muted" : ""}
            >
              Email :
            </Label>
          </Col>
          <Col>
            <Input
              type="email"
              name="email"
              id="email"
              placeholder="vous@example.com"
              value={this.props.value}
              onChange={this.props.onChange}
              valid={this.props.error ? false : undefined}
            />
          </Col>
          <Col>
            <Button color="primary">
              Demander un code
              {this.props.isLoading && (
                <FAIcon name="spinner" className="fa-spin ms-3" />
              )}
            </Button>
          </Col>
        </Form>
        {this.props.isDone && (
          <p className="text-success">
            Un code vous a été envoyé par email. Si vous ne recevez rien,
            vérifiez votre dossier spam.
          </p>
        )}
        {this.props.error && <p className="text-danger">{this.props.error}</p>}
      </div>
    );
  }
}

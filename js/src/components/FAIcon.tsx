import * as React from "react";

interface Props {
  [prop: string]: string;
}

export default class FAIcon extends React.Component<Props> {
  static defaultProps = {
    className: ""
  };

  render() {
    const { name, className, ...otherProps } = this.props;
    const fullClassName = `fa fa-${name} ${className}`;
    return <i className={fullClassName} {...otherProps} />;
  }
}

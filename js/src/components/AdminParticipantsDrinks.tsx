import React from "react";
import { Col, Row } from "reactstrap";
import { IEvent, IPal, IParticipant } from "../api/types";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import ParticipantsTable from "./ParticipantsTable";
import { SimplePalsList } from "./SimplePalsList";

export interface Props {
  event: IEvent;
  participants: Array<IParticipant>;
}

const PACKS = [11, 2];

export function AdminParticipantsDrinks(props: Props) {
  const breadcrumbs = <BreadcrumbHeader crumbs={["Participants", "Drinks"]} />;
  if (props.participants.length === 0) {
    return (
      <React.Fragment>
        {breadcrumbs}
        <h3>Drinks</h3>
        <p>No drinks.</p>
      </React.Fragment>
    );
  }

  const packs: { [c: number]: number } = {};
  PACKS.forEach(
    count =>
      (packs[count] = props.participants
        .map(p => p.drinks)
        .filter(c => c % count === 0).length)
  );
  console.log(packs);

  return (
    <React.Fragment>
      {breadcrumbs}
      <h3 className="mb-3">
        Drinks{" "}
        <small>
          (
          {props.participants
            .map(p => p.drinks)
            .reduce((prev, cur) => prev + cur, 0)}
          )
        </small>
      </h3>
      <div className="mb-3">
        {PACKS.map(count => (
          <Row key={count}>
            <Col xs={6} md={4} lg={2} xl={1}>
              Packs of {count}:
            </Col>
            <Col>{packs[count]}</Col>
          </Row>
        ))}
      </div>
      <ParticipantsTable
        event={props.event}
        participants={props.participants}
        sortBy="drinks"
        reverse
        columns={[
          {
            attribute: "profile.full_name",
            header: "Name"
          },
          {
            attribute: "email",
            header: "Email"
          },
          {
            attribute: "pals",
            header: "PALs",
            render: (value: Array<IPal>): React.ReactNode => (
              <SimplePalsList pals={value} event={props.event} />
            )
          },
          {
            attribute: "drinks",
            header: "Drinks"
          }
        ]}
      />
    </React.Fragment>
  );
}

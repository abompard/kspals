import * as React from "react";
import { Col, Input, Label, Row } from "reactstrap";
import { IParticipant } from "../api/types";
import ApprovedStatus from "./ApprovedStatus";
import PalFormFields from "./PalFormFields";
import TicketStatus from "./TicketStatus";

export interface Props {
  participant: IParticipant;
  onPalChange(index: number, email: string): Promise<void>;
}

export default class PalsForm extends React.Component<Props> {
  render() {
    return (
      <React.Fragment>
        <Row className="my-3">
          <Label htmlFor="useremail" md={2}>
            Votre email :
          </Label>
          <Col md={7}>
            <Input plaintext readOnly value={this.props.participant.email} />
            {this.props.participant.mutual_pal_group && (
              <>
                <ApprovedStatus
                  pal={this.props.participant}
                  isSelf={true}
                  className="d-md-none"
                />
                <TicketStatus
                  pal={this.props.participant}
                  isSelf={true}
                  className="d-md-none"
                />
              </>
            )}
          </Col>
          {this.props.participant.mutual_pal_group && (
            <Col md={3} className="d-none d-md-block">
              {/* This next icon is for alignment only */}
              <i className="fa fa-fw fa-lg invisible" />
              <ApprovedStatus
                pal={this.props.participant}
                iconOnly={true}
                isSelf={true}
                className="ms-md-3"
              />
              <TicketStatus
                pal={this.props.participant}
                iconOnly={true}
                isSelf={true}
                className="ms-md-3"
              />
            </Col>
          )}
        </Row>
        <PalFormFields
          label="Votre PAL :"
          index={0}
          key={
            this.props.participant.pals[0]
              ? this.props.participant.pals[0].email
              : undefined
          }
          pal={this.props.participant.pals[0]}
          onChange={this.props.onPalChange}
        />
        <PalFormFields
          label={
            <span>
              Votre autre PAL :
              <br className="d-none d-md-block" />
              <small className="text-muted ms-2">(optionnel)</small>
            </span>
          }
          index={1}
          key={
            this.props.participant.pals[1]
              ? this.props.participant.pals[1].email
              : undefined
          }
          pal={this.props.participant.pals[1] || null}
          onChange={this.props.onPalChange}
          disabled={!this.props.participant.pals[0]}
        />
      </React.Fragment>
    );
  }
}

import React, { Component } from "react";
import { Breadcrumb, BreadcrumbItem } from "reactstrap";

interface Props {
  crumbs: Array<string | React.ReactNode>;
}

export class BreadcrumbHeader extends Component<Props> {
  render() {
    return (
      <Breadcrumb className="mb-4">
        {this.props.crumbs.map((crumb, index) => (
          <BreadcrumbItem
            active={index === this.props.crumbs.length - 1}
            key={index}
          >
            {crumb}
          </BreadcrumbItem>
        ))}
      </Breadcrumb>
    );
  }
}

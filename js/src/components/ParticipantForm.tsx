import { ErrorMessage, Field, Formik, FormikHelpers } from "formik";
import { History } from "history";
import * as React from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Col,
  Form,
  FormGroup,
  FormText,
  Label,
  PopoverBody,
  PopoverHeader,
  UncontrolledPopover
} from "reactstrap";
import { FlashMessageProps } from "../actions/types";
import { IEvent, IParticipant, IParticipantProfile, IUser } from "../api/types";
import CancelParticipation, {
  Props as CancelParticipationProps
} from "./CancelParticipation";
import FAIcon from "./FAIcon";
import { FormInput } from "./FormInput";

export interface Props extends CancelParticipationProps {
  event: IEvent;
  user: IUser;
  participant: IParticipant | null;
  registerParticipant(
    data: FormData,
    actions: FormikHelpers<ParticipantFormValues>
  ): any;
  isParticipantLoading: boolean;
  history: History;
  addFlashMessage(message: FlashMessageProps): void;
}

type ParticipantAttributes = Pick<
  IParticipantProfile,
  | "first_name"
  | "last_name"
  | "introduction"
  | "learned_about"
  | "picture"
  | "picture_thumb"
  | "low_income"
>;
type ParticipantFormValues = ParticipantAttributes;

export default class ParticipantForm extends React.Component<Props> {
  isNew = () => {
    return (
      this.props.participant === null || this.props.participant.profile === null
    );
  };

  handleSubmit = (
    values: ParticipantFormValues,
    actions: FormikHelpers<ParticipantFormValues>
  ) => {
    actions.setStatus(null);
    const isNew = this.isNew(); // Save the status, we'll need it later in some async jobs
    let formData = new FormData();
    formData.append("first_name", values.first_name);
    formData.append("last_name", values.last_name);
    formData.append("introduction", values.introduction);
    formData.append("learned_about", values.learned_about);
    formData.append("low_income", values.low_income ? "true" : "false");
    if (isNew || values.picture) {
      formData.append("picture", values.picture);
    }
    this.props.registerParticipant(formData, actions).then(
      () => {
        const message = {
          color: "success",
          body: "Votre profil a bien été enregistré."
        };
        if (isNew && this.props.event) {
          // Redirect to the event, we need a flash message
          this.props.addFlashMessage(message);
          this.props.history.push(`/${this.props.event.code}/`);
        } else {
          // Not a creation, we'll stay on the page, no need for a flash message
          actions.setStatus(message);
        }
      },
      () => {
        actions.setStatus({
          color: "danger",
          body: "Il y a eu une erreur, voir ci-dessus pour les détails."
        });
      }
    );
  };

  validatePicture = (value: File | null) => {
    const profile: Partial<ParticipantAttributes> =
      (this.props.participant && this.props.participant.profile) || {};
    if (!profile.picture && !value) {
      return "Il faut choisir une photo.";
    }
    if (value && value.size > 10 * 1024 * 1024) {
      return "La photo doit faire moins de 10Mo";
    }
  };
  validateNoEmoji = (value: string) => {
    if (value.match(/(?:[\uD800-\uDBFF][\uDC00-\uDFFF])/g)) {
      return "Pas d'emojis ou de smileys, merci ;-)";
    }
  };

  render() {
    const profile: Partial<ParticipantAttributes> =
      (this.props.participant && this.props.participant.profile) || {};
    const lowIncomeEnabled =
      this.props.event.ticket_rates.filter(r => r.low_income).length > 0;
    return (
      <React.Fragment>
        {this.props.event.is_on_waiting_list &&
          this.props.participant === null &&
          !this.props.isParticipantLoading && (
            <div className="alert alert-warning">
              <strong>La limite des pré-inscriptions est atteinte !</strong>{" "}
              Vous pouvez tout de même vous pré-inscrire à l'aide du formulaire
              ci-dessous puis constituer votre groupe de PALs, mais vous serez
              sur liste d'attente. Nous vous écrirons directement pour vous
              prévenir si des places se libèrent.
            </div>
          )}
        {this.isNew() ? (
          <p>
            Vous êtes connecté⋅e avec l'adresse email
            <code className="ms-2">{this.props.user.email}</code>. Pour vous
            pré-enregistrer pour l'évènement
            <i className="ms-2">{this.props.event.name}</i>, et choisir votre
            (ou vos) PAL(s), remplissez le formulaire ci-dessous. Tous les
            champs sont obligatoires.
          </p>
        ) : (
          <p>
            <strong>Vous êtes bien pré-enregistré⋅e.</strong> Vous pouvez
            modifier votre profil à l'aide du formulaire ci-dessous. Si tout
            vous semble correct, vous pouvez{" "}
            <Link to={`/${this.props.event.code}/`}>désigner vos PALs</Link>.
          </p>
        )}
        <Formik
          initialValues={{
            first_name: profile.first_name || "",
            last_name: profile.last_name || "",
            introduction: profile.introduction || "",
            learned_about: profile.learned_about || "",
            picture: "",
            picture_thumb: profile.picture_thumb || "",
            low_income: profile.low_income || false
          }}
          onSubmit={this.handleSubmit}
        >
          {({
            status,
            errors,
            touched,
            handleSubmit,
            handleReset,
            isSubmitting,
            setFieldValue
          }) => (
            <Form onSubmit={handleSubmit} onReset={handleReset}>
              <FormGroup row>
                <Label htmlFor="first_name" sm={3} xl={2}>
                  Prénom :
                </Label>
                <Col sm={9} xl={10}>
                  <Field component={FormInput} type="text" name="first_name" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label htmlFor="last_name" sm={3} xl={2}>
                  Nom :
                </Label>
                <Col sm={9} xl={10}>
                  <Field component={FormInput} type="text" name="last_name" />
                </Col>
              </FormGroup>
              <FormGroup>
                <Label htmlFor="introduction">
                  Vous semblez enthousiaste à l'idée de venir au Kinky Salon…
                  Pourquoi ?
                </Label>
                <Field
                  component={FormInput}
                  type="textarea"
                  name="introduction"
                  rows="3"
                  validate={this.validateNoEmoji}
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="learned_about">
                  Comment avez-vous connu le Kinky Salon ?
                </Label>
                <Field
                  component={FormInput}
                  type="textarea"
                  name="learned_about"
                  rows="3"
                  validate={this.validateNoEmoji}
                />
              </FormGroup>
              {lowIncomeEnabled && (
                <FormGroup row>
                  <Label for="low_income" sm={3} xl={2}>
                    Tarif solidaire
                    <Button
                      id="HelpLowIncome"
                      type="button"
                      color="link"
                      className="p-0 ms-2"
                    >
                      <FAIcon name="question-circle" />
                    </Button>
                    <UncontrolledPopover
                      trigger="focus"
                      placement="right"
                      target="HelpLowIncome"
                    >
                      <PopoverHeader>
                        Qu'est-ce que le tarif solidaire ?
                      </PopoverHeader>
                      <PopoverBody>
                        <p className="mb-0">
                          Les billets au tarif solidaire sont destinés aux
                          personnes qui ne pourraient pas assister au Kinky
                          Salon s'ielles devaient payer le tarif plein. Ils sont
                          en nombre limités. Merci de ne faire cette demande que
                          si c’est nécessaire pour vous, afin de laisser ces
                          billets à celleux qui en ont le plus besoin.
                        </p>
                        <p className="mb-0">
                          Pour bénéficier du tarif solidaire, vous devez écrire
                          à l’adresse mail{" "}
                          <a href="mailto:contact@kinkysalon.fr">
                            contact@kinkysalon.fr
                          </a>{" "}
                          en nous indiquant la raison pour laquelle vous en
                          faites la demande. Nous vous enverrons un coupon de
                          réduction.
                        </p>
                      </PopoverBody>
                    </UncontrolledPopover>
                  </Label>
                  <Col sm={9} xl={10}>
                    <FormGroup check className="py-2">
                      <Field
                        component={FormInput}
                        type="checkbox"
                        name="low_income"
                        label="Je demande le tarif solidaire"
                      />
                    </FormGroup>
                  </Col>
                </FormGroup>
              )}
              <FormGroup row>
                <Label htmlFor="picture" sm={3} xl={2}>
                  Une photo de vous:
                </Label>
                <Col sm={9} xl={10}>
                  {profile.picture ? (
                    <img
                      src={profile.picture_thumb}
                      style={{ maxWidth: "100px", maxHeight: "100px" }}
                      className="my-2"
                      alt="profile"
                    />
                  ) : null}
                  <Field
                    component={FormInput}
                    type="file"
                    name="picture"
                    style={{ width: "auto" }}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                      setFieldValue(
                        "picture",
                        event.currentTarget.files !== null
                          ? event.currentTarget.files[0]
                          : null
                      );
                    }}
                    value={undefined}
                    invalid={errors.picture && touched.picture ? true : false}
                    validate={this.validatePicture}
                  />

                  <FormText color="muted">
                    Vous devez être seul⋅e et reconnaissable sur la photo, et le
                    fichier doit faire moins de 10Mo.
                  </FormText>
                </Col>
              </FormGroup>
              {status && status.body && (
                <div className={`alert alert-${status.color} text-center mt-3`}>
                  {status.body}
                </div>
              )}
              <FormGroup row>
                <Col sm={{ size: 9, offset: 3 }} xl={{ size: 10, offset: 2 }}>
                  <Button color="primary" type="submit" disabled={isSubmitting}>
                    {this.isNew() ? "Se pré-enregistrer" : "Mettre à jour"}
                    {isSubmitting && (
                      <FAIcon name="spinner" className="fa-spin ms-3" />
                    )}
                  </Button>
                  <ErrorMessage name="non_field_errors" />
                </Col>
              </FormGroup>
            </Form>
          )}
        </Formik>
        {!this.isNew() && (
          <div className="mt-5 text-end">
            <CancelParticipation {...this.props} />
          </div>
        )}
      </React.Fragment>
    );
  }
}

declare module "react-piwik";

interface ReactPiwikProps {
  url: string;
  siteId: number;
  enableLinkTracking?: boolean;
  trackDocumentTitle?: boolean;
  jsFilename?: string;
  phpFilename?: string;
}

type HistoryObject = import("history").History;

declare class ReactPiwik extends React.Component<ReactPiwikProps> {
  push(args: Array<string>): void;
  connectToHistory(history: HistoryObject): HistoryObject;
  disconnectFromHistory(): boolean;
}

import { Field, Formik, FormikHelpers } from "formik";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Alert,
  Button,
  Card,
  CardText,
  Col,
  Form,
  FormGroup,
  Label,
  Row
} from "reactstrap";
import { IEvent, IParticipant, IRole, ITicketRate } from "../api/types";
import { FormValues, nullBoolToString, stringToNullBool } from "../api/utils";
import { BreadcrumbHeader } from "./BreadcrumbHeader";
import { FormInput } from "./FormInput";
import { ParticipantCard } from "./ParticipantCard";

export interface CommonProps {
  event: IEvent;
  roles: Array<IRole>;
  ticketRates: Array<ITicketRate>;
  readOnly: boolean;
  updateParticipant: (
    participant: IParticipant,
    values: ParticipantAttributes,
    actions: FormikHelpers<ParticipantFormValues>
  ) => Promise<IParticipant>;
}

interface ParticipantProps extends CommonProps {
  participant: IParticipant;
  index: number;
  willOverflow: boolean;
}

interface ComponentProps extends CommonProps {
  palGroup: Array<IParticipant>;
  prevParticipant: IParticipant | null;
  nextParticipant: IParticipant | null;
  approvalQueueIds: Array<number>;
  willOverflow: boolean;
}

export class AdminQueuesValidationProfile extends Component<ComponentProps> {
  render() {
    const firstParticipant = this.props.palGroup[0];
    return (
      <React.Fragment>
        <BreadcrumbHeader
          crumbs={[
            "To do",
            <Link to={`/admin/${this.props.event.code}/queues/validation/`}>
              To approve
            </Link>,
            firstParticipant.profile.full_name
          ]}
        />

        <ValidationNavigation
          baseUrl={`/admin/${this.props.event.code}/queues/validation/`}
          prev={this.props.prevParticipant}
          next={this.props.nextParticipant}
        />

        <h3>Participant to approve: {firstParticipant.profile.full_name}</h3>

        {this.props.willOverflow ? (
          <Alert color="warning" className="my-4 text-center">
            Warning: approving this PAL group will overflow the event's
            threshold!
          </Alert>
        ) : null}

        <Row>
          {this.props.palGroup.map(participant => (
            <Col
              key={participant.id}
              lg="6"
              xl={this.props.palGroup.length > 2 ? "4" : "6"}
            >
              <ParticipantToApprove
                {...this.props}
                participant={participant}
                index={this.props.approvalQueueIds.indexOf(participant.id)}
              />
            </Col>
          ))}
        </Row>
      </React.Fragment>
    );
  }
}

class ParticipantToApprove extends Component<ParticipantProps> {
  render() {
    if (
      !this.props.participant.self_registered ||
      !this.props.participant.profile
    ) {
      return (
        <Card className="my-3" body>
          <CardText>
            <strong>{this.props.participant.email}</strong> (has not registered
            yet)
          </CardText>
        </Card>
      );
    }
    return (
      <React.Fragment>
        <ParticipantCard
          participant={this.props.participant}
          event={this.props.event}
          bottom={
            this.props.index !== -1 ? (
              <CardText>
                <strong>Seat in the queue:</strong> {this.props.index + 1}
              </CardText>
            ) : (
              undefined
            )
          }
        />

        <ValidationForm {...this.props} />
      </React.Fragment>
    );
  }
}

type ParticipantAttributes = Pick<
  IParticipant,
  "approved" | "role" | "ticket_rate"
>;
type ParticipantFormValues = FormValues<ParticipantAttributes>;

class ValidationForm extends React.Component<ParticipantProps> {
  handleSubmit = (
    values: ParticipantFormValues,
    actions: FormikHelpers<ParticipantFormValues>
  ) => {
    if (
      this.props.readOnly ||
      !this.props.participant ||
      Object.keys(values).length === 0
    ) {
      actions.setSubmitting(false);
      return;
    }
    const realValues: ParticipantAttributes = {
      ...values,
      approved: stringToNullBool(values.approved)
    };
    this.props
      .updateParticipant(this.props.participant, realValues, actions)
      .then(
        () => {
          actions.setStatus({
            color: "success",
            body: `Participant ${
              this.props.participant.profile.full_name
            } has been ${realValues.approved ? "approved" : "refused"}.`
          });
        },
        () => {
          actions.setStatus({
            color: "danger",
            body: "There was an error approving the participant."
          });
        }
      );
  };

  willSendEmail = (): boolean => {
    if (this.props.participant.ticket_status !== "NOT_READY") {
      return false;
    }
    if (!this.props.participant.mutual_pal_group) {
      return false;
    }
    const stillNeedsApproval = this.props.participant.pals.filter(
      pal => !pal.approved
    );
    if (stillNeedsApproval.length > 0) {
      return false;
    }
    return true;
  };

  render() {
    if (this.props.readOnly) {
      return null;
    }
    let lowIncomeTicketRate: ITicketRate | null = null;
    const lowIncomeTicketRates = this.props.event.ticket_rates.filter(
      r => r.low_income
    );
    if (lowIncomeTicketRates.length > 0) {
      lowIncomeTicketRate = lowIncomeTicketRates[0];
    }
    return (
      <Formik
        initialValues={{
          approved: nullBoolToString(this.props.participant.approved),
          role: this.props.participant.role || "",
          ticket_rate:
            this.props.participant.profile.low_income &&
            lowIncomeTicketRate !== null
              ? lowIncomeTicketRate.name
              : this.props.participant.ticket_rate
        }}
        onSubmit={this.handleSubmit}
      >
        {({
          status,
          handleSubmit,
          handleReset,
          isSubmitting,
          setFieldValue
        }) => (
          <Form onSubmit={handleSubmit} onReset={handleReset}>
            {this.props.participant.approved !== null ? null : (
              <React.Fragment>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <Label for="role">Role</Label>
                      <Field component={FormInput} type="select" name="role">
                        {this.props.roles.map(role => (
                          <option value={role.code} key={role.id}>
                            {role.name}
                          </option>
                        ))}
                      </Field>
                    </FormGroup>
                  </Col>
                  <Col sm={6}>
                    <FormGroup>
                      <Label for="ticket_rate">Ticket rate</Label>
                      <Field
                        component={FormInput}
                        type="select"
                        name="ticket_rate"
                      >
                        {this.props.ticketRates.map(ticketRate => (
                          <option value={ticketRate.name} key={ticketRate.id}>
                            {ticketRate.name}
                          </option>
                        ))}
                      </Field>
                    </FormGroup>
                  </Col>
                </Row>
                <div className="text-center">
                  <Button
                    color={this.props.willOverflow ? "warning" : "success"}
                    type="submit"
                    onClick={() => setFieldValue("approved", "true")}
                    className="mx-2"
                    disabled={isSubmitting}
                  >
                    Approve
                  </Button>
                  <Button
                    color="danger"
                    type="submit"
                    onClick={() => setFieldValue("approved", "false")}
                    className="mx-2"
                    disabled={isSubmitting}
                  >
                    Refuse
                  </Button>

                  <p>
                    <small>
                      {this.willSendEmail()
                        ? "The PAL group will be notified if you approve this participant."
                        : "No email will be sent."}
                    </small>
                  </p>
                </div>
              </React.Fragment>
            )}

            {status && status.body && (
              <div className={`alert alert-${status.color} text-center mt-3`}>
                {status.body}
              </div>
            )}
          </Form>
        )}
      </Formik>
    );
  }
}

class ValidationNavigation extends Component<{
  baseUrl: string;
  prev: IParticipant | null;
  next: IParticipant | null;
}> {
  render() {
    return (
      <div className="d-flex my-3">
        {this.props.prev && (
          <Link
            to={`${this.props.baseUrl}${this.props.prev.id}/`}
            className="me-auto"
          >
            ← {this.props.prev.profile.full_name}
          </Link>
        )}
        {this.props.next && (
          <Link
            to={`${this.props.baseUrl}${this.props.next.id}/`}
            className="ms-auto"
          >
            {this.props.next.profile.full_name} →
          </Link>
        )}
      </div>
    );
  }
}

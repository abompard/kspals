import * as React from "react";
import { Link, NavLink } from "react-router-dom";
import {
  Badge,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  UncontrolledDropdown
} from "reactstrap";
import { IEvent, IUser } from "../api/types";
import Logo from "./logo.png";
import TopUserMenu from "./TopUserMenu";

export interface ILink {
  text: string;
  title?: string;
  href: string;
  exact?: boolean;
  count?: number;
  countColor?: string;
}
interface IMenuHeader {
  text: string;
  links: Array<ILink>;
}

interface Props {
  user: IUser | null;
  event: IEvent | null;
  logoUrl: string;
  links?: Array<ILink | IMenuHeader>;
  userLinks?: Array<ILink>;
  onLogout(): any;
}

interface State {
  collapsed: boolean;
}

export default class TopBar extends React.Component<Props, State> {
  state = { collapsed: true };

  toggleNavbar = () => {
    this.setState((prevState, props) => ({
      collapsed: !prevState.collapsed
    }));
  };

  makeLink = (link: ILink, topLevel: boolean) => {
    //var Component = topLevel ? NavLink : Link;
    if (topLevel) {
      return (
        <NavItem key={link.href}>
          <NavLink
            className="nav-link"
            to={link.href}
            title={link.title}
            exact={link.exact}
          >
            {link.text}
          </NavLink>
        </NavItem>
      );
    } else {
      return (
        <DropdownItem
          tag={Link}
          to={link.href}
          title={link.title}
          key={link.href}
        >
          {link.text}
          {link.count ? (
            <Badge pill color={link.countColor} className="ms-2">
              {link.count}
            </Badge>
          ) : null}
        </DropdownItem>
      );
    }
  };

  render() {
    // Don't display the toggle if there's only the user menu
    const showToggler = this.props.links && this.props.links.length > 0;

    return (
      <Navbar color="dark" dark expand="lg" className="rounded mt-1 mb-3">
        <NavbarBrand href={this.props.logoUrl}>
          <img src={Logo} className="d-inline-block align-middle" alt="" />
        </NavbarBrand>
        {this.props.event ? (
          <span className="navbar-text text-light event-title me-3">
            {this.props.event.name}
          </span>
        ) : null}
        {showToggler && (
          <NavbarToggler onClick={this.toggleNavbar} className="me-2" />
        )}
        <Collapse isOpen={!showToggler || !this.state.collapsed} navbar>
          {this.props.links && (
            <Nav className="" navbar>
              {this.props.links.map(item => {
                if ("links" in item) {
                  const menu = item as IMenuHeader;
                  const count = item.links
                    .map(link => link.count || 0)
                    .reduce((prev, current) => prev + current);
                  return (
                    <UncontrolledDropdown nav inNavbar key={menu.text}>
                      <DropdownToggle nav caret>
                        {menu.text}
                        {count ? (
                          <small>
                            <Badge pill color="secondary" className="ms-1">
                              {count}
                            </Badge>
                          </small>
                        ) : null}
                      </DropdownToggle>
                      <DropdownMenu>
                        {menu.links.map(subitem =>
                          this.makeLink(subitem, false)
                        )}
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  );
                } else {
                  const link = item as ILink;
                  return this.makeLink(link, true);
                }
              })}
            </Nav>
          )}
          <Nav className="ms-auto" navbar>
            <TopUserMenu
              user={this.props.user}
              event={this.props.event}
              onLogout={this.props.onLogout}
              links={this.props.userLinks}
            />
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

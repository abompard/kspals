import * as React from "react";
import { IPal } from "../api/types";
import FAIcon from "./FAIcon";

interface Props {
  className: string;
  pal?: IPal | null;
  iconOnly: boolean;
}

export default class MutualStatus extends React.Component<Props> {
  static defaultProps = {
    className: "",
    iconOnly: false
  };

  render() {
    if (!this.props.pal || typeof this.props.pal.is_mutual === "undefined") {
      return null;
    }
    let color, text, icon;
    if (this.props.pal && this.props.pal.is_mutual) {
      icon = "handshake-o";
      color = "success";
      text = "Ce PAL vous a aussi désigné comme PAL.";
    } else {
      icon = "lock";
      color = "warning";
      text = "Ce PAL ne vous a pas encore désigné comme PAL.";
    }
    const { className, pal, iconOnly, children, ...otherProps } = this.props;
    if (this.props.iconOnly) {
      const newClassName = `fa-lg fa-fw text-${color} ${className}`;
      return (
        <FAIcon
          name={icon}
          className={newClassName}
          title={text}
          {...otherProps}
        />
      );
    } else {
      const newClassName = `mt-1 mb-0 text-${color} ${className}`;
      return (
        <p className={newClassName} {...otherProps}>
          <FAIcon name={icon} className="me-2 d-md-none" />
          {text}
        </p>
      );
    }
  }
}

export type FilterDict = { [key: string]: string | string[] };

#!/usr/bin/env python3

import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), "README.rst")) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name="ks-pals",
    version="0.1.0",
    description="PALs selection for the Kinky Salon",
    long_description=README,
    author="Aurelien Bompard",
    author_email="aurelien@bompard.org",
    url="https://gitlab.com/abompard/ks-pals",
    license="AGPLv3+",
    classifiers=[
        "Environment :: Web Environment",
        "Framework :: Django",
        "Framework :: Django :: 1.11",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
    ],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Django>=1.11",
        "djangorestframework >= 3.9.0",  # for bitwise composition of permissions
        "drf-nested-routers",
        "django-cors-headers",
        "drfpasswordless",
        "django-filter",
        "pytz",
        "Pillow",
        "requests",
        "python-memcached",
        "python-dateutil",
        "authlib",
        "python-socketio>=5.5.0",
        "redis",
        "aioredis",
        "uvicorn[standard]",
    ],
)
